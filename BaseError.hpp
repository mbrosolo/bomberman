//
// BaseError.hh for bm in /home/gressi_b/bm
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon Mar 26 16:49:46 2012 gressi_b
// Last update Mon Mar 26 16:49:46 2012 gressi_b
//

#ifndef	__BASE_ERROR_HPP__
#define	__BASE_ERROR_HPP__

#include <string>

namespace bm
{
  /**
   * @author gressi_b
   * @class BaseError
   * @brief Template class.
   * It intented to be ONLY used as a base class.
   * It should never be instanciated.
   */

  template<typename T>
  class BaseError
  {
  protected:
    /**
     * @brief this attribut must be mutable since en error is not really
     * part from an object
     */
    mutable T	_error;

  protected:
    BaseError(T err = 0)
      : _error(err)
    {
    }

  public:
    virtual ~BaseError() {}

    T		getError() const
    {
      return (this->_error);
    }
  };

  /**
   * @author gressi_b
   * @class BaseError<std::string>
   * @brief BaseError specialized class for std::string template argument
   */

  template<>
  class BaseError<std::string>
  {
  protected:
    mutable std::string		_error;

  public:
    BaseError(std::string const& err = "")
      : _error(err)
    {
    }

    virtual ~BaseError() {}

    std::string const&	getError() const
    {
      return (this->_error);
    }
  };
}

#endif

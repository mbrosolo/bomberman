//
// Menu.hh for Menu in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 17:39:33 2012 tosoni
// Last update Sun Jun  3 21:21:54 2012 tosoni_t
//

#ifndef		__MENU_HH__
#define		__MENU_HH__

/**
 * @author tosoni_t
 * @file Menu.hh
 */

#include	<iostream>
#include	<sstream>
#include	<cstdio>
#include	"AObject.hh"
#include	"KeyManager.hh"
#include	"MyGame.hh"
#include	"SubMenu.hh"
#include	"MenuEventManager.hh"
#include	"Option.hh"
#include	"PauseMenu.hh"
#include	"GameLoader.hh"
#include	"Score.hh"
#include	"MenuCamera.hh"
#include	"ExceptionLoadMap.hh"

/*!
 * @brief Gap between slides
 */
#define		GAP	(700)
/*!
 * @brief Height of slides
 */
#define		LOW	(100)
/*!
 * @brief Side position of slides
 */
#define		SIDE	(2200)
/*!
 * @brief Position of text in slides
 */
#define		BORDER	(-420)
/*!
 * @brief Close position of Menu Camera
 */
#define		CLOSE_CAM	(1100.f)
/*!
 * @brief Far position of Menu Camera
 */
#define		FAR_CAM		(2000.f)
/*!
 * @brief Slow MenuCamera Movement Velocity 
 */
#define		VELOCITY	(40.f)
/*!
 * @brief Quick MenuCamera Movement Velocity 
 */
#define		VELOCITY2	(70.f)

class		Menu;
class		Files;
class		SubMenu;
class		GameLoader;
class		MenuCamera;
class		MenuEventManager;

/*!
 * @brief Typedef for OptionList
 */
typedef void (Menu::*OPTPTR)(void);
/*!
 * @brief Typedef for GameEventManager
 */
typedef void (AObject::*FUTUREPTR)(void);
/*!
 * @brief Typedef for Pause Menu
 */
typedef void (PauseMenu::*PausePTR)(void);

/*!
 * @brief Typedef for loaded files listing
 */
typedef	std::list<Files*>	Files_t;
/*!
 * @brief Typedef for Sub Menus of principal Menu
 */
typedef	std::list<SubMenu*>	SubMenus_t;
/*!
 * @brief Typedef for option listing
 */
typedef	std::list<Option*>	OptionList_t;
/*!
 * @brief Typedef for Game Ev Manager
 */
typedef std::map<std::string, FUTUREPTR>	KMap_t;
/*!
 * @brief Typedef for Map des Options listees
 */
typedef std::map<std::string, OPTPTR>		OptMap_t;
/*!
 * @brief Typedef for Map des Pointeurs sur Pause Menu
 */
typedef std::map<std::string, PausePTR>	        PauseMap_t;
/*!
 * @brief Typedef for Map of function pointers Key <-> option
 */
typedef	std::map<gdl::Keys::Key, FUTUREPTR >	AObjectEventList_t;

/*!
 * @class Menu
 * @brief Principal Menu Managing Core Launch And Options
 */
class		Menu
{
public:
  /*!
   * @brief returns instance of menu class
   */
  static Menu		*getInstance(void);
  /*!
   * @brief kills Menu instance
   */
  static void		killInstance(void);

  /*MENU*/
  /*!
   * @brief displays menu data
   */
  void			display();
  /*!
   * @brief displays Saves, Maps Songs
   */
  void			displaySelectedOption(Files_t &, float);
  /*!
   * @brief displays IA
   */
  void			displayIA(OptionList_t & list) const;
  /*!
   * @brief displays Highscore 
   */
  void			displayHighscores(graphic::Vector & pos,
					  score::HighScore::listScore_t & list);
  /*!
   * @brief init Menu
   */
  void			initialize();
  /*!
   * @brief change key mapping to selection
   */
  void			lockMenu(float);
  /*!
   * @brief change key mapping to menu
   */
  void			unlockMenu(float);
  /*!
   * @brief key mapping to option
   */
  void			lockOption(float);
  /*!
   * @brief key mapping to submenu
   */
  void			unlockOption(float);

  /*!
   * @brief slides in depth
   */
  void			updateMenuInDepth(float);
  /*!
   * @brief change displayed menu
   */
  void			updateHorizontalMenu(float);
  /*!
   * @brief update options
   */
  void			updateOptions(float);
  /*!
   * @brief update locked opt
   */
  void			updateLocked(float);

  /*!
   * @brief valid Option of SubMenu
   */
  void			validOption(float);
  /*!
   * @brief check Keyboard mapping
   */
  void			checkKeyboardOptions(int, OptionList_t &);
  /*!
   * @brief set player nb
   */
  void			setPlayer(int);
  /*!
   * @brief shut down the game
   */
  void			closeGame(float value);
  /*!
   * @brief change music volume
   */
  void			updateVolume(float _side);

  /*!
   * @brief principal update
   */
  void			update(MyClock const &, gdl::Input &);
  /*!
   * @brief addOption tu submenu
   */
  void			menuAddOption(SubMenu&, std::string const &, std::string const &,
				      graphic::AForm*, gdl::Keys::Key, float, float);
  /*!
   * @brief make a rectangle form for option
   */
  graphic::AForm*	makeRectangle(float x, float y, float z,
				      float xs, float ys, std::string const &);

  /*!
   * @brief remove key mapping
   */
  void			erazeFromCurrentList(AObjectEventList_t & list);
  /*!
   * @brief remove key mapping from other player
   */
  void			erazeFromOtherList(AObjectEventList_t & list);

  /*!
   * @brief get future mapping
   */
  FUTUREPTR		getFuturePtr() const;
  /*!
   * @brief get current menus
   */
  SubMenus_t		getMenus() const;
  /*!
   * @brief get shown menu
   */
  SubMenu*		getShownMenu() const;
  /*!
   * @brief get first shown menu
   */
  SubMenu*		getFirstShownMenu() const;
  /*!
   * @brief get player number
   */
  int			getPlayer(void) const;
  /*!
   * @brief get option list of submenu
   */
  OptionList_t*		getCurrentOptionList() const;
  /*!
   * @brief get rounds number
   */
  int			getRounds(void) const;

  /*!
   * @brief get game options loader
   */
  GameLoader		*getGameLoader() const;

  /*!
   * @brief changes x size of the map
   */
  void			updateX(float);
  /*!
   * @brief changes y size of the map
   */
  void			updateY(float);
  /*!
   * @brief changes x mapping for map size
   */
  void			lockX(void);
  /*!
   * @brief changes y mapping for map size
   */
  void			lockY(void);
  /*!
   * @brief changes rounds number
   */
  void			updateRounds(float _val);
  /*!
   * @brief changes mapping to update rounds
   */
  void			lockRounds(void);
  /*!
   * @brief unlock rounds to options
   */
  void			unlockXYRounds(float);

  /*OPTIONS*/
  /*!
   * @brief set one player
   */
  void		        onePlayer(void);
  /*!
   * @brief set two players
   */
  void			twoPlayers(void);
  /*!
   * @brief exit option
   */
  void			exit(void);
  /*!
   * @brief load option
   */
  void			load(void);
  /*!
   * @brief volume option
   */
  void			volume(void);
  /*!
   * @brief maps option
   */
  void			maps(void);
  /*!
   * @brief ai options
   */
  void			IA(void);
  /*!
   * @brief music options
   */
  void			music(void);

  /*MENU PAUSE*/
  /*!
   * @brief maps to pause menu
   */
  void			pauseMenu(float in);
  /*!
   * @brief changes pause options data
   */
  void			updatePauseOptions(float side);
  /*!
   * @brief valid pause menu option
   */
  void			validPauseOption(float _useless);
  /*!
   * @brief changes volume of pause menu
   */
  void			updatePauseVolume(float _side);

  /*SAVES, MAPS, MUSIC, IA*/
  /*!
   * @brief changes saves list
   */
  void			updateSaves(float side);
  /*!
   * @brief selects a save
   */
  void			selectSave(float _useless);
  /*!
   * @brief change maps list
   */
  void			updateMaps(float side);
  /*!
   * @brief select a map
   */
  void			selectMap(float _useless);
  /*!
   * @brief unlocks load to menu
   */
  void			unlockLoad(float);
  /*!
   * @brief unlocks Music to menu
   */
  void			unlockMusic(float);
  /*!
   * @brief change song
   */
  void			updateMusic(float side);
  /*!
   * @brief chooses music
   */
  void			selectMusic(float _val);
  /*!
   * @brief changes ai
   */
  void			updateIA(float side);
  /*!
   * @brief chooses ai
   */
  void			selectIA(float _useless);
  /*!
   * @brief unlock IA to menu
   */
  void			unlockIA(float);
  /*!
   * @brief changes ai number
   */
  void			IANumber(float _val);
  /*!
   * @brief unlock ai to menu
   */
  void			unlockNbIA(float _useless);

private:
  SubMenus_t		_hmenus; /*!< horizontal menus */
  MenuEventManager	*_menuEventManager; /*!< menu key event manager */
  MenuCamera		*_menuCamera; /*!< menu camera */
  static Menu		*_instance; /*!< menu instance */
  FUTUREPTR		_ptr; /*!< future mapping */
  KMap_t		_keyboardMap; /*!<  keyboard mapping */
  OptMap_t		_optionsMap; /*!< map options */
  PauseMap_t		_pauseMap; /*!< pause map */
  int			_player; /*!< player number */
  float			_volume; /*!< volume number */
  GameLoader		*_gameLoader; /*!< load maps saves music */
  std::string		_mapName; /*!< map chosen */
  std::string		_musicName; /*!< music chosen  */
  int			_optionSelector; /*!< option used */
  std::string		_iaType; /*!< level ai */
  size_t		_xmap; /*!< x map size*/
  size_t		_ymap; /*!< y map size */
  size_t		_rounds; /*!< rounds number */
  size_t		_ai; /*!< ai number */
  int			_dI; /*!< choosing opt */
  float			_lastPos; /*!< last opt */
  int			_framePos; /*!< current opt */

private:
  /*!
   * @brief menu ctor
   */
  Menu();
  /*!
   * @brief menu dtor
   */
  ~Menu();
  /*!
   * @brief menu cpy ctor 
   */
  Menu(Menu const &);
  /*!
   * @brief menu oprator =
   */
  Menu & operator=(Menu const &);

  /*!
   * @brief called for 1st and 2nd player
   */
  void initModePlay(const bool mode);

};

#endif

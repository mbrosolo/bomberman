//
// TrioDatas.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Fri May 11 23:20:44 2012 benjamin bourdin
// Last update Fri May 11 23:45:38 2012 benjamin bourdin
//

#include	"TrioDatas.hh"

graphic::TrioDatas::TrioDatas(void)
  : first(0.0f), second(0.0f), third(0.0f)
{
}

graphic::TrioDatas::~TrioDatas()
{
}

graphic::TrioDatas::TrioDatas(TrioDatas const &v)
  : first(v.first), second(v.second), third(v.third)
{
}

graphic::TrioDatas::TrioDatas(const float first, const float second, const float third)
  : first(first), second(second), third(third)
{
}

//
// GameLoader.cpp for GameLoader in /home/tosoni_t/BitBucket/bomberman/graphic
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Thu May 31 16:39:05 2012 tosoni_t
// Last update Sun Jun  3 19:14:17 2012 tosoni_t
//

#include	"Primitive.hh"
#include	"GameLoader.hh"

GameLoader::GameLoader()
  : _maps(),
    _saves(),
    _songs(),
    _ia()
{
}

GameLoader::~GameLoader()
{
}

graphic::AForm*		GameLoader::makeSquare(int x, int y, int z,
					       std::string const & bg)
{
  graphic::AForm *Form = new graphic::Primitive::Rectangle(graphic::Vector(x, y, z),
							   graphic::Vector(0.f, 0.f, 0.f),
							   graphic::Color(0.f, 0.f, 0.f),
							   graphic::Scale(1.5f, 1.5f, 1.5f));
  Form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture(bg));
  return (Form);
}

graphic::AForm*		GameLoader::makeRectangle(float x, float y, float z,
						  std::string const & bg)
{
  graphic::AForm *Form = new graphic::Primitive::Rectangle(graphic::Vector(x, y, z),
  							   graphic::Vector(0.f, 0.f, 0.f),
  							   graphic::Color(0.f, 0.f, 0.f),
  							   graphic::Scale(5.0f, 1.f, 1.f));
  Form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture(bg));
  return (Form);
}

void		GameLoader::loadSaves()
{
  int		i = 0., y = -2200, x = -500;
  DIR		*dp;
  struct dirent	*dirp;
  std::string	directory("saves");

  if ((dp = opendir(directory.c_str())) == NULL)  {
    std::cerr << "opendir() failed: couldn't open saves directory" << std::endl;
    throw ExceptionRuntime(strerror(errno), __FILE__);
  }
  while ((dirp = readdir(dp)) != NULL)
    {
      if (std::string(dirp->d_name).compare(".") && std::string(dirp->d_name).compare("..")) {
	if ((i % 3) == 0)	{
	  x = -500.;
	  y -= 400.;
	}
	graphic::AForm	*bg = makeSquare(x, y, 0.f, "SQUARE1");
	graphic::AForm	*s = makeSquare(x, y, 0.f, "SQUARE2");
	_saves.push_back(new Files(std::string(dirp->d_name), bg, s, i+1, i));
	x += 500.f;
	++i;
      }
    }
  if (closedir(dp) == -1)
    std::cerr << "closedir() failed" << std::endl;
}

void		GameLoader::loadMaps()
{
  int		i = 0., y = -2200, x = -500.;
  DIR		*dp;
  struct dirent	*dirp;
  std::string	directory("maps");

  if ((dp = opendir(directory.c_str())) == NULL){
    std::cerr << "opendir() failed: couldn't open maps directory" << std::endl;
    throw ExceptionRuntime(strerror(errno), __FILE__);
  }
  while ((dirp = readdir(dp)) != NULL)
    {
      if (std::string(dirp->d_name).compare(".") && std::string(dirp->d_name).compare("..")) {
	if ((i % 3) == 0) {
	  x = -500;
	  y -= 400;
	}
	graphic::AForm	*bg = makeSquare(x, y, 0.f, "SQUARE1");
	graphic::AForm	*s = makeSquare(x, y, 0.f, "SQUARE2");
	_maps.push_back(new Files(std::string(dirp->d_name), bg, s, i+1, i));
	x += 500.f;
	++i;
      }
    }
  if (closedir(dp) == -1)
    std::cerr << "closedir() failed" << std::endl;
}

void		GameLoader::loadMusic()
{
  int		i = 0., y = 2700, x = 1700.;
  DIR		*dp;
  struct dirent	*dirp;
  std::string	directory("music");

  if ((dp = opendir(directory.c_str())) == NULL) {
    std::cerr << "opendir() failed: couldn't open maps directory" << std::endl;
    throw ExceptionRuntime(strerror(errno), __FILE__);
  }
  while ((dirp = readdir(dp)) != NULL)
    {
      if (std::string(dirp->d_name).compare(".") && std::string(dirp->d_name).compare("..")) {
	if ((i % 3) == 0) {
	  x = 1700;
	  y -= 400;
	}
	graphic::AForm	*bg = makeSquare(x, y, 0.f, "SQUARE1");
	graphic::AForm	*s = makeSquare(x, y, 0.f, "SQUARE2");
	_songs.push_back(new Files(std::string(dirp->d_name), bg, s, i+1, i));
	x += 500.f;
	++i;
      }
    }
  if (closedir(dp) == -1)
    std::cerr << "closedir() failed" << std::endl;
}

void		GameLoader::initIA()
{
  graphic::AForm	*easy1 = makeSquare(-500., 3100, 0.f, "SQUARE1");
  graphic::AForm	*easy2 = makeSquare(-500, 3100, 0.f, "SQUARE2");
  graphic::AForm	*normal1 = makeSquare(0, 3100, 0.f, "SQUARE1");
  graphic::AForm	*normal2 = makeSquare(0, 3100, 0.f, "SQUARE2");
  graphic::AForm	*hard1 = makeSquare(500, 3100, 0.f, "SQUARE1");
  graphic::AForm	*hard2 = makeSquare(500, 3100, 0.f, "SQUARE2");

  graphic::AForm	*numberIA1 = makeRectangle(0., 2600, 0.f, "SHORT");
  graphic::AForm	*numberIA2 = makeRectangle(0., 2600, 0.f, "LONGBLACK");

  _ia.push_back(new Option("Easy", easy1, easy2, gdl::Keys::Delete, -150, 1));
  _ia.push_back(new Option("Normal", normal1, normal2, gdl::Keys::Delete, -150, 2));
  _ia.push_back(new Option("Hard", hard1, hard2, gdl::Keys::Delete, -150, 3));
  _ia.push_back(new Option("AI Number:1", numberIA1, numberIA2, gdl::Keys::Delete, -250, 4));
}

Files_t&		GameLoader::getMaps()
{
  return (_maps);
}

Files_t&		GameLoader::getMusic()
{
  return (_songs);
}

Files_t&		GameLoader::getSaves()
{
  return (_saves);
}

OptionList_t&		GameLoader::getIA()
{
  return (_ia);
}

//
// SubMenus.hh for SubMenus in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 18:30:32 2012 tosoni
// Last update Sun Jun  3 22:06:33 2012 benjamin bourdin
//

#ifndef		__SUBMENUS_HH__
#define		__SUBMENUS_HH__

/**
 * @author thomas tosoni
 * @file SubMenu.hh
 */

#include	<string>
#include	<iostream>
#include	"Primitive.hh"
#include	"Option.hh"
#include	"AForm.hh"
#include	"Menu.hh"

class		AForm;
class		Menu;
class		SubMenu;
class		Option;

/*!
 * @brief Typedef for SubMenus' list 
 */
typedef	std::list<SubMenu*>	SubMenus_t;
/*!
 * @brief Typedef for Options' list 
 */
typedef	std::list<Option*>	OptionList_t;

/*!
 * @class SubMenu
 * @brief Define the SubMenu
 */
class			SubMenu
{
  std::string 		name; /*!< The name of the menu */
  graphic::AForm	*background; /*!< The background of the menu */
  SubMenus_t		_dmenus; /*!< The list of menu */
  OptionList_t		_options; /*!< The options of menu */
  float			_menuNb; /*!< The number of  menu */
  bool			_shown; /*!< The flag for the menu's visibility */
  bool			_locked; /*!< The flag for the menu accessibility */

public:
  /*!
   * @brief Constructor of SubMenu 
   * @param name The name of the menu
   * @param form The form of the menu
   * @param nb The number of the submenu
   */
  SubMenu(std::string const &, graphic::AForm*, float);
  /*!
   * @brief Destructor of SubMenu 
   */
  ~SubMenu();
  /*!
   * @brief Constructor by copy of SubMenu 
   * @param The submenu to copy
   */
  SubMenu(SubMenu const &);
  /*!
   * @brief operator= override of SubMenu 
   */
  SubMenu &		operator=(SubMenu const &);
  
  /*!
   * @brief DrawBackground method of SubMenu 
   */
  void			drawBackground() const;
  /*!
   * @brief ChangePosition method of SubMenu 
   * @param x The position x
   * @param y The position y
   * @param z The position z
   */
  void			changePosition(const float, const float, const float);
  /*!
   * @brief DrawBackground method of SubMenu
   * @param _s The name of the menu
   * @param Form The form of the menu
   * @param nb The number of submenu
   */
  void			addMenu(std::string const & _s, graphic::AForm *Form, float nb);
  /*!
   * @brief AddOption method of SubMenu 
   * @param _s The name of the menu
   * @param Form The form of the menu
   * @param k The key assigned at the menu
   * @param ypos The position y
   * @param v The value
   */
  void			addOption(std::string const & _s, graphic::AForm *Form,
				  gdl::Keys::Key k, float ypos, float v);
  /*!
   * @brief ChangeMenuNb method of SubMenu 
   * @param nb The number of menu
   */
  void			changeMenuNb(float);

  /*!
   * @brief SetOption method of SubMenu 
   * @param name The name of the menu
   * @param nb The number of the option
   */
  void			setOption(std::string const &, float);
  /*!
   * @brief SetMenuNb method of SubMenu
   * @param nb The number of the menu
   */
  void			setMenuNb(float);
  /*!
   * @brief SetShown method of SubMenu 
   * @param b The flag shown
   */
  void			setShown(bool);
  /*!
   * @brief SetLocked method of SubMenu 
   * @param b The flag locked
   */
  void			setLocked(bool);

  /*!
   * @brief GetMenus method of SubMenu 
   * @return List of menus
   */
  SubMenus_t&		getMenus();
  /*!
   * @brief GetOptions method of SubMenu 
   * @return List of options
   */
  OptionList_t*		getOptions();
  /*!
   * @brief GetLocked method of SubMenu
   * @return Flag locked
   */
  bool			getLocked() const;
  /*!
   * @brief GetName method of SubMenu
   * @return The name of the menu
   */
  std::string		getName() const;
  /*!
   * @brief GetShown method of SubMenu
   * @return Flag shown
   */
  bool			getShown() const;
  /*!
   * @brief GetMenuNb method of SubMenu
   * @return The number of menus
   */
  float			getMenuNb() const;
  /*!
   * @brief GetBackground method of SubMenu
   * @return The form of the background
   */
  graphic::AForm&	getBackground() const;
};

#endif

//
// MyGame.cpp for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Mon May 14 14:38:23 2012 tosoni
//

#include	<GL/gl.h>
#include	<GL/glu.h>
#include	<iostream>
#include	<sstream>

#include	"KeyManager.hh"
#include	"MyGame.hh"
#include	"Primitive.hh"
#include	"ComplexObj.hh"
#include	"Model.hh"
#include	"LoadTexture.hh"
#include	"LoadModel.hh"
#include	"Text.hh"
#include	"Menu.hh"
#include	"Board.hh"
#include	"Core.hh"

#include	"LoadText.hh"
#include	"Intro.hh"
#include	"Score.hh"

graphic::MyGame			*graphic::MyGame::_instance = NULL;

graphic::MyGame::eGamePlace	graphic::MyGame::place = graphic::MyGame::INTRO;

const float	graphic::MyGame::WINDOW_WIDTH = 1440.0f;
const float	graphic::MyGame::WINDOW_HEIGHT = 900.0f;

graphic::MyGame::MyGame()
  : clock_(gameClock_),
    _soundManager(NULL),
    _textureManager(NULL),
    _modelManager(NULL),
    _cameraManagement(NULL),
    _intro(NULL),
    _menu(Menu::getInstance()),
    _pauseMenu(PauseMenu::getInstance()),
    _menuEventManager(MenuEventManager::getInstance()),
    _keyManager(KeyManager::getInstance()),
    _printFPS(NULL), _printTimePlay(NULL),
    _printRound(NULL),
    _background(NULL),
    _transition(NULL),
    _printDead(NULL),
    _printWin(NULL),
    _ptrFuncUpdate(NBSTATE),
    _ptrFuncDraw(NBSTATE),
    _score1(NULL), _score2(NULL),
    _life1(NULL), _life2(NULL),
    _toKillGame(CURRENT),
    _nbWinP1(0), _nbWinP2(0),
    _nbRound(0), _maxRound(NB_ROUND_DEFAULT),
    _videoBackgroundMenu(NULL)
{
  _ptrFuncUpdate[INTRO] = &graphic::MyGame::_updateIntro;
  _ptrFuncUpdate[MENU] = &graphic::MyGame::_updateMenu;
  _ptrFuncUpdate[PAUSE] = &graphic::MyGame::_updatePause;
  _ptrFuncUpdate[GAME] = &graphic::MyGame::_updateGame;
  _ptrFuncUpdate[TRANSITION] = &graphic::MyGame::_updateTransition;

  _ptrFuncDraw[INTRO] = &graphic::MyGame::_drawIntro;
  _ptrFuncDraw[MENU] = &graphic::MyGame::_drawMenu;
  _ptrFuncDraw[PAUSE] = &graphic::MyGame::_drawPause;
  _ptrFuncDraw[GAME] = &graphic::MyGame::_drawGame;
  _ptrFuncDraw[TRANSITION] = &graphic::MyGame::_drawTransition;
}


graphic::MyGame::~MyGame()
{
}

graphic::MyGame		*graphic::MyGame::getInstance(void)
{
  if (_instance == NULL)
    _instance = new MyGame();
  return (_instance);
}

void		graphic::MyGame::killInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}

graphic::LoadTexture	*graphic::MyGame::getTextureManager(void) const
{
  return _textureManager;
}

sound::LoadSound	*graphic::MyGame::getSoundManager(void) const
{
  return _soundManager;
}

graphic::LoadModel	*graphic::MyGame::getModelManager(void) const
{
  return _modelManager;
}

graphic::CameraManagement	*graphic::MyGame::getCameraManagement(void) const
{
  return _cameraManagement;
}


void	graphic::MyGame::setMaxRound(const int m)
{
  _maxRound = m;
}

void	graphic::MyGame::setCurrentRound(const int r)
{
  _nbRound = r;
}

void	graphic::MyGame::setWinP1(const int w)
{
  _nbWinP1 = w;
}

void	graphic::MyGame::setWinP2(const int w)
{
  _nbWinP2 = w;
}

void		graphic::MyGame::initialize(void)
{
  window_.create();
  window_.setWidth(WINDOW_WIDTH);
  window_.setHeight(WINDOW_HEIGHT);

  _textureManager = new LoadTexture();
  _modelManager = new LoadModel();
  _cameraManagement = new CameraManagement();
  _soundManager = new sound::LoadSound();


  _cameraManagement->getCamera1()->initialize();

  _soundManager->addEntry("PRESS_START", "medias/explosion.ogg", sound::SOUND);
  _soundManager->addEntry("MENU", "music/game.ogg", sound::MUSIC);
  _soundManager->addEntry("INGAME", "medias/ingame.mp3", sound::MUSIC);

  _soundManager->addEntry("SELECT", "medias/select.mp3", sound::SOUND);
  _soundManager->addEntry("UNSELECT", "medias/unselect.mp3", sound::SOUND);
  _soundManager->addEntry("TRANSITION", "medias/321.wav", sound::SOUND);
  _soundManager->addEntry("TRANSITION_END", "medias/go.wav", sound::SOUND);
  _soundManager->addEntry("EXPLOSION", "medias/explosion.ogg", sound::SOUND);

  _soundManager->loadSounds();

  _soundManager->setLoop("EXPLOSION", 0);
  _soundManager->setLoop("INGAME");
  _soundManager->setLoop("MENU");

  _textureManager->addEntry("SHORT", "medias/Button0.5.png");
  _textureManager->addEntry("LONG", "medias/Button0.2.png");
  _textureManager->addEntry("LONGBLACK", "medias/LongButtonBlack.png");

  _textureManager->addEntry("PRESS_START", "medias/BombermanIntro.png");

  _textureManager->addEntry("THREE", "medias/3.png");
  _textureManager->addEntry("TWO", "medias/2.png");
  _textureManager->addEntry("ONE", "medias/1.png");
  _textureManager->addEntry("GO", "medias/GO.png");

  _textureManager->addEntry("PLAY", "medias/Play.png");
  _textureManager->addEntry("OPTIONS", "medias/Options.png");
  _textureManager->addEntry("HIGHSCORES", "medias/Highscores.png");
  _textureManager->addEntry("PAUSEMENU", "medias/PauseMenu.png");
  _textureManager->addEntry("KEYBOARD1", "medias/Keyboard1.png");
  _textureManager->addEntry("KEYBOARD2", "medias/Keyboard2.png");
  _textureManager->addEntry("SQUARE1", "medias/Square0.6.png");
  _textureManager->addEntry("SQUARE2", "medias/Square0.7.png");
  _textureManager->addEntry("BACKGROUND", "medias/Background2.png");

  _textureManager->addEntry("VOLUME1", "medias/Volume1.png");
  _textureManager->addEntry("VOLUME2", "medias/Volume2.png");
  _textureManager->addEntry("VOLUME3", "medias/Volume3.png");
  _textureManager->addEntry("VOLUME4", "medias/Volume4.png");
  _textureManager->addEntry("VOLUME5", "medias/Volume5.png");
  _textureManager->addEntry("VOLUME6", "medias/Volume6.png");
  _textureManager->addEntry("VOLUME7", "medias/Volume7.png");
  _textureManager->addEntry("VOLUME8", "medias/Volume8.png");
  _textureManager->addEntry("VOLUME9", "medias/Volume9.png");
  _textureManager->addEntry("VOLUME10", "medias/Volume10.png");

  _textureManager->addEntry("?", "medias/Interrogation.png");
  _textureManager->addEntry("BONUSSPEED", "medias/Speed.png");
  _textureManager->addEntry("MALUSLEFT", "medias/MalusLeft.png");
  _textureManager->addEntry("ULTIMATEBOMB", "medias/UltimateBombs.png");
  _textureManager->addEntry("STEEL", "medias/MetalBox.jpg");
  _textureManager->addEntry("FLAME", "medias/Flames.png");
  _textureManager->addEntry("TEXT_TABLE", "medias/ExportedFontBorder.png");

  _textureManager->addEntry("DEAD", "medias/GameOver.png");
  _textureManager->addEntry("WIN", "medias/Win.png");

  _textureManager->addEntry("WOOD", "medias/boxDestruct.jpg");

  // _textureManager->infos();
  _textureManager->loadTextures();

  _modelManager->addEntry("PLAYER1", "medias/assets/marvin.fbx");
  _modelManager->addEntry("PLAYER2", "medias/assets/marvin.fbx");

  _modelManager->addEntry("BOMB", "medias/assets/bomb.fbx");
  // _modelManager->infos();
  _modelManager->loadModels();

  _menu->initialize();
  _menuEventManager->initialize();
  _menuEventManager->loadMapping(1);
  _pauseMenu->initialize();

#if defined FPS_PRINT && FPS_PRINT > 0
  _printFPS = new Text(Vector(0.0f, 800.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
		       Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _printFPS->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _printFPS->rotEnableX();
#endif

  _printTimePlay = new Text(Vector(0.0f, 20.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
			    Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _printTimePlay->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _printTimePlay->rotEnableX();

  clock_.play();

  _background = new Primitive::Rectangle(Vector(WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f, 0.0f),
					 Vector(180.0f, 0.0f, 0.0f),
					 Color(1.0f, 1.0f, 1.0f),
					 Scale(5.0f, 5.0f, 2.0f));

  _videoBackgroundMenu = new Video("medias/copy.avi");
  _background->setVideo(_videoBackgroundMenu);

  _background->rotEnableX();

  _transition = new Transition;

  _intro = new Intro;
  _intro->initialize();

  _printRound = new Text(Vector((WINDOW_WIDTH / 2.0f) - 200.0f, 50.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
			 Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _printRound->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _printRound->rotEnableX();


  _score1 = new Text(Vector(0.0f, 40.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
		     Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _score1->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _score1->rotEnableX();
  _score2 = new Text(Vector(WINDOW_WIDTH - 300.0f, 40.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
		     Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _score2->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _score2->rotEnableX();

  _life1 = new Text(Vector(0.0f, 70.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
		    Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _life1->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _life1->rotEnableX();
  _life2 = new Text(Vector(WINDOW_WIDTH - 300.0f, 70.0f, 0.0f), Vector(180.0f, 1.0f, 0.0f),
		    Color(1.0f, 1.0f, 1.0f), Scale(0.15f, 0.15f, 0.15f));
  _life2->setTexture(_textureManager->getTexture("TEXT_TABLE"));
  _life2->rotEnableX();

  _printDead = new Primitive::Rectangle(Vector(0.0f, 0.0f, 1.0f), Vector(180.0f, 1.0f, 0.0f),
					Color(1.0f, 1.0f, 1.0f) );
  _printDead->setTexture(_textureManager->getTexture("DEAD"));
  _printDead->rotEnableX();

  _printWin = new Primitive::Rectangle(Vector(0.0f, 0.0f, 1.0f), Vector(180.0f, 1.0f, 0.0f),
				       Color(1.0f, 1.0f, 1.0f) );
  _printWin->setTexture(_textureManager->getTexture("WIN"));
  _printWin->rotEnableX();

  // we play the intro music at the beginning
  _soundManager->play("MENU");
}

void		graphic::MyGame::update(void)
{
  _maxRound = _menu->getRounds();

  if (place == MENU || place == INTRO)
    {
      if (_background->getVideo() == NULL) {
	_background->setTexture(NULL);
	_background->setVideo(_videoBackgroundMenu);
      }
    }
  else
    {
      if (_background->getVideo()) {
	_background->setVideo(NULL);
	_background->setTexture(_textureManager->getTexture("BACKGROUND"));
      }
    }


  clock_.updateFPS();
#if defined FPS_PRINT && FPS_PRINT > 0
  {
    std::stringstream ss;
    std::string str;

    ss << clock_.getFPS();
    ss >> str;
    str = "FPS: "+str;
    _printFPS->updateText(std::string(str));
  }
#endif

  // MENU EVENT MANAGER
  (this->*_ptrFuncUpdate[place])(clock_, input_);

  if (_cameraManagement->isMulti())
    _cameraManagement->getCamera2()->update(clock_, input_);
  _cameraManagement->getCamera1()->update(clock_, input_);

  if (_keyManager->getClose())
    window_.close();


  if (place != TRANSITION && place != INTRO)
    {
      _menuEventManager->checkEvents();
      _keyManager->updateKeys();
    }

  // reset round datas
  if (place != GAME && place != TRANSITION && place != PAUSE)
    {
      _nbWinP1 = 0; _nbWinP2 = 0; _nbRound = 0;
      _background->update(clock_, input_);
    }

  // for the score/life print
  {
    if (place == GAME)
      {
	std::map<APlayer *, bool>::const_iterator it = Core::getInstance()->getPlayers().begin();

	std::stringstream ss;
	std::string str;

	ss << it->first->getScore();
	ss >> str;
	_score1->updateText("Score player 1: "+str);
	ss.clear(); str.clear();

	ss << it->first->life();
	ss >> str;
	_life1->updateText("Life player 1: "+str);
	ss.clear(); str.clear();

	if (_cameraManagement->isMulti())
	  {
	    ++it;
	    ss << it->first->getScore();
	    ss >> str;
	    _score2->updateText("Score player 2: "+str);
	    ss.clear(); str.clear();

	    ss << it->first->life();
	    ss >> str;
	    _life2->updateText("Life player 2: "+str);
	    ss.clear(); str.clear();
	  }
      }
  }

  _printDead->update(clock_, input_);

  // for the round print
  if (place == GAME)
    {
      std::stringstream ss;
      std::string tmp;
      std::string str("Round: ");

      ss << _nbRound;
      ss >> tmp;
      str = str + tmp;
      ss.clear(); tmp.clear();

      ss << _maxRound;
      ss >> tmp;
      str = str + std::string(" / ");
      str = str + tmp;
      ss.clear(); tmp.clear();


      ss << _nbWinP1;
      ss >> tmp;
      str = str + std::string("  P1win:");
      str = str + tmp;
      ss.clear(); tmp.clear();

      if (_cameraManagement->isMulti()) {
	ss << _nbWinP2;
	ss >> tmp;
	str = str + std::string("//  P2win:");
	str = str + tmp;
	ss.clear(); tmp.clear();
      }

      _printRound->updateText(str);
    }
}

void	graphic::MyGame::draw(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  glClearColor(1.f, 1.f, 1.f, 1.0f);
  glClearDepth(1.0f);

  Camera::switchToOrtho();
  _background->draw();
  Camera::switchBackToPerspective();


  std::map<APlayer *, bool>::const_iterator it = Core::getInstance()->getPlayers().begin();
  for (char n = 0; n < ((_cameraManagement->isMulti()) ? 2 : 1) ; ++n)
    {

      if (n == 0)
	{

	  if ((place == GAME || place == TRANSITION)
	      && it != Core::getInstance()->getPlayers().end())
	    {
	      _cameraManagement->moveCamera1(it->first);
	      _cameraManagement->getCamera1()->update(clock_, input_);
	      ++it;
	    }
	}
      else
	{
	  if (place == GAME || place == TRANSITION)
	    {
	      if (_cameraManagement->isMulti()
		  && it != Core::getInstance()->getPlayers().end())
		{
		  _cameraManagement->moveCamera2(it->first);
		  _cameraManagement->getCamera2()->update(clock_, input_);
		  ++it;
		}
	    }
	  else
	    break;
	}

      if (place != TRANSITION)
	(this->*_ptrFuncDraw[place])();
      else if (place == TRANSITION) // transition draw game and later the real transition screen to having a background
	(this->*_ptrFuncDraw[GAME])();
    }

  glViewport(0, 0,
	     MyGame::WINDOW_WIDTH, MyGame::WINDOW_HEIGHT);

  // 2D mode
  Camera::switchToOrtho();
#if defined FPS_PRINT && FPS_PRINT > 0
  _printFPS->draw();
#endif
  if (place == TRANSITION)
    (this->*_ptrFuncDraw[place])();

  if (place == GAME)
    {
      _printTimePlay->draw();
      _score1->draw();
      if (_cameraManagement->isMulti())
	_score2->draw();
      _life1->draw();
      if (_cameraManagement->isMulti())
	_life2->draw();
    }

  if (place == GAME)
    {
      std::map<APlayer *, bool>::const_iterator it = Core::getInstance()->getPlayers().begin();
      for (char n = 0 ; it != Core::getInstance()->getPlayers().end() && n < 2; ++it, ++n)
	{
	  if (it->second == false)
	    {
	      if (n == 0 && _cameraManagement->isMulti())
		_printDead->setPosition(Vector(WINDOW_WIDTH / 4.0f, WINDOW_HEIGHT / 2.0f, 0.0f));
	      else if (n == 0)
		{
		  _printDead->setPosition(Vector(WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f, 0.0f));
		  _toKillGame = DEAD;
		}
	      else
		_printDead->setPosition(Vector(WINDOW_WIDTH - WINDOW_WIDTH / 4.0f, WINDOW_HEIGHT / 2.0f, 0.0f));
	      _printDead->draw();
	    }
	}
    }

  if (_toKillGame != CURRENT)
    {
      if (_toKillGame == WIN)
	_printWin->draw();
      else
	_printDead->draw();

      if (_nbRound >= _maxRound)
	{
	  Core::killInstance();
	  _printRound->draw();
	  place = MENU;

	  _cameraManagement->getCamera1()->resetToMenu();
	  _cameraManagement->enableSingle();
	  _menuEventManager->loadMapping(1);
	  _menu->getShownMenu()->setLocked(false);
	  _soundManager->stop("INGAME");
	}
    }

  if (place == GAME)
    _printRound->draw();

  Camera::switchBackToPerspective();


  if (_toKillGame != CURRENT && _nbRound < _maxRound)
    {
      this->window_.display();
      sleep(1);
      _pauseMenu->restart();
      _toKillGame = CURRENT;
    }
  // if the game is finished, we freeze the screen for the print
  if (_toKillGame != CURRENT)
    {
      _toKillGame = CURRENT;
      this->window_.display();
      sleep(1);
    }

  this->window_.display();

}


void	graphic::MyGame::unload(void)
{
  delete _soundManager;
  delete _textureManager;
  delete _modelManager;
  _pauseMenu->killInstance();
  _menu->killInstance();

  LoadText::killInstance();

  _keyManager->killInstance();
#if defined FPS_PRINT && FPS_PRINT > 0
  if (_printFPS)
    delete _printFPS;
#endif
  score::HighScore::killHighScoreInstance();

  delete _background;
  delete _printTimePlay;
  delete _cameraManagement;
}

void	graphic::MyGame::_updateMenu(MyClock const &cl, gdl::Input &in)
{
  _menu->update(cl, in);
}

void	graphic::MyGame::_updateIntro(MyClock const &cl, gdl::Input &in)
{
  _intro->update(cl, in);
  if (_intro->isFinished())
    {
      _cameraManagement->getCamera1()->resetToMenu();
      place = MENU;
      _soundManager->play("PRESS_START");
    }
}

void	graphic::MyGame::_updatePause(MyClock const &cl, gdl::Input &in)
{
  _menu->update(cl, in);
}

void	graphic::MyGame::_updateTransition(MyClock const &cl, gdl::Input &in)
{
  _updateGame(cl, in);
  _transition->update(cl, in);
  if (_transition->isFinished())
    {
      _transition->initialize();
      place = GAME;
      _soundManager->stop("TRANSITION_END");
      _soundManager->play("INGAME");
    }
}

void	graphic::MyGame::_updateGame(MyClock const &cl, gdl::Input &in)
{
  Core::getInstance()->update(cl, in);
  _printTimePlay->updateText(cl.timeToString(Core::getInstance()->getTotalTime()));
  if (Core::getInstance()->isFinished())
    {
      _nbRound++;
      _toKillGame = DEAD;
      char n = 0;
      for (std::map<APlayer *, bool>::const_iterator it = Core::getInstance()->getPlayers().begin();
	   it != Core::getInstance()->getPlayers().end(); ++it, ++n)
	{
	  if (it->second)
	    {
	      _toKillGame = WIN;
	      if (n == 0 && _cameraManagement->isMulti()) {
		_printWin->setPosition(Vector(WINDOW_WIDTH / 4.0f, WINDOW_HEIGHT / 2.0f, 0.0f));
		_nbWinP1++;
	      }
	      else if (n == 0) {
		_printWin->setPosition(Vector(WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f, 0.0f));
		_nbWinP1++;
	      }
	      else {
		_printWin->setPosition(Vector(WINDOW_WIDTH - WINDOW_WIDTH / 4.0f, WINDOW_HEIGHT / 2.0f, 0.0f));
		_nbWinP2++;
	      }
	    }
	}
    }
}

void	graphic::MyGame::_drawIntro(void)
{
  _intro->draw();
}

void	graphic::MyGame::_drawMenu(void)
{
  _menu->display();
}

void	graphic::MyGame::_drawPause(void)
{
  _pauseMenu->display();
}

void	graphic::MyGame::_drawGame(void)
{
  Core::getInstance()->draw();
}

void	graphic::MyGame::_drawTransition(void)
{
  _transition->draw();
}

MyClock const &	graphic::MyGame::getMyClock(void) const
{
  return clock_;
}

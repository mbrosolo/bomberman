//
// MyGame.hh for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Mon May 14 14:38:33 2012 tosoni
// Last update Sun Jun  3 22:43:26 2012 benjamin bourdin
//

#ifndef		__MYGAME_HH__
#define		__MYGAME_HH__

#include	<vector>

#include	"SFML/Graphics.hpp"
#include	"SFML/Window.hpp"
#include	"SFML/System.hpp"
#include	"Game.hpp"
#include	"Clock.hpp"
#include	"Camera.hh"
#include	"LoadModel.hh"
#include	"LoadSound.hh"
#include	"LoadTexture.hh"
#include	"MenuEventManager.hh"
#include	"PauseMenu.hh"
#include	"Text.hh"
#include	"CameraManagement.hh"
#include	"Primitive.hh"
#include	"Intro.hh"
#include	"Transition.hh"

class		Menu;
class		PauseMenu;
class		KeyManager;
class		MenuEventManager;

namespace graphic
{
  /*!
   * @class MyGame
   * @brief Define the game
   */
  class MyGame : public gdl::Game
  {

  /*!
   * @brief The default number of round
   */
#define	NB_ROUND_DEFAULT	3

  /*!
   * @brief The default ?
   */
#define FPS_PRINT 1

  public:
    /*!
     * @brief Enum place in game
     */
    enum eGamePlace
      {
	INTRO,
	MENU,
	PAUSE,
	TRANSITION,
	GAME,
	NBSTATE
      };

    /*!
     * @brief Enum end of the game
     */
    enum eEndGame
      {
	CURRENT,
	WIN,
	DEAD
      };

  public:
    /*!
     * @brief initialize method of MyGame
     */
    void	initialize(void);
    /*!
     * @brief update method of MyGame
     */
    void	update(void);
    /*!
     * @brief draw method of MyGame
     */
    void	draw(void);
    /*!
     * @brief unload method of MyGame
     */
    void	unload(void);

    /*!
     * @brief getInstance static method of MyGame
     * @return Instance of MyGame object
     */
    static MyGame	*getInstance(void);
    /*!
     * @brief killInstance static method of MyGame
     */
    static void		killInstance(void);

    /*!
     * @brief WINDOW_WIDTH static const varaible of MyGame
     */
    static const float	WINDOW_WIDTH;
    /*!
     * @brief WINDOW_HEIGHT static const varaible of MyGame
     */
    static const float	WINDOW_HEIGHT;

  public:
    /*!
     * @brief getSoundManager method of MyGame
     * @return Object LoadSound
     */
    sound::LoadSound	*getSoundManager() const;
    /*!
     * @brief getTextureManager method of MyGame
     * @return Object LoadTexture
     */
    LoadTexture		*getTextureManager() const;
    /*!
     * @brief getModelManager method of MyGame
     * @return Object LoadModel
     */
    LoadModel		*getModelManager() const;
    /*!
     * @brief getCameraManager method of MyGame
     * @return Object CameraManagement
     */
    CameraManagement	*getCameraManagement() const;

    /*!
     * @brief getMyClock method of MyGame
     * @return Object MyClock
     */
    MyClock	const &getMyClock(void) const;

    /*!
     * @brief setMaxRound method of MyGame
     * @param m The maximum of rounds
     */
    void	setMaxRound(const int m);
    /*!
     * @brief setCurrentRound method of MyGame
     * @param m The current rounds
     */
    void	setCurrentRound(const int r);
    /*!
     * @brief setWinP1 method of MyGame
     * @param w ?
     */
    void	setWinP1(const int w);
    /*!
     * @brief setWinP2 method of MyGame
     * @param w ?
     */
    void	setWinP2(const int w);

  protected:
    MyClock	clock_;

  private:
    /*!
     * @brief Constructor of MyGame
     */
    MyGame();
    /*!
     * @brief Destructor of MyGame
     */
    ~MyGame();
    MyGame(MyGame const &);
    MyGame & operator=(MyGame const &);

  private:

    sound::LoadSound	*_soundManager; /*!< The sound manager object */
    LoadTexture		*_textureManager; /*!< The texture manager object */
    LoadModel		*_modelManager; /*!< The model manager object */
    CameraManagement	*_cameraManagement; /*!< The camera manager object */

    Intro		*_intro; /*!< The intro object */
    Menu		*_menu; /*!< The menu object */
    PauseMenu		*_pauseMenu; /*!< The pause menu object */
    MenuEventManager	*_menuEventManager; /*!< The menu event manager object */
    KeyManager		*_keyManager; /*!< The key manager object */

    static MyGame	*_instance; /*!< The instance object */

    Text		*_printFPS; /*!< The FPS text object */
    Text		*_printTimePlay; /*!< The time play text object */
    Text		*_printRound; /*!< The print round text object */

    Primitive::Rectangle	*_background; /*!< The background object */

    Transition			*_transition; /*!< The transition object */
    Primitive::Rectangle	*_printDead; /*!< The dead print object */
    Primitive::Rectangle	*_printWin; /*!< The win print object */

    typedef void	(MyGame::*ptrUpdate)(MyClock const &, gdl::Input &);
    typedef void	(MyGame::*ptrDraw)(void);

    std::vector<ptrUpdate>	_ptrFuncUpdate; /*!< Vector of pointer function for update */
    std::vector<ptrDraw>	_ptrFuncDraw; /*!< Vector of pointer function for draw */

    Text		*_score1; /*!< Text for score player 1 */
    Text		*_score2; /*!< Text for score player 2 */

    Text		*_life1; /*!< Text for life player 1 */
    Text		*_life2; /*!< Text for life player 2 */

    eEndGame		_toKillGame; /*!< Result of the game */
    int			_nbWinP1; /*!< Result p1 */
    int			_nbWinP2; /*!< Result p2 */
    int			_nbRound; /*!< Number of rounds */

    int			_maxRound; /*!< Number max of rounds */

    Video		*_videoBackgroundMenu; /*!< Background video */

  public:
    static eGamePlace		place; /*!< Current place in game */

  private:
    /*!
     * @brief _updateIntro method of the MyGame
     * @param clock The clock of the game
     * @param input The input
     */
    void	_updateIntro(MyClock const &, gdl::Input &);
    /*!
     * @brief _updateMenu method of the MyGame
     * @param clock The clock of the game
     * @param input The input
     */
    void	_updateMenu(MyClock const &, gdl::Input &);
    /*!
     * @brief _updatePause method of the MyGame
     * @param clock The clock of the game
     * @param input The input
     */
    void	_updatePause(MyClock const &, gdl::Input &);
    /*!
     * @brief _updateGame method of the MyGame
     * @param clock The clock of the game
     * @param input The input
     */
    void	_updateGame(MyClock const &, gdl::Input &);
    /*!
     * @brief _updateTransition method of the MyGame
     * @param clock The clock of the game
     * @param input The input
     */
    void	_updateTransition(MyClock const &, gdl::Input &);

    /*!
     * @brief _drawIntro method of the MyGame
     */
    void	_drawIntro(void);
    /*!
     * @brief _drawMenu method of the MyGame
     */
    void	_drawMenu(void);
    /*!
     * @brief _drawPause method of the MyGame
     */
    void	_drawPause(void);
    /*!
     * @brief _drawGame method of the MyGame
     */
    void	_drawGame(void);
    /*!
     * @brief _drawTransition method of the MyGame
     */
    void	_drawTransition(void);

  };
}

#endif

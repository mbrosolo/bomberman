//
// LoadSound.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Thu May 10 16:48:16 2012 benjamin bourdin
// Last update Sun Jun  3 20:51:46 2012 benjamin bourdin
//

#include	<vector>
#include	<algorithm>
#include	<iostream>
#include	"LoadSound.hh"
#include	"ExceptionLoading.hh"

// fct for the match of what key we remove
namespace loadSound_utils
{
  bool	_matchKeyEntry(sound::LoadSound::sound_entry_assoc_t const &p, std::string const &k)
  {
    if (p.first == k)
      return true;
    return false;
  }

  bool	_matchKeyLoaded(sound::LoadSound::sound_load_assoc_t const &p, std::string const &k)
  {
    if (p.first == k)
      return true;
    return false;
  }

  void	_printAll(sound::LoadSound::sound_entry_assoc_t const &p)
  {
    std::cout << "Key=[" << p.first << "] __ Path_file=[" << p.second << "]" << std::endl;
  }

  void	_rmAllSounds(sound::LoadSound::sound_load_assoc_t & obj)
  {
    FMOD_RESULT res;
    if (obj.second)
      {
	res = FMOD_Sound_Release(obj.second);
	if (res != FMOD_OK)
	  std::cerr << "Error while releasing sound" << obj.first << std::endl;
      }
  }

  void	_rmAllEntries(sound::LoadSound::sound_entry_assoc_t & obj)
  {
    if (obj.second)
      delete obj.second;
    obj.second = NULL;
  }

}


sound::LoadSound::LoadSound()
  : _soundList(), _loadedSound(), _system()
{
  FMOD_RESULT res;
  if ((res = FMOD_System_Create(&_system)) != FMOD_OK)
    std::cerr << "Error during system FMOD creation" << std::endl;
  else
    if ((res = FMOD_System_Init(_system, 5, FMOD_INIT_NORMAL, NULL)) != FMOD_OK) {
      std::cerr << "Error during system FMOD initialisation" << std::endl;
      _system = NULL;
    }
}

sound::LoadSound::~LoadSound()
{
  std::for_each(_soundList.begin(), _soundList.end(), &loadSound_utils::_rmAllEntries);
  std::for_each(_loadedSound.begin(), _loadedSound.end(), &loadSound_utils::_rmAllSounds);
  FMOD_RESULT res;
  if ((res = FMOD_System_Close(_system)) != FMOD_OK)
    std::cerr << "Error during closing FMOD system" << std::endl;
  if ((res = FMOD_System_Release(_system)) != FMOD_OK)
    std::cerr << "Error during releasing FMOD system" << std::endl;
}

sound::LoadSound::LoadSound(LoadSound const &c)
  : _soundList(c._soundList), _loadedSound(c._loadedSound), _system(c._system)
{
}

sound::LoadSound &sound::LoadSound::operator=(LoadSound const &c)
{
  _soundList = c._soundList;
  _loadedSound = c._loadedSound;
  _system = c._system;
  return *this;
}


void	sound::LoadSound::addEntry(std::string const &key, std::string const &file, eTypeSound const type)
{
  std::vector<std::string> s(1, key);
  sound_entry_list_t::iterator it;
  if ((it = std::search(_soundList.begin(), _soundList.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyEntry))
      == _soundList.end())
    {
      _soundList.push_back(sound_entry_assoc_t(key, new SoundType(type, file)));
    }
  else
    {
      it->second->path = file;
    }
}


bool	sound::LoadSound::delEntry(std::string const &key)
{
  std::vector<std::string> s(1, key);
  sound_entry_list_t::iterator it;
  if ((it = std::search(_soundList.begin(), _soundList.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyEntry)) != _soundList.end())
    {
      _soundList.erase(it);
      return true;
    }
  return false;
}

void	sound::LoadSound::infos(void) const
{
  std::for_each(_soundList.begin(), _soundList.end(), &loadSound_utils::_printAll);
}

void	sound::LoadSound::loadSounds(void)
{
  for (sound_entry_list_t::const_iterator it = _soundList.begin(); it != _soundList.end(); ++it)
    {
      FMOD_SOUND	*mus;

      SoundType *t = it->second;
      FMOD_MODE mode;
      if (t->type == MUSIC)
	mode = FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM | FMOD_LOOP_NORMAL;
      else
	mode = FMOD_CREATESAMPLE | FMOD_LOOP_NORMAL;

      if (FMOD_System_CreateSound(_system, t->path.c_str(), mode, 0, &mus) != FMOD_OK)
	std::cerr << "Error during create sound" << std::endl;
      else
	if (FMOD_Sound_SetLoopCount(mus, 0) != FMOD_OK)
	  std::cerr << "Error during set loop sound" << std::endl;
	else
	  _loadedSound.push_back(sound_load_assoc_t(it->first, mus));
    }
}


bool	sound::LoadSound::isValidKeyLoaded(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::const_iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded)) != _loadedSound.end())
    {
      return true;
    }
  return false;
}


void	sound::LoadSound::loadSoundDynamic(std::string const &key,
					   std::string const &file,
					   eTypeSound const type)
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded))
      == _loadedSound.end())
    { // not found, it's ok
      FMOD_SOUND	*mus;

      FMOD_MODE mode;
      if (type == MUSIC)
	mode = FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM | FMOD_LOOP_NORMAL;
      else
	mode = FMOD_CREATESAMPLE | FMOD_LOOP_NORMAL;

      if (FMOD_System_CreateSound(_system, file.c_str(), mode, 0, &mus) != FMOD_OK)
	std::cerr << "Error during create sound" << std::endl;
      else
	if (FMOD_Sound_SetLoopCount(mus, 0) != FMOD_OK)
	  std::cerr << "Error during set loop sound" << std::endl;
	else
	  _loadedSound.push_back(sound_load_assoc_t(it->first, mus));
    }
  else
    {
      FMOD_SOUND	*mus;

      FMOD_MODE mode;
      if (type)
	mode = FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM | FMOD_LOOP_NORMAL;
      else
	mode = FMOD_CREATESAMPLE | FMOD_LOOP_NORMAL;

      if (FMOD_System_CreateSound(_system, file.c_str(), mode, 0, &mus) != FMOD_OK)
	std::cerr << "Error during create sound" << std::endl;
      else
	if (FMOD_Sound_SetLoopCount(mus, 0) != FMOD_OK)
	  std::cerr << "Error during set loop sound" << std::endl;
	else
	  it->second = mus;
    }
}

bool	sound::LoadSound::play(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::const_iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded)) != _loadedSound.end())
    {
      if (FMOD_System_PlaySound(_system, FMOD_CHANNEL_FREE, it->second, 0, NULL) != FMOD_OK)
	{
	  std::cerr << "Error during play sound" << std::endl;
	  return false;
	}
      return true;
    }
  return false;
}

bool	sound::LoadSound::setLoop(std::string const &key, const int loop) const
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::const_iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded)) != _loadedSound.end())
    {
      if (FMOD_Sound_SetLoopCount(it->second, loop) != FMOD_OK)
	{
	  std::cerr << "Error during setting loop" << std::endl;
	  return false;
	}
      return true;
    }
  return false;
}

bool	sound::LoadSound::pause(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::const_iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded)) != _loadedSound.end())
    {
      FMOD_CHANNELGROUP *canal;
      FMOD_BOOL st;

      if (FMOD_System_GetMasterChannelGroup(_system, &canal) != FMOD_OK)
	std::cerr << "Error during getting channel group" << std::endl;
      else
	{
	  if (FMOD_ChannelGroup_GetPaused(canal, &st) != FMOD_OK)
	    std::cerr << "Error during getting paused the sound" << std::endl;
	  else
	    {
	      if (st) {
		if (FMOD_ChannelGroup_SetPaused(canal, 0) != FMOD_OK)
		  std::cerr << "Error during sertting pause" << std::endl; // we remove pause
	      }
	      else
		if (FMOD_ChannelGroup_SetPaused(canal, 1) != FMOD_OK)
		  std::cerr << "Error during setting pause" << std::endl; // we set to pause
	      return true;
	    }
	}
    }
  return false;
}

bool	sound::LoadSound::setVolume(std::string const &key, const float vol) const
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::const_iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded)) != _loadedSound.end())
    {
      FMOD_CHANNELGROUP *canal;

      if (FMOD_System_GetMasterChannelGroup(_system, &canal) != FMOD_OK)
	std::cerr << "Error during getting channel master" << std::endl;
      else
	if (FMOD_ChannelGroup_SetVolume(canal, vol) != FMOD_OK)
	  std::cerr << "Error during setting volume" << std::endl;
	else
	  return true;
      return false;
    }
  return false;
}

void	sound::LoadSound::setAllVolume(const float vol) const
{
  for (sound_load_list_t::const_iterator it = _loadedSound.begin();
       it != _loadedSound.end(); ++it)
    {
      setVolume(it->first, vol);
    }
}

void	sound::LoadSound::stopAllSounds(void) const
{
  for (sound_load_list_t::const_iterator it = _loadedSound.begin();
       it != _loadedSound.end(); ++it)
    {
      stop(it->first);
    }
}

bool	sound::LoadSound::stop(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  sound_load_list_t::const_iterator it;
  if ((it = std::search(_loadedSound.begin(), _loadedSound.end(),
			s.begin(), s.end(), &loadSound_utils::_matchKeyLoaded)) != _loadedSound.end())
    {
      FMOD_CHANNELGROUP *canal;

      if (FMOD_System_GetMasterChannelGroup(_system, &canal) != FMOD_OK)
	std::cerr << "Error during getting channel master" << std::endl;
      else
	if (FMOD_ChannelGroup_Stop(canal) != FMOD_OK)
	  std::cerr << "Error during stopping sound" << std::endl;
	else
	  return true;
      return false;
    }
  return false;
}

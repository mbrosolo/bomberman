//
// ComplexObj.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May  7 14:40:11 2012 benjamin bourdin
// Last update Sun Jun  3 20:22:20 2012 benjamin bourdin
//

#ifndef __COMPLEXOBJ_HH__
#define __COMPLEXOBJ_HH__

/**
 * @author benjamin bourdin
 * @file ConplexObj.hh
 */

#include	<GL/gl.h>
#include	"AForm.hh"

namespace graphic
{
  namespace Complex
  {
    /*!
     * @class Cube
     * @brief Define an object's form
     */
    class Cube : public AForm
    {
    public:
      /*!
       * @brief Constructor of Cube
       * @param pos The position
       * @param rot The rotation
       * @param color The color
       * @param scale The scale
       */
      Cube(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	   Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	   Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	   Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
      /*!
       * @brief Destructor of Cube
       */
      ~Cube();
      /*!
       * @brief Constructor by copy of Cube
       * @param copy The copy
       */
      Cube(Cube const &copy);
      /*!
       * @brief operator= override of Cube
       * @param copy The copy
       * @return class Cube
       */
      Cube &operator=(Cube const &copy);

      /*!
       * @brief Initialize method of Cube
       */
      void	initialize(void);
      /*!
       * @brief Update method of Cube
       * @param clock The clock of the game
       * @param input The input
       */
      void	update(MyClock const &clock, gdl::Input &input);
      /*!
       * @brief draw method of Cube
       */
      void	draw(void);

      /*!
       * @brief getWidth method of Cube
       */
      float	getWidth(void) const;
      /*!
       * @brief getHeight method of Cube
       */
      float	getHeight(void) const;
      /*!
       * @brief getDepth method of Cube
       */
      float	getDepth(void) const;

    private:
      const float	_width; /*!< The width of the object */
      const float	_height; /*!< The height of the object */
      const float	_depth; /*!< The depth of the object */

    };

    /*
     * @class Pyramid
     * @brief The pyramid object
     */
    class Pyramid : public AForm
    {
    public:
      /*!
       * @brief Constructor of Cube
       * @param pos The position
       * @param rot The rotation
       * @param color The color
       * @param scale The scale
       */
      Pyramid(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	   Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	   Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	   Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
      /*!
       * @brief Destructor of Cube
       */
      ~Pyramid();
      /*!
       * @brief Constructor by copy of Cube
       * @param copy The copy
       */
      Pyramid(Pyramid const &copy);
      /*!
       * @brief operator= override of Cube
       * @param copy The copy
       * @return The class
       */
      Pyramid &operator=(Pyramid const &);

      /*!
       * @brief Initialize method of Cube
       */
      void initialize(void);
      /*!
       * @brief Update method of Cube
       * @param clock The clock of the game
       * @param input The input
       */
      void update(MyClock const &clock, gdl::Input &input);
      /*!
       * @brief Draw method of Cube
       */
      void draw(void);

      /*!
       * @brief getWidth method of Cube
       */
      float	getWidth(void) const;
      /*!
       * @brief getHeight method of Cube
       */
      float	getHeight(void) const;
      /*!
       * @brief getDepth method of Cube
       */
      float	getDepth(void) const;

    private:
      const float	_width; /*!< The width of the object */
      const float	_height; /*!< The height of the object */
      const float	_depth; /*!< The depth of the object */

    };

  }
}

#endif

//
// LoadModel.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May  9 16:21:06 2012 benjamin bourdin
// Last update Sun Jun  3 20:38:42 2012 benjamin bourdin
//

#ifndef	__LOAD_MODEL_HH__
#define __LOAD_MODEL_HH__

/**
 * @author bourdi_b
 * @file LoadModel.hh
 */

#include	<list>
#include	<utility>
#include	<string>
#include	"Model.hpp"

namespace graphic
{

  /*! @class LoadModel
   * @brief Contain all models which will be used later.
   * it is really a models manager which can load model for their use
   */
  class LoadModel
  {
  public:
    /*!
     * @brief Intern correspondance key/path for entries
     */
    typedef	std::pair<std::string, std::string>	model_entry_assoc_t;
    /*!
     * @brief Intern list of entries
     */
    typedef	std::list<model_entry_assoc_t>	model_entry_list_t;
    /*!
     * @brief Intern correspondance key/model for models loaded
     */
    typedef	std::pair<std::string, gdl::Model *>	model_load_assoc_t;
    /*!
     * @brief Intern list of models loaded
     */
    typedef	std::list<model_load_assoc_t>		model_load_list_t;

  public:
    /*!
     * @brief Contructor, private of LoadModel
     */
    LoadModel();
    /*!
     * @brief Destructor, private of LoadModel
     */
    ~LoadModel();

  public:

    /*!
     * @brief Add a new entry in the database, has to be used before 'loadModels',
     * otherwise model will be ignored
     * @param key The key linked to the path (for calling the model loaded through this key)
     * @param path The path of the model
     */
    void	addEntry(std::string const &key, std::string const &path);
    /*!
     * @brief Delete an entry in the database
     * @param key The key which will deleted. If the model is already loaded,
     * there is no effect on it
     */
    bool	delEntry(std::string const &key);

    /*!
     * @brief Print all key/path in the database entries
     */
    void	infos(void) const;

    /*!
     * @brief Load all models in entries.
     * It has to be called once.
     */
    void	loadModels(void);

    /*!
     * @brief Check if a key correspond to a model loaded
     * @param key The key of the model
     */
    bool	isValidKeyLoaded(std::string const &key) const;

    /*!
     * @brief Get a model loaded in order to be printed on screen
     * @param key The key of the model
     * @return The model, or NULL if key is invalid (use isValidKeyLoaded before to be sure)
     */
    gdl::Model	*getModel(std::string const &key) const;

    /*!
     * @brief Load a model dynamically without passing by the phase add/delEntry.
     * It is used when we want to add entry after having called loadModels method.
     * @param key The key of the model
     * @param path The path of the model
     */
    void	loadModelDynamic(std::string const &key, std::string const &path);

  private:
    model_entry_list_t	_texList; /*!< List of all models entries */
    model_load_list_t		_loadedModel; /*!< list of loaded models */

  };

}

#endif

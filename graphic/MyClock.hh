//
// MyClock.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 17:28:00 2012 benjamin bourdin
// Last update Sun Jun  3 22:47:01 2012 benjamin bourdin
//

#ifndef __MYCLOCK_HH__
#define __MYCLOCK_HH__

/**
 * @author bourdi_b
 * @file Files.hh
 */

#include	<string>
#include	"GameClock.hpp"

/*!
 * @class MyClock
 * @brief Clock for the Game
 */
class MyClock
{
public:
  /*!
   * @brief Constructor for MyClock
   * @param
   */
  MyClock(gdl::GameClock &);
  /*!
   * @brief Destructor for MyClock
   */
  ~MyClock();

  /*!
   * @brief Updates the FPS
   */
  void	updateFPS(void);
  /*!
   * @brief Updates the Clock
   */
  void	update(void);
  /*!
   * @brief Launches the Clock
   */
  void	play(void);
  /*!
   * @brief Resets the Clock
   */
  void	reset(void);
  /*!
   * @brief Pauses the Clock
   */
  void	pause(void);
  /*!
   * @brief Getter for the elapse time
   * @return
   */
  float	getElapsedTime(void) const;
  //float	getTotalElapsedTime(void) const;
  /*!
   * @brief Getter for the total game time
   * @return
   */
  float	getTotalGameTime(void) const;
  /*!
   * @brief Getter the Game Clock
   * @return
   */
  gdl::GameClock &getGameClock(void) const;

  /*!
   * @brief Getter for the FPS
   * @return
   */
  int	getFPS(void) const;
  /*!
   * @brief Getter for the elapse FPS
   * @return
   */
  float	getElapsedFPS(void) const;

  /*!
   * @brief Returns the time in a String
   * @param
   * @return
   */
  std::string timeToString(float) const;

  /*!
   * @brief Getter for the current date
   * @return
   */
  std::string getCurDate(void) const;

private:
  int		_fps; /*!< FPS */
  int		_tmpfps; /*!< tmp FPS */
  gdl::Clock	_clock; /*!< clock */
  float		_timeElapsedFPS; /*!< time elapse FPS */

  float		_timeTotalFPS; /*!< time total FPS */

  gdl::GameClock & _gc; /*!< game clock */
};

#endif

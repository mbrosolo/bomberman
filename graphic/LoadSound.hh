//
// LoadSound.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Thu May 10 16:43:34 2012 benjamin bourdin
// Last update Sun Jun  3 20:51:18 2012 benjamin bourdin
//

#ifndef	__LOADSOUND_HH__
#define	__LOADSOUND_HH__

/**
 * @author bourdi_b
 * @file LoadSound.hh
 */

#include	<list>
#include	<utility>
#include	<string>
#include	"FMOD/fmod.h"

namespace sound
{
  /*!
   * @enum eTypeSound
   * @brief Define the type of a sound
   */
  enum eTypeSound
    {
      SOUND, /*!< it is a sound */
      MUSIC /*!< it is a music */
    };

  /*!
   * @struct SoundType
   * @brief The structure giving a correspondance of a type with a path
   */
  struct SoundType
  {
    /*!
     * @brief THe constructor
     * @param t The type
     * @param p The Path
     */
    SoundType(eTypeSound const t, const std::string &p)
      : type(t), path(p)
    {
    }

    eTypeSound	type; /*!< The type of a sound */
    std::string path; /*!< The path of a sound */
  };

  /*! @class LoadSound
   * @brief Contain all sound which will be used later.
   * it is really a sound manager which can load all sound for their later use.
   */
  class LoadSound
  {
  public:
    /*!
     * @brief Intern correspondance key/path for entries
     */
    typedef	std::pair<std::string, SoundType *>	sound_entry_assoc_t;
    /*!
     * @brief Intern list of entries
     */
    typedef	std::list<sound_entry_assoc_t>		sound_entry_list_t;
    /*!
     * @brief Intern correspondance key/sound for sounds loaded
     */
    typedef	std::pair<std::string, FMOD_SOUND *>	sound_load_assoc_t;
    /*!
     * @brief Intern list of sounds loaded
     */
    typedef	std::list<sound_load_assoc_t>		sound_load_list_t;

  public:
    /*!
     * @brief Contructor
     */
    LoadSound();
    /*!
     * @brief Destructor
     */
    ~LoadSound();

  public:

    /*!
     * @brief Add a new entry in the database, has to be used before 'loadSounds',
     * otherwise model will be ignored
     * @param key The key linked to the path (for calling the sound loaded through this key)
     * @param path The path of the sound
     * @param type The type of the sound (sound or music)
     */
    void	addEntry(std::string const &key, std::string const &path,
			 eTypeSound const type);
    /*!
     * @brief Delete an entry in the database
     * @param key The key which will deleted. If the sound is already loaded,
     * there is no effect on it
     */
    bool	delEntry(std::string const &key);

    /*!
     * @brief Print all key/path in the database entries
     */
    void	infos(void) const;

    /*!
     * @brief Load all sounds in entries.
     * It has to be called once.
     */
    void	loadSounds(void);

    /*!
     * @brief Check if a key correspond to a sound loaded
     * @param key The key of the sound
     */
    bool	isValidKeyLoaded(std::string const &key) const;

    /*!
     * @brief Set to the sound the number of repetition
     * @param key The key of the sound
     * @param nb_loop The number of repetition of the sound (or infinite by default (-1))
     * @return True if all is ok, otherwise false
     */
    bool	setLoop(std::string const &key, const int nb_loop = -1) const;
    /*!
     * @brief Play a sound
     * @param key The key of the sound
     * @return True if all is ok, otherwise false
     */
    bool	play(std::string const &key) const;
    /*!
     * @brief Pause a sound
     * @param key The key of the sound
     * @return True if all is ok, otherwise false
     */
    bool	pause(std::string const &key) const;
    /*!
     * @brief Set the volume of a sound
     * @param key The key of the sound
     * @param vol The volume between 0.0f and 1.0f
     * @return True if all is ok, otherwise false
     */
    bool	setVolume(std::string const &key, const float vol) const;
    /*!
     * @brief Set the volume for all sound
     * @param vol The volume between 0.0f and 1.0f
     * @return True if all is ok, otherwise false
     */
    void	setAllVolume(const float vol) const;
    /*!
     * @brief Stop a sound
     * @param key The key of the sound
     * @return True if all is ok, otherwise false
     */
    bool	stop(std::string const &key) const;

    /*!
     * @brief Stop all sounds
     */
    void	stopAllSounds(void) const;


    /*!
     * @brief Load a sound dynamically without passing by the phase add/delEntry.
     * It is used when we want to add entry after having called loadSounds method.
     * @param key The key of the sound
     * @param path The path of the sound
     * @param type The type of the sound (a music or a sound)
     */
    void	loadSoundDynamic(std::string const &key, std::string const &path,
				 eTypeSound const type);

  private:
    sound_entry_list_t		_soundList; /*!< List of all sounds entries */
    sound_load_list_t		_loadedSound; /*!< list of loaded sounds */

    FMOD_SYSTEM			*_system; /*!< The FMOD system */

  public:
    /*!
     * @brief Constructor by copy, not implemented
     * @param copy The copy
     */
    LoadSound(LoadSound const &copy);
    /*!
     * @brief Override operator=
     * @param copy The copy
     * @return The class
     */
    LoadSound &operator=(LoadSound const &copy);

  };

}

#endif

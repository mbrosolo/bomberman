//
// LoadTexture.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Fri May 11 21:05:58 2012 benjamin bourdin
// Last update Sun Jun  3 20:37:31 2012 benjamin bourdin
//

#include	<vector>
#include	<algorithm>
#include	<iostream>
#include	"LoadTexture.hh"
#include	"ExceptionLoading.hh"

// fct for the match of what key we remove
namespace loadTexture_utils
{
  bool	_matchKeyEntry(graphic::LoadTexture::texture_entry_assoc_t const &p, std::string const &k)
  {
    if (p.first == k)
      return true;
    return false;
  }

  bool	_matchKeyLoaded(graphic::LoadTexture::texture_load_assoc_t const &p, std::string const &k)
  {
    if (p.first == k)
      return true;
    return false;
  }

  bool	_matchTextureLoaded(graphic::LoadTexture::texture_load_assoc_t const &p, gdl::Image const *t)
  {
    if (p.second == t)
      return true;
    return false;
  }

  void	_printAll(graphic::LoadTexture::texture_entry_assoc_t const &p)
  {
    std::cout << "Key=[" << p.first << "] __ Path_file=[" << p.second << "]" << std::endl;
  }

  void	_rmAllLoaded(graphic::LoadTexture::texture_load_assoc_t &obj)
  {
    if (obj.second)
      delete obj.second;
    obj.second = NULL;
  }
}


graphic::LoadTexture::LoadTexture()
  : _texList(), _loadedTexture(), _defaultNoMatch("")
{
}

graphic::LoadTexture::~LoadTexture()
{
  std::for_each(_loadedTexture.begin(), _loadedTexture.end(), &loadTexture_utils::_rmAllLoaded);
}


void	graphic::LoadTexture::addEntry(std::string const &key, std::string const &file)
{
  std::vector<std::string> s(1, key);
  texture_entry_list_t::iterator it;
  if ((it = std::search(_texList.begin(), _texList.end(),
				       s.begin(), s.end(), &loadTexture_utils::_matchKeyEntry))
      == _texList.end())
    { // not found, it's ok
      _texList.push_back(texture_entry_assoc_t(key, file));
    }
  else // otherwise we modify the file
    {
      it->second = file;
    }
}


bool	graphic::LoadTexture::delEntry(std::string const &key)
{
  std::vector<std::string> s(1, key);
  texture_entry_list_t::iterator it;
  if ((it = std::search(_texList.begin(), _texList.end(),
			s.begin(), s.end(), &loadTexture_utils::_matchKeyEntry)) != _texList.end())
    {
      _texList.erase(it);
      return true;
    }
  return false;
}

void	graphic::LoadTexture::infos(void) const
{
  std::for_each(_texList.begin(), _texList.end(), &loadTexture_utils::_printAll);
}

void	graphic::LoadTexture::loadTextures(void)
{
  for (texture_entry_list_t::const_iterator it = _texList.begin(); it != _texList.end(); ++it)
    {
      try {
	_loadedTexture.push_back(texture_load_assoc_t(it->first, new gdl::Image(gdl::Image::load(it->second))));
      }
      catch (...) {
	throw ExceptionLoading("Cannot load texture "+it->second, __FILE__+std::string(", loadTextures"));
      }
    }
}

bool	graphic::LoadTexture::isValidKeyLoaded(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  texture_load_list_t::const_iterator it;
  if ((it = std::search(_loadedTexture.begin(), _loadedTexture.end(),
			s.begin(), s.end(), &loadTexture_utils::_matchKeyLoaded)) != _loadedTexture.end())
    {
      return true;
    }
  return false;
}

gdl::Image*	graphic::LoadTexture::getTexture(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  texture_load_list_t::const_iterator it;
  if ((it = std::search(_loadedTexture.begin(), _loadedTexture.end(),
			s.begin(), s.end(), &loadTexture_utils::_matchKeyLoaded)) != _loadedTexture.end())
    {
      return it->second;
    }
  return NULL;
}

void	graphic::LoadTexture::loadTextureDynamic(std::string const &key, std::string const &file)
{
  std::vector<std::string> s(1, key);
  texture_load_list_t::const_iterator it;
  if ((it = std::search(_loadedTexture.begin(), _loadedTexture.end(),
			s.begin(), s.end(), &loadTexture_utils::_matchKeyLoaded))
      == _loadedTexture.end())
    { // not found, it's ok
      try {
	_loadedTexture.push_back(texture_load_assoc_t(key, new gdl::Image(gdl::Image::load(file))));
      }
      catch (...) {
	throw ExceptionLoading("Cannot load texture "+file, __FILE__+std::string(", loadTextureDynamic "));
      }
    }
}

std::string const &	graphic::LoadTexture::getKey(gdl::Image const *t) const
{
  std::vector<gdl::Image const *> s(1, t);
  texture_load_list_t::const_iterator it;
  if ((it = std::search(_loadedTexture.begin(), _loadedTexture.end(),
			s.begin(), s.end(), &loadTexture_utils::_matchTextureLoaded)) != _loadedTexture.end())
    {
      return it->first;
    }
  return _defaultNoMatch;
}

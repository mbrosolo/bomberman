//
// LoadModel.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sat May 12 10:11:52 2012 benjamin bourdin
// Last update Sun Jun  3 20:38:33 2012 benjamin bourdin
//

#include	<vector>
#include	<algorithm>
#include	<iostream>
#include	"LoadModel.hh"
#include	"ExceptionLoading.hh"


// fct for the match of what key we remove
namespace loadModel_utils
{
  bool	_matchKeyEntry(graphic::LoadModel::model_entry_assoc_t const &p, std::string const &k)
  {
    if (p.first == k)
      return true;
    return false;
  }

  bool	_matchKeyLoaded(graphic::LoadModel::model_load_assoc_t const &p, std::string const &k)
  {
    if (p.first == k)
      return true;
    return false;
  }

  void	_printAll(graphic::LoadModel::model_entry_assoc_t const &p)
  {
    std::cout << "Key=[" << p.first << "] __ Path_file=[" << p.second << "]" << std::endl;
  }

  void	_rmAllLoaded(graphic::LoadModel::model_load_assoc_t &obj)
  {
    delete obj.second;
  }
}

graphic::LoadModel::LoadModel()
  : _texList(), _loadedModel()
{
}

graphic::LoadModel::~LoadModel()
{
  std::for_each(_loadedModel.begin(), _loadedModel.end(), &loadModel_utils::_rmAllLoaded);
}


void	graphic::LoadModel::addEntry(std::string const &key, std::string const &file)
{
  std::vector<std::string> s(1, key);
  model_entry_list_t::iterator it;
  if ((it = std::search(_texList.begin(), _texList.end(),
				       s.begin(), s.end(), &loadModel_utils::_matchKeyEntry))
      == _texList.end())
    { // not found, it's ok
      _texList.push_back(model_entry_assoc_t(key, file));
    }
  else // otherwise we modify the file
    {
      it->second = file;
    }
}


bool	graphic::LoadModel::delEntry(std::string const &key)
{
  std::vector<std::string> s(1, key);
  model_entry_list_t::iterator it;
  if ((it = std::search(_texList.begin(), _texList.end(),
			s.begin(), s.end(), &loadModel_utils::_matchKeyEntry)) != _texList.end())
    {
      _texList.erase(it);
      return true;
    }
  return false;
}

void	graphic::LoadModel::infos(void) const
{
  std::for_each(_texList.begin(), _texList.end(), &loadModel_utils::_printAll);
}

void	graphic::LoadModel::loadModels(void)
{
  for (model_entry_list_t::const_iterator it = _texList.begin(); it != _texList.end(); ++it)
    {
      try {
	_loadedModel.push_back(model_load_assoc_t(it->first, new gdl::Model(gdl::Model::load(it->second))));
      }
      catch (...) {
	throw ExceptionLoading("Cannot load model"+it->second, __FILE__+std::string(", loadModels, "));
      }
    }
}

bool	graphic::LoadModel::isValidKeyLoaded(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  model_load_list_t::const_iterator it;
  if ((it = std::search(_loadedModel.begin(), _loadedModel.end(),
			s.begin(), s.end(), &loadModel_utils::_matchKeyLoaded)) != _loadedModel.end())
    {
      return true;
    }
  return false;
}

gdl::Model*	graphic::LoadModel::getModel(std::string const &key) const
{
  std::vector<std::string> s(1, key);
  model_load_list_t::const_iterator it;
  if ((it = std::search(_loadedModel.begin(), _loadedModel.end(),
			s.begin(), s.end(), &loadModel_utils::_matchKeyLoaded)) != _loadedModel.end())
    {
      return it->second;
    }
  return NULL;
}

void	graphic::LoadModel::loadModelDynamic(std::string const &key, std::string const &file)
{
  std::vector<std::string> s(1, key);
  model_load_list_t::const_iterator it;
  if ((it = std::search(_loadedModel.begin(), _loadedModel.end(),
			s.begin(), s.end(), &loadModel_utils::_matchKeyLoaded))
      == _loadedModel.end())
    { // not found, it's ok
      try {
	_loadedModel.push_back(model_load_assoc_t(key, new gdl::Model(gdl::Model::load(file))));
      }
      catch (...) {
	throw ExceptionLoading("Cannot load model"+file, __FILE__+std::string(", loadModelDynamic, "));
      }
    }
}

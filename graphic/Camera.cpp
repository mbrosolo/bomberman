//
// Camera.cpp for Camera in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Mon May  7 11:48:23 2012 tosoni
// Last update Sun Jun  3 20:58:40 2012 benjamin bourdin
//

#include	<GL/gl.h>
#include	<GL/glu.h>
#include	"Camera.hh"
#include	"MyGame.hh"

graphic::Camera::Camera(void)
  : position_(XMENU_POS, YMENU_POS, ZMENU_POS),
    rotation_(XMENU_ROT, YMENU_ROT, ZMENU_ROT),
    _lx(0.0f),
    _lz(0.0f)
{
}

graphic::Camera::~Camera()
{
}

void graphic::Camera::initialize(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glViewport(0, 0, MyGame::WINDOW_WIDTH, MyGame::WINDOW_HEIGHT);
  gluPerspective(60.0f, MyGame::WINDOW_WIDTH / MyGame::WINDOW_HEIGHT, 1.0f, 10000.0f);
  gluLookAt(position_.first, position_.second, position_.third,
	    position_.first + _lx, position_.second + _lz, -1.0f,
	    0.0f, 1.0f, 0.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

void	graphic::Camera::incPosX(const float inc)
{
  position_.first += inc;
}

void	graphic::Camera::incPosY(const float inc)
{
  position_.second += inc;
}

void	graphic::Camera::incPosZ(const float inc)
{
  position_.third += inc;
}

void	graphic::Camera::incLx(const float inc)
{
  _lx += inc;
}

void	graphic::Camera::incLz(const float inc)
{
  _lz += inc;
}

void	graphic::Camera::setPos(Vector const &pos)
{
  position_ = pos;
}

graphic::Vector const &	graphic::Camera::getPos(void) const
{
  return position_;
}

void graphic::Camera::update(MyClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(60.0f, MyGame::WINDOW_WIDTH / MyGame::WINDOW_HEIGHT, 1.0f, 10000.0f);
  gluLookAt(position_.first, position_.second, position_.third,
	    position_.first + _lx, position_.second + _lz, -1.0f,
	    0.0f, 0.1f, 0.f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

void    graphic::Camera::switchToOrtho()
{
  glDisable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0, MyGame::WINDOW_WIDTH, MyGame::WINDOW_HEIGHT, 0, -MyGame::WINDOW_WIDTH, MyGame::WINDOW_WIDTH);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


void    graphic::Camera::switchBackToPerspective()
{
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}

void	graphic::Camera::resetToMenu(void)
{
  position_.first = XMENU_POS;
  position_.second = YMENU_POS;
  position_.third = ZMENU_POS;

  rotation_.first = XMENU_ROT;
  rotation_.second = YMENU_ROT;
  rotation_.third = ZMENU_ROT;
}

void	graphic::Camera::resetToGame(void)
{
  position_.first = XGAME_POS;
  position_.second = YGAME_POS;
  position_.third = ZGAME_POS;

  rotation_.first = XGAME_ROT;
  rotation_.second = YGAME_ROT;
  rotation_.third = ZGAME_ROT;
}

//
// Files.hh for bomberman in /home/tosoni_t/BitBucket/bomberman/graphic
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Thu May 31 17:06:37 2012 tosoni_t
// Last update Sun Jun  3 21:02:50 2012 benjamin bourdin
//

#ifndef		__FILES_HH__
#define		__FILES_HH__

/**
 * @author tosoni_t
 * @file Files.hh
 */

#include	<string>
#include	<iostream>
#include	"Text.hh"
#include	"AForm.hh"

/*!
 * @class Files
 * @brief Files for graphic part
 */
class			Files
{
  std::string		_name; /*!< Name of the files */
  graphic::Text*	_nameText; /*!< Name for the graphic */
  graphic::AForm*	_background; /*!< Background */
  graphic::AForm*	_selected; /*!< Form if selected */
  float			_fileNb; /*!< Number of files */
  int			_fixedNb; /*!< Fixed */
  bool			_validate; /*!< If the files are validated or not */

public:
  /*!
   * @brief Constructor for Files
   * @param
   * @param
   * @param
   * @param
   */
  Files(std::string const &, graphic::AForm *, graphic::AForm *, const float, const float);
  /*!
   * @brief Destructor for Files
   */
  ~Files();
  /*!
   * @brief Copy Constructor for Files
   */
  Files(Files const &);
  /*!
   * @brief Assignement operator for Files
   * @param
   * @return
   */
  Files &	operator=(Files const &);

  /*!
   * @brief Getter for the Files name
   * @return
   */
  std::string const &	getName() const;
  /*!
   * @brief Getter for the Files number
   * @return
   */
  float			getFileNb() const;
  /*!
   * @brief Getter for the Files background
   * @return
   */
  graphic::AForm&	getBackground() const;
  /*!
   * @brief Getter for the Files form if selected
   * @return
   */
  graphic::AForm&	getSelected() const;

  /*!
   * @brief Setter for the name
   * @param
   */
  void			setName(std::string const &);
  /*!
   * @brief Setter for the Number of Files
   * @param
   */
  void			setFileNb(const float);
  /*!
   * @brief Setter for the Validate
   * @param
   */
  void			setValidate(const bool);
  /*!
   * @brief For changing the Number of Files
   * @param
   */
  void			changeFileNb(const float);

  /*!
   * @brief Draws the background
   */
  void			drawBackground(void) const;
  /*!
   * @brief Draws the selection image
   */
  void			drawSelected(void) const;
  /*!
   * @brief Draws the text
   */
  void			drawText(void) const;

  /*!
   * @brief Getter for Validate
   * @return
   */
  bool			getValidate(void) const;
  /*!
   * @brief Getter for Fixed Number
   * @return
   */
  int			getFixedNb(void) const;

  /*!
   * @brief Changes the position of the text
   * @param
   * @param
   * @param
   */
  void			changePosition(const float x, const float y, const float z);
};


#endif

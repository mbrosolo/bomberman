//
// MenuCamera.cpp for bomberman in /home/tosoni_t/BitBucket/bomberman
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Sat Jun  2 23:01:21 2012 tosoni_t
// Last update Sun Jun  3 13:05:14 2012 tosoni_t
//

#include	"MyGame.hh"
#include	"MenuCamera.hh"

MenuCamera::MenuCamera()
  : _futureP(0, 0, 0),
    _xPositionOK(true),
    _yPositionOK(true),
    _zPositionOK(true),
    _velocity(10)
{
}

MenuCamera::~MenuCamera()
{
}

void		MenuCamera::moveXPosition(graphic::Vector & v)
{
  if (v.first >= (_futureP.first - _velocity) && v.first <= (_futureP.first + _velocity))
    {
      _xPositionOK = true;
      return;
    }
  if (v.first < _futureP.first)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosX(_velocity);
  else if (v.first > _futureP.first)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosX(-_velocity);
}

void		MenuCamera::moveYPosition(graphic::Vector & v)
{
  if (v.second >= (_futureP.second - _velocity) && v.second <= (_futureP.second + _velocity))
    {
      _yPositionOK = true;
      return;
    }
  if (v.second < _futureP.second)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosY(_velocity);
  else if (v.second > _futureP.second)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosY(-_velocity);
}

void		MenuCamera::moveZPosition(graphic::Vector & v)
{
  if (v.third >= (_futureP.third - _velocity) && v.third <= (_futureP.third + _velocity))
    {
      _zPositionOK = true;
      return;
    }
  if (v.third < _futureP.third)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosZ(_velocity);
  else if (v.third > _futureP.third)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosZ(-_velocity);
}

void		MenuCamera::moveCamera(void)
{
  graphic::Vector	v = graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->getPos();

  if (!_xPositionOK)
    moveXPosition(v);
  if (!_yPositionOK)
    moveYPosition(v);
  if (!_zPositionOK)
    moveZPosition(v);
}

void		MenuCamera::setPositionOK(bool tf, int v)
{
  _xPositionOK = tf;
  _yPositionOK = tf;
  _zPositionOK = tf;
  _velocity = v;
}

bool		MenuCamera::getPositionOK(void) const
{
  if (_xPositionOK && _yPositionOK && _zPositionOK)
    return (true);
  return (false);
}

void		MenuCamera::setFutureCameraPosition(float posx, float posy, float posz)
{
  _futureP.first = posx;
  _futureP.second = posy;
  _futureP.third = posz;
}

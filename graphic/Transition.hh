//
// Transition.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 30 21:06:48 2012 benjamin bourdin
// Last update Sun Jun  3 22:02:31 2012 benjamin bourdin
//

#ifndef __TRANSITION_HH__
#define __TRANSITION_HH__

/**
 * @author benjamin bourdin
 * @file Transiion.hh
 */

#include	<vector>
#include	"Input.hpp"
#include	"MyClock.hh"
#include	"AForm.hh"

namespace graphic
{
  /*!
   * @class Transition
   * @brief Define the transition
   */
  class Transition
  {
  public:
    /*!
     * @brief Constructor of Transition
     */
    Transition();
    /*!
     * @brief Destructor of Transition
     */
    ~Transition();
    /*!
     * @brief Constructor by copy of Transition
     * @param copy The copy
     */
    Transition(Transition const &);
    /*!
     * @brief operator= override of Transition
     * @param copy The copy
     * @return The class
     */
    Transition &operator=(Transition const &);

    /*!
     * @brief Initialize method of Transition
     */
    void	initialize(void);
    /*!
     * @brief Update method of Transition
     * @param clock The clock of the game
     * @param input The input
     */
    void	update(MyClock const &, gdl::Input &);
    /*!
     * @brief Draw method of Transition
     */
    void	draw(void) const;

    /*!
     * @brief IsFinished method of Transition
     * @return flag of accomplishment
     */
    bool	isFinished(void) const;

  private:
    /*!
     * @brief Typedef of form's list
     */
    typedef std::list<AForm *>	list_t;

    AForm	*_one; /*!< The form of one */
    AForm	*_two; /*!< The form of two */
    AForm	*_three; /*!< The form of three */
    AForm	*_go; /*!< The form of go position coordinates */
    list_t	_content; /*!< The list of form */
    list_t::iterator	_it; /*!< The iterator of the list */

    float	_step; /*!< The step */

  };
}

#endif

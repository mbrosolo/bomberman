//
// GameLoader.hh for bomberman in /home/tosoni_t/BitBucket/bomberman/graphic
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Thu May 31 16:37:21 2012 tosoni_t
// Last update Sun Jun  3 18:59:19 2012 benjamin bourdin
//

#ifndef		__GAMELOADER_HH__
#define		__GAMELOADER_HH__

/**
 * @author benjamin bourdin
 * @file GameLoader.hh
 */

#include	<sys/types.h>
#include	<dirent.h>
#include	<errno.h>
#include	<cstdio>
#include	"ExceptionRuntime.hh"
#include	"AForm.hh"
#include	"MyGame.hh"
#include	"Files.hh"
#include	"Option.hh"

/*!
 * @brief Typedef for Options list
 */
typedef	std::list<Option*>	OptionList_t;
/*!
 * @brief Typedef for Files list
 */
typedef	std::list<Files*>	Files_t;

/*!
 * @class GameLoader
 * @brief Define the Game loader
 */
class		GameLoader
{
  Files_t	_maps; /*!< The list of files */
  Files_t	_saves; /*!< The list of saves */
  Files_t	_songs; /*!< The list of songs */
  OptionList_t	_ia; /*!< The Options' list */

public:
  /*!
   * @brief Constructor of GameLoader
   */
  GameLoader();
  /*!
   * @brief Destructor of GameLoader
   */
  ~GameLoader();

  /*!
   * @brief MakeSquare method of GameLoader
   * @param x position x
   * @param y position y
   * @param z position z
   * @param bg background
   */
  graphic::AForm*		makeSquare(int x, int y, int z,
					   std::string const & bg);
  /*!
   * @brief MakeRectangle method of GameLoader
   * @param x position x
   * @param y position y
   * @param z position z
   * @param bg background
   */
  graphic::AForm*		makeRectangle(float x, float y, float z,
					      std::string const & bg);

  /*!
   * @brief loadSaves method of GameLoader
   */
  void		loadSaves(void);
  /*!
   * @brief loadMaps method of GameLoader
   */
  void		loadMaps(void);
  /*!
   * @brief loadMusic method of GameLoader
   */
  void		loadMusic(void);
  /*!
   * @brief InitIA method of GameLoader
   */
  void		initIA(void);

  /*!
   * @brief getSaves method of GameLoader
   */
  Files_t&	getSaves(void);
  /*!
   * @brief getMaps method of GameLoader
   */
  Files_t&	getMaps(void);
  /*!
   * @brief getMusic method of GameLoader
   */
  Files_t&	getMusic(void);
  /*!
   * @brief getIA method of GameLoader
   */
  OptionList_t&	getIA(void);
};

#endif

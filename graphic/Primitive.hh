////////////////////////////////////////////////////////////////////////////////
/// Declaration des classes Triangle et Rectangle
////////////////////////////////////////////////////////////////////////////////

#ifndef __PRIMITIVE_HH__
#define __PRIMITIVE_HH__

/**
 * @author ?
 * @file Primitive.hh
 */

#include	<GL/gl.h>
#include	"AForm.hh"

namespace graphic
{
  namespace Primitive
  {
    /*!
     * @class Triangle
     * @brief Define the triangle
     */
    class Triangle : public AForm
    {
    public:
      /*!
       * @brief Constructor of Triangle 
       * @param pos The vector position of Triangle
       * @param rot The vector rotation of Triangle
       * @param color The color data of Triangle
       * @param scale The scale of Triangle
       */
      Triangle(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	       Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	       Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	       Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
      /*!
       * @brief Destructor of Triangle 
       */
      ~Triangle();
      /*!
       * @brief Constructor by copy of Triangle 
       * @param cp The triangle to copy
       */
      Triangle(Triangle const &);
      /*!
       * @brief operator= override of Triangle 
       * @param cp The triangle to copy
       * @return the result of copy
       */
      Triangle &operator=(Triangle const &);

      /*!
       * @brief Initialize method of Triangle 
       */
      void	initialize(void);
      /*!
       * @brief Update method of Triangle 
       * @param clock The clock of the game
       * @param input The input
       */
      void	update(MyClock const &, gdl::Input &);
      /*!
       * @brief Draw method of Triangle 
       */
      void	draw(void);

      /*!
       * @brief GetWidth method of Triangle 
       * @param The height
       */
      float	getWidth(void) const;
      /*!
       * @brief GetHeight method of Triangle 
       * @param The height
       */
      float	getHeight(void) const;
      /*!
       * @brief GetDepth method of Triangle 
       * @param The depth
       */
      float	getDepth(void) const;

    private:
      const float	_width; /*!< The width of the triangle */
      const float	_height; /*!< The height of the triangle */
      const float	_depth; /*!< The depth of the triangle */

    };

    /*!
     * @class Rectangle
     * @brief Define the Rectangle
     */
    class Rectangle : public AForm
    {
    public:
      /*!
       * @brief Constructor of Rectangle 
       * @param pos The vector position of Rectangle
       * @param rot The vector rotation of Rectangle
       * @param color The color data of Rectangle
       * @param scale The scale of Rectangle
       */
      Rectangle(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	       Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	       Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	       Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
      /*!
       * @brief Destructor of Rectangle 
       */
      ~Rectangle();
      /*!
       * @brief Constructor by copy of Rectangle 
       * @param cp The triangle to copy
       */
      Rectangle(Rectangle const &);
      /*!
       * @brief operator= override of Rectangle 
       * @param cp The triangle to copy
       * @return the result of copy
       */
      Rectangle &operator=(Rectangle const &);

      /*!
       * @brief Initialize method of Rectangle 
       */
      void initialize(void);
      /*!
       * @brief Update method of Rectangle 
       * @param clock The clock of the game
       * @param input The input
       */
      void update(MyClock const &, gdl::Input &);
      /*!
       * @brief Draw method of Rectangle 
       */
      void draw(void);

      /*!
       * @brief GetWidth method of Rectangle 
       * @param The height
       */
      float	getWidth(void) const;
      /*!
       * @brief GetHeight method of Rectangle 
       * @param The height
       */
      float	getHeight(void) const;
      /*!
       * @brief GetDepth method of Rectangle 
       * @param The depth
       */
      float	getDepth(void) const;

    private:
      const float	_width; /*!< The width of the rectangle */
      const float	_height; /*!< The height of the rectangle */
      const float	_depth; /*!< The depth of the rectangle */

    };
  }
}

#endif

//
// Video.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sat May 19 14:27:40 2012 benjamin bourdin
// Last update Sun Jun  3 22:01:03 2012 benjamin bourdin
//

#ifndef __VIDEO_HH__
#define __VIDEO_HH__

/**
 * @author bourdi_b
 * @file Video.hh
 */

#include	<string>
#include	<GL/gl.h>
#include	<GL/glu.h>
#include	"cv.h"
#include	"highgui.h"
#include	"MyClock.hh"

namespace graphic
{
  /*!
   * class Video
   * @brief The video class
   */
  class Video
  {
  public:
    /*!
     * @brief Constructor of Video
     * @param file The file to load
     */
    explicit Video(std::string const &file);
    /*!
     * @brief Destructor of Video
     */
    ~Video();
    /*!
     * @brief Contructor by copy of Video
     * @param copy The copy to do
     */
    Video(Video const &copy);
    /*!
     * @brief Override operator=
     * @param copy The copy to do
     * @return The class
     */
    Video &operator=(Video const &copy);

    /*!
     * @brief Capture one image from the video
     * To call in update function
     * @param clock The clock in order to manage well FPS
     */
    bool	captureImg(MyClock const &clock);
    /*!
     * @brief Load an image captured with captureImg()
     * The image is transformed to a texture and is already binded
     */
    void	loadImg(void) const;
    /*!
     * @brief Indicate if the video is finished or not.
     * The flag is always true as soon as the first loop is done
     * If you want to do 2 loops, reset the flag the first time
     * (at the end of the first loop), and so on the
     * next time it will be set to true for the end of the second loop.
     * @return True if it's finished, false otherwise
     */
    bool	isFinished(void) const;
    /*!
     * @brief Set the flag of finish
     * @param val The value of flag to set
     */
    void	setFinished(const bool val);

  private:
    CvCapture	*_capture; /*!< The video loaded */
    IplImage	*_img; /*!< The image from the video at a moment T */
    int		_fps; /*!< The number of fps from the video */
    GLuint	_texture; /*!< The texture made from the image */
    std::string	_file; /*!< The file of the video */
    bool	_isFinished; /*!< Indicate if the video readding is finished */
    int		_curFPS; /*!< The current FPS number */

  };
}

#endif

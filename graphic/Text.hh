//
// Text.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sun May 13 21:27:30 2012 benjamin bourdin
// Last update Sun Jun  3 22:05:51 2012 benjamin bourdin
//

#ifndef __TEXT_HH__
#define __TEXT_HH__

/**
 * @author bourdi_b
 * @file Text.hh
 */

#include	<string>
#include	<GL/gl.h>
#include	"AForm.hh"

namespace graphic
{

  /*!
   * @class Text
   * @brief The text AForm, contain a string
   */
  class Text : public AForm
  {
  public:
    /*!
     * @brief Contructor of Text
     * @param pos The position
     * @param rot The rotation
     * @param col The color
     * @param sc The scale
     */
    Text(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	   Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	   Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	   Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
    /*!
     * @brief The destructor of Text
     */
    ~Text();
    /*!
     * @brief The Contructor by copy
     * @param copy The copy
     */
    Text(Text const &copy);
    /*!
     * @brief The operator= override
     * @param copy The copy
     * @return The Text
     */
    Text &operator=(Text const &copy);

    /*!
     * @brief Initialize Text context
     */
    void	initialize(void);
    /*!
     * @brief Update method of Text
     * @param clock The clock of the game
     * @param input The input
     */
    void	update(MyClock const &clock, gdl::Input &input);
    /*!
     * @brief Draw method of Text
     */
    void	draw(void);

    /*!
     * @brief Get the width of Text
     * @return The width of Text
     */
    float	getWidth(void) const;
    /*!
     * @brief Get the height of Text
     * @return The height of Text
     */
    float	getHeight(void) const;
    /*!
     * @brief Get the depth of Text
     * @return The depth of Text
     */
    float	getDepth(void) const;

    /*!
     * @brief Set the string to print in update
     * @param str The string to print
     */
    void	updateText(std::string const &str);
    /*!
     * @brief Return the string updated in the class
     * @return Th string
     */
    std::string const & getString(void) const;

  private:
    std::string	_str; /*!< The string to print*/

    const float	_width; /*!< The width of Text */
    const float	_height; /*!< The height of Text */
    const float	_depth; /*!< The depth of Text */

  };

}

#endif

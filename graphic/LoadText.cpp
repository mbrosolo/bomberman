//
// LoadText.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sun May 13 22:02:34 2012 benjamin bourdin
// Last update Sun Jun  3 20:33:00 2012 benjamin bourdin
//

#include	"LoadText.hh"
#include <iostream>
graphic::LoadText	*graphic::LoadText::_instance = NULL;

graphic::LoadText::LoadText()
  : _x0(0.0f), _x1(0.0f), _y0(0.0f), _y1(0.0f), _corresp(), _width(0.061f), _height(0.065f)
{
  float x0 = 0.01f, y0 = 0.0f;
  char tmp = 0;
  for (char c = 0; c < 127; ++c)
    {
      tmp ++;
      if (tmp == 16)
	{
	  tmp = 0;
	  x0 = 0.01f;
	  y0 += _height;
	}
      _corresp[c].push_back(x0 + 0.004f);
      _corresp[c].push_back(x0 + 0.065f);
      _corresp[c].push_back(y0);
      _corresp[c].push_back(y0 + 0.04f);
      x0 += _width;
    }

  for (text_coord_t::const_iterator it = _corresp.begin(); it != _corresp.end(); ++it)
    {
      _x0 = it->second[0];
      _x1 = it->second[1];
      _y0 = it->second[2];
      _y1 = it->second[3];
    }
}

graphic::LoadText::~LoadText()
{
}

graphic::LoadText	*graphic::LoadText::getInstance(void)
{
  if (_instance == NULL)
    _instance = new LoadText();
  return _instance;
}

void	graphic::LoadText::killInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}


float	graphic::LoadText::getWidth(void) const
{
  return _width;
}

float	graphic::LoadText::getHeight(void) const
{
  return _height;
}

float	graphic::LoadText::getX0(void) const
{
  return _x0;
}

float	graphic::LoadText::getX1(void) const
{
  return _x1;
}

float	graphic::LoadText::getY0(void) const
{
  return _y0;
}

float	graphic::LoadText::getY1(void) const
{
  return _y1;
}

void	graphic::LoadText::loadCharacter(char const c)
{
  text_coord_t::const_iterator it = _corresp.find(c);
  if (it != _corresp.end())
    {
      _x0 = it->second[0];
      _x1 = it->second[1];
      _y0 = it->second[2];
      _y1 = it->second[3];
    }
}

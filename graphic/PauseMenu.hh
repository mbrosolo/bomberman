//
// PauseMenu.hh for bomberman in /home/tosoni/BitBucket/bomberman
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Tue May 22 15:24:32 2012 tosoni
// Last update Sun Jun  3 22:27:35 2012 benjamin bourdin
//

#ifndef		__PAUSEMENU_HH__
#define		__PAUSEMENU_HH__

/**
 * @author thomas tosoni
 * @file Primitive.hh
 */

#include	<list>
#include	<string>
#include	"Option.hh"
#include	"MyGame.hh"

class		Option;

typedef	std::list<Option*>	OptionList_t;

/*!
 * @class PauseMenu
 * @brief Define the Game loader
 */
class		PauseMenu
{
public:
  /*!
   * @brief getInstance method of PauseMenu 
   * @return The instance of PauseMenu
   */
  static PauseMenu	*getInstance(void);
  /*!
   * @brief killInstance method of PauseMenu 
   */
  static void		killInstance(void);
  /*!
   * @brief display method of PauseMenu 
   */
  void			display(void);
  /*!
   * @brief addOption method of PauseMenu 
   * @param _s The name of the option
   * @param Form The form of the option
   * @param v The value of the option
   */
  void			addOption(std::string const & _s, graphic::AForm *Form, float v);
  /*!
   * @brief getOptions method of PauseMenu 
   * @return The list of option
   */
  OptionList_t&		getOptions(void) const;
  /*!
   * @brief makeRectangle method of PauseMenu 
   * @param x The position x
   * @param y The position y
   * @param z The position z
   * @param xs The position xs
   * @param ys The position ys
   * @param zsd The position zd
   * @param texture The name of the texture
   */
  graphic::AForm*	makeRectangle(const float x, const float y, const float z,
				      const float xs, const float ys, const float zd,
				      std::string const & texture);
  /*!
   * @brief initalize method of PauseMenu 
   */
  void			initialize();

  /*!
   * @brief resume method of PauseMenu 
   */
  void		        resume();
  /*!
   * @brief restart method of PauseMenu 
   */
  void		        restart();
  /*!
   * @brief save method of PauseMenu 
   */
  void		        save();
  /*!
   * @brief volume method of PauseMenu 
   */
  void		        volume();
  /*!
   * @brief quit method of PauseMenu 
   */
  void		        quit();

private:
  OptionList_t		*_options; /*!< The list of files options */
  graphic::AForm	*_background; /*!< The background */
  static PauseMenu	*_instance; /*!< The instance of PauseMenu */

private:
  /*!
   * @brief Constructor of PauseMenu
   */
  PauseMenu();
  /*!
   * @brief Destructor of PauseMenu
   */
  ~PauseMenu();
  PauseMenu(PauseMenu const &);
  PauseMenu & operator=(PauseMenu const &);
};

#endif

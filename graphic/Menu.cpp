//
// Menu.cpp for Menu in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 17:48:27 2012 tosoni
// Last update Sun Jun  3 22:06:58 2012 tosoni_t
//

#include	"Menu.hh"
#include	"ExceptionCore.hh"
#include	"Save.hh"
#include	"Core.hh"
#include	"Video.hh"

Menu		*Menu::_instance = NULL;
typedef	std::list<SubMenu*>	SubMenus_t;
typedef	std::list<Option*>	OptionList_t;

Menu::Menu()
  : _hmenus(),
    _menuEventManager(MenuEventManager::getInstance()),
    _menuCamera(new MenuCamera()),
    _ptr(),
    _keyboardMap(),
    _optionsMap(),
    _pauseMap(),
    _player(1),
    _volume(5),
    _gameLoader(new GameLoader()),
    _mapName("Unknown"),
    _musicName("Unknown"),
    _optionSelector(0),
    _iaType("Normal"),
    _xmap(20),
    _ymap(20),
    _rounds(1),
    _ai(1),
    _dI(0),
    _lastPos(0),
    _framePos(0)
{
  _keyboardMap["Up"] = &AObject::moveUp;
  _keyboardMap["Down"] = &AObject::moveDown;
  _keyboardMap["Left"] = &AObject::moveLeft;
  _keyboardMap["Right"] = &AObject::moveRight;
  _keyboardMap["Leave"] = &AObject::leave;
  _optionsMap["One Player"] = &Menu::onePlayer;
  _optionsMap["Two Players"] = &Menu::twoPlayers;
  _optionsMap["Load"] = &Menu::load;
  _optionsMap["Exit"] = &Menu::exit;
  _optionsMap["Volume"] = &Menu::volume;
  _optionsMap["Maps"] = &Menu::maps;
  _optionsMap["Music"] = &Menu::music;
  _optionsMap["IA"] = &Menu::IA;
  _optionsMap["X:20"] = &Menu::lockX;
  _optionsMap["Y:20"] = &Menu::lockY;
  _optionsMap["Rounds:1"] = &Menu::lockRounds;
  _pauseMap["Resume"] = &PauseMenu::resume;
  _pauseMap["Restart"] = &PauseMenu::restart;
  _pauseMap["Save"] = &PauseMenu::save;
  _pauseMap["Volume"] = &PauseMenu::volume;
  _pauseMap["Quit"] = &PauseMenu::quit;
}

Menu::~Menu()
{
  delete _gameLoader;
  delete _menuCamera;
  _menuEventManager->killInstance();
}

Menu		*Menu::getInstance(void)
{
  if (_instance == NULL)
    _instance = new Menu();
  return (_instance);
}

void		Menu::killInstance(void)
{
  if (_instance != NULL)
    delete _instance;
  _instance = NULL;
}

SubMenus_t	Menu::getMenus() const
{
  return (_hmenus);
}

FUTUREPTR	Menu::getFuturePtr() const
{
  return (_ptr);
}

void		Menu::closeGame(float value)
{
  if (value)
    _menuEventManager->setClose(true);
}

void		Menu::setPlayer(int p)
{
  _player = p;
}

int		Menu::getPlayer(void) const
{
  return (_player);
}

OptionList_t*		Menu::getCurrentOptionList() const
{
 SubMenus_t		list;
  OptionList_t		options;

  list = getShownMenu()->getMenus();
  for (SubMenus_t::iterator it1 = list.begin(); it1 != list.end(); ++it1)
    {
      if ((*it1)->getMenuNb() == 1)
	return ((*it1)->getOptions());
    }
  return (NULL);
}

int	Menu::getRounds(void) const
{
  return _rounds;
}

void			Menu::updateX(float _val)
{
  OptionList_t		*options = getCurrentOptionList();
  std::ostringstream	oss;
  std::string		s("X:");

  oss << s;
  if (!_val)
    _xmap = 20;
  if ((_xmap + _val) <= 0)
    return;
  _xmap += _val;
  oss << static_cast<int>(_xmap);
  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1)
	(*it2)->getName().updateText(oss.str());
    }
}

void			Menu::updateY(float _val)
{
  OptionList_t		*options = getCurrentOptionList();
  std::ostringstream	oss;
  std::string		s("Y:");

  oss << s;
  if (!_val)
    _ymap = 20;
  if ((_ymap + _val) <= 0)
    return;
  _ymap += _val;
  oss << static_cast<int>(_ymap);
  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1)
	(*it2)->getName().updateText(oss.str());
    }
}

void			Menu::updateRounds(float _val)
{
  OptionList_t		*options = getCurrentOptionList();
  std::ostringstream	oss;
  std::string		s("Rounds:");

  oss << s;
  if (!_val)
    _rounds = 1;
  if ((_rounds + _val) <= 0)
    return;
  _rounds += _val;
  oss << static_cast<int>(_rounds);
  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1)
	(*it2)->getName().updateText(oss.str());
    }
}

void			Menu::updateVolume(float _side)
{
  OptionList_t		*options;
  std::ostringstream	oss;
  std::string		s("VOLUME");

  oss << s;
  if ((_volume + _side) <= 0 || (_volume + _side) > 11)
    return;
  if (_side > 0)
    _volume += 0.50f;
  else
    _volume -= 0.50f;
  graphic::MyGame::getInstance()->getSoundManager()->setAllVolume(_volume / 10.0f);
  oss << static_cast<int>(_volume);
  if (!(options = getCurrentOptionList()))
    return;
  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1) {
	(*it2)->getSelected().setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture(oss.str()));
	return;
      }
    }
}


void		Menu::volume(void)
{
  _menuEventManager->loadMapping(6);
}

void		Menu::lockMenu(float)
{
  SubMenu	*menu = getFirstShownMenu();
  graphic::Vector	v = menu->getBackground().getPosition();

  _menuCamera->setPositionOK(false, VELOCITY);
  _menuCamera->setFutureCameraPosition(v.first, v.second, v.third + CLOSE_CAM);
  _menuEventManager->loadMapping(2);
  getShownMenu()->setLocked(true);

  graphic::MyGame::getInstance()->getSoundManager()->play("SELECT");
}

void		Menu::unlockMenu(float)
{
  SubMenu	*_shown;
  SubMenus_t	list;
  OptionList_t	*options;
  float		i;

  _shown = getShownMenu();
  _shown->setLocked(false);
  _menuCamera->setPositionOK(false, VELOCITY);
  _menuEventManager->loadMapping(1);
  list = _shown->getMenus();
  for (SubMenus_t::iterator it1 = list.begin(); it1 != list.end(); ++it1)
    {
      i = 1;
      options = (*it1)->getOptions();
      if ((*it1)->getMenuNb() == 1)
	{
	  graphic::Vector	v = (*it1)->getBackground().getPosition();
	  _menuCamera->setFutureCameraPosition(v.first, v.second, v.third + FAR_CAM);
	}
      for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
	(*it2)->setOptionNb(i++);
    }
  graphic::MyGame::getInstance()->getSoundManager()->play("UNSELECT");
}

void		Menu::lockOption(float _useless)
{
  static_cast<void>(_useless);
  _menuEventManager->loadMapping(3);
  _menuEventManager->setIsMapping(true);
  _menuEventManager->setErazor(0);
}

void		Menu::erazeFromCurrentList(AObjectEventList_t & list)
{
  MenuEventManager	*evM = MenuEventManager::getInstance();

  for (AObjectEventList_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if (it->second == _ptr && evM->getKeyToEraze() != gdl::Keys::Delete)
	list.erase(it->first);
    }
}

void		Menu::erazeFromOtherList(AObjectEventList_t & list)
{
  MenuEventManager	*evM = MenuEventManager::getInstance();

  for (AObjectEventList_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if (it->first == evM->getKeyToEraze())
	list.erase(it->first);
    }
}

void		Menu::unlockOption(float)
{
  SubMenus_t		list;
  OptionList_t		*options;
  MenuEventManager	*evManager = MenuEventManager::getInstance();

  _menuEventManager->loadMapping(2);
  _menuEventManager->setIsMapping(false);
  list = this->getShownMenu()->getMenus();
  if (_player == 1)
    erazeFromCurrentList(*evManager->getPlayer1Mapping());
  else if (_player == 2)
    erazeFromCurrentList(*evManager->getPlayer2Mapping());
  if (evManager->getErazor() == 1)
    erazeFromOtherList(*evManager->getPlayer1Mapping());
  else if (evManager->getErazor() == 2)
    erazeFromOtherList(*evManager->getPlayer2Mapping());
  for (SubMenus_t::iterator it1 = list.begin(); it1 != list.end(); ++it1)
    {
      options = (*it1)->getOptions();
      for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
	{
	  if ((*it2)->getKey() == evManager->getKeyToEraze()
	      && ((evManager->getErazor() == 2 && (*it1)->getName().compare("Keyboard Options 2") == 0)
		  ||(evManager->getErazor() == 1 && (*it1)->getName().compare("Keyboard Options 1") == 0)))
	    (*it2)->setKeyText("NO KEY");
	  if ((*it2)->getOptionNb() == 1) {
	    if ((*it1)->getMenuNb() == 1 && evManager->getKeyToEraze() != gdl::Keys::Delete)
	      {
		(*it2)->setKey(evManager->getKeyToEraze());
		(*it2)->setKeyText(KeyManager::getInstance()->getStringKey((*it2)->getKey()));
	      }
	    (*it2)->getSelected().setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("LONG"));
	  }
	}
    }
  if (_player == 1 && evManager->getKeyToEraze() != gdl::Keys::Delete)
    (*_menuEventManager->getPlayer1Mapping())[evManager->getKeyToEraze()] = _ptr;
  else if (evManager->getKeyToEraze() != gdl::Keys::Delete)
    (*_menuEventManager->getPlayer2Mapping())[evManager->getKeyToEraze()] = _ptr;
  evManager->setKeyToEraze(gdl::Keys::Delete);
}

void		Menu::updateLocked(float _value)
{
  unlockMenu(0.f);
  updateHorizontalMenu(_value);
  lockMenu(_value);
}

void		Menu::displaySelectedOption(Files_t & list, float pos)
{
  int		i = 0;

  for (Files_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getFileNb() == 1)
	(*it)->changePosition(20, 20, 0);
      if ((*it)->getFileNb() == 1 && (*it)->getFixedNb() != _lastPos && _dI)
	{
	  _lastPos = (*it)->getFixedNb();
	  _framePos += _dI;
	  graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();
	  _menuCamera->setPositionOK(false, VELOCITY);
	  _menuCamera->setFutureCameraPosition(v.first, v.second + pos - _framePos * 400,
					       v.third + CLOSE_CAM);
	}
      if ((*it)->getValidate())
      	(*it)->drawSelected();
      else
	(*it)->drawBackground();
      (*it)->drawText();
      if ((*it)->getFileNb() == 1)
	(*it)->changePosition(-20, -20, 0);
      ++i;
    }
}

void		Menu::displayIA(OptionList_t & list) const
{
  for (OptionList_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getOptionNb() == 1)
	(*it)->changePosition(20, 20, 0);
      if ((*it)->getValidate())
      	(*it)->drawSelected();
      else
	(*it)->drawBackground();
      (*it)->drawText();
      if ((*it)->getOptionNb() == 1)
	(*it)->changePosition(-20, -20, 0);
    }
}

void		Menu::displayHighscores(graphic::Vector & pos,
					score::HighScore::listScore_t & list)
{
  float		y = 120.f;

  for (score::HighScore::listScore_t::const_iterator it = list.begin(); it != list.end(); ++it)
    {
      graphic::Text	text = (*it)->getText();
      text.setPosition(graphic::Vector(pos.first - 500.f, pos.second + y, pos.third + 100.f));
      y -=  60.f;
      text.draw();
    }
}

void		Menu::display()
{
  SubMenus_t	list1, list2;
  OptionList_t	*options;
  Files_t	files;

  list1 = this->getMenus();
  for (SubMenus_t::iterator it = list1.begin(); it != list1.end(); ++it)
    {
      list2 = (*it)->getMenus();
      for (SubMenus_t::iterator it2 = list2.begin(); it2 != list2.end(); ++it2)
	{
	  (*it2)->drawBackground();
	  options = (*it2)->getOptions();
	  if (!(*it2)->getName().compare("Highscores")) {
	    graphic::Vector	pos = (*it2)->getBackground().getPosition();
	    displayHighscores(pos, score::HighScore::getHighScoreInstance()->getListScore());
	  }
	  for (OptionList_t::iterator it3 = options->begin(); it3 != options->end(); ++it3)
	    {
	      if ((*it3)->getOptionNb() == 1)
		(*it3)->drawSelected();
	      else
		(*it3)->drawBackground();
	      (*it3)->drawText();
	      if ((*it3)->getKey() != gdl::Keys::Delete)
		(*it3)->getKeyText()->draw();
	    }
	}
    }
  switch (_optionSelector) {
  case 1: displaySelectedOption(_gameLoader->getSaves(), -2700); break;
  case 2: displaySelectedOption(_gameLoader->getMaps(), -2700); break;
  case 3: displaySelectedOption(_gameLoader->getMusic(), 2200); break;
  case 4: displayIA(_gameLoader->getIA()); break;
  default: break;
  }
}

void			Menu::update(MyClock const &cl, gdl::Input &in)
{
  SubMenus_t		_list;
  OptionList_t		*options;

  options = getCurrentOptionList();
  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1 && _menuEventManager->getIsMapping()
	  && _menuEventManager->getPressedKey() != gdl::Keys::Delete)
	{
	  (*it2)->getKeyText()->updateText(KeyManager::getInstance()->getStringKey(_menuEventManager->getPressedKey()));
	  _menuEventManager->setPressedKey(gdl::Keys::Delete);
	  return;
	}
    }
  for (SubMenus_t::iterator it = _hmenus.begin(); it != _hmenus.end(); ++it)
    {
      _list = (*it)->getMenus();
      for (SubMenus_t::iterator it2 = _list.begin(); it2 != _list.end(); ++it2)
      	{
	  if (!(*it2)->getName().compare("Credits"))
	    (*it2)->getBackground().update(cl, in);
	}
    }
  if (!_menuCamera->getPositionOK())
    _menuCamera->moveCamera();
  Core::getInstance()->setReferenceTime(cl);
}

graphic::AForm*		Menu::makeRectangle(float x, float y, float z,
					    float xs, float ys,
					    std::string const & bg)
{
  graphic::AForm *Form = new graphic::Primitive::Rectangle(graphic::Vector(x, y, z),
  							   graphic::Vector(0.f, 0.f, 0.f),
  							   graphic::Color(0.f, 0.f, 0.f),
  							   graphic::Scale(xs, ys, 1.f));
  Form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture(bg));
  return (Form);
}

void			Menu::menuAddOption(SubMenu & s, std::string const & _mName,
					    std::string const & _optName, graphic::AForm *_bg,
					    gdl::Keys::Key k, float posy, float nb)
{
  SubMenus_t		list = s.getMenus();

  for (SubMenus_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getName().compare(_mName) == 0)
	(*it)->addOption(_optName, _bg, k, posy, nb);
    }
}

void		Menu::initialize()
{
  /*-----------------------------------PLAY---------------------------------------*/

  graphic::AForm *FMenuPlay = makeRectangle(0.0f, 0.0f, 0.0f, 5.8f, 5.0f, "PLAY");
  graphic::AForm *FMenuLoad = makeRectangle(-GAP, -LOW, -GAP, 5.8f, 5.f, "PLAY");

  graphic::AForm *FOptionPlay = makeRectangle(BORDER, 200.0f, 0.0f, 3.f, 0.5f, "SHORT");
  graphic::AForm *FOptionPlay2 = makeRectangle(BORDER, 0.0f, 0.0f, 3.f, 0.5f, "SHORT");
  graphic::AForm *FOptionExit = makeRectangle(BORDER, -300.0f, 0.0f, 3.f, 0.5f, "SHORT");

  graphic::AForm *FOptionLoad = makeRectangle(BORDER -GAP, 200.f -LOW, -GAP, 3.f, 0.5f, "SHORT");
  graphic::AForm *FOptionIA = makeRectangle(BORDER -GAP, 0.f -LOW, -GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FOptionMap = makeRectangle(BORDER -GAP, -200.f -LOW, -GAP, 3.0f, 0.5f, "SHORT");

  /*---------------------------------OPTIONS--------------------------------------*/

  graphic::AForm *FMenuOptions = makeRectangle(SIDE, 0.0f, 0.0f, 5.8f, 5.0f, "OPTIONS");
  graphic::AForm *FP1KeyboardOptions = makeRectangle(SIDE -GAP, -LOW, -GAP, 5.8f, 5.0f, "KEYBOARD1");
  graphic::AForm *FP2KeyboardOptions = makeRectangle(SIDE -2.0f*GAP, -2.0f*LOW, -2.0f*GAP, 5.8f, 5.0f, "KEYBOARD2");

  graphic::AForm *FMusic = makeRectangle(SIDE + BORDER, 300.f, 0.f, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FVolume = makeRectangle(SIDE + BORDER, 150.f, 0.f, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FRounds = makeRectangle(SIDE + BORDER, 0.f, 0.f, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FXMap = makeRectangle(SIDE + BORDER, -150.f, 0.f, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FYMap = makeRectangle(SIDE + BORDER, -300.f, 0.f, 3.0f, 0.5f, "SHORT");

  graphic::AForm *FP1OUp = makeRectangle(SIDE + BORDER - GAP, -LOW +300.0f, -GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP1ODown = makeRectangle(BORDER + SIDE -GAP, -LOW +150.0f, -GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP1OLeft = makeRectangle(BORDER + SIDE -GAP, -LOW +0.0f, -GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP1ORight = makeRectangle(BORDER + SIDE -GAP, -LOW -150.0f, -GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP1OLeave = makeRectangle(BORDER + SIDE -GAP, -LOW -300.0f, -GAP, 3.0f, 0.5f, "SHORT");

  graphic::AForm *FP2OUp = makeRectangle(BORDER + SIDE -2.0f*GAP, -2.0f*LOW +300.0f, -2.0f*GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP2ODown = makeRectangle(BORDER + SIDE -2.0f*GAP, -2.0f*LOW +150.0f, -2.0f*GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP2OLeft = makeRectangle(BORDER + SIDE -2.0f*GAP, -2.0f*LOW +0.0f, -2.0f*GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP2ORight = makeRectangle(BORDER + SIDE -2.0f*GAP, -2.0f*LOW -150.0f, -2.0f*GAP, 3.0f, 0.5f, "SHORT");
  graphic::AForm *FP2OLeave = makeRectangle(BORDER + SIDE -2.0f*GAP, -2.0f*LOW -300.0f, -2.0f*GAP, 3.0f, 0.5f, "SHORT");

  /*---------------------------------HIGHSCORES-----------------------------------*/

  graphic::AForm *FHighscores = makeRectangle(SIDE * 2.0f, 0.0f, 0.0f, 5.8, 5.0f, "HIGHSCORES");
  graphic::AForm *FCredits = makeRectangle(SIDE * 2.0f -GAP, -LOW, -GAP, 5.8, 5.0f, "HIGHSCORES");

  /*------------------------------------------------------------------------------*/

  SubMenu	*MenuPlay = new SubMenu("Play", FMenuPlay, 1);
  SubMenu	*MenuOptions = new SubMenu("Options", FMenuOptions, 1);
  SubMenu	*MenuCredits = new SubMenu("Highscores", FHighscores, 1);

  /*PLAY*/
  MenuPlay->setShown(true);
  _hmenus.push_back(MenuPlay);
  MenuPlay->getMenus().push_back(MenuPlay);
  MenuPlay->addMenu("Load Menu", FMenuLoad, 2);
  menuAddOption(*MenuPlay, "Play", "One Player", FOptionPlay, gdl::Keys::Delete, -50, 1);
  menuAddOption(*MenuPlay, "Play", "Two Players", FOptionPlay2, gdl::Keys::Delete, -100, 2);
  menuAddOption(*MenuPlay, "Play", "Exit",  FOptionExit, gdl::Keys::Delete, 230, 3);
  menuAddOption(*MenuPlay, "Load Menu", "Load",  FOptionLoad, gdl::Keys::Delete, 230, 1);
  menuAddOption(*MenuPlay, "Load Menu", "Maps",  FOptionIA, gdl::Keys::Delete, 240, 2);
  menuAddOption(*MenuPlay, "Load Menu", "IA",  FOptionMap, gdl::Keys::Delete, 340, 3);

  /*OPTIONS*/
  _hmenus.push_back(MenuOptions);
  MenuOptions->getMenus().push_back(MenuOptions);
  MenuOptions->addMenu("Keyboard Options 1", FP1KeyboardOptions, 2);
  MenuOptions->addMenu("Keyboard Options 2", FP2KeyboardOptions, 3);

  menuAddOption(*MenuOptions, "Options", "Music", FMusic, gdl::Keys::Delete, 190, 1);
  menuAddOption(*MenuOptions, "Options", "Volume", FVolume, gdl::Keys::Delete, 140, 2);
  menuAddOption(*MenuOptions, "Options", "Rounds:1", FRounds, gdl::Keys::Delete, 0, 3);
  menuAddOption(*MenuOptions, "Options", "X:20", FXMap, gdl::Keys::Delete, 140, 4);
  menuAddOption(*MenuOptions, "Options", "Y:20", FYMap, gdl::Keys::Delete, 140, 5);

  menuAddOption(*MenuOptions, "Keyboard Options 1", "Up", FP1OUp, gdl::Keys::Up, -200, 1);
  menuAddOption(*MenuOptions, "Keyboard Options 1", "Down", FP1ODown, gdl::Keys::Down, -200, 2);
  menuAddOption(*MenuOptions, "Keyboard Options 1", "Left", FP1OLeft, gdl::Keys::Left, -200, 3);
  menuAddOption(*MenuOptions, "Keyboard Options 1", "Right", FP1ORight, gdl::Keys::Right, -200, 4);
  menuAddOption(*MenuOptions, "Keyboard Options 1", "Leave", FP1OLeave, gdl::Keys::Space, -200, 5);

  menuAddOption(*MenuOptions, "Keyboard Options 2", "Up", FP2OUp, gdl::Keys::W, -200, 1);
  menuAddOption(*MenuOptions, "Keyboard Options 2", "Down", FP2ODown, gdl::Keys::S, -200, 2);
  menuAddOption(*MenuOptions, "Keyboard Options 2", "Left", FP2OLeft, gdl::Keys::A, -200, 3);
  menuAddOption(*MenuOptions, "Keyboard Options 2", "Right", FP2ORight, gdl::Keys::D, -200, 4);
  menuAddOption(*MenuOptions, "Keyboard Options 2", "Leave", FP2OLeave, gdl::Keys::E, -200, 5);

  /*CREDITS*/
  _hmenus.push_back(MenuCredits);
  MenuCredits->getMenus().push_back(MenuCredits);
  FCredits->setVideo(new graphic::Video("medias/BombermanCredits.avi"));
  MenuCredits->addMenu("Credits", FCredits, 2);

  /*LOAD FILES*/
  _gameLoader->loadSaves();
  _gameLoader->loadMaps();
  _gameLoader->loadMusic();
  _gameLoader->initIA();
}

/*---------------------------------DEPTH MENUS-----------------------------------*/

void		Menu::updateMenuInDepth(float side)
{
  float		reset1, reset2;
  SubMenu	*_shown;
  SubMenus_t	list;
  SubMenus_t::iterator it1, it2, it3;

  _shown = this->getShownMenu();
  if (_shown->getLocked())
    return;
  list = _shown->getMenus();
  reset1 = -GAP * (static_cast<float>(list.size()) - 1.f);
  reset2 = -LOW * (static_cast<float>(list.size()) - 1.f);
  for (it2 = list.begin(); it2 != list.end(); ++it2)
    {
      if (side == -1 && (*it2)->getMenuNb() == static_cast<int>(list.size()))
	{
	  (*it2)->changePosition(-reset1, -reset2, -reset1);
	  (*it2)->setMenuNb(1);
	}
      else if (side == 1 && (*it2)->getMenuNb() == 1)
	{
	  (*it2)->changePosition(reset1, reset2, reset1);
	  (*it2)->setMenuNb(static_cast<float>(list.size()));
	}
      else
	{
	  (*it2)->changePosition(side * GAP, side * LOW, side * GAP);
	  (*it2)->changeMenuNb(side * -1);
	}
    }
  return;
}

/*-------------------------------HORIZONTAL MENUS---------------------------------*/

void		Menu::updateHorizontalMenu(float side)
{
  SubMenus_t	list;
  SubMenu	*menu = getFirstShownMenu();
  SubMenus_t::iterator it, it2;
  graphic::Vector v;

  list = this->getMenus();
  v = menu->getBackground().getPosition();
  for (it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getShown() && !(*it)->getLocked())
	{
	  (*it)->setShown(false);
	  it2 = it;
	  if (side == 1 && ++it2 == list.end()) {
	    it = list.begin();
	    _menuCamera->setPositionOK(false, VELOCITY2);
	    _menuCamera->setFutureCameraPosition(v.first -2 * SIDE, v.second, v.third + FAR_CAM);
	  }
	  else if (side == -1 && it == list.begin()) {
	    _menuCamera->setPositionOK(false, VELOCITY2);
	    _menuCamera->setFutureCameraPosition(v.first + 2 * SIDE , v.second, v.third + FAR_CAM);
	    it = list.end();
	    --it;
	  }
	  else {
	    _menuCamera->setPositionOK(false, VELOCITY);
	    _menuCamera->setFutureCameraPosition(v.first + side * SIDE, v.second, v.third + FAR_CAM);
	    if (side > 0)
	      ++it;
	    else
	      --it;
	  }
	  (*it)->setShown(true);
	  return;
	}
    }
}

SubMenu*		Menu::getShownMenu() const
{
  SubMenus_t	list;

  list = this->getMenus();
  for (SubMenus_t::iterator it1 = list.begin(); it1 != list.end(); ++it1)
    {
      if ((*it1)->getShown())
	return ((*it1));
    }
  return (NULL);
}

SubMenu*		Menu::getFirstShownMenu() const
{
  SubMenus_t		list;

  list = this->getShownMenu()->getMenus();
  for (SubMenus_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getMenuNb() == 1)
	return ((*it));
    }
  return (NULL);
}

/*-------------------------------------OPTIONS-------------------------------------*/

void			Menu::updateOptions(float side)
{
  SubMenu	*_shown;
  SubMenus_t	list;
  OptionList_t	*options;

  _shown = this->getShownMenu();
  list = _shown->getMenus();
  for (SubMenus_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getMenuNb() == 1)
	{
	  options = (*it)->getOptions();
	  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
	    {
	      if (side == -1 && (*it2)->getOptionNb() == static_cast<int>(options->size()))
		(*it2)->setOptionNb(1);
	      else if (side == 1 && (*it2)->getOptionNb() == 1)
		(*it2)->setOptionNb(static_cast<float>(options->size()));
	      else
		(*it2)->changeOptionNb(-side);
	    }
	  return;
	}
    }
}

void			Menu::checkKeyboardOptions(int player, OptionList_t & options)
{
  for (OptionList_t::iterator it = options.begin(); it != options.end(); ++it)
    {
      if ((*it)->getOptionNb() == 1)
	{
	  (*it)->getSelected().setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("LONGBLACK"));
	  lockOption(0.f);
	  _ptr = _keyboardMap[(*it)->getStringName()];
	  setPlayer(player);
	  return;
	}
    }
}

void			Menu::onePlayer(void)
{
  GameEventManager	*manager = new GameEventManager();

  manager->setMap(_menuEventManager->getPlayer1Mapping());
  initModePlay(false);
  if (!_mapName.compare("Unknown"))
    Core::getInstance()->initialize(_xmap, _ymap);
  else
    Core::getInstance()->initializeMap("maps/" + _mapName);
  Core::getInstance()->addPlayer(manager);
  Core::getInstance()->run(_ai);
  graphic::MyGame::getInstance()->getSoundManager()->stop("MENU");
  graphic::MyGame::getInstance()->getSoundManager()->play("TRANSITION");
  Core::getInstance()->setReferenceTime(graphic::MyGame::getInstance()->getMyClock());

}

void			Menu::twoPlayers(void)
{
  GameEventManager	*manager1 = new GameEventManager();
 GameEventManager	*manager2 = new GameEventManager();

  manager1->setMap(_menuEventManager->getPlayer1Mapping());
  manager2->setMap(_menuEventManager->getPlayer2Mapping());
  initModePlay(true);
  if (!_mapName.compare("Unknown"))
    Core::getInstance()->initialize(_xmap, _ymap);
  else
    Core::getInstance()->initializeMap(_mapName);
  Core::getInstance()->addPlayer(manager1);
  Core::getInstance()->addPlayer(manager2);
  Core::getInstance()->run(_ai);

  graphic::MyGame::getInstance()->getSoundManager()->stop("MENU");
  graphic::MyGame::getInstance()->getSoundManager()->play("TRANSITION");
  Core::getInstance()->setReferenceTime(graphic::MyGame::getInstance()->getMyClock());
}

void			Menu::exit(void)
{
  _menuEventManager->setClose(true);
}

void			Menu::load(void)
{
  graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();

  _menuEventManager->loadMapping(8);
  _optionSelector = 1;
  _lastPos = 0.;
  _framePos = 0.;
  _gameLoader->getSaves().clear();
  _gameLoader->loadSaves();
  _menuCamera->setPositionOK(false, VELOCITY);
  _menuCamera->setFutureCameraPosition(v.first, v.second - 2700, v.third + CLOSE_CAM);
}


void			Menu::maps(void)
{
  graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();

  _menuEventManager->loadMapping(9);
  _optionSelector = 2;
  _lastPos = 0.;
  _framePos = 0.;
  _menuCamera->setPositionOK(false, VELOCITY);
  _menuCamera->setFutureCameraPosition(v.first, v.second - 2700, v.third + CLOSE_CAM);
}

void			Menu::music(void)
{
  graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();

  _lastPos = 0.;
  _framePos = 0.;
  _optionSelector = 3;
  _menuEventManager->loadMapping(14);
  _menuCamera->setPositionOK(false, VELOCITY);
  _menuCamera->setFutureCameraPosition(v.first, v.second + 2200, v.third + CLOSE_CAM);
}

void			Menu::IA(void)
{
  graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();

  _menuEventManager->loadMapping(10);
  _optionSelector = 4;
  _lastPos = 0.;
  _framePos = 0.;
  _menuCamera->setPositionOK(false, VELOCITY);
  _menuCamera->setFutureCameraPosition(v.first, v.second + 3000, v.third + CLOSE_CAM);
}

void			Menu::unlockNbIA(float)
{
  OptionList_t		options = _gameLoader->getIA();

  _menuEventManager->loadMapping(10);
  for (OptionList_t::iterator it = options.begin(); it != options.end(); ++it)
    {
      if ((*it)->getOptionNb() == 1)
	(*it)->getSelected().setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("LONG"));
    }
}

void			Menu::lockX(void)
{
  _menuEventManager->loadMapping(11);
}

void			Menu::lockRounds(void)
{
  _menuEventManager->loadMapping(15);
}

void			Menu::lockY(void)
{
  _menuEventManager->loadMapping(12);
}

void			Menu::unlockXYRounds(float)
{
  _menuEventManager->loadMapping(2);
}

void			Menu::unlockLoad(float)
{
  graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();

  _optionSelector = 0.;
  _menuCamera->setPositionOK(false, VELOCITY);
  _menuEventManager->loadMapping(2);
  _menuCamera->setFutureCameraPosition(v.first, v.second, v.third + CLOSE_CAM);
}

void			Menu::validOption(float)
{
  SubMenus_t		list;
  OptionList_t		*options;

  list = getShownMenu()->getMenus();
  for (SubMenus_t::iterator it = list.begin(); it != list.end(); ++it)
    {
      if ((*it)->getMenuNb() == 1)
  	{
  	  options = (*it)->getOptions();
	  if ((*it)->getName().compare("Keyboard Options 1") == 0) {
	    checkKeyboardOptions(1, *options);
	    return;
	  }
	  else if ((*it)->getName().compare("Keyboard Options 2") == 0) {
	    checkKeyboardOptions(2, *options);
	    return;
	  }
	  for (OptionList_t::iterator it2 = options->begin(); it2 != options->end(); ++it2)
	    {
	      try {
		if ((*it2)->getOptionNb() == 1) {
		  (this->*_optionsMap[(*it2)->getStringName()])();
		}
	      }
	      catch (ExceptionLoadMap & e) {
		return;
	      }
	      catch (ExceptionCore & e) {
		graphic::MyGame::place = graphic::MyGame::MENU;
		unlockMenu(0.f);
		return;
	      }
	    }
	}
    }
}

/*-------------------------------------PAUSE--------------------------------------*/

void		Menu::updatePauseVolume(float _side)
{
  OptionList_t		options;
  std::ostringstream	oss;
  std::string		s("VOLUME");

  oss << s;
  if ((_volume + _side) <= 0 || (_volume + _side) > 11)
    return;
  if (_side > 0)
    _volume += 0.50f;
  else
    _volume -= 0.50f;
  graphic::MyGame::getInstance()->getSoundManager()->setAllVolume(_volume / 10.0f);
  oss << static_cast<int>(_volume);
  options = PauseMenu::getInstance()->getOptions();
  for (OptionList_t::iterator it2 = options.begin(); it2 != options.end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1) {
	(*it2)->getSelected().setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture(oss.str()));
	return;
      }
    }
}

void		Menu::pauseMenu(float in)
{
  if (in == 1.f)
    {
      graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->resetToMenu();
      graphic::Vector	v = getFirstShownMenu()->getBackground().getPosition();
      _menuCamera->setPositionOK(false, VELOCITY);
      _menuCamera->setFutureCameraPosition(v.first, v.second, v.third + CLOSE_CAM);
      _menuEventManager->loadMapping(5);
      graphic::MyGame::place = graphic::MyGame::PAUSE;
    }
  else
    {
      graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->resetToGame();
      _menuEventManager->loadMapping(4);
      graphic::MyGame::place = graphic::MyGame::GAME;
    }
}

void		Menu::updatePauseOptions(float side)
{
  OptionList_t	_options = PauseMenu::getInstance()->getOptions();

  for (OptionList_t::iterator it = _options.begin(); it != _options.end(); ++it)
    {
      if (side == -1 && (*it)->getOptionNb() == static_cast<int>(_options.size()))
	(*it)->setOptionNb(1);
      else if (side == 1 && (*it)->getOptionNb() == 1)
	(*it)->setOptionNb(static_cast<float>(_options.size()));
      else
	(*it)->changeOptionNb(-side);
    }
  return;
}

void		Menu::validPauseOption(float _useless)
{
  OptionList_t	_options = PauseMenu::getInstance()->getOptions();

  static_cast<void>(_useless);
  for (OptionList_t::iterator it = _options.begin(); it != _options.end(); ++it)
    {
      if ((*it)->getOptionNb() == 1) {
	(PauseMenu::getInstance()->*_pauseMap[(*it)->getStringName()])();
	return;
      }
    }
}

/*-------------------------------------SAVES------------------------------------*/

void			Menu::updateSaves(float side)
{
  Files_t		_saves = _gameLoader->getSaves();

  _dI = 0;
  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    {
      if ((side == 1 && (*it)->getFileNb() == 1 && (*it)->getFixedNb() == static_cast<int>(_saves.size() - 1))
	  || (side == -1 && (*it)->getFileNb() == 1 && (*it)->getFixedNb() == 0)) {
	_dI = 0.;
	return;
      }
      if (side == -1 && (*it)->getFileNb() == 1
	  && (*it)->getFixedNb() % 3 == 0 && (*it)->getFixedNb() != 0)
	_dI = -1;
    }
  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    (*it)->changeFileNb(-side);
  if (side == 1)
    for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
      if ((*it)->getFileNb() == 1 && (*it)->getFixedNb() % 3 == 0 && (*it)->getFixedNb() != 0)
	_dI = 1;
  return;
}

void			Menu::selectSave(float)
{
  Files_t		_saves = _gameLoader->getSaves();
  std::list<GameEventManager *>		list;

  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    {
      if ((*it)->getFileNb() == 1)
	{
	  Save	s;
	  GameEventManager	*map1 = new GameEventManager();
	  GameEventManager	*map2 = new GameEventManager();

	  try {
	  map1->setMap(_menuEventManager->getPlayer1Mapping());
	  map2->setMap(_menuEventManager->getPlayer2Mapping());
	  list.push_back(map1);
	  list.push_back(map2);

	  s.loadGame(std::string("saves/") + (*it)->getName());
	  if (Core::getInstance()->getPlayers().size() == 2)
	    initModePlay(true);
	  else
	    initModePlay(false);
	  float		tmp = Core::getInstance()->getTotalTime();
	  Core::getInstance()->setEventManager(list);
	  Core::getInstance()->runSave();
	  Core::getInstance()->setTotalTime(tmp);
	  }
	  catch (ExceptionCore const & e)
	    {
	      unlockMenu(0.f);
	      return ;
	    }
	}
    }
  return;
}

/*-------------------------------------MAPS------------------------------------*/

void			Menu::updateMaps(float side)
{
  Files_t		_saves = _gameLoader->getMaps();

  _dI = 0;
  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    {
      if ((side == 1 && (*it)->getFileNb() == 1 && (*it)->getFixedNb() == static_cast<int>(_saves.size() - 1))
	  || (side == -1 && (*it)->getFileNb() == 1 && (*it)->getFixedNb() == 0)) {
	_dI = 0.;
	return;
      }
      if (side == -1 && (*it)->getFileNb() == 1
	  && (*it)->getFixedNb() % 3 == 0 && (*it)->getFixedNb() != 0)
	_dI = -1;
    }
  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    (*it)->changeFileNb(-side);
  if (side == 1)
    for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
      if ((*it)->getFileNb() == 1 && (*it)->getFixedNb() % 3 == 0 && (*it)->getFixedNb() != 0)
	_dI = 1;
  return;
}

void			Menu::selectMap(float _val)
{
  Files_t		_saves = _gameLoader->getMaps();

  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    {
      if (_val)
	_mapName = "Unknown";
      if (!_val && (*it)->getFileNb() == 1)
	{
	  _mapName = (*it)->getName();
	  (*it)->setValidate(true);
	}
      else
	(*it)->setValidate(false);
    }
  return;
}


/*-------------------------------------MUSIC------------------------------------*/

void			Menu::updateMusic(float side)
{
  Files_t		_saves = _gameLoader->getMusic();

  _dI = 0;
  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    {
      if ((side == 1 && (*it)->getFileNb() == 1 && (*it)->getFixedNb() == static_cast<int>(_saves.size() - 1))
	  || (side == -1 && (*it)->getFileNb() == 1 && (*it)->getFixedNb() == 0)) {
	_dI = 0.;
	return;
      }
      if (side == -1 && (*it)->getFileNb() == 1
	  && (*it)->getFixedNb() % 3 == 0 && (*it)->getFixedNb() != 0)
	_dI = -1;
    }
  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    (*it)->changeFileNb(-side);
  if (side == 1)
    for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
      if ((*it)->getFileNb() == 1 && (*it)->getFixedNb() % 3 == 0 && (*it)->getFixedNb() != 0)
	_dI = 1;
  return;
}

void			Menu::selectMusic(float _val)
{
  Files_t		_saves = _gameLoader->getMusic();

  for (Files_t::iterator it = _saves.begin(); it != _saves.end(); ++it)
    {
      if (_val)
	_musicName = "Unknown";
      if (!_val && (*it)->getFileNb() == 1)
	{
	  _musicName = (*it)->getName();
	  graphic::MyGame::getInstance()->getSoundManager()->stopAllSounds();
	  graphic::MyGame::getInstance()->getSoundManager()->loadSoundDynamic("MENU", "music/" + _musicName, sound::MUSIC);
	  graphic::MyGame::getInstance()->getSoundManager()->play("MENU");
	  (*it)->setValidate(true);
	}
      else
	(*it)->setValidate(false);
    }
  return;
}

/*------------------------------------IA---------------------------------------*/

void			Menu::IANumber(float _val)
{
  OptionList_t		options = _gameLoader->getIA();
  std::ostringstream	oss;
  std::string		s("AI Number:");

  oss << s;
  if (!_val)
    _ai = 1;
  if ((_ai + _val) <= 1)
    return ;
  _ai += _val;
  oss << static_cast<int>(_ai);
  for (OptionList_t::iterator it2 = options.begin(); it2 != options.end(); ++it2)
    {
      if ((*it2)->getOptionNb() == 1)
	(*it2)->getName().updateText(oss.str());
    }
}

void			Menu::updateIA(float side)
{
  OptionList_t		_ia = _gameLoader->getIA();

  for (OptionList_t::iterator it = _ia.begin(); it != _ia.end(); ++it)
    {
      if (side == -1 && (*it)->getOptionNb() == static_cast<int>(_ia.size()))
	(*it)->setOptionNb(1);
      else if (side == 1 && (*it)->getOptionNb() == 1)
	(*it)->setOptionNb(static_cast<float>(_ia.size()));
      else
	(*it)->changeOptionNb(-side);
    }
  return;
}

void			Menu::selectIA(float)
{
  OptionList_t		_ia = _gameLoader->getIA();

  for (OptionList_t::iterator it = _ia.begin(); it != _ia.end(); ++it)
    {
      if ((*it)->getOptionNb() == 1 && !(*it)->getStringName().compare("AI Number:1")) {
	(*it)->getSelected().setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("LONGBLACK"));
	(*it)->setValidate(true);
	_menuEventManager->loadMapping(13);
	return;
      }
    }
  for (OptionList_t::iterator it = _ia.begin(); it != _ia.end(); ++it)
    {
      if ((*it)->getOptionNb() == 1) {
	_iaType = (*it)->getStringName();
	(*it)->setValidate(true);
      }
      else
	(*it)->setValidate(false);
    }
  return;
}

void	Menu::initModePlay(const bool mode)
{
  _menuEventManager->loadMapping(4);

  if (mode == true)
    {
      setPlayer(2);
      graphic::MyGame::getInstance()->getCameraManagement()->enableMulti();
      graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->resetToGame();
      graphic::MyGame::getInstance()->getCameraManagement()->getCamera2()->resetToGame();
    }
  else
    {
      setPlayer(1);
      graphic::MyGame::getInstance()->getCameraManagement()->enableSingle();
      graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->resetToGame();
    }
  graphic::MyGame::place = graphic::MyGame::TRANSITION;
}

GameLoader	*Menu::getGameLoader() const
{
  return _gameLoader;
}

//
// Text.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sun May 13 21:30:26 2012 benjamin bourdin
// Last update Sun Jun  3 22:04:37 2012 benjamin bourdin
//

#include	<GL/gl.h>
#include	<GL/glu.h>
#include	"Text.hh"
#include	"LoadText.hh"
#include	"Camera.hh"

graphic::Text::Text(Vector const & pos, Vector const & rot,
		    Color const &color, Scale const &scale)
  : AForm(pos, rot, color, scale), _str(), _width(300.0f), _height(300.0f), _depth(300.0f)
{
}

graphic::Text::~Text()
{
}

graphic::Text::Text(Text const &t)
  : AForm(t.position_, t.rotation_, t.color_, t.scale_), _str(t._str), _width(300.0f), _height(300.0f), _depth(300.0f)
{
  setTexture(t.texture_);
  setModel(t.model_);
}

graphic::Text&	graphic::Text::operator=(Text const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  _str = t._str;
  return *this;
}


float	graphic::Text::getWidth(void) const
{
  return _width;
}

float	graphic::Text::getHeight(void) const
{
  return _height;
}

float	graphic::Text::getDepth(void) const
{
  return _depth;
}

std::string const &	graphic::Text::getString(void) const
{
  return _str;
}

void	graphic::Text::initialize(void)
{
  _str = std::string("");
}

void	graphic::Text::update(MyClock const & gameClock, gdl::Input & input)
{
  static_cast<void>(gameClock);
  static_cast<void>(input);
}

void	graphic::Text::updateText(std::string const &str)
{
  _str = str;
}

void	graphic::Text::draw(void)
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (texture_)
    {
      glEnable(GL_TEXTURE_2D);
      texture_->bind();
    }

  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.first, position_.second, position_.third);

  if (rotationEnable_.first)
    glRotatef(rotation_.first, 1.0f, 0.0f, 0.0f);
  if (rotationEnable_.second)
    glRotatef(rotation_.second, 0.0f, 1.0f, 0.0f);
  if (rotationEnable_.third)
    glRotatef(rotation_.third, 0.0f, 0.0f, 1.0f);

  glScalef(scale_.first, scale_.second, scale_.third);
  glBegin(GL_QUADS);

  glColor3f(color_.first, color_.second, color_.third);

  float xm = 0.0f;
  for (size_t i = 0; i < _str.size(); ++i)
    {
      LoadText::getInstance()->loadCharacter(static_cast<char>(_str.c_str()[i] - 1));

      glTexCoord2f(LoadText::getInstance()->getX0(), LoadText::getInstance()->getY1());
      glVertex2d(xm + (LoadText::getInstance()->getWidth()), 0);
      glTexCoord2f(LoadText::getInstance()->getX1(), LoadText::getInstance()->getY1());
      glVertex2d((LoadText::getInstance()->getWidth()) + xm + 100.0f, 0);
      glTexCoord2f(LoadText::getInstance()->getX1(), LoadText::getInstance()->getY0());
      glVertex2d((LoadText::getInstance()->getWidth()) + xm + 100.0f, 100);
      glTexCoord2f(LoadText::getInstance()->getX0(), LoadText::getInstance()->getY0());
      glVertex2d(xm + (LoadText::getInstance()->getWidth()), 100);

      xm += 100.0f;
    }
  glEnd();

  glPopMatrix();
  if (texture_)
    glDisable(GL_TEXTURE_2D);

  glDisable(GL_BLEND);
}

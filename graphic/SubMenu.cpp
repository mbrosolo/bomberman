//
// SubMenu.cpp for Bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 18:37:09 2012 tosoni
// Last update Sun Jun  3 22:06:27 2012 benjamin bourdin
//

#include	"SubMenu.hh"

SubMenu::SubMenu(std::string const & n, graphic::AForm* form, float nb)
  : name(n),
    background(form),
    _dmenus(),
    _options(),
    _menuNb(nb),
    _shown(false),
    _locked(false)
{
}

SubMenu::~SubMenu()
{
}

SubMenu::SubMenu(SubMenu const & other)
  : name(other.name),
    background(other.background),
    _dmenus(other._dmenus),
    _options(other._options),
    _menuNb(0),
    _shown(false),
    _locked(false)
{
}

SubMenu	&		SubMenu::operator=(SubMenu const & other)
{
  if (this != &other)
    {
      this->name = other.name;
      this->background = other.background;
      this->_dmenus = other._dmenus;
      this->_options = other._options;
      this->_menuNb = other._menuNb;
      this->_shown = other._shown;
      this->_locked = other._locked;
    }
  return (*this);
}

void			SubMenu::addOption(std::string const & _s, graphic::AForm *bg,
					   gdl::Keys::Key key, float ypos, float val)
{
  graphic::Scale scale = bg->getScale();
  graphic::Vector position = bg->getPosition();

  scale.first += 0.2f;
  position.first += 30.f;

  graphic::AForm *s = new graphic::Primitive::Rectangle(position,
							graphic::Vector(0.f, 0.f, 0.f),
							graphic::Color(0.f, 0.f, 0.f),
							scale);
  if (_s.compare("Volume"))
    s->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("LONG"));
  else
    s->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("VOLUME5"));
  _options.push_back(new Option(_s, bg, s, key, ypos, val));
}

void		SubMenu::addMenu(std::string const & _s, graphic::AForm *Form, float nb)
{
  _dmenus.push_back(new SubMenu(_s, Form, nb));
}

SubMenus_t&		SubMenu::getMenus()
{
  return (_dmenus);
}

OptionList_t*		SubMenu::getOptions()
{
  return (&_options);
}

std::string		SubMenu::getName() const
{
  return (name);
}

bool			SubMenu::getLocked() const
{
  return (_locked);
}

graphic::AForm&		SubMenu::getBackground() const
{
  return (*background);
}

void			SubMenu::setLocked(bool b)
{
  _locked = b;
}

void			SubMenu::setOption(std::string const & name, float o)
{
  for (OptionList_t::iterator it = _options.begin(); it != _options.end(); ++it)
    {
      if (name.compare((*it)->getStringName()) == 0)
	{
	  (*it)->setOptionNb(o);
	  break;
	}
    }
}

void		SubMenu::drawBackground() const
{
  background->draw();
}

void		SubMenu::changePosition(const float x, const float y, const float z)
{
  background->incPosX(x);
  background->incPosY(y);
  background->incPosZ(z);
  for (OptionList_t::iterator it = _options.begin(); it != _options.end(); ++it)
    {
      (*it)->changePosition(x, y, z);
    }
}

/*-----------------------MENU NB---------------------------*/


void		SubMenu::changeMenuNb(float nb)
{
  _menuNb += nb;
}

void		SubMenu::setMenuNb(float nb)
{
  _menuNb = nb;
}

float		SubMenu::getMenuNb() const
{
  return (_menuNb);
}

/*------------------HORIZONTAL MENU-------------------------*/

void		SubMenu::setShown(bool b)
{
  _shown = b;
}

bool		SubMenu::getShown() const
{
  return (_shown);
}


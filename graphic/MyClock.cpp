//
// MyClock.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 17:32:50 2012 benjamin bourdin
// Last update Sun Jun  3 22:47:01 2012 benjamin bourdin
//

#include	<sstream>
#include	<iostream>
#include	<errno.h>
#include	<sstream>
#include	<cstring>
#include	<time.h>
#include	"MyClock.hh"

#define LEN_DATE	30

MyClock::MyClock(gdl::GameClock &gc)
  : _fps(1000000), _tmpfps(0), _clock(), _timeElapsedFPS(0.0f), _timeTotalFPS(0.0f), _gc(gc)
{
}

MyClock::~MyClock()
{
}

void	MyClock::updateFPS(void)
{
  if (_timeElapsedFPS >= 0.90f)
    _timeElapsedFPS = 0.0f;

  _clock.update();
  _timeElapsedFPS += _clock.getTotalElapsedTime() - _timeTotalFPS;
  _timeTotalFPS = _clock.getTotalElapsedTime();
  if (_timeElapsedFPS >= 0.90f)
    {
      _fps = _tmpfps;
      _tmpfps = 0.0f;
    }
  else
    {
      _tmpfps++;
    }
}

float	MyClock::getElapsedFPS(void) const
{
  return _timeElapsedFPS;
}

int	MyClock::getFPS(void) const
{
  return _fps;
}

void	MyClock::update(void)
{
  _clock.update();
  _gc.update();
}

void	MyClock::play(void)
{
  _clock.play();
}

void	MyClock::reset(void)
{
  _clock.reset();
}

void	MyClock::pause(void)
{
  _clock.pause();
}

float	MyClock::getElapsedTime(void) const
{
  return _clock.getElapsedTime();
}

float	MyClock::getTotalGameTime(void) const
{
  return _gc.getTotalGameTime();
}

gdl::GameClock &	MyClock::getGameClock(void) const
{
  return _gc;
}

std::string	MyClock::timeToString(float time) const
{
  std::string	str;

  int	hour = static_cast<int>(time) / 3600;
  int	min = (static_cast<int>(time) % 3600) / 60;
  int	sec = (static_cast<int>(time) % 3600) % 60;
  int	milli = static_cast<int>
    (static_cast<float>(time - static_cast<float>(static_cast<int>(time))) * 100);

  {
    std::stringstream	ss;
    std::string		tmp;
    ss << hour; ss >> tmp;
    str = (tmp + std::string(":"));
    tmp.clear() ; ss.clear();

    ss << min; ss >> tmp;
    str += (tmp + std::string(":"));
    tmp.clear() ; ss.clear();

    ss << sec; ss >> tmp;
    str += (tmp + std::string(":"));
    tmp.clear() ; ss.clear();

    ss << milli; ss >> tmp;
    str += tmp;
    tmp.clear() ; ss.clear();
  }
  return str;
}

std::string MyClock::getCurDate(void) const
{
  time_t	time_cur;
  struct tm	*time_local;
  std::string	date;

  if (time(&time_cur) == static_cast<time_t>(-1))
    std::cerr << "time: " << strerror(errno) << std::endl;
  else
    {
      if ((time_local = localtime(&time_cur)) == NULL)
	std::cerr << "localtime: " << strerror(errno) << std::endl;
      else
	{
	  char	str_time[LEN_DATE];
	  if (strftime(&str_time[0], LEN_DATE, "%x %X", time_local) == 0)
	    std::cerr << "strftime: " << strerror(errno) << std::endl;
	  else
	    {
	      date = str_time;
	      return date;
	    }
	}
    }
  date = "unknown (problem happened)";
  return date;
}

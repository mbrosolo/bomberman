//
// Video.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Tue May 22 22:02:15 2012 benjamin bourdin
// Last update Sun Jun  3 22:02:03 2012 benjamin bourdin
//

#include	"Video.hh"
#include	"ExceptionLoading.hh"

graphic::Video::Video(std::string const &file)
  : _capture(cvCaptureFromAVI(file.c_str())), _img(NULL), _fps(0),
    _texture(0), _file(file), _isFinished(false), _curFPS(0)
{
  if (!_capture)
    {
      _isFinished = true;
      throw ExceptionLoading("Cannot load video "+file, __FILE__+std::string(", Video::Video"));
    }
  if ((_img = cvQueryFrame(_capture)) == NULL)
    _isFinished = NULL;
  else
    _fps = static_cast<int>(cvGetCaptureProperty(_capture, CV_CAP_PROP_FPS));
  glGenTextures(1, &_texture);
}

graphic::Video::~Video()
{
  if (_capture)
    cvReleaseCapture(&_capture);
  _capture = NULL;
  glDeleteTextures(1, &_texture);
}

graphic::Video::Video(Video const &copy)
  : _capture(copy._capture), _img(copy._img), _fps(copy._fps), _texture(0),
    _file(copy._file), _isFinished(copy._isFinished), _curFPS(copy._curFPS)
{
}

graphic::Video &	graphic::Video::operator=(Video const &copy)
{
  _capture = copy._capture;
  _img = copy._img;
  _fps = copy._fps;
  _file = copy._file;
  _isFinished = copy._isFinished;
  _curFPS = copy._curFPS;
  return *this;
}

bool	graphic::Video::captureImg(MyClock const &clock)
{
  _curFPS++;
  if (_curFPS >= clock.getFPS() / 25) // on veut une image tous les x passages
    {
      _curFPS = 0;

      if ((_img = cvQueryFrame(_capture)) == NULL)
	{
	  _isFinished = true;
	  cvReleaseCapture(&_capture);
	  _capture = cvCaptureFromAVI(_file.c_str());
	  if (!_capture)
	    throw ExceptionLoading("Cannot load video "+_file, __FILE__+std::string(", CaptureImg"));
	  if ((_img = cvQueryFrame(_capture)))
	    _fps = static_cast<int>(cvGetCaptureProperty(_capture, CV_CAP_PROP_FPS));
	  return false;
	}

      return false;
    }
  return true;
}

void	graphic::Video::loadImg(void) const
{
  if (_img)
    {
      glBindTexture(GL_TEXTURE_2D, _texture);
      glEnable(GL_TEXTURE_2D);
      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _img->width, _img->height,
		   0,GL_BGR,GL_UNSIGNED_BYTE, _img->imageData);
    }
}

bool	graphic::Video::isFinished(void) const
{
  return _isFinished;
}

void	graphic::Video::setFinished(const bool val)
{
  _isFinished = val;
}

//
// LoadTexture.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Fri May 11 21:03:06 2012 benjamin bourdin
// Last update Sun Jun  3 20:37:38 2012 benjamin bourdin
//

#ifndef	__LOAD_TEXTURE_HH__
#define __LOAD_TEXTURE_HH__

/**
 * @author bourdi_b
 * @file LoadTexture.hh
 */

#include	<list>
#include	<utility>
#include	<string>
#include	"Image.hpp"

namespace graphic
{

  /*! @class LoadTexture
   * @brief Contain all texture which will be used later.
   * it is really a texture manager which can load all texture for their later use.
   */
  class LoadTexture
  {
  public:
    /*!
     * @brief Intern correspondance key/path for entries
     */
    typedef	std::pair<std::string, std::string>	texture_entry_assoc_t;
    /*!
     * @brief Intern list of entries
     */
    typedef	std::list<texture_entry_assoc_t>	texture_entry_list_t;
    /*!
     * @brief Intern correspondance key/texture for textures loaded
     */
    typedef	std::pair<std::string, gdl::Image *>	texture_load_assoc_t;
    /*!
     * @brief Intern list of textures loaded
     */
    typedef	std::list<texture_load_assoc_t>		texture_load_list_t;

  public:
    /*!
     * @brief Contructor, private of LoadTexture
     */
    LoadTexture();
    /*!
     * @brief Destructor, private of LoadTexture
     */
    ~LoadTexture();

  public:

    /*!
     * @brief Add a new entry in the database, has to be used before 'loadTextures',
     * otherwise model will be ignored
     * @param key The key linked to the path (for calling the texture loaded through this key)
     * @param path The path of the texture
     */
    void	addEntry(std::string const &key, std::string const &path);
    /*!
     * @brief Delete an entry in the database
     * @param key The key which will deleted. If the texture is already loaded,
     * there is no effect on it
     */
    bool	delEntry(std::string const &key);

    /*!
     * @brief Print all key/path in the database entries
     */
    void	infos(void) const;

    /*!
     * @brief Load all textures in entries.
     * It has to be called once.
     */
    void	loadTextures(void);

    /*!
     * @brief Check if a key correspond to a texture loaded
     * @param key The key of the texture
     */
    bool	isValidKeyLoaded(std::string const &key) const;

    /*!
     * @brief Get a texture loaded in order to be printed on screen
     * @param key The key of the texture
     * @return The texture, or NULL if key is invalid (use isValidKeyLoaded before to be sure)
     */
    gdl::Image	*getTexture(std::string const &key) const;

    /*!
     * @brief Load a texture dynamically without passing by the phase add/delEntry.
     * It is used when we want to add entry after having called loadTextures method.
     * @param key The key of the texture
     * @param path The path of the texture
     */
    void	loadTextureDynamic(std::string const &key, std::string const &path);

    /*!
     * @brief Get the key of a texture loaded
     * @param t The texture
     * @return The key
     */
    std::string const &	getKey(gdl::Image const *t) const ;

  private:
    texture_entry_list_t	_texList; /*!< List of all textures entries */
    texture_load_list_t		_loadedTexture; /*!< list of loaded textures */

    std::string			_defaultNoMatch; /*!< string when no match for getter */

  };

}

#endif

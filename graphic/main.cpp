//
// main.cpp for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Wed May 16 15:42:21 2012 tosoni
// Last update Sun Jun  3 23:31:42 2012 benjamin bourdin
//

#include	<iostream>
#include	"MyGame.hh"
#include	"ExceptionRuntime.hh"
#include	"Exception.hh"
#include	"../lua/Lua.hh"

int		main(void)
{
  srand(time(NULL));

  try
    {
      graphic::MyGame *game = graphic::MyGame::getInstance();
      game->run();
    }
  catch (ExceptionRuntime const &e) {
    std::cerr << e.what() << " in " << e.where() << std::endl;
  }
  catch (lua::LuaException const &e) {
    std::cerr << "LuaException: " << e.what() << " in " << e.where() << std::endl;
  }
  catch (Exception const &e) {
    std::cerr << e.what() << " in " << e.where() << std::endl;
  }
  catch (...) {
    std::cerr << "Exception non geree" << std::endl;
  }

  return EXIT_SUCCESS;
}

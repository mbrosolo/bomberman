//
// MenuEventManager.hh for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 19:36:51 2012 tosoni
// Last update Sun Jun  3 22:49:43 2012 benjamin bourdin
//

#ifndef			__MENUEVENTMANAGER_HH__
#define			__MENUEVENTMANAGER_HH__

/**
 * @author tosoni_t
 * @file MenuEventManager.hh
 */

#include		<map>
#include		<iostream>
#include		<vector>
#include		"Input.hpp"
#include		"AForm.hh"
#include		"KeyManager.hh"
#include		"Menu.hh"
#include		"AObject.hh"
#include		"GameEventManager.hh"

class			Menu;
class			MenuEventManager;

/*!
 * @brief Typedef for the Member function pointer FPTR
 */
typedef void (Menu::*FPTR)(float);
/*!
 * @brief Typedef for the Member function pointer EvPTR
 */
typedef void (MenuEventManager::*EvPTR)(void);

/*!
 * @brief Typedef for the Event Load Vector
 */
typedef	std::vector<EvPTR> EventLoadVector_t;
/*!
 * @brief Typedef for the Event List Object
 */
typedef	std::map<gdl::Keys::Key, std::pair<FPTR, float> > EventListObject_t;

/*!
 * @class MenuEventManager
 * @brief Event Manager for the Menu
 */
class			MenuEventManager
{
public:
  /*!
   * @brief Getter for the class instance
   * @return
   */
  static MenuEventManager	*getInstance();
  /*!
   * @brief Destroys the calss instance
   */
  static void			killInstance();

  /*!
   * @brief Initializes the class instance
   */
  void				initialize();
  /*!
   * @brief Checks the Events
   */
  void				checkEvents();
  /*!
   * @brief Setter for the Loader
   */
  void				setLoaded();
 
  /*!
   * @brief Loads the first Mapping
   */
  void				loadFirstMapping(void); 
  /*!
   * @brief Loads the second Mapping
   */
  void				loadSecondMapping(void); 
  /*!
   * @brief Loads the third Mapping
   */
  void				loadThirdMapping(void); 
  /*!
   * @brief Loads the fourth Mapping
   */
  void				loadFourthMapping(void); 
  /*!
   * @brief Loads the fifth Mapping
   */
  void				loadFifthMapping(void); 
  /*!
   * @brief Loads the sixth Mapping
   */
  void				loadSixthMapping(void); 
  /*!
   * @brief Loads the seventh Mapping
   */
  void				loadSeventhMapping(void); 
  /*!
   * @brief Loads the eighth Mapping
   */
  void				loadEighthMapping(void); 
  /*!
   * @brief Loads the ninth Mapping
   */
  void				loadNinthMapping(void); 
  /*!
   * @brief Loads the tenth Mapping
   */
  void				loadTenthMapping(void); 
  /*!
   * @brief Loads the eleventh Mapping
   */
  void				loadEleventhMapping(void); 
  /*!
   * @brief Loads the twelfth Mapping
   */
  void				loadTwelfthMapping(void); 
  /*!
   * @brief Loads the thirteenth Mapping
   */
  void				loadThirteenthMapping(void); 
  /*!
   * @brief Loads the fourteenth Mapping
   */
  void				loadFourteenthMapping(void); 
  /*!
   * @brief Loads the fifteenth Mapping
   */
  void				loadFifteenthMapping(void); 
  /*!
   * @brief Loads the Mapping
   * @param
   */
  void				loadMapping(const int);

  /*!
   * @brief Setter for the Erazor
   * @param
   */
  void				setErazor(const int);
  /*!
   * @brief Getter for the Erazor
   * @return
   */
  int				getErazor() const;
  /*!
   * @brief Getter for the pressed Key
   * @return
   */
  gdl::Keys::Key		getPressedKey() const;
  /*!
   * @brief Getter for the Key to eraze
   * @return
   */
  gdl::Keys::Key		getKeyToEraze() const;
  /*!
   * @brief Setter for the Key to eraze
   * @param
   */
  void				setKeyToEraze(const gdl::Keys::Key);
  /*!
   * @brief Setter for the pressed Key
   * @param
   */
  void				setPressedKey(const gdl::Keys::Key);

  /*!
   * @brief Setter for Mapping
   * @param
   */
  void				setIsMapping(const bool);
  /*!
   * @brief Getter for Mapping
   */
  bool				getIsMapping() const;
  /*!
   * @brief Getter for the Player1 Mapping
   */
  AObjectEventList_t*		getPlayer1Mapping() const;
  /*!
   * @brief Getter for the Player2 Mapping
   */
  AObjectEventList_t*		getPlayer2Mapping() const;

  /*!
   * @brief Setter for the Close
   * @param
   */
  void				setClose(const bool v);

private:
  /*!
   * @brief Constructor for MenuEventManager
   */
  MenuEventManager();
  /*!
   * @brief Destructor for MenuEventManager
   */
  ~MenuEventManager();
  /*!
   * @brief Copy Constructor for MenuEventManager
   */
  MenuEventManager(MenuEventManager const &);
  /*!
   * @brief Assignment operator for MenuEventManager
   * @param
   * @return
   */
  MenuEventManager &	operator=(MenuEventManager const &);

private:
  EventListObject_t		*_loaded; /*!< if loaded */
  EventListObject_t		*_firstMapping; /*!< first mapping */
  EventListObject_t		*_secondMapping; /*!< second mapping */
  EventListObject_t		*_thirdMapping; /*!< third mapping */
  EventListObject_t		*_fourthMapping; /*!< fourth mapping */
  EventListObject_t		*_fifthMapping; /*!< fifth mapping */
  EventListObject_t		*_sixthMapping; /*!< sixth mapping */
  EventListObject_t		*_seventhMapping; /*!< seventh mapping */
  EventListObject_t		*_eighthMapping; /*!< eighth mapping */
  EventListObject_t		*_ninthMapping; /*!< ninth mapping */
  EventListObject_t		*_tenthMapping; /*!< tenth mapping */
  EventListObject_t		*_eleventhMapping; /*!< eleventh mapping */
  EventListObject_t		*_twelfthMapping; /*!< twelfth mapping */
  EventListObject_t		*_thirteenthMapping; /*!< thirteenth mapping */
  EventListObject_t		*_fourteenthMapping; /*!< fourteenth mapping */
  EventListObject_t		*_fifteenthMapping; /*!< fifteenth mapping */
  AObjectEventList_t		*_player1Mapping; /*!< player 1 mapping */
  AObjectEventList_t		*_player2Mapping; /*!< player 2 mapping */
  bool				_isMapping; /*!< if is mapping or not */
  KeyManager			*_keyManager; /*!< key manager */
  static MenuEventManager	*_instance; /*!< the instance of this class */
  EventLoadVector_t		_loadVector; /*!< load vector */
  gdl::Keys::Key		_key; /*!< key */
  gdl::Keys::Key		_keyToEraze; /*!< key to eraze */
  int				_erazor; /*!< erazor */
};

#endif

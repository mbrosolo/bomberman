//
// Intro.hh for  in /home/bourdi_b//bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 30 18:17:51 2012 benjamin bourdin
// Last update Sun Jun  3 20:26:13 2012 benjamin bourdin
//

#ifndef __INTRO_HH__
#define __INTRO_HH__

/**
 * @author bourdi_b
 * @file Intro.hh
 */

#include	"AForm.hh"
#include	"MyClock.hh"
#include	"Input.hpp"

namespace graphic
{
  /*!
   * @class Intro
   * @brief Introduction for the Game
   */
  class Intro
  {
  public:
    /*!
     * @brief Constructor for Intro
     */
    Intro();
    /*!
     * @brief Destructor for Intro
     */
    ~Intro();
    /*!
     * @brief Copy Constructor for Intro
     * @param copy The copy
     */
    Intro(Intro const &);
    /*!
     * @brief Assignment operator for Intro
     * @param The copy
     * @return The class
     */
    Intro &operator=(Intro const &);

    /*!
     * @brief Initializes the Introduction
     */
    void	initialize(void);
    /*!
     * @brief Updates
     * @param clock The clock
     * @param input The input
     */
    void	update(MyClock const &, gdl::Input &);
    /*!
     * @brief Draws
     */
    void	draw(void);

    /*!
     * @brief Check if is finished
     */
    bool	isFinished(void) const;

    static const float cameraPosFinal; /*!< Final position of the Camera */
    static const float cameraPosBegin; /*!< First position of the Camera */

  private:
    AForm	*_title; /*!< Title of the Introduction */

    bool	_mode; /*!< If we have to move backward the camera */
    bool	_finished; /*!< If the introduction is finished or not */
  };
}

#endif

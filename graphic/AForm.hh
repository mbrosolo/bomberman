//
// AForm.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May  7 13:37:09 2012 benjamin bourdin
// Last update Sun Jun  3 21:05:06 2012 benjamin bourdin
//

#ifndef __AOBJECT_H__
#define __AOBJECT_H__

/**
 * @author bourdi_b
 * @file AForm.hh
 */

#include	"MyClock.hh"
#include	"Input.hpp"
#include	"TrioDatas.hh"
#include	"Image.hpp"
#include	"Model.hpp"
#include	"Video.hh"

namespace graphic
{
  /*!
   * @brief Typedef for Vector type
   */
  typedef	TrioDatas	Vector;
  /*!
   * @brief Typedef for Color type
   */
  typedef	TrioDatas	Color;
  /*!
   * @brief Typedef for Scale type
   */
  typedef	TrioDatas	Scale;

  /*!
   * @brief The default X position of a AForm
   */
#define	DEFAULT_POS_X	0.0f
  /*!
   * @brief The default Y position of a AForm
   */
#define	DEFAULT_POS_Y	0.0f
  /*!
   * @brief The default Z position of a AForm
   */
#define	DEFAULT_POS_Z	0.0f

  /*!
   * @brief The default X rotation of a AForm
   */
#define	DEFAULT_ROT_X	0.0f
  /*!
   * @brief The default Y rotation of a AForm
   */
#define	DEFAULT_ROT_Y	1.0f
  /*!
   * @brief The default Z rotation of a AForm
   */
#define	DEFAULT_ROT_Z	0.0f

  /*!
   * @brief The default R component color of a AForm
   */
#define	DEFAULT_COLOR_R	1.0f
  /*!
   * @brief The default G component color of a AForm
   */
#define	DEFAULT_COLOR_G	1.0f
  /*!
   * @brief The default B component color of a AForm
   */
#define	DEFAULT_COLOR_B	1.0f

  /*!
   * @brief The default X coefficient scale of a AForm
   */
#define	DEFAULT_SCALE_X	1.0f
  /*!
   * @brief The default Y coefficient scale of a AForm
   */
#define	DEFAULT_SCALE_Y	1.0f
  /*!
   * @brief The default Z coefficient scale of a AForm
   */
#define	DEFAULT_SCALE_Z	1.0f

  /*!
   * @class AForm
   * @brief Define an object's form
   */
  class AForm
  {
  public:
    /*!
     * @brief Constructor of AForm
     * @param pos The position coordonate in space,
     * by default Vector(graphic::DEFAULT_POS_X, graphic::DEFAULT_POS_Y, graphic::DEFAULT_POS_Z)
     * @param rot The rotation axes
     * by default Vector(graphic::DEFAULT_ROT_X, graphic::DEFAULT_ROT_Y, graphic::DEFAULT_ROT_Z)
     * @param color The color RGB components
     * by default Color(graphic::DEFAULT_COLOR_R, graphic::DEFAULT_COLOR_G, graphic::DEFAULT_COLOR_B)
     * @param scale The scale coefficients
     * by default Scale(graphic::DEFAULT_SCALE_X, graphic::DEFAULT_SCALE_Y, graphic::DEFAULT_SCALE_Z))
     */
    AForm(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	  Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	  Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	  Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
    /*!
     * @brief The constructor by copy of AForm
     * @param copy The copy which will be copied
     */
    AForm(AForm const &copy);
    /*!
     * @brief The operator= override
     * @param copy The copy
     * @return The AForm set
     */
    AForm &operator=(AForm const &copy);
    /*!
     * @brief The destructor of AForm
     */
    virtual ~AForm() {}

    /*!
     * @brief Initialize method (virtual pure) of the AForm
     */
    virtual void	initialize(void) = 0;
    /*!
     * @brief Update method (virtual pure) of the AForm
     * @param clock The clock of the game
     * @param input The input
     */
    virtual void	update(MyClock const &clock, gdl::Input &input) = 0;
    /*!
     * @brief Draw method (virtual pure) of the AForm
     */
    virtual void	draw(void) = 0;

    /*!
     * @brief Get the width of the AForm
     * @return The width of the AForm
     */
    virtual float	getWidth(void) const = 0;
    /*!
     * @brief Get the height of the AForm
     * @return The height of the AForm
     */
    virtual float	getHeight(void) const = 0;
    /*!
     * @brief Get the depth of the AForm
     * @return The depth of the AForm
     */
    virtual float	getDepth(void) const = 0;

    /*!
     * @brief Increase the X position of a AForm in space
     * @param nb X position will be add of nb (+=)
     */
    void	incPosX(const float nb);
    /*!
     * @brief Increase the Y position of a AForm in space
     * @param nb Y position will be add of nb (+=)
     */
    void	incPosY(const float nb);
    /*!
     * @brief Increase the Z position of a AForm in space
     * @param nb Z position will be add of nb (+=)
     */
    void	incPosZ(const float nb);

    /*!
     * @brief Increase the X rotation angle of a AForm in space
     * @param nb X rotation angle will be add of nb (+=)
     */
    void	incRotX(const float nb);
    /*!
     * @brief Increase the Y rotation angle of a AForm in space
     * @param nb Y rotation angle will be add of nb (+=)
     */
    void	incRotY(const float nb);
    /*!
     * @brief Increase the Z rotation angle of a AForm in space
     * @param nb Z rotation angle will be add of nb (+=)
     */
    void	incRotZ(const float nb);

    /*!
     * @brief Set a texture to the AForm
     * @param texture The texture already loaded to apply
     */
    void	setTexture(gdl::Image *texture);
    /*!
     * @brief Set a model to the AForm
     * @param model The model already loaded to apply
     */
    void	setModel(gdl::Model *model);
    /*!
     * @brief Set a video to the AForm
     * @param video The video already loaded to apply
     */
    void	setVideo(Video *video);


    /*!
     * @brief Swap position between two AForm
     * @param first The first AForm
     * @param second The second AForm
     */
    void	swapPosition(AForm &first, AForm &second);
    /*!
     * @brief Swap rotation angles between two AForm
     * @param first The first AForm
     * @param second The second AForm
     */
    void	swapRotation(AForm &first, AForm &second);

    /*!
     * @brief Get the position of the AForm
     */
    Vector const&	getPosition(void) const;
    /*!
     * @brief Get the rotation of the AForm
     */
    Vector const&	getRotation(void) const;
    /*!
     * @brief Get the color of the AForm
     */
    Color const&	getColor(void) const;
    /*!
     * @brief Get the scale of the AForm
     */
    Scale const&	getScale(void) const;

    /*!
     * @brief Enable x rotation during the rotation
     */
    void	rotEnableX(void);
    /*!
     * @brief Enable y rotation during the rotation
     */
    void	rotEnableY(void);
    /*!
     * @brief Enable z rotation during the rotation
     */
    void	rotEnableZ(void);

    /*!
     * @brief Disable x rotation during the rotation
     */
    void	rotDisableX(void);
    /*!
     * @brief Disable y rotation during the rotation
     */
    void	rotDisableY(void);
    /*!
     * @brief Disable z rotation during the rotation
     */
    void	rotDisableZ(void);

    /*!
     * @brief Set the scale of the AForm
     * Avoid to use it, it may be dangerous
     * @param scale The scale to set
     */
    void	setScale(Scale const &scale);
    /*!
     * @brief Set the position of the AForm
     * Avoid to use it, it may be dangerous
     * @param pos The position to set
     */
    void	setPosition(Vector const &pos);
    /*!
     * @brief Set the rotation of the AForm
     * Avoid to use it, it may be dangerous
     * @param rot The rotation to set
     */
    void	setRotation(Vector const &rot);
    /*!
     * @brief Set the color of the AForm
     * Avoid to use it, it may be dangerous
     * @param color The color to set
     */
    void	setColor(Color const &color);

    /*!
     * @brief Turn a AForm on the left side of the screen (then it blocks)
     * @param speed The speed of the rotation
     */
    void	turnLeft(const float speed);
    /*!
     * @brief Turn a AForm on the right side of the screen (then it blocks)
     * @param speed The speed of the rotation
     */
    void	turnRight(const float speed);
    /*!
     * @brief Turn a AForm on the top side of the screen (then it blocks)
     * @param speed The speed of the rotation
     */
    void	turnUp(const float speed);
    /*!
     * @brief Turn a AForm on the down side of the screen (then it blocks)
     * @param speed The speed of the rotation
     */
    void	turnDown(const float speed);

    /*!
     * @brief Say if the rotation axe X is enable or not
     * @return True if it is, otherwise false
     */
    bool	isRotXEnable(void) const;
    /*!
     * @brief Say if the rotation axe Y is enable or not
     * @return True if it is, otherwise false
     */
    bool	isRotYEnable(void) const;
    /*!
     * @brief Say if the rotation axe Z is enable or not
     * @return True if it is, otherwise false
     */
    bool	isRotZEnable(void) const;

    /*!
     * @brief Get a texture
     * @return NULL if it does not exist, or the texture
     */
    gdl::Image	*getTexture(void) const;
    /*!
     * @brief Get a model
     * @return NULL if it does not exist, or the texture
     */
    gdl::Model	*getModel(void) const;
    /*!
     * @brief Get a video
     * @return NULL if it does not exist, or the texture
     */
    Video	*getVideo(void) const;

  protected:
    Vector	position_; /*!< The position coordinates */
    Vector	rotation_; /*!< The rotation coordinates */
    Color	color_; /*!< The color RGB */
    Scale	scale_; /*!< The scale coefficient */
    Vector	rotationEnable_; /*!< Enable value rotation x/y/z during the rotaion */
    gdl::Image	*texture_; /*!< The texture of the form */
    gdl::Model	*model_; /*!< The model of the form */
    Video	*video_; /*!< The video as a texture */

  };
}

#endif

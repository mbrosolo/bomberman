//
// GameEventManager.hh for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 19:35:22 2012 tosoni
// Last update Sun Jun  3 18:07:23 2012 Sarglen
//

#ifndef			__GAMEEVENTMANAGER_HH__
#define			__GAMEEVENTMANAGER_HH__

/**
 * @author tosoni_t
 * @file GameEventManager.hh
 */

#include		<map>
#include		<iostream>
#include		"Input.hpp"
#include		"AObject.hh"
#include		"KeyManager.hh"

/*!
 * @brief Typedef for Member function pointer
 */
typedef void (AObject::*GAMEPTR)(void);
/*!
 * @brief Typedef for the Event list of the Objects
 */
typedef	std::map<gdl::Keys::Key, GAMEPTR > AObjectEventList_t;

/*!
 * @class GameEventManager
 * @brief Event Manager for the Game
 */
class			GameEventManager
{
public:
  /*!
   * @brief Constructor for GameEventManager
   */
  GameEventManager();
  /*!
   * @brief Destructor for GameEventManager
   */
  ~GameEventManager();
  /*!
   * @brief Copy Constructor for GameEventManager
   */
  GameEventManager(GameEventManager const &);
  /*!
   * @brief Assignment operator for GameEventManager
   */
  GameEventManager &	operator=(GameEventManager const &);

  /*!
   * @brief Setter for the map
   */
  void				setMap(AObjectEventList_t*);
  /*!
   * @brief Getter for the map
   */
  AObjectEventList_t*		getMap() const;
  /*!
   * @brief Checks the Event List of the Object
   */
  void				checkEvents(AObject*);
  /*!
   * @brief Setter for the Loader
   */
  void				setLoaded();

private:
  AObjectEventList_t		*_playerMapping; /*!< Mapping for the player */
  KeyManager			*_keyManager; /*!< Key manager */
  gdl::Keys::Key		_leave; /*!< Key for leaving */
};

#endif

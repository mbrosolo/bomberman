//
// graphicTriple.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Fri May 11 23:18:04 2012 benjamin bourdin
// Last update Thu May 17 12:55:55 2012 benjamin bourdin
//

#ifndef	__GRAPHIC_TRIPLET_HH__
#define __GRAPHIC_TRIPLET_HH__

/**
 * @author bourdi_b
 * @file TrioDatas.hh
 */

namespace graphic
{

  /*!
   * @struct TrioDatas
   * @brief Contain three datas, usefull to represent a vector3D, a trio of Color, ...
   */
  struct TrioDatas
  {
    float	first; /*!< The first element */
    float	second; /*!< The second element */
    float	third; /*!< The third element */

    /*!
     * @brief Constructor of TrioDatas
     */
    TrioDatas();
    /*!
     * @brief Constructor by copy of TrioDatas
     * @param copy The copy
     */
    TrioDatas(TrioDatas const &copy);
    /*!
     * @brief Destructor of TrioDatas
     */
    ~TrioDatas();
    /*!
     * @brief Constructor of TrioDatas with arguments
     * @param f The first element
     * @param s The second element
     * @param t The third element
     */
    TrioDatas(const float f, const float s, const float t);

  };

}

#endif

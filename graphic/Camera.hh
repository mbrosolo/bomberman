//
// Camera.hh for Camera in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Mon May  7 11:49:22 2012 tosoni
// Last update Sun Jun  3 20:58:41 2012 benjamin bourdin
//

#ifndef __CAMERA_HH__
#define __CAMERA_HH__

/**
 * @author thomas tosoni
 * @file Camera.hh
 */

#include "Input.hpp"
#include "TrioDatas.hh"
#include "MyClock.hh"

namespace graphic
{
  /*!
   * @brief Typedef for a vector
   */
  typedef	TrioDatas	Vector;
  /*!
   * @brief Typedef for a Color
   */
  typedef	TrioDatas	Color;
  /*!
   * @brief Typedef for a Scale
   */
  typedef	TrioDatas	Scale;

  /*!
   * @class Camera
   * @brief Class Camera
   */
  class	Camera
  {

  /*!
   * @brief Enum for Camera's position
   */
    enum PositionCamera
      {
	XMENU_POS = 0,
	YMENU_POS = 0,
	ZMENU_POS = 2000,
	XGAME_POS = 0,
	YGAME_POS = 0,
	ZGAME_POS = 900
      };

  /*!
   * @brief Enum for Camera's rotation
   */
    enum RotationCamera
      {
	XMENU_ROT = 0,
	YMENU_ROT = 0,
	ZMENU_ROT = 0,
	XGAME_ROT = 0,
	YGAME_ROT = 0,
	ZGAME_ROT = 0
      };

  public:
    /*!
     * @brief Constructor of Camera
     */
    Camera(void);

    /*!
     * @brief The destructor of Camera
     */
    ~Camera();

    /*!
     * @brief Initialize method of the Camera
     */
    void initialize(void);
    /*!
     * @brief Update method of the Camera
     * @param clock The clock of the game
     * @param input The input
     */
    void update(MyClock const &clock, gdl::Input &input);

    /*!
     * @brief SwitchToOrtho method of the Camera
     */
    static void	switchToOrtho();
    /*!
     * @brief SwitchToPerspective method of the Camera
     */
    static void	switchBackToPerspective();

    /*!
     * @brief incPosX method of the Camera
     * @param inc The angle of rotation
     */
    void	incPosX(const float);
    /*!
     * @brief incPosY method of the Camera
     * @param inc The angle of rotation
     */
    void	incPosY(const float);
    /*!
     * @brief incPosZ method of the Camera
     * @param inc The angle of rotation
     */
    void	incPosZ(const float);

    /*!
     * @brief incLx method of the Camera
     * @param inc The angle of rotation
     */
    void	incLx(const float);
    /*!
     * @brief incLz method of the Camera
     * @param inc The angle of rotation
     */
    void	incLz(const float);

    /*!
     * @brief setPos method of the Camera
     * @param pos The vector position
     */
    void	setPos(Vector const & pos);
    /*!
     * @brief setPos method of the Camera
     * @return The vector position registered
     */
    Vector const &	getPos(void) const;

    /*!
     * @brief resetToMenu method of the Camera
     */
    void	resetToMenu(void);
    /*!
     * @brief resetToGame method of the Camera
     */
    void	resetToGame(void);

  protected:
    Vector	position_; /*!< The position coordinates */

  private:
    Vector	rotation_; /*!< The rotation coordonates */
    float	_lx;
    float	_lz;

  public:
    static const float cameraPosFinal; /*!< The final camera position */
    static const float cameraPosBegin; /*!< The started position camera */
  };
}

#endif //Camera.hh

//
// Option.cpp for Option in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 19:20:17 2012 tosoni
// Last update Sun Jun  3 22:29:25 2012 benjamin bourdin
//

#include	"Option.hh"

Option::Option(std::string const & n, graphic::AForm *bg, graphic::AForm *s,
	       const gdl::Keys::Key key, const float ypos, const float nb)
  : _stringName(n),
    _name(NULL),
    background(bg),
    selected(s),
    _optionNb(nb),
    _locked(false),
    _key(key),
    _keyText(NULL),
    _validate(false)
{
  graphic::Vector position = bg->getPosition();

  _name = new graphic::Text(graphic::Vector(position.first + ypos, position.second - 30.f,
					    position.third + 20.f),
			    graphic::Vector(0.0f, 1.0f, 0.0f),
			    graphic::Color(1.0f, 1.0f, 0.0f), graphic::Scale(0.5f, 0.5f, 0.5f));
  _name->updateText(n);
  _keyText = new graphic::Text(graphic::Vector(position.first + 120.f, position.second - 30.f,
					       position.third + 100.f),
			       graphic::Vector(0.0f, 1.0f, 0.0f),
			       graphic::Color(1.0f, 1.0f, 0.0f),
			       graphic::Scale(0.5f, 0.5f, 0.5f));
  _keyText->updateText(KeyManager::getInstance()->getStringKey(key));
  _name->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("TEXT_TABLE"));
  _keyText->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("TEXT_TABLE"));
}

Option::~Option()
{
}

Option::Option(Option const & other)
  : _stringName(other._stringName),
    _name(other._name),
    background(other.background),
    selected(other.selected),
    _optionNb(other._optionNb),
    _locked(other._locked),
    _key(other._key),
    _keyText(other._keyText),
    _validate(other._validate)
{
}

Option&			Option::operator=(Option const & other)
{
  if (this != &other)
    {
      this->_name = other._name;
      this->background = other.background;
      this->selected = other.selected;
      this->_optionNb = other._optionNb;
      this->_locked = other._locked;
      this->_key = other._key;
      this->_validate = other._validate;
    }
  return (*this);
}

void			Option::drawBackground() const
{
  background->draw();
}

void			Option::drawSelected() const
{
  selected->draw();
}

bool			Option::getValidate() const
{
  return (_validate);
}

void			Option::setValidate(const bool _v)
{
  _validate = _v;
}

void			Option::changePosition(const float x, const float y, const float z)
{
  background->incPosX(x);
  background->incPosY(y);
  background->incPosZ(z);
  selected->incPosX(x);
  selected->incPosY(y);
  selected->incPosZ(z);
  _name->incPosX(x);
  _name->incPosY(y);
  _name->incPosZ(z);
  _keyText->incPosX(x);
  _keyText->incPosY(y);
  _keyText->incPosZ(z);
}

void			Option::setKeyText(std::string const & str)
{
  _keyText->updateText(str);
}

void			Option::changeOptionNb(const float o)
{
  _optionNb += o;
}

void			Option::setOptionNb(const float o)
{
  _optionNb = o;
}

graphic::Text&		Option::getName() const
{
  return (*_name);
}

float			Option::getOptionNb() const
{
  return (_optionNb);
}

graphic::AForm&		Option::getBackground() const
{
  return (*background);
}

graphic::AForm&		Option::getSelected() const
{
  return (*selected);
}

void			Option::drawText() const
{
  _name->draw();
}

std::string const &		Option::getStringName() const
{
  return (_stringName);
}

gdl::Keys::Key		Option::getKey() const
{
  return (_key);
}

graphic::Text*		Option::getKeyText() const
{
  return (_keyText);
}

void			Option::setKey(const gdl::Keys::Key k)
{
  _key = k;
}

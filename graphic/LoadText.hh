//
// LoadText.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sun May 13 21:58:28 2012 benjamin bourdin
// Last update Sun Jun  3 20:32:26 2012 benjamin bourdin
//

#ifndef __LOAD_TEXT_HH__
#define __LOAD_TEXT_HH__

/**
 * @author bourdi_b
 * @file LoadText.hh
 */

#include	<map>
#include	<vector>
#include	<string>

namespace graphic
{

  /*!
   * @class LoadText
   * @brief Manipule an image to print a character given with the font of the image
   */
  class LoadText
  {
  private:
    /*!
     * @brief The correspondance between a character and its coordonates
     */
    typedef std::map<char, std::vector<float> > text_coord_t;

  public:
    /*!
     * @brief Get an instance of LoadText
     * @return The singleton class
     */
    static LoadText	*getInstance(void);
    /*!
     * @brief Kill the instance of LoadText
     */
    static void		killInstance(void);

    /*!
     * @brief Stock the coordonates of a character.
     * It has to be calles before getters
     * @param c The character to keep back coordonates on the image
     */
    void	loadCharacter(char const c);

    /*!
     * @brief Get the x0 data of a character on the texture
     */
    float	getX0(void) const;
    /*!
     * @brief Get the x1 data of a character on the texture
     */
    float	getX1(void) const;
    /*!
     * @brief Get the y0 data of a character on the texture
     */
    float	getY0(void) const;
    /*!
     * @brief Get the y1 data of a character on the texture
     */
    float	getY1(void) const;

    /*!
     * @brief Get the width of a letter.
     * Do not trust it entirely... because of some bugs on calculs
     */
    float	getWidth(void) const;
    /*!
     * @brief Get the height of a letter.
     * Do not trust it entirely... because of some bugs on calculs
     */
    float	getHeight(void) const;

  private:
    float	_x0; /*!< the x0 coordonate of the character loaded */
    float	_x1; /*!< the x1 coordonate of the character loaded */
    float	_y0; /*!< the y0 coordonate of the character loaded */
    float	_y1; /*!< the y1 coordonate of the character loaded */
    text_coord_t	_corresp; /*!< the correspondance between each characters and heir coordonates*/

    const float	_width; /*!< The width of a lettre */
    const float	_height; /*!< The height of a lettre */

    static LoadText	*_instance; /*!< The instance of the singleton */

    /*!
     * @brief Constructor (private) of LoadText
     */
    LoadText();
    /*!
     * @brief Destructor (private) of LoadText
     */
    ~LoadText();

  };
}

#endif

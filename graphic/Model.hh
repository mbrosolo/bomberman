//
// Model.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May  9 16:21:06 2012 benjamin bourdin
// Last update Sun Jun  3 19:59:35 2012 Sarglen
//

#ifndef __MODEL_HH__
#define __MODEL_HH__

/**
 * @author bourdi_b
 * @file Model.hh
 */

#include	"AForm.hh"
#include	"Input.hpp"
#include	"Model.hpp"

namespace graphic
{
  /*!
   * @class Model
   * @brief Graphic Model
   */
  class Model : public AForm
  {
  public:
    /*!
     * @brief Constructor for Model
     * @param
     * @param
     * @param
     * @param
     */
    Model(Vector const &pos = Vector(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_POS_Z),
	  Vector const &rot = Vector(DEFAULT_ROT_X, DEFAULT_ROT_Y, DEFAULT_ROT_Z),
	  Color const &color = Color(DEFAULT_COLOR_R, DEFAULT_COLOR_G, DEFAULT_COLOR_B),
	  Scale const &scale = Scale(DEFAULT_SCALE_X, DEFAULT_SCALE_Y, DEFAULT_SCALE_Z));
    /*!
     * @brief Destructor for Model
     */
    ~Model(void);
    /*!
     * @brief Copy Constructor for Model
     */
    Model(Model const &);
    /*!
     * @brief Assignment operator for Model
     */
    Model &operator=(Model const &);

    /*!
     * @brief Initializes the Model
     */
    void initialize(void);
    /*!
     * @brief Updates the Model
     * @param
     */
    void update(MyClock const &, gdl::Input &);
    /*!
     * @brief Draws the Model
     */
    void draw(void);

    /*!
     * @brief Getter for the Width
     * @return
     */
    float	getWidth(void) const;
    /*!
     * @brief Getter for the Height
     * @return
     */
    float	getHeight(void) const;
    /*!
     * @brief Getter for the Depth
     * @return
     */
    float	getDepth(void) const;

  private:
    const float	_width; /*!< width of the model */
    const float	_height; /*!< height of the model */
    const float	_depth; /*!< deth of the model */
  };
}

#endif

//
// AForm.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May  9 13:50:35 2012 benjamin bourdin
// Last update Sun Jun  3 20:56:38 2012 benjamin bourdin
//

#include	"AForm.hh"

graphic::AForm::AForm(Vector const &pos, Vector const &rot,
	Color const &color, Scale const &scale)
  : position_(pos), rotation_(rot), color_(color), scale_(scale),
    rotationEnable_(0.0f, 1.0f, 0.0f),
    texture_(NULL), model_(NULL), video_(NULL)
{
}

graphic::AForm::AForm(AForm const &t)
  : position_(t.position_), rotation_(t.rotation_), color_(t.color_),
    scale_(t.scale_),
    rotationEnable_(t.rotationEnable_.first, t.rotationEnable_.second, t.rotationEnable_.third),
    texture_(t.texture_), model_(t.model_), video_(t.video_)
{
}

graphic::AForm&	graphic::AForm::operator=(AForm const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  rotationEnable_ = t.rotationEnable_;
  video_ = t.video_;
  return *this;
}


void	graphic::AForm::incPosX(const float inc)
{
  position_.first += inc;
}

void	graphic::AForm::incPosY(const float inc)
{
  position_.second += inc;
}

void	graphic::AForm::incPosZ(const float inc)
{
  position_.third += inc;
}


void	graphic::AForm::incRotX(const float inc)
{
  rotation_.first = static_cast<float>(static_cast<int>(rotation_.first + inc) % 360);
  if (rotation_.first < 0)
    rotation_.first = 360 + rotation_.first;
}

void	graphic::AForm::incRotY(const float inc)
{
  rotation_.second = static_cast<float>(static_cast<int>(rotation_.second + inc) % 360);
  if (rotation_.second < 0)
    rotation_.second = 360 + rotation_.second;
}

void	graphic::AForm::incRotZ(const float inc)
{
  rotation_.third = static_cast<float>(static_cast<int>(rotation_.third + inc) % 360);
  if (rotation_.third < 0)
    rotation_.third = 360 + rotation_.third;
}

void	graphic::AForm::setTexture(gdl::Image *texture)
{
  texture_ = texture;
}

void	graphic::AForm::setModel(gdl::Model *model)
{
  model_ = model;
}

void	graphic::AForm::setVideo(Video *video)
{
  video_ = video;
}

void	graphic::AForm::setScale(Scale const &sc)
{
  scale_ = sc;
}

void	graphic::AForm::setPosition(Vector const &pos)
{
  position_ = pos;
}

void	graphic::AForm::setRotation(Vector const &rot)
{
  rotation_ = rot;
}

void	graphic::AForm::setColor(Color const &col)
{
  color_ = col;
}

void	graphic::AForm::swapPosition(AForm &form1, AForm &form2)
{
  Vector tmp = form1.position_;

  form1.position_ = form2.position_;
  form2.position_ = tmp;
}

void	graphic::AForm::swapRotation(AForm &form1, AForm &form2)
{
  Vector tmp = form1.rotation_;

  form1.rotation_ = form2.rotation_;
  form2.rotation_ = tmp;
}

graphic::Vector const&	graphic::AForm::getPosition(void) const
{
  return position_;
}

graphic::Vector const&	graphic::AForm::getRotation(void) const
{
  return rotation_;
}

graphic::Color const&	graphic::AForm::getColor(void) const
{
  return color_;
}

graphic::Scale const&	graphic::AForm::getScale(void) const
{
  return scale_;
}

void	graphic::AForm::rotEnableX(void)
{
  rotationEnable_.first = 1.0f;
}

void	graphic::AForm::rotEnableY(void)
{
  rotationEnable_.second = 1.0f;
}

void	graphic::AForm::rotEnableZ(void)
{
  rotationEnable_.third = 1.0f;
}

void	graphic::AForm::rotDisableX(void)
{
  rotationEnable_.first = 0.0f;
}

void	graphic::AForm::rotDisableY(void)
{
  rotationEnable_.second = 0.0f;
}

void	graphic::AForm::rotDisableZ(void)
{
  rotationEnable_.third = 0.0f;
}

bool	graphic::AForm::isRotXEnable(void) const
{
  if (rotationEnable_.first == 1.0f)
    return true;
  return false;
}

bool	graphic::AForm::isRotYEnable(void) const
{
  if (rotationEnable_.second == 1.0f)
    return true;
  return false;
}

bool	graphic::AForm::isRotZEnable(void) const
{
  if (rotationEnable_.third == 1.0f)
    return true;
  return false;
}

gdl::Image	*graphic::AForm::getTexture(void) const
{
  return texture_;
}

gdl::Model	*graphic::AForm::getModel(void) const
{
  return model_;
}

graphic::Video	*graphic::AForm::getVideo(void) const
{
  return video_;
}


void	graphic::AForm::turnRight(const float speed)
{
  const float axe = 90;
  if (getRotation().second >= 270 || getRotation().second <= 90)
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(1.0f);
      }
    }
  else
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(-1.0f);
      }
    }
  rotEnableY();
}

void	graphic::AForm::turnUp(const float speed)
{
  const float axe = 180;
  if (getRotation().second <= 180)
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(1.0f);
      }
    }
  else
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(-1.0f);
      }
    }
  rotEnableY();
}


void	graphic::AForm::turnDown(const float speed)
{
  const float axe = 0;
  if (getRotation().second >= 180)
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(1.0f);
      }
    }
  else
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(-1.0f);
      }
    }
  rotEnableY();
}

void	graphic::AForm::turnLeft(const float speed)
{
  const float axe = 270;
  if (getRotation().second >= 90 && getRotation().second <= 270)
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(1.0f);
      }
    }
  else
    {
      for (float i = 0.0f; i < speed && getRotation().second != axe; i++) {
	incRotY(-1.0f);
      }
    }
  rotEnableY();
}

//
// PauseMenu.cpp for bomberman in /home/tosoni/BitBucket/bomberman
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Tue May 22 15:24:13 2012 tosoni
// Last update Sun Jun  3 22:27:20 2012 benjamin bourdin
//

#include	"Save.hh"
#include	"Menu.hh"
#include	"Core.hh"
#include	"MenuEventManager.hh"
#include	"PauseMenu.hh"

PauseMenu	*PauseMenu::_instance = NULL;

PauseMenu::PauseMenu()
  : _options(new OptionList_t()),
    _background(NULL)
{
}

PauseMenu::~PauseMenu()
{
}

PauseMenu	*PauseMenu::getInstance(void)
{
  if (_instance == NULL)
    _instance = new PauseMenu();
  return (_instance);
}

void		PauseMenu::killInstance(void)
{
  if (_instance != NULL)
    delete _instance;
  _instance = NULL;
}

void		PauseMenu::addOption(std::string const & _s, graphic::AForm *bg, float v)
{
  graphic::Scale scale = bg->getScale();
  graphic::Vector position = bg->getPosition();

  scale.first += 0.2f;
  position.first += 30.f;

  graphic::AForm *s = new graphic::Primitive::Rectangle(position,
							graphic::Vector(0.f, 0.f, 0.f),
							graphic::Color(0.f, 0.f, 0.f),
							scale);
  s->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("LONG"));
  _options->push_back(new Option(_s, bg, s, gdl::Keys::Delete, -100., v));
}

void		PauseMenu::display(void)
{
  _background->draw();
  for (OptionList_t::iterator it = _options->begin(); it != _options->end(); ++it)
    {
      if ((*it)->getOptionNb() == 1)
	(*it)->drawSelected();
      else
	(*it)->drawBackground();
      (*it)->drawText();
    }
}

OptionList_t&		PauseMenu::getOptions() const
{
  return (*_options);
}

graphic::AForm*		PauseMenu::makeRectangle(const float x, const float y, const float z,
						 const float xs, const float ys, const float zd,
						 std::string const & texture)
{
  graphic::AForm *Form = new graphic::Primitive::Rectangle(graphic::Vector(x, y, z),
  							   graphic::Vector(0, 0, 0),
  							   graphic::Color(1, 1, 1),
  							   graphic::Scale(xs, ys, zd));
  Form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture(texture));
  return (Form);
}

void			PauseMenu::resume()
{
  MenuEventManager::getInstance()->loadMapping(4);
  graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->resetToGame();
  if (Menu::getInstance()->getPlayer() == 2)
    graphic::MyGame::getInstance()->getCameraManagement()->getCamera2()->resetToGame();
  graphic::MyGame::place = graphic::MyGame::GAME;
}

void			PauseMenu::restart()
{
  GameEventManager	*manager1 = new GameEventManager();

  const size_t w = Core::getInstance()->getWidth();
  const size_t h = Core::getInstance()->getHeight();
  const size_t nblua = Core::getInstance()->getCpu().size();

  Core::killInstance();
  Core::getInstance()->initialize(w, h);
  manager1->setMap(MenuEventManager::getInstance()->getPlayer1Mapping());
  Core::getInstance()->addPlayer(manager1);
  graphic::MyGame::getInstance()->getCameraManagement()->getCamera1()->resetToGame();
  if (Menu::getInstance()->getPlayer() == 2)
    {
      GameEventManager	*manager2 = new GameEventManager();
      manager2->setMap(MenuEventManager::getInstance()->getPlayer2Mapping());
      Core::getInstance()->addPlayer(manager2);
      graphic::MyGame::getInstance()->getCameraManagement()->enableMulti();
      graphic::MyGame::getInstance()->getCameraManagement()->getCamera2()->resetToGame();
    }
  else
    graphic::MyGame::getInstance()->getCameraManagement()->enableSingle();
  MenuEventManager::getInstance()->loadMapping(4);

  graphic::MyGame::place = graphic::MyGame::TRANSITION;
  Core::getInstance()->run(nblua);
  Core::getInstance()->setTotalTime(0.0f);
  Core::getInstance()->setReferenceTime(graphic::MyGame::getInstance()->getMyClock());
}


namespace pauseMenu_utils
{
  bool	_matchInvalidCharacter(const char c)
  {
    if (c == '/' || c == ' ')
      return true;
    return false;
  }
}

void			PauseMenu::save()
{
  static int nb = -1;

  if (nb == -1)
    nb = Menu::getInstance()->getGameLoader()->getSaves().size();

  Save	save;
  std::string file("Save");
  std::stringstream ss;
  std::string tmp;
  ss << nb;
  ss >> tmp;
  file = file + tmp;
  std::replace_if(file.begin(), file.end(),
		  &pauseMenu_utils::_matchInvalidCharacter, ',');
  save.saveGame((std::string("saves/") + file) + std::string(".sav"));
  nb++;
}

void			PauseMenu::volume()
{
  MenuEventManager::getInstance()->loadMapping(7);
}

void			PauseMenu::quit()
{
  int			i = 1;

  Core::killInstance();
  for (OptionList_t::const_iterator it = _options->begin(); it != _options->end(); ++it)
    (*it)->setOptionNb(i++);
  MenuEventManager::getInstance()->loadMapping(1);
  Menu::getInstance()->unlockMenu(0.f);
  graphic::MyGame::place = graphic::MyGame::MENU;
  graphic::MyGame::getInstance()->getSoundManager()->stop("INGAME");
  graphic::MyGame::getInstance()->getSoundManager()->play("MENU");
  Core::killInstance();
}

void			PauseMenu::initialize()
{
  graphic::AForm *Opt1 = makeRectangle(-30.0f, 250.0f, 0.0f, 3.0f, 0.5f, 1.0f, "SHORT");
  graphic::AForm *Opt2 = makeRectangle(-30.0f, 100.0f, 0.0f, 3.0f, 0.5f, 1.0f, "SHORT");
  graphic::AForm *Opt3 = makeRectangle(-30.0f, -50.0f, 0.0f, 3.0f, 0.5f, 1.0f, "SHORT");
  graphic::AForm *Opt4 = makeRectangle(-30.0f, -200.0f, 0.0f, 3.0f, 0.5f, 1.0f, "SHORT");
  graphic::AForm *Opt5 = makeRectangle(-30.0f, -350.0f, 0.0f, 3.0f, 0.5f, 1.0f, "SHORT");

  _background = makeRectangle(0.0f, 0.0f, 0.0f, 3.2f, 6.0f, 1.0f, "PAUSEMENU");
  addOption("Resume", Opt1, 1.f);
  addOption("Restart", Opt2, 2.f);
  addOption("Save", Opt3, 3.f);
  addOption("Volume", Opt4, 4.f);
  addOption("Quit", Opt5, 5.f);
}

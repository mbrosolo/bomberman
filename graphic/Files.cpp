//
// Files.cpp for bomberman in /home/tosoni_t/BitBucket/bomberman/graphic
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Thu May 31 17:11:07 2012 tosoni_t
// Last update Sun Jun  3 21:02:29 2012 benjamin bourdin
//

#include		"MyGame.hh"
#include		"Files.hh"

Files::Files(std::string const & n,  graphic::AForm *bg, graphic::AForm *s, const float nb, const float nb2)
  : _name(n),
    _nameText(NULL),
    _background(bg),
    _selected(s),
    _fileNb(nb),
    _fixedNb(nb2),
    _validate(false)
{
  graphic::Vector position = bg->getPosition();

  _nameText = new graphic::Text(graphic::Vector(position.first - 220.f, position.second - 30.f,
						position.third + 10.f),
				graphic::Vector(0.0f, 1.0f, 0.0f),
				graphic::Color(1.0f, 1.0f, 0.0f),
				graphic::Scale(0.5f, 0.5f, 0.5f));
  _nameText->updateText(n);
  _nameText->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("TEXT_TABLE"));
}

Files::~Files()
{
  delete _background;
  delete _selected;
  delete _nameText;
}

std::string const &	Files::getName() const
{
  return (_name);
}

int			Files::getFixedNb() const
{
  return (_fixedNb);
}

float			Files::getFileNb() const
{
  return (_fileNb);
}

bool			Files::getValidate() const
{
  return (_validate);
}

graphic::AForm&		Files::getBackground() const
{
  return (*_background);
}

graphic::AForm&		Files::getSelected() const
{
  return (*_selected);
}

void			Files::setName(std::string const & _n)
{
  _name = _n;
}

void			Files::setFileNb(const float _v)
{
  _fileNb = _v;
}

void			Files::setValidate(const bool _v)
{
  _validate = _v;
}

void			Files::drawBackground(void) const
{
  _background->draw();
}

void			Files::drawSelected(void) const
{
  _selected->draw();
}

void			Files::drawText(void) const
{
  _nameText->draw();
}

void			Files::changeFileNb(const float v)
{
  _fileNb += v;
}

void			Files::changePosition(const float x, const float y, const float z)
{
  _background->incPosX(x);
  _background->incPosY(y);
  _background->incPosZ(z);
  _selected->incPosX(x);
  _selected->incPosY(y);
  _selected->incPosZ(z);
  _nameText->incPosX(x);
  _nameText->incPosY(y);
  _nameText->incPosZ(z);
}

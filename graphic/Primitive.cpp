//
// Primitive.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May  7 13:31:48 2012 benjamin bourdin
// Last update Sun Jun  3 22:09:02 2012 benjamin bourdin
//

#include	<GL/gl.h>
#include	<GL/glu.h>
#include	"Primitive.hh"

graphic::Primitive::Triangle::Triangle(Vector const & pos, Vector const & rot,
				       Color const &color, Scale const &scale)
  : AForm(pos, rot, color, scale), _width(300.0f), _height(300.0f), _depth(300.0f)
{
}

graphic::Primitive::Triangle::~Triangle()
{
}

graphic::Primitive::Triangle::Triangle(Triangle const &t)
  : AForm(t.position_, t.rotation_, t.color_, t.scale_), _width(t._width), _height(t._height), _depth(t._depth)
{
  setTexture(t.texture_);
  setVideo(t.video_);
  setModel(t.model_);
}

graphic::Primitive::Triangle&	graphic::Primitive::Triangle::operator=(Triangle const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  video_ = t.video_;
  return *this;
}

float	graphic::Primitive::Triangle::getWidth(void) const
{
  return _width;
}

float	graphic::Primitive::Triangle::getHeight(void) const
{
  return _height;
}

float	graphic::Primitive::Triangle::getDepth(void) const
{
  return _depth;
}

void	graphic::Primitive::Triangle::initialize(void)
{
}

void	graphic::Primitive::Triangle::update(MyClock const & gameClock, gdl::Input & input)
{
  static_cast<void>(gameClock);
  static_cast<void>(input);
  if (video_)
    video_->captureImg(gameClock);
}

void	graphic::Primitive::Triangle::draw(void)
{
  if (texture_)
    {
      glEnable(GL_TEXTURE_2D);
      texture_->bind();
    }
  if (video_)
    video_->loadImg();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.first, position_.second, position_.third);

  if (rotationEnable_.first)
    glRotatef(rotation_.first, 1.0f, 0.0f, 0.0f);
  if (rotationEnable_.second)
    glRotatef(rotation_.second, 0.0f, 1.0f, 0.0f);
  if (rotationEnable_.third)
    glRotatef(rotation_.third, 0.0f, 0.0f, 1.0f);

  glScalef(scale_.first, scale_.second, scale_.third);
  glBegin(GL_TRIANGLES);
  glColor3f(color_.first, color_.second, color_.third);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(0.0f, _height / 2.0f, _depth);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, 0.0f, _depth);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, 0.0f, _depth);

  glEnd();
  glPopMatrix();
  if (video_ || texture_)
    glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
}



graphic::Primitive::Rectangle::Rectangle(Vector const & pos, Vector const & rot,
				       Color const &color, Scale const &scale)
  : AForm(pos, rot, color, scale), _width(300.0f), _height(200.0f), _depth(0.0f)
{
}

graphic::Primitive::Rectangle::~Rectangle()
{
}

graphic::Primitive::Rectangle::Rectangle(Rectangle const &t)
  : AForm(t.position_, t.rotation_, t.color_, t.scale_), _width(t._width), _height(t._height), _depth(t._depth)
{
  setTexture(t.texture_);
  setModel(t.model_);
  setVideo(t.video_);
}

graphic::Primitive::Rectangle&	graphic::Primitive::Rectangle::operator=(Rectangle const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  video_ = t.video_;
  return *this;
}

float	graphic::Primitive::Rectangle::getWidth(void) const
{
  return _width;
}

float	graphic::Primitive::Rectangle::getHeight(void) const
{
  return _height;
}

float	graphic::Primitive::Rectangle::getDepth(void) const
{
  return _depth;
}

void	graphic::Primitive::Rectangle::initialize(void)
{
}

void	graphic::Primitive::Rectangle::update(MyClock const & gameClock, gdl::Input & input)
{
  static_cast<void>(gameClock);
  static_cast<void>(input);
  if (video_)
    video_->captureImg(gameClock);
}

void	graphic::Primitive::Rectangle::draw(void)
{
  if (texture_)
    {
      glEnable(GL_TEXTURE_2D);
      texture_->bind();
    }
  if (video_)
    video_->loadImg();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.first, position_.second, position_.third);
  if (rotationEnable_.first)
    glRotatef(rotation_.first, 1.0f, 0.0f, 0.0f);
  if (rotationEnable_.second)
    glRotatef(rotation_.second, 0.0f, 1.0f, 0.0f);
  if (rotationEnable_.third)
    glRotatef(rotation_.third, 0.0f, 0.0f, 1.0f);

  glScalef(scale_.first, scale_.second, scale_.third);
  glBegin(GL_QUADS);
  glColor3f(color_.first, color_.second, color_.third);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, _depth);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, 0.0f);

  glEnd();
  glPopMatrix();
  if (video_ || texture_)
    glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
}

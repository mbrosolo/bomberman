//
// CameraManagement.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May 28 22:13:33 2012 benjamin bourdin
// Last update Sun Jun  3 20:13:09 2012 benjamin bourdin
//

#ifndef __CAMERA_MANAGEMENT_HH__
#define __CAMERA_MANAGEMENT_HH__

/**
 * @author bourdi_b
 * @file CameraManagement.hh
 */

#include	"Camera.hh"
#include	"APlayer.hh"

namespace graphic
{
  /*!
   * @class CameraManagement
   * @brief Defines the management of the Camera
   */
  class CameraManagement
  {
  public:
    /*!
     * @brief Enables the multi-Cameras
     */
    void	enableMulti(void);
    /*!
     * @brief Enables the single Camera
     */
    void	enableSingle(void);

    /*!
     * @brief Checks if the multi-Cameras is enabled
     */
    bool	isMulti(void) const;

    /*!
     * @brief Getter for Camera 1
     * @return The Camera 1
     */
    Camera	*getCamera1(void) const;
    /*!
     * @brief Getter for Camera 2
     * @return The Camera 2
     */
    Camera	*getCamera2(void) const;

    /*!
     * @brief Moves the Camera 1
     * @param player The player to follow
     */
    void	moveCamera1(APlayer const *const player);
    /*!
     * @brief Moves the Camera 2
     * @param player The player to follow
     */
    void	moveCamera2(APlayer const * const player);

  public:
    /*!
     * @brief Constructor for CameraManagement
     */
    CameraManagement();
    /*!
     * @brief Destructor for CameraManagement
     */
    ~CameraManagement();
    /*!
     * @brief Assignement operator for CameraManagement
     * @param copy The copy
     * @return The class CameraManagement
     */
    CameraManagement &operator=(CameraManagement const &copy);
    /*!
     * @brief Copy Constructor for CameraManagement
     * @param copy The copy
     */
    CameraManagement(CameraManagement const &copy);

    bool			_multi; /*!< Boolean for multi-Cameras enabled or not */

    Camera			*_camera1; /*!< Pointer of Camera 1 */
    Camera			*_camera2; /*!< Pointer of Camera 2 */

  };
}

#endif

//
// MenuCamera.hh for bomberman in /home/tosoni_t/BitBucket/bomberman
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Sat Jun  2 22:54:43 2012 tosoni_t
// Last update Sun Jun  3 18:38:01 2012 Sarglen
//

#ifndef		__MENUCAMERA_HH__
#define		__MENUCAMERA_HH__

/**
 * @author tosoni_t
 * @file MenuCamera
 */

#include	"MyGame.hh"

/*!
 * @brief Rapidity constant of the Camera
 */
#define		RAPIDITY (30)

/*!
 * @class AForm
 * @brief Camera for the Menu
 */
class		MenuCamera
{
  graphic::Vector	_futureP; /*!< Vector for future position */
  bool		_xPositionOK; /*!< checks if x position is Ok */
  bool		_yPositionOK; /*!< checks if y position is Ok */
  bool		_zPositionOK; /*!< checks if z position id Ok */
  int		_velocity; /*!< velocity of the camera */

public:
  /*!
   * @brief Constructor for MenuCamera
   */
  MenuCamera();
  /*!
   * @brief Destructor for MenuCamera
   */
  virtual ~MenuCamera();

  /*!
   * @brief For moving the MenuCamera
   */
  void		moveCamera(void);
  /*!
   * @brief  Setter for the Future position of the Camera
   */
  void		setFutureCameraPosition(float, float, float);
  /*!
   * @brief Moves the x position of the camera
   */
  void	        moveXPosition(graphic::Vector & v);
  /*!
   * @brief Moves the y position of the camera
   */
  void	        moveYPosition(graphic::Vector & v);
  /*!
   * @brief Moves the z position of the camera
   */
  void	        moveZPosition(graphic::Vector & v);
  /*!
   * @brief Sets if the position is Ok or not
   */
  void		setPositionOK(bool, int);
  /*!
   * @brief Getter to know if the position is Ok or not
   */
  bool		getPositionOK(void) const;
};

#endif

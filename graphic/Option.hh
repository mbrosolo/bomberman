//
// Option.hh for Option in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 18:53:55 2012 tosoni
// Last update Sun Jun  3 22:29:33 2012 benjamin bourdin
//

#ifndef		__OPTION_HH__
#define		__OPTION_HH__

/**
 * @author thomas tosoni
 * @file Option.hh
 */

#include	<iostream>
#include	<string>
#include	"MyGame.hh"
#include	"AForm.hh"
#include	"Text.hh"

class		Option
{
  std::string		_stringName; /*!< The name of the option */
  graphic::Text		*_name; /*!< The text name of the option */
  graphic::AForm	*background; /*!< The background of the option */
  graphic::AForm	*selected; /*!< The form of the option */
  float			_optionNb; /*!< The number of the option */
  bool			_locked; /*!< The flag locked */
  gdl::Keys::Key	_key; /*!< The key assigned to the option */
  graphic::Text		*_keyText; /*!< The text of the option */
  bool			_validate; /*!< The flag validate */

public:
  /*!
   * @brief Constructor of Option
   * @param name The name of the option
   * @param form The form of the option
   * @param form2 The second form of the option
   * @param k The key assigned
   * @param x The position x
   * @param y The position y
   */
  Option(std::string const &, graphic::AForm*, graphic::AForm *, const gdl::Keys::Key, const float, const float);
  /*!
   * @brief Constructor by copy of Option
   * @param The option to copy
   */
  Option(Option const &);
  /*!
   * @brief Destructor of Option
   */
  ~Option();

  /*!
   * @brief operator= override of Option
   */
  Option&		operator=(Option const &);
  /*!
   * @brief drawText method of Option
   */
  void			drawText(void) const;
  /*!
   * @brief drawBackground method of Option
   */
  void			drawBackground(void) const;
  /*!
   * @brief drawSelected method of Option
   */
  void			drawSelected(void) const;
  /*!
   * @brief changePosition method of Option
   * @param x The position x
   * @param y The position y
   * @param z The position z
   */
  void			changePosition(const float x, const float y, const float z);
  /*!
   * @brief changeOptionNb method of Option
   * @param nb THe number of the option
   */
  void			changeOptionNb(const float);

  /*!
   * @brief getKey method of Option
   * @return The key assigned
   */
  gdl::Keys::Key	getKey() const;
  /*!
   * @brief getKeyText method of Option
   * @return The text of the key assigned
   */
  graphic::Text*	getKeyText() const;
  /*!
   * @brief getValidate method of Option
   * @return The flag validate
   */
  bool			getValidate(void) const;
  /*!
   * @brief setKeyText method of Option
   * @param s The key text to set
   */
  void			setKeyText(std::string const &);
  /*!
   * @brief setKey method of Option
   * @param k The key to set
   */
  void			setKey(const gdl::Keys::Key);
  /*!
   * @brief setValidate method of Option
   * @param b The flag validate to set
   */
  void			setValidate(const bool);

  /*!
   * @brief setOptionNb method of Option
   * @param nb The number of the option to set
   */
  void			setOptionNb(const float);
  /*!
   * @brief setBackground method of Option
   * @param form The form of the background to set
   */
  void			setBackground(graphic::AForm*);

  /*!
   * @brief getName method of Option
   * @return The name of the option
   */
  graphic::Text&	getName() const;
  /*!
   * @brief getStringName method of Option
   * @return The name of the key of the option
   */
  std::string const &	getStringName() const;
  /*!
   * @brief getOptionNb method of Option
   * @return The number of the option of the option
   */
  float			getOptionNb() const;
  /*!
   * @brief getBackground method of Option
   * @return The background's form of the option
   */
  graphic::AForm&       getBackground() const;
  /*!
   * @brief getSelected method of Option
   * @return The selected's form of the option
   */
  graphic::AForm&       getSelected() const;
};

#endif

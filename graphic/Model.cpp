//
// Model.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May  9 16:22:16 2012 benjamin bourdin
// Last update Sat Jun  2 22:00:33 2012 tosoni_t
//

#include	"Model.hpp"
#include	"Model.hh"

graphic::Model::Model(Vector const & pos, Vector const & rot,
		      Color const &color, Scale const &scale)
  : AForm(pos, rot, color, scale), _width(300.0f), _height(300.0f), _depth(300.0f)
{
}

graphic::Model::~Model()
{
}

graphic::Model::Model(Model const &t)
  : AForm(t.position_, t.rotation_, t.color_, t.scale_), _width(t._width), _height(t._height), _depth(t._depth)
{
  setTexture(t.texture_);
  setModel(t.model_);
}

graphic::Model&	graphic::Model::operator=(Model const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  return *this;
}

float	graphic::Model::getWidth(void) const
{
  return _width;
}

float	graphic::Model::getHeight(void) const
{
  return _height;
}

float	graphic::Model::getDepth(void) const
{
  return _depth;
}

void	graphic::Model::initialize(void)
{
  if (model_)
    {
      model_->cut_animation(
			    *model_,
			    "Take 001",
			    35,
			    53,
			    "RUN");
    }
}

void	graphic::Model::update(MyClock const & gameClock, gdl::Input &)
{
  if (model_)
    {
      model_->update(gameClock.getGameClock());
    }
}

void	graphic::Model::draw(void)
{
  glEnable(GL_TEXTURE_2D);

  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.first, position_.second, position_.third);

  if (rotationEnable_.first)
    glRotatef(rotation_.first, 1.0f, 0.0f, 0.0f);
  if (rotationEnable_.second)
    glRotatef(rotation_.second, 0.0f, 1.0f, 0.0f);
  if (rotationEnable_.third)
    glRotatef(rotation_.third, 0.0f, 0.0f, 1.0f);

  glScalef(scale_.first, scale_.second, scale_.third);

  if (model_)
    model_->draw();
  glPopMatrix();
  glDisable(GL_TEXTURE_2D);
}

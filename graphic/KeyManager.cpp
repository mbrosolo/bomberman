//
// KeyManager.cpp for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Wed May 16 11:33:47 2012 tosoni
// Last update Thu May 31 06:42:56 2012 benjamin bourdin
//

#include		<algorithm>
#include		<vector>
#include		<iostream>
#include		"KeyManager.hh"

KeyManager		*KeyManager::_instance = NULL;
typedef	std::map<gdl::Keys::Key, bool>	KeyMap_t;

KeyManager::KeyManager()
  : map(),
    mapstr(),
    _close(false)
{
  map[gdl::Keys::A] = false;
  map[gdl::Keys::B] = false;
  map[gdl::Keys::C] = false;
  map[gdl::Keys::D] = false;
  map[gdl::Keys::E] = false;
  map[gdl::Keys::F] = false;
  map[gdl::Keys::G] = false;
  map[gdl::Keys::H] = false;
  map[gdl::Keys::I] = false;
  map[gdl::Keys::J] = false;
  map[gdl::Keys::K] = false;
  map[gdl::Keys::L] = false;
  map[gdl::Keys::M] = false;
  map[gdl::Keys::N] = false;
  map[gdl::Keys::O] = false;
  map[gdl::Keys::P] = false;
  map[gdl::Keys::Q] = false;
  map[gdl::Keys::R] = false;
  map[gdl::Keys::S] = false;
  map[gdl::Keys::T] = false;
  map[gdl::Keys::U] = false;
  map[gdl::Keys::V] = false;
  map[gdl::Keys::W] = false;
  map[gdl::Keys::X] = false;
  map[gdl::Keys::Y] = false;
  map[gdl::Keys::Z] = false;

  map[gdl::Keys::Num0] = false;
  map[gdl::Keys::Num1] = false;
  map[gdl::Keys::Num2] = false;
  map[gdl::Keys::Num3] = false;
  map[gdl::Keys::Num4] = false;
  map[gdl::Keys::Num5] = false;
  map[gdl::Keys::Num6] = false;
  map[gdl::Keys::Num7] = false;
  map[gdl::Keys::Num8] = false;
  map[gdl::Keys::Num9] = false;
  map[gdl::Keys::Escape] = false;
  map[gdl::Keys::LControl] = false;
  map[gdl::Keys::LShift] = false;
  map[gdl::Keys::LAlt] = false;
  map[gdl::Keys::LSystem] = false;
  map[gdl::Keys::RControl] = false;
  map[gdl::Keys::RShift] = false;
  map[gdl::Keys::RAlt] = false;
  map[gdl::Keys::RSystem] = false;
  map[gdl::Keys::Menu] = false;
  map[gdl::Keys::LBracket] = false;
  map[gdl::Keys::RBracket] = false;
  map[gdl::Keys::SemiColon] = false;
  map[gdl::Keys::Comma] = false;
  map[gdl::Keys::Period] = false;
  map[gdl::Keys::Quote] = false;
  map[gdl::Keys::Slash] = false;
  map[gdl::Keys::BackSlash] = false;
  map[gdl::Keys::Tilde] = false;
  map[gdl::Keys::Equal] = false;
  map[gdl::Keys::Dash] = false;
  map[gdl::Keys::Space] = false;
  map[gdl::Keys::Return] = false;
  map[gdl::Keys::Back] = false;
  map[gdl::Keys::Tab] = false;
  map[gdl::Keys::PageUp] = false;
  map[gdl::Keys::PageDown] = false;
  map[gdl::Keys::End] = false;
  map[gdl::Keys::Home] = false;
  map[gdl::Keys::Insert] = false;
  map[gdl::Keys::Delete] = false;
  map[gdl::Keys::Add] = false;
  map[gdl::Keys::Subtract] = false;
  map[gdl::Keys::Multiply] = false;
  map[gdl::Keys::Divide] = false;
  map[gdl::Keys::Left] = false;
  map[gdl::Keys::Right] = false;
  map[gdl::Keys::Up] = false;
  map[gdl::Keys::Down] = false;

  map[gdl::Keys::Numpad0] = false;
  map[gdl::Keys::Numpad1] = false;
  map[gdl::Keys::Numpad2] = false;
  map[gdl::Keys::Numpad3] = false;
  map[gdl::Keys::Numpad4] = false;
  map[gdl::Keys::Numpad5] = false;
  map[gdl::Keys::Numpad6] = false;
  map[gdl::Keys::Numpad7] = false;
  map[gdl::Keys::Numpad8] = false;
  map[gdl::Keys::Numpad9] = false;

  map[gdl::Keys::F1] = false;
  map[gdl::Keys::F2] = false;
  map[gdl::Keys::F3] = false;
  map[gdl::Keys::F4] = false;
  map[gdl::Keys::F5] = false;
  map[gdl::Keys::F6] = false;
  map[gdl::Keys::F7] = false;
  map[gdl::Keys::F8] = false;
  map[gdl::Keys::F9] = false;
  map[gdl::Keys::F10] = false;
  map[gdl::Keys::F11] = false;
  map[gdl::Keys::F12] = false;
  map[gdl::Keys::F13] = false;
  map[gdl::Keys::F14] = false;
  map[gdl::Keys::F15] = false;

  map[gdl::Keys::Pause] = false;
  map[gdl::Keys::Count] = false;


  mapstr[gdl::Keys::A] = std::string("A");
  mapstr[gdl::Keys::B] = std::string("B");
  mapstr[gdl::Keys::C] = std::string("C");
  mapstr[gdl::Keys::D] = std::string("D");
  mapstr[gdl::Keys::E] = std::string("E");
  mapstr[gdl::Keys::F] = std::string("F");
  mapstr[gdl::Keys::G] = std::string("G");
  mapstr[gdl::Keys::H] = std::string("H");
  mapstr[gdl::Keys::I] = std::string("I");
  mapstr[gdl::Keys::J] = std::string("J");
  mapstr[gdl::Keys::K] = std::string("K");
  mapstr[gdl::Keys::L] = std::string("L");
  mapstr[gdl::Keys::M] = std::string("M");
  mapstr[gdl::Keys::N] = std::string("N");
  mapstr[gdl::Keys::O] = std::string("O");
  mapstr[gdl::Keys::P] = std::string("P");
  mapstr[gdl::Keys::Q] = std::string("Q");
  mapstr[gdl::Keys::R] = std::string("R");
  mapstr[gdl::Keys::S] = std::string("S");
  mapstr[gdl::Keys::T] = std::string("T");
  mapstr[gdl::Keys::U] = std::string("U");
  mapstr[gdl::Keys::V] = std::string("V");
  mapstr[gdl::Keys::W] = std::string("W");
  mapstr[gdl::Keys::X] = std::string("X");
  mapstr[gdl::Keys::Y] = std::string("Y");
  mapstr[gdl::Keys::Z] = std::string("Z");

  mapstr[gdl::Keys::Num0] = std::string("Num0");
  mapstr[gdl::Keys::Num1] = std::string("Num1");
  mapstr[gdl::Keys::Num2] = std::string("Num2");
  mapstr[gdl::Keys::Num3] = std::string("Num3");
  mapstr[gdl::Keys::Num4] = std::string("Num4");
  mapstr[gdl::Keys::Num5] = std::string("Num5");
  mapstr[gdl::Keys::Num6] = std::string("Num6");
  mapstr[gdl::Keys::Num7] = std::string("Num7");
  mapstr[gdl::Keys::Num8] = std::string("Num8");
  mapstr[gdl::Keys::Num9] = std::string("Num9");
  mapstr[gdl::Keys::Escape] = std::string("Esc");
  mapstr[gdl::Keys::LControl] = std::string("Left-Ctrl");
  mapstr[gdl::Keys::LShift] = std::string("Left-Shift");
  mapstr[gdl::Keys::LAlt] = std::string("Left-Alt");
  mapstr[gdl::Keys::LSystem] = std::string("Left-System");
  mapstr[gdl::Keys::RControl] = std::string("Right-Ctrl");
  mapstr[gdl::Keys::RShift] = std::string("Right-Shift");
  mapstr[gdl::Keys::RAlt] = std::string("Right-Alt");
  mapstr[gdl::Keys::RSystem] = std::string("Right-System");
  mapstr[gdl::Keys::Menu] = std::string("Menu");
  mapstr[gdl::Keys::LBracket] = std::string("{");
  mapstr[gdl::Keys::RBracket] = std::string("}");
  mapstr[gdl::Keys::SemiColon] = std::string(";");
  mapstr[gdl::Keys::Comma] = std::string(",");
  mapstr[gdl::Keys::Period] = std::string(".");
  mapstr[gdl::Keys::Quote] = std::string("'");
  mapstr[gdl::Keys::Slash] = std::string("/");
  mapstr[gdl::Keys::BackSlash] = std::string("\\");
  mapstr[gdl::Keys::Tilde] = std::string("~");
  mapstr[gdl::Keys::Equal] = std::string("=");
  mapstr[gdl::Keys::Dash] = std::string("-");
  mapstr[gdl::Keys::Space] = std::string("Space");
  mapstr[gdl::Keys::Return] = std::string("Return");
  mapstr[gdl::Keys::Back] = std::string("Back");
  mapstr[gdl::Keys::Tab] = std::string("Tab");
  mapstr[gdl::Keys::PageUp] = std::string("PageUp");
  mapstr[gdl::Keys::PageDown] = std::string("PageDown");
  mapstr[gdl::Keys::End] = std::string("End");
  mapstr[gdl::Keys::Home] = std::string("Home");
  mapstr[gdl::Keys::Insert] = std::string("Insert");
  mapstr[gdl::Keys::Delete] = std::string("Del");
  mapstr[gdl::Keys::Add] = std::string("+");
  mapstr[gdl::Keys::Subtract] = std::string("-");
  mapstr[gdl::Keys::Multiply] = std::string("*");
  mapstr[gdl::Keys::Divide] = std::string("/");
  mapstr[gdl::Keys::Left] = std::string("Left");
  mapstr[gdl::Keys::Right] = std::string("Right");
  mapstr[gdl::Keys::Up] = std::string("Up");
  mapstr[gdl::Keys::Down] = std::string("Down");

  mapstr[gdl::Keys::Numpad0] = std::string("Numpad0");
  mapstr[gdl::Keys::Numpad1] = std::string("Numpad1");
  mapstr[gdl::Keys::Numpad2] = std::string("Numpad2");
  mapstr[gdl::Keys::Numpad3] = std::string("Numpad3");
  mapstr[gdl::Keys::Numpad4] = std::string("Numpad4");
  mapstr[gdl::Keys::Numpad5] = std::string("Numpad5");
  mapstr[gdl::Keys::Numpad6] = std::string("Numpad6");
  mapstr[gdl::Keys::Numpad7] = std::string("Numpad7");
  mapstr[gdl::Keys::Numpad8] = std::string("Numpad8");
  mapstr[gdl::Keys::Numpad9] = std::string("Numpad9");

  mapstr[gdl::Keys::F1] = std::string("F1");
  mapstr[gdl::Keys::F2] = std::string("F2");
  mapstr[gdl::Keys::F3] = std::string("F3");
  mapstr[gdl::Keys::F4] = std::string("F4");
  mapstr[gdl::Keys::F5] = std::string("F5");
  mapstr[gdl::Keys::F6] = std::string("F6");
  mapstr[gdl::Keys::F7] = std::string("F7");
  mapstr[gdl::Keys::F8] = std::string("F8");
  mapstr[gdl::Keys::F9] = std::string("F9");
  mapstr[gdl::Keys::F10] = std::string("F10");
  mapstr[gdl::Keys::F11] = std::string("F11");
  mapstr[gdl::Keys::F12] = std::string("F12");
  mapstr[gdl::Keys::F13] = std::string("F13");
  mapstr[gdl::Keys::F14] = std::string("F14");
  mapstr[gdl::Keys::F15] = std::string("F15");

  mapstr[gdl::Keys::Pause] = std::string("Pause");
  mapstr[gdl::Keys::Count] = std::string("Count");
}

KeyManager::~KeyManager()
{
}

KeyManager	*KeyManager::getInstance(void)
{
  if (_instance == NULL)
    _instance = new KeyManager();
  return (_instance);
}

void		KeyManager::killInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}

bool		KeyManager::isFirstPressed(gdl::Keys::Key key)
{
  if (input_.isKeyDown(key) == true && map[key] == false)
    return (true);
  return (false);
}

bool		KeyManager::isFirstReleased(gdl::Keys::Key key)
{
  if (input_.isKeyDown(key) == false && map[key] == true)
    return (true);
  return (false);
}

bool		KeyManager::isPressed(gdl::Keys::Key key)
{
  if (input_.isKeyDown(key) == true && map[key] == true)
    return (true);
  return (false);
}

bool		KeyManager::isReleased(gdl::Keys::Key key)
{
  if (input_.isKeyDown(key) == false && map[key] == false)
    return (true);
  return (false);
}

void		KeyManager::updateKeys()
{
  for (KeyMap_t::iterator it = map.begin(); it != map.end(); ++it)
    {
      if (input_.isKeyDown(it->first) == true)
	it->second = true;
      else
	it->second = false;
    }
}

gdl::Keys::Key	KeyManager::getPressedKey()
{
  for (KeyMap_t::const_iterator it = map.begin(); it != map.end(); ++it)
    {
      if (input_.isKeyDown(it->first) == true)
	return (it->first);
    }
  return (gdl::Keys::Delete);
}

bool		KeyManager::getClose() const
{
  if (_close == true)
    std::cout << "Thanks for playing Bomberman!" << std::endl;
  return (_close);
}

void		KeyManager::setClose(bool value)
{
  _close = value;
}

void		KeyManager::initialize()
{
}

void		KeyManager::update()
{
}

void		KeyManager::draw()
{
}

void		KeyManager::unload()
{
}


std::string const	KeyManager::getStringKey(const gdl::Keys::Key key) const
{
  KeyString_t::const_iterator it = mapstr.find(key);

  if (it != mapstr.end())
    return it->second;
  return std::string("unknown");
}

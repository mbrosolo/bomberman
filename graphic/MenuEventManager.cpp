//
// GameEventManager.cpp for EventManager in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Wed May  9 22:48:29 2012 tosoni
// Last update Sun Jun  3 22:50:54 2012 benjamin bourdin
//

#include	"MenuEventManager.hh"

MenuEventManager		*MenuEventManager::_instance = NULL;

MenuEventManager::MenuEventManager()
  : _loaded(NULL),
    _firstMapping(new EventListObject_t()),
    _secondMapping(new EventListObject_t()),
    _thirdMapping(new EventListObject_t()),
    _fourthMapping(new EventListObject_t()),
    _fifthMapping(new EventListObject_t()),
    _sixthMapping(new EventListObject_t()),
    _seventhMapping(new EventListObject_t()),
    _eighthMapping(new EventListObject_t()),
    _ninthMapping(new EventListObject_t()),
    _tenthMapping(new EventListObject_t()),
    _eleventhMapping(new EventListObject_t()),
    _twelfthMapping(new EventListObject_t()),
    _thirteenthMapping(new EventListObject_t()),
    _fourteenthMapping(new EventListObject_t()),
    _fifteenthMapping(new EventListObject_t()),
    _player1Mapping(new AObjectEventList_t()),
    _player2Mapping(new AObjectEventList_t()),
    _isMapping(false),
    _keyManager(KeyManager::getInstance()),
    _loadVector(),
    _key(gdl::Keys::Delete),
    _keyToEraze(gdl::Keys::Delete),
    _erazor(0)
{
  _loadVector.push_back(&MenuEventManager::loadFirstMapping);
  _loadVector.push_back(&MenuEventManager::loadSecondMapping);
  _loadVector.push_back(&MenuEventManager::loadThirdMapping);
  _loadVector.push_back(&MenuEventManager::loadFourthMapping);
  _loadVector.push_back(&MenuEventManager::loadFifthMapping);
  _loadVector.push_back(&MenuEventManager::loadSixthMapping);
  _loadVector.push_back(&MenuEventManager::loadSeventhMapping);
  _loadVector.push_back(&MenuEventManager::loadEighthMapping);
  _loadVector.push_back(&MenuEventManager::loadNinthMapping);
  _loadVector.push_back(&MenuEventManager::loadTenthMapping);
  _loadVector.push_back(&MenuEventManager::loadEleventhMapping);
  _loadVector.push_back(&MenuEventManager::loadTwelfthMapping);
  _loadVector.push_back(&MenuEventManager::loadThirteenthMapping);
  _loadVector.push_back(&MenuEventManager::loadFourteenthMapping);
  _loadVector.push_back(&MenuEventManager::loadFifteenthMapping);
}

MenuEventManager::~MenuEventManager()
{
  delete _firstMapping;
  delete _secondMapping;
  delete _thirdMapping;
  delete _fourthMapping;
  delete _fifthMapping;
  delete _sixthMapping;
  delete _seventhMapping;
  delete _eighthMapping;
  delete _ninthMapping;
  delete _tenthMapping;
  delete _eleventhMapping;
  delete _twelfthMapping;
  delete _thirteenthMapping;
  delete _fourteenthMapping;
  delete _fifteenthMapping;
  delete _player1Mapping;
  delete _player2Mapping;
  _keyManager->killInstance();
}

MenuEventManager::MenuEventManager(MenuEventManager const & other)
  : _loaded(other._loaded),
    _firstMapping(other._firstMapping),
    _secondMapping(other._secondMapping),
    _thirdMapping(other._thirdMapping),
    _fourthMapping(other._fourthMapping),
    _fifthMapping(other._fifthMapping),
    _sixthMapping(other._sixthMapping),
    _seventhMapping(other._seventhMapping),
    _eighthMapping(other._eighthMapping),
    _ninthMapping(other._ninthMapping),
    _tenthMapping(other._tenthMapping),
    _eleventhMapping(other._eleventhMapping),
    _twelfthMapping(other._twelfthMapping),
    _thirteenthMapping(other._thirteenthMapping),
    _fourteenthMapping(other._fourteenthMapping),
    _fifteenthMapping(other._fifteenthMapping),
    _player1Mapping(other._player1Mapping),
    _player2Mapping(other._player2Mapping),
    _isMapping(other._isMapping),
    _keyManager(other._keyManager),
    _loadVector(other._loadVector),
    _key(other._key),
    _keyToEraze(other._keyToEraze),
    _erazor(other._erazor)
{
}

MenuEventManager	*MenuEventManager::getInstance(void)
{
  if (_instance == NULL)
    _instance = new MenuEventManager();
  return (_instance);
}

void			MenuEventManager::killInstance(void)
{
  if (_instance != NULL)
    delete _instance;
  _instance = NULL;
}

void			MenuEventManager::setErazor(const int v)
{
  _erazor = v;
}

void			MenuEventManager::setKeyToEraze(const gdl::Keys::Key v)
{
  _keyToEraze = v;
}

void			MenuEventManager::setClose(const bool v)
{
  _keyManager->setClose(v);
}

void			MenuEventManager::setIsMapping(const bool v)
{
  _isMapping = v;
}

bool			MenuEventManager::getIsMapping() const
{
  return (_isMapping);
}

int			MenuEventManager::getErazor() const
{
  return (_erazor);
}

AObjectEventList_t*	MenuEventManager::getPlayer1Mapping() const
{
  return (_player1Mapping);
}

AObjectEventList_t*	MenuEventManager::getPlayer2Mapping() const
{
  return (_player2Mapping);
}

void			MenuEventManager::loadFirstMapping()
{
  _loaded = _firstMapping;
}

void			MenuEventManager::loadSecondMapping()
{
  _loaded = _secondMapping;
}

void			MenuEventManager::loadThirdMapping()
{
  _loaded = _thirdMapping;
}

void			MenuEventManager::loadFourthMapping()
{
  _loaded = _fourthMapping;
}

void			MenuEventManager::loadFifthMapping()
{
  _loaded = _fifthMapping;

}
void			MenuEventManager::loadSixthMapping()
{
  _loaded = _sixthMapping;
}

void			MenuEventManager::loadSeventhMapping()
{
  _loaded = _seventhMapping;
}

void			MenuEventManager::loadEighthMapping()
{
  _loaded = _eighthMapping;
}

void			MenuEventManager::loadNinthMapping()
{
  _loaded = _ninthMapping;
}

void			MenuEventManager::loadTenthMapping()
{
  _loaded = _tenthMapping;
}

void			MenuEventManager::loadEleventhMapping()
{
  _loaded = _eleventhMapping;
}

void			MenuEventManager::loadTwelfthMapping()
{
  _loaded = _twelfthMapping;
}

void			MenuEventManager::loadThirteenthMapping()
{
  _loaded = _thirteenthMapping;
}

void			MenuEventManager::loadFourteenthMapping()
{
  _loaded = _fourteenthMapping;
}

void			MenuEventManager::loadFifteenthMapping()
{
  _loaded = _fifteenthMapping;
}

gdl::Keys::Key		MenuEventManager::getPressedKey() const
{
  return (_key);
}

gdl::Keys::Key		MenuEventManager::getKeyToEraze() const
{
  return (_keyToEraze);
}

void			MenuEventManager::setPressedKey(const gdl::Keys::Key k)
{
  _key = k;
}

void			MenuEventManager::loadMapping(const int m)
{
  (this->*_loadVector[m - 1])();
}

void			MenuEventManager::checkEvents()
{
  gdl::Keys::Key	k;
  Menu			*_menu = Menu::getInstance();

  if (_isMapping == true && (k = _keyManager->getPressedKey()) != gdl::Keys::Delete
      && _keyManager->isFirstPressed(k))
    {
      setPressedKey(k);
      _keyToEraze = k;
      for (AObjectEventList_t::const_iterator it = _player1Mapping->begin();
	   it != _player1Mapping->end(); ++it)
	if (it->first == k)
	  {
	    _erazor = 1;
	    return;
	  }
      for (AObjectEventList_t::const_iterator it = _player2Mapping->begin();
	   it != _player2Mapping->end(); ++it)
	if (it->first == k)
	  {
	    _erazor = 2;
	    return;
	  }
      _erazor = 0;
      return;
    }
  for (EventListObject_t::const_iterator it = _loaded->begin(); it != _loaded->end(); ++it)
    {
      if (_keyManager->isFirstPressed(it->first) == true)
	{
	  (_menu->*(it->second.first))(it->second.second);
	  return;
	}
    }
}

void			MenuEventManager::initialize()
{
  // Menu
  (*_firstMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updateMenuInDepth, 1.f);
  (*_firstMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updateMenuInDepth, -1.f);
  (*_firstMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateHorizontalMenu, -1.f);
  (*_firstMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateHorizontalMenu, 1.f);
  (*_firstMapping)[gdl::Keys::Return] = std::make_pair(&Menu::lockMenu, 0.f);
  (*_firstMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);

  // Menu Locked
  (*_secondMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updateOptions, -1.f);
  (*_secondMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updateOptions, 1.f);
  (*_secondMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateLocked, -1.f);
  (*_secondMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateLocked, 1.f);
  (*_secondMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockMenu, 0.f);
  (*_secondMapping)[gdl::Keys::Return] = std::make_pair(&Menu::validOption, 0.f);
  (*_secondMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);

  // Keyboard Options
  (*_thirdMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockOption, 0.f);
  (*_thirdMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);

  // In Game
  (*_fourthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::pauseMenu, 1.f);

  // Pause Menu
  (*_fifthMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updatePauseOptions, -1.f);
  (*_fifthMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updatePauseOptions, 1.f);
  (*_fifthMapping)[gdl::Keys::Return] = std::make_pair(&Menu::validPauseOption, -1.f);
  (*_fifthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::pauseMenu, -1.f);

  // Volume
  (*_sixthMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updateVolume, 1.f);
  (*_sixthMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updateVolume, -1.f);
  (*_sixthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateVolume, -1.f);
  (*_sixthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateVolume, 1.f);
  (*_sixthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockMenu, 0.f);
  (*_sixthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);

  // Volume Pause
  (*_seventhMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updatePauseVolume, 1.f);
  (*_seventhMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updatePauseVolume, -1.f);
  (*_seventhMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updatePauseVolume, -1.f);
  (*_seventhMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updatePauseVolume, 1.f);
  (*_seventhMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::pauseMenu, 1.f);
  (*_seventhMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::pauseMenu, -1.f);

  // Saves
  (*_eighthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateSaves, -1.f);
  (*_eighthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateSaves, 1.f);
  (*_eighthMapping)[gdl::Keys::Return] = std::make_pair(&Menu::selectSave, 0.f);
  (*_eighthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockLoad, 0.f);
  (*_eighthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);

  // Maps
  (*_ninthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateMaps, -1.f);
  (*_ninthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateMaps, 1.f);
  (*_ninthMapping)[gdl::Keys::Return] = std::make_pair(&Menu::selectMap, 0.f);
  (*_ninthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockLoad, 0.f);
  (*_ninthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);
  (*_ninthMapping)[gdl::Keys::Back] = std::make_pair(&Menu::selectMap, 1.f);

  // IA
  (*_tenthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateIA, -1.f);
  (*_tenthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateIA, 1.f);
  (*_tenthMapping)[gdl::Keys::Return] = std::make_pair(&Menu::selectIA, 0.f);
  (*_tenthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockLoad, 0.f);
  (*_tenthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);

  //LoadX
  (*_eleventhMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateX, -10.f);
  (*_eleventhMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateX, 10.f);
  (*_eleventhMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updateX, 1.f);
  (*_eleventhMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updateX, -1.f);
  (*_eleventhMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockXYRounds, 0.f);
  (*_eleventhMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);
  (*_eleventhMapping)[gdl::Keys::Back] = std::make_pair(&Menu::updateX, 0.f);

  //LoadY
  (*_twelfthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateY, -10.f);
  (*_twelfthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateY, 10.f);
  (*_twelfthMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updateY, 1.f);
  (*_twelfthMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updateY, -1.f);
  (*_twelfthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockXYRounds, 0.f);
  (*_twelfthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);
  (*_twelfthMapping)[gdl::Keys::Back] = std::make_pair(&Menu::updateY, 0.f);

  //IA Number
  (*_thirteenthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::IANumber, -10.f);
  (*_thirteenthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::IANumber, 10.f);
  (*_thirteenthMapping)[gdl::Keys::Up] = std::make_pair(&Menu::IANumber, 1.f);
  (*_thirteenthMapping)[gdl::Keys::Down] = std::make_pair(&Menu::IANumber, -1.f);
  (*_thirteenthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockNbIA, 0.f);
  (*_thirteenthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);
  (*_thirteenthMapping)[gdl::Keys::Back] = std::make_pair(&Menu::IANumber, 0.f);

  //MUSIC
  (*_fourteenthMapping)[gdl::Keys::Left] = std::make_pair(&Menu::updateMusic, -1.f);
  (*_fourteenthMapping)[gdl::Keys::Right] = std::make_pair(&Menu::updateMusic, 1.f);
  (*_fourteenthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockLoad, 0.f);
  (*_fourteenthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);
  (*_fourteenthMapping)[gdl::Keys::Return] = std::make_pair(&Menu::selectMusic, 0.f);

  // ROUNDS
  (*_fifteenthMapping)[gdl::Keys::Up] = std::make_pair(&Menu::updateRounds, 1.f);
  (*_fifteenthMapping)[gdl::Keys::Down] = std::make_pair(&Menu::updateRounds, -1.f);
  (*_fifteenthMapping)[gdl::Keys::Delete] = std::make_pair(&Menu::unlockXYRounds, 0.f);
  (*_fifteenthMapping)[gdl::Keys::Escape] = std::make_pair(&Menu::closeGame, 1.f);
  (*_fifteenthMapping)[gdl::Keys::Back] = std::make_pair(&Menu::updateRounds, 0.f);

  // PLAYER 1
  (*_player1Mapping)[gdl::Keys::Up] = &AObject::moveUp;
  (*_player1Mapping)[gdl::Keys::Down] = &AObject::moveDown;
  (*_player1Mapping)[gdl::Keys::Left] = &AObject::moveLeft;
  (*_player1Mapping)[gdl::Keys::Right] = &AObject::moveRight;
  (*_player1Mapping)[gdl::Keys::Space] = &AObject::leave;

  // PLAYER 2
  (*_player2Mapping)[gdl::Keys::W] = &AObject::moveUp;
  (*_player2Mapping)[gdl::Keys::S] = &AObject::moveDown;
  (*_player2Mapping)[gdl::Keys::A] = &AObject::moveLeft;
  (*_player2Mapping)[gdl::Keys::D] = &AObject::moveRight;
  (*_player2Mapping)[gdl::Keys::E] = &AObject::leave;
}

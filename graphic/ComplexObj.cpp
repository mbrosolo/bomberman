//
// ComplexObj.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May  7 14:41:39 2012 benjamin bourdin
// Last update Sun Jun  3 21:00:53 2012 benjamin bourdin
//

#include	"ComplexObj.hh"

graphic::Complex::Cube::Cube(Vector const & pos, Vector const & rot,
			     Color const &color, Scale const &scale)
  : AForm(pos, rot, color, scale), _width(300.0f), _height(300.0f), _depth(300.0f)
{
}


graphic::Complex::Cube::~Cube()
{
}

graphic::Complex::Cube::Cube(Cube const &t)
  : AForm(t.position_, t.rotation_, t.color_, t.scale_), _width(t._width), _height(t._height), _depth(t._depth)
{
  setTexture(t.texture_);
  setModel(t.model_);
  setVideo(t.video_);
}

graphic::Complex::Cube&	graphic::Complex::Cube::operator=(Cube const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  video_ = t.video_;
  return *this;
}


float	graphic::Complex::Cube::getWidth(void) const
{
  return _width;
}

float	graphic::Complex::Cube::getHeight(void) const
{
  return _height;
}

float	graphic::Complex::Cube::getDepth(void) const
{
  return _depth;
}

void	graphic::Complex::Cube::initialize(void)
{
}

void	graphic::Complex::Cube::update(MyClock const & gameClock, gdl::Input & input)
{
  static_cast<void>(gameClock);
  static_cast<void>(input);
  if (video_)
    video_->captureImg(gameClock);
}

void	graphic::Complex::Cube::draw(void)
{
  if (texture_)
    {
      glEnable(GL_TEXTURE_2D);
      texture_->bind();
    }
  if (video_)
    {
      video_->loadImg();
    }

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.first, position_.second, position_.third);

  if (rotationEnable_.first)
    glRotatef(rotation_.first, 1.0f, 0.0f, 0.0f);
  if (rotationEnable_.second)
    glRotatef(rotation_.second, 0.0f, 1.0f, 0.0f);

  glScalef(scale_.first, scale_.second, scale_.third);

  glBegin(GL_QUADS);
  glColor3f(color_.first, color_.second, color_.third);

  glBegin(GL_POLYGON);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, _depth / 2.0f);
  glEnd();

  glBegin(GL_POLYGON);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, -_depth / 2.0f);
  glEnd();


  glBegin(GL_POLYGON);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  glEnd();

  glBegin(GL_POLYGON);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  glEnd();


  glBegin(GL_POLYGON);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(-_width / 2.0f, _height / 2.0f, -_depth / 2.0f);
  glEnd();

  glBegin(GL_POLYGON);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 0.0f);
  glVertex3f(_width / 2.0f, _height / 2.0f, -_depth / 2.0f);
  glEnd();


  glEnd();
  glPopMatrix();
  if (video_ || texture_)
    glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
}



graphic::Complex::Pyramid::Pyramid(Vector const & pos, Vector const & rot,
			     Color const &color, Scale const &scale)
  : AForm(pos, rot, color, scale), _width(300.0f), _height(300.0f), _depth(300.0f)
{
}


graphic::Complex::Pyramid::~Pyramid()
{
}

graphic::Complex::Pyramid::Pyramid(Pyramid const &t)
  : AForm(t.position_, t.rotation_, t.color_, t.scale_), _width(t._width), _height(t._height), _depth(t._depth)
{
  setTexture(t.texture_);
  setModel(t.model_);
  setVideo(t.video_);
}

graphic::Complex::Pyramid&	graphic::Complex::Pyramid::operator=(Pyramid const &t)
{
  position_ = t.position_;
  rotation_ = t.rotation_;
  color_ = t.color_;
  scale_ = t.scale_;
  texture_ = t.texture_;
  model_ = t.model_;
  video_ = t.video_;
  return *this;
}


float	graphic::Complex::Pyramid::getWidth(void) const
{
  return _width;
}

float	graphic::Complex::Pyramid::getHeight(void) const
{
  return _height;
}

float	graphic::Complex::Pyramid::getDepth(void) const
{
  return _depth;
}

void	graphic::Complex::Pyramid::initialize(void)
{
}

void	graphic::Complex::Pyramid::update(MyClock const & gameClock, gdl::Input & input)
{
  static_cast<void>(gameClock);
  static_cast<void>(input);

  if (video_)
    video_->captureImg(gameClock);
}

void	graphic::Complex::Pyramid::draw(void)
{
  if (texture_)
    {
      glEnable(GL_TEXTURE_2D);
      texture_->bind();
    }
  if (video_)
    video_->loadImg();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glPushMatrix();
  glLoadIdentity();
  glTranslatef(position_.first, position_.second, position_.third);

  if (rotationEnable_.first)
    glRotatef(rotation_.first, 1.0f, 0.0f, 0.0f);
  if (rotationEnable_.second)
    glRotatef(rotation_.second, 0.0f, 1.0f, 0.0f);
  if (rotationEnable_.third)
    glRotatef(rotation_.third, 0.0f, 0.0f, 1.0f);

  glScalef(scale_.first, scale_.second, scale_.third);

  glBegin(GL_TRIANGLES);

  glColor3f(color_.first, color_.second, color_.third);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(0.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth / 2.0f);

  glColor3f(color_.first, color_.second, color_.third);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(0.0f, _height / 2.0f, 0.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth / 2.0f);

  glColor3f(color_.first, color_.second, color_.third);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(0.0f, _height / 2.0f, 0.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(0.0f, -_height / 2.0f, -_depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(-_width / 2.0f, -_height / 2.0f, _depth / 2.0f);

  glColor3f(color_.first, color_.second, color_.third);
  if (video_ || texture_) glTexCoord2f(0.0f, 0.0f);
  glVertex3f(0.0f, _height / 2.0f, 0.0f);
  if (video_ || texture_) glTexCoord2f(0.0f, 1.0f);
  glVertex3f(_width / 2.0f, -_height / 2.0f, _depth / 2.0f);
  if (video_ || texture_) glTexCoord2f(1.0f, 1.0f);
  glVertex3f(0.0f, -_height / 2.0f, -_depth / 2.0f);

  glEnd();
  glPopMatrix();
  if (video_ || texture_)
    glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
}

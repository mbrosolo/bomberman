//
// Transition.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 30 21:17:02 2012 benjamin bourdin
// Last update Sun Jun  3 22:02:23 2012 benjamin bourdin
//

#include	<algorithm>
#include	"Transition.hh"
#include	"Primitive.hh"
#include	"LoadTexture.hh"
#include	"MyGame.hh"
#include	"Core.hh"

graphic::Transition::Transition()
  : _one(NULL), _two(NULL), _three(NULL), _go(NULL),
    _content(), _it(), _step(1.0f)
{
  _one = new Primitive::Rectangle(Vector(MyGame::WINDOW_WIDTH / 2.0f,
					 MyGame::WINDOW_HEIGHT / 2.0f, 0.0f),
				  Vector(180.0f, 1.0f, 0.0f),
				  Color(1.0f, 1.0f, 1.0f),
				  Scale(0.7f, 1.f, 1.0f));
  _one->rotEnableX();
  _two = new Primitive::Rectangle(Vector(MyGame::WINDOW_WIDTH / 2.0f,
					 MyGame::WINDOW_HEIGHT / 2.0f, 0.0f),
				  Vector(180.0f, 1.0f, 0.0f),
				  Color(1.0f, 1.0f, 1.0f),
				  Scale(0.7f, 1.0f, 1.0f));
  _two->rotEnableX();
  _three = new Primitive::Rectangle(Vector(MyGame::WINDOW_WIDTH / 2.0f,
					   MyGame::WINDOW_HEIGHT / 2.0f, 0.0f),
				    Vector(180.0f, 1.0f, 0.0f),
				    Color(1.0f, 1.0f, 1.0f),
				    Scale(0.7f, 1.0f, 1.0f));
  _three->rotEnableX();
  _go = new Primitive::Rectangle(Vector(MyGame::WINDOW_WIDTH / 2.0f,
					MyGame::WINDOW_HEIGHT / 2.0f, 0.0f),
				 Vector(180.0f, 1.0f, 0.0f),
				 Color(1.0f, 1.0f, 1.0f),
				 Scale(0.7f, 1.0f, 1.0f));
  _go->rotEnableX();


  _one->setTexture(MyGame::getInstance()->getTextureManager()->getTexture("ONE"));
  _two->setTexture(MyGame::getInstance()->getTextureManager()->getTexture("TWO"));
  _three->setTexture(MyGame::getInstance()->getTextureManager()->getTexture("THREE"));
  _go->setTexture(MyGame::getInstance()->getTextureManager()->getTexture("GO"));

  _content.push_back(_three);
  _content.push_back(_two);
  _content.push_back(_one);
  _content.push_back(_go);
  _it = _content.begin();
}


namespace transition_utils
{
  void	_foreach_del(graphic::AForm *form)
  {
    delete form;
    form = NULL;
  }
}

graphic::Transition::~Transition()
{
  std::for_each(_content.begin(), _content.end(), &transition_utils::_foreach_del);
}

graphic::Transition::Transition(Transition const &copy)
  : _one(copy._one), _two(copy._two), _three(copy._three),
    _go(copy._go), _content(copy._content), _it(copy._it),
    _step(copy._step)
{
}

graphic::Transition &graphic::Transition::operator=(Transition const &copy)
{
  _one = copy._one;
  _two = copy._two;
  _three = copy._three;
  _go = copy._go;
  _content = copy._content;
  _it = copy._it;
  _step = copy._step;
  return *this;
}

void	graphic::Transition::initialize(void)
{
  _it = _content.begin();
  _step = 1.0f;
}

void	graphic::Transition::update(MyClock const &cl, gdl::Input &in)
{
  if (_it != _content.end())
    {
      (*_it)->update(cl, in);
      if (Core::getInstance()->getTotalTime() >= _step)
	{
	  ++_it;
	  ++_step;
	  if (_step == 4.0f)
	    MyGame::getInstance()->getSoundManager()->play("TRANSITION_END");
	  else
	    MyGame::getInstance()->getSoundManager()->play("TRANSITION");
	}
    }
}

void	graphic::Transition::draw(void) const
{
  if (_it != _content.end())
    (*_it)->draw();
}

bool	graphic::Transition::isFinished(void) const
{
  if (_it == _content.end())
    return true;
  return false;
}

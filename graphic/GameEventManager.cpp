//
// MenuEventManager.cpp for GameEventManager in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Thu May 10 19:33:08 2012 tosoni
// Last update Fri Jun  1 22:06:40 2012 tosoni_t
//

#include	"MenuEventManager.hh"
#include	"GameEventManager.hh"

GameEventManager::GameEventManager()
  : _playerMapping(new AObjectEventList_t()),
    _keyManager(KeyManager::getInstance()),
    _leave()
{
}

GameEventManager::~GameEventManager()
{
  _keyManager->killInstance();
}

GameEventManager::GameEventManager(GameEventManager const & other)
  : _playerMapping(other._playerMapping),
    _keyManager(other._keyManager),
    _leave(other._leave)
{
}

void			GameEventManager::checkEvents(AObject *_obj)
{
  for (AObjectEventList_t::iterator it = _playerMapping->begin();
       it != _playerMapping->end(); ++it)
    {
      if (it->first == _leave && _keyManager->isFirstPressed(it->first) == true)
  	(_obj->*(it->second))();
      else if (it->first != _leave && _keyManager->isPressed(it->first) == true)
  	(_obj->*(it->second))();
    }
}

void			GameEventManager::setMap(AObjectEventList_t * map)
{
  _playerMapping = map;
  for (AObjectEventList_t::iterator it = map->begin(); it != map->end(); ++it)
    {
      if (it->second == &AObject::leave)
	_leave = it->first;
    }
}

AObjectEventList_t*	GameEventManager::getMap() const
{
  return (_playerMapping);
}

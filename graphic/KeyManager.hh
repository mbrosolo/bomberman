//
// KeyManager.hh for bomberman in /home/tosoni/BitBucket/bomberman/graphic
//
// Made by tosoni
// Login   <tosoni@epitech.net>
//
// Started on  Wed May 16 11:26:11 2012 tosoni
// Last update Wed May 30 15:36:57 2012 benjamin bourdin
//

#ifndef		__KEYMANAGER_HH__
#define		__KEYMANAGER_HH__

/**
 * @author thomas tosoni
 * @file KeyManager.hh
 */

#include	<string>
#include	<map>
#include	"Game.hpp"

/*!
 * @brief Typedef for map of keys 
 */
typedef	std::map<gdl::Keys::Key, bool>		KeyMap_t;
/*!
 * @brief Typedef for map of keys with strings 
 */
typedef	std::map<gdl::Keys::Key, std::string>	KeyString_t;

/*!
 * @class KeyManager
 * @brief Define the Manager of keys
 */
class		KeyManager: public gdl::Game
{
public:
  /*!
   * @brief GetInstance static method of KeyManager 
   */
  static KeyManager	*getInstance(void);
  /*!
   * @brief KillInstance static method of KeyManager 
   */
  static void		killInstance(void);

  /*!
   * @brief isFirstPressed method of KeyManager 
   */
  bool		isFirstPressed(gdl::Keys::Key);
  /*!
   * @brief isPressed method of KeyManager 
   */
  bool		isPressed(gdl::Keys::Key);
  /*!
   * @brief isFirstReleased method of KeyManager 
   */
  bool		isFirstReleased(gdl::Keys::Key);
  /*!
   * @brief isReleased method of KeyManager 
   */
  bool		isReleased(gdl::Keys::Key);
  /*!
   * @brief getClose method of KeyManager 
   */
  bool		getClose() const;
  /*!
   * @brief setClose method of KeyManager 
   */
  void		setClose(bool);
  /*!
   * @brief updateKeys method of KeyManager 
   */
  void		updateKeys();
  /*!
   * @brief getPressedKey method of KeyManager 
   */
  gdl::Keys::Key	getPressedKey();
  /*!
   * @brief getStringKey method of KeyManager 
   */
  std::string const	getStringKey(const gdl::Keys::Key) const;

  /*!
   * @brief Initialize (virtual) method of KeyManager 
   */
  virtual void  initialize();
  /*!
   * @brief Update (virtual) method of KeyManager 
   */
  virtual void  update();
  /*!
   * @brief Draw (virtual) method of KeyManager 
   */
  virtual void  draw();
  /*!
   * @brief Unload (virtual) method of KeyManager 
   */
  virtual void  unload();

private:
  KeyMap_t		map; /*!< The map ofkeys */
  KeyString_t		mapstr; /*!< The map string of keys */
  bool			_close; /*!< The close flag */
  static KeyManager	*_instance; /*!< The instance */

private:
  KeyManager();
  ~KeyManager();
};

#endif

//
// Intro.cpp for  in /home/bourdi_b//bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 30 18:19:26 2012 benjamin bourdin
// Last update Sun Jun  3 12:48:24 2012 benjamin bourdin
//

#include	"Intro.hh"
#include	"MyGame.hh"
#include	"KeyManager.hh"
#include	"Input.hpp"

const float graphic::Intro::cameraPosFinal = 800.0f;
const float graphic::Intro::cameraPosBegin = 3500.0f;

graphic::Intro::Intro()
  : _title(NULL), _mode(false), _finished(false)
{
}

graphic::Intro::~Intro()
{
  if (_title)
    delete _title;
}

graphic::Intro::Intro(Intro const &copy)
  : _title(copy._title), _mode(false), _finished(false)
{
}

graphic::Intro &	graphic::Intro::operator=(Intro const &copy)
{
  _title = copy._title;
  _mode = copy._mode;
  _finished = copy._finished;
  return *this;
}

void	graphic::Intro::initialize()
{
  _title = new Primitive::Rectangle();
  _title->setScale(Scale(3.0f, 1.5f, 1.5f));
  MyGame::getInstance()->getCameraManagement()->getCamera1()->setPos(Vector(0.0f, 0.0f, cameraPosBegin));
  _title->setTexture(MyGame::getInstance()->getTextureManager()->getTexture("PRESS_START"));
}

void	graphic::Intro::update(MyClock const &cl, gdl::Input &in)
{
  if (_mode == false)
    {
      MyGame::getInstance()->getCameraManagement()->getCamera1()->incPosZ(-10.0f);
      Vector pos = MyGame::getInstance()->getCameraManagement()->getCamera1()->getPos();
      if (pos.third == cameraPosFinal)
	{
	  _mode = true;
	  _title->setTexture(MyGame::getInstance()->getTextureManager()->getTexture("PRESS_START"));
	}
    }
  if (_mode)
    {
      if (KeyManager::getInstance()->isFirstPressed(gdl::Keys::Space))
	_finished = true;
    }
  _title->update(cl, in);
}

void	graphic::Intro::draw(void)
{
  _title->draw();
}

bool	graphic::Intro::isFinished(void) const
{
  return _finished;
}

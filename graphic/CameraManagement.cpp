//
// CameraManagement.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May 28 22:38:23 2012 benjamin bourdin
// Last update Sun Jun  3 11:34:11 2012 benjamin bourdin
//

#include	"CameraManagement.hh"
#include	"MyGame.hh"

graphic::CameraManagement::CameraManagement()
  : _multi(false),
    _camera1(new Camera()), _camera2(NULL)
{
}

graphic::CameraManagement::~CameraManagement()
{
}

graphic::CameraManagement::CameraManagement(CameraManagement const &copy)
  : _multi(copy._multi), _camera1(copy._camera1), _camera2(copy._camera2)
{
}

graphic::CameraManagement&	graphic::CameraManagement::operator=(CameraManagement const &copy)
{
  _multi = copy._multi;
  _camera1 = copy._camera1;
  _camera2 = copy._camera2;
  return *this;
}


void	graphic::CameraManagement::enableMulti(void)
{
  _multi = true;
  if (_camera2 == NULL)
    {
      _camera2 = new Camera();
      _camera2->initialize();
    }
}

void	graphic::CameraManagement::enableSingle(void)
{
  _multi = false;
  if (_camera2)
    delete _camera2;
  _camera2 = NULL;
}

bool	graphic::CameraManagement::isMulti(void) const
{
  return _multi;
}

graphic::Camera	*graphic::CameraManagement::getCamera1(void) const
{
  return _camera1;
}

graphic::Camera	*graphic::CameraManagement::getCamera2(void) const
{
  return _camera2;
}

void	graphic::CameraManagement::moveCamera1(APlayer const * const player)
{
  if (_multi)
    glViewport(-MyGame::WINDOW_WIDTH / 2.0f, 0,
	       MyGame::WINDOW_WIDTH - 10.0f, MyGame::WINDOW_HEIGHT);
  else
    glViewport(0, 0,
	       MyGame::WINDOW_WIDTH, MyGame::WINDOW_HEIGHT);
  if (player)
    {
      Vector	pos = _camera1->getPos();
      Vector	newPos = player->getPosition();

      if (_multi)
	pos.first = newPos.first - graphic::MyGame::WINDOW_HEIGHT / 2.0f;
      else
	pos.first = newPos.first;
      pos.second = newPos.second;
      _camera1->setPos(pos);
    }
}

void	graphic::CameraManagement::moveCamera2(APlayer const * const player)
{
  glViewport(MyGame::WINDOW_WIDTH / 2.0f, 0,
	     MyGame::WINDOW_WIDTH - 10.0f, MyGame::WINDOW_HEIGHT);
  if (player)
    {
      Vector	pos = _camera2->getPos();
      Vector	newPos = player->getPosition();

      pos.first = newPos.first + graphic::MyGame::WINDOW_HEIGHT / 2.0f;
      pos.second = newPos.second;
      _camera2->setPos(pos);
    }
}

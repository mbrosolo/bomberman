//
// Exception.hh for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon Mar 26 11:15:00 2012 gressi_b
// Last update Wed May 23 06:49:42 2012 benjamin bourdin
//

#ifndef	__EXCEPTION_HH__
#define	__EXCEPTION_HH__

#include <exception>
#include <string>

/**
 * @author gressi_b
 * @class Exception
 * @brief Exception class used everywhere in the project.
 * It is often extended.
 */

class Exception : public std::exception
{
protected:
  std::string const	_what;
  std::string const	_where;

public:
  Exception(std::string const&, std::string const& = "");
  virtual ~Exception() throw() {}

  virtual char const*	what() const throw();
  std::string const&	where() const;
};

#endif

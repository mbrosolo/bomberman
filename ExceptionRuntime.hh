//
// ExceptionRuntime.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:47:42 2012 benjamin bourdin
// Last update Wed May 23 06:53:08 2012 benjamin bourdin
//

#ifndef __EXCEPTION_RUNTIME_HH__
#define __EXCEPTION_RUNTIME_HH__

#include	<stdexcept>
#include	<exception>
#include	<string>

class ExceptionRuntime : public std::runtime_error
{
public:
protected:
  std::string const	_what;
  std::string const	_where;

public:
  ExceptionRuntime(std::string const&, std::string const& = "");
  virtual ~ExceptionRuntime() throw() {}

  virtual char const*	what() const throw();
  std::string const&	where() const;
};

#endif

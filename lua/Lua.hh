//
// Lua.hh for bomberman in /home/gressi_b/Epitech/lua
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 25 17:20:53 2012 gressi_b
// Last update Wed Apr 25 17:20:53 2012 gressi_b
//

#ifndef	__LUA_HH__
#define	__LUA_HH__

#include "Script.hh"
#include "Table.hh"
#include "LuaException.hh"

namespace lua
{
  void		warn(LuaException const& e);
}

#endif

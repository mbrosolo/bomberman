//
// LuaException.cpp for bomberman in /home/gressi_b/Epitech/lua
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 25 17:29:13 2012 gressi_b
// Last update Wed Apr 25 17:29:13 2012 gressi_b
//

#include "LuaException.hh"

// lua::LuaException -----------------------------------------------------------

lua::LuaException::LuaException(std::string const& what, std::string const& where)
  : Exception(what, where)
{
}

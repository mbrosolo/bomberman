//
// Table.cpp for lua in /home/gressi_b/Epitech/B4/bomberman/lua/sample
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon May 28 11:02:00 2012 gressi_b
// Last update Mon May 28 11:02:00 2012 gressi_b
//

#include <iostream>

#include "Table.hh"

// lua::Table ------------------------------------------------------------------

lua::Table::Table(prim_t *p, bool created)
  : _script(p), _n(0), _size(0), _deep(0), _created(created)
{
  if (created == true)
    lua_newtable(p);
}

lua::Table::Table(prim_t *p, std::string const& name)
  : _script(p), _n(0), _size(0), _deep(0), _created(false)
{
  lua_getglobal(p, name.c_str());
}

lua::Table::Table(Table const& s)
  : _script(s._script), _n(s._n), _size(s._size), _deep(0), _created(true)
{
  lua_newtable(this->_script);
}

lua::Table&
lua::Table::operator=(Table const& s)
{
  this->_script = s._script;
  this->_n = s._n;
  this->_size = s._size;
  return *this;
}

lua::Table::~Table()
{
  if (this->_deep > 0 && this->_created == true)
    lua_settable(this->_script, -3);
}

// Member functions ------------------------------------------------------------

void
lua::Table::reset()
{
  this->_n = 0;
  this->_size = 0;
}

void
lua::Table::record(std::string const& globName)
{
  lua_setglobal(this->_script, globName.c_str());
  this->_n = 0;
  this->_size = 0;
}

lua::Table*
lua::Table::getTable(int n) const
{
  lua_pushinteger(this->_script, n);
  lua_gettable(this->_script, -2);
  Table*	t = new Table(this->_script, false);
  t->_deep = this->_deep + 1;
  return t;
}

lua::Table*
lua::Table::getTable(std::string const& key) const
{
  lua_pushstring(this->_script, key.c_str());
  lua_gettable(this->_script, -2);
  Table*	t = new Table(this->_script, false);
  t->_deep = this->_deep + 1;
  return t;
}

lua::Table*
lua::Table::newTable(std::string const& key)
{
  ++this->_size;
  lua_pushstring(this->_script, key.c_str());
  Table*	t = new Table(this->_script);
  t->_deep = this->_deep + 1;
  return t;
}

lua::Table*
lua::Table::newTable()
{
  ++this->_n;
  ++this->_size;
  lua_pushnumber(this->_script, this->_n);
  Table*	t = new Table(this->_script);
  t->_deep = this->_deep + 1;
  return t;
}

void
lua::Table::push(int i)
{
  ++this->_n;
  ++this->_size;
  lua_pushnumber(this->_script, this->_n);
  lua_pushnumber(this->_script, i);
  lua_rawset(this->_script, -3);
}

void
lua::Table::push(std::string const& str)
{
  ++this->_n;
  ++this->_size;
  lua_pushnumber(this->_script, this->_n);
  lua_pushstring(this->_script, str.c_str());
  lua_rawset(this->_script, -3);
}

void
lua::Table::define(std::string const& key, int n)
{
  ++this->_size;
  lua_pushstring(this->_script, key.c_str());
  lua_pushnumber(this->_script, n);
  lua_rawset(this->_script, -3);
}

void
lua::Table::define(std::string const& key, std::string const& str)
{
  ++this->_size;
  lua_pushstring(this->_script, key.c_str());
  lua_pushstring(this->_script, str.c_str());
  lua_rawset(this->_script, -3);
}

size_t
lua::Table::size() const
{
  return this->_size;
}

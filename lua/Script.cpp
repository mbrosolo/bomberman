//
// Script.cpp for bomberman in /home/gressi_b/bomberman/lua
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed May  9 10:36:50 2012 gressi_b
// Last update Sun Jun  3 17:32:48 2012 benjamin bourdin
//

#include "Script.hh"

// lua::Script -----------------------------------------------------------------

lua::Script::Script(prim_t* p)
  : _script(p)
{
}

lua::Script::Script(Script const& s)
  : _script(s._script)
{
}

lua::Script&
lua::Script::operator=(Script const& s)
{
  this->_script = s._script;
  return *this;
}

lua::Script::~Script()
{
}

// Public Member functions -----------------------------------------------------

void
lua::Script::up(std::string const& name) const
{
  lua_getglobal(this->_script, name.c_str());
}

void
lua::Script::error(std::string const& errMsg) const
{
  luaL_error(this->_script, errMsg.c_str());
}

void
lua::Script::close() const
{
  if (this->_script)
    {
      lua_pop(this->_script, 1);
      lua_close(this->_script);
    }
}

lua::Table*
lua::Script::newTable() const
{
  if (this->_script == 0)
    return 0;
  return new Table(this->_script);
}

lua::Table*
lua::Script::getTable(std::string const& name) const
{
  if (this->_script == 0)
    return 0;
  return new Table(this->_script, name);
}

#include <iostream>
void
lua::Script::load(std::string const& filename)
{
  this->_script = luaL_newstate();

  if (this->_script == 0)
    throw LuaException("Memory Exhausted", "Script::load()");
  luaopen_io(this->_script);
  luaopen_base(this->_script);
  luaopen_table(this->_script);
  luaopen_string(this->_script);
  luaopen_math(this->_script);
  luaL_openlibs(this->_script);

  int ret = luaL_loadfile(this->_script, filename.c_str());

  lua_setglobal(this->_script, "__START");

  if (ret)
    throw LuaException(this->_getError(ret), "Script::load()");
}

void
lua::Script::call() const
{
  lua_getglobal(this->_script, "__START");
  int	ret = lua_pcall(this->_script, 0, LUA_MULTRET, 0);

  if (ret)
    throw LuaException(this->_getError(ret), "Script::call()");
}

void
lua::Script::record(function_t f, std::string const& fname)
{
  lua_register(this->_script, fname.c_str(), f);
}

void
lua::Script::record(int i, std::string const& fname)
{
  lua_pushnumber(this->_script, i);
  lua_setglobal(this->_script, fname.c_str());
}

void
lua::Script::ret(int n) const
{
  lua_pushinteger(this->_script, n);
}

void
lua::Script::ret(double n) const
{
  lua_pushnumber(this->_script, n);
}

void
lua::Script::ret(std::string const& s) const
{
  lua_pushstring(this->_script, s.c_str());
}

void
lua::Script::ret(bool b) const
{
  lua_pushboolean(this->_script, b);
}

lua::prim_t*
lua::Script::getPrim() const
{
  return this->_script;
}

int
lua::Script::getArgc() const
{
  return lua_gettop(this->_script);
}

std::string
lua::Script::getArgv(int n) const
{
  return lua_tostring(this->_script, n + 1);
}

inline bool
lua::Script::operator==(prim_t* p) const
{
  return (this->_script == p);
}

// Private ---------------------------------------------------------------------

std::string
lua::Script::_getError(int eno) const
{
  std::string	msg("Unknown error");
  char const*	add = lua_tostring(this->_script, -1);

  switch (eno)
    {
    case 0:
      return "Success";
    case LUA_ERRSYNTAX: 
      msg = "Syntax error";
      break;
    case LUA_ERRMEM:
      msg = "Memory exhausted";
      break;
    case LUA_ERRFILE:
      msg = "File could not be opened";
      break;
    case LUA_ERRRUN:
      msg = "Runtime error";
      break;
    case LUA_ERRERR:
      msg = "Error while running the error handler function";
      break;
    }
  if (add)
    {
      msg += ": ";
      msg += add;
    }
  lua_pop(this->_script, lua_gettop(this->_script));
  return msg;
}

//
// LuaException.hh for bomberman in /home/gressi_b/Epitech/lua
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 25 17:27:50 2012 gressi_b
// Last update Wed Apr 25 17:27:50 2012 gressi_b
//

#ifndef	__LUA_EXCEPTION_HH__
#define	__LUA_EXCEPTION_HH__

#include "../Exception.hh"

namespace lua
{
  /**
   * @author gressi_b
   * @class LuaException
   * @brief Exception class used in lua/ directory only
   */

  class LuaException : public Exception
  {
  public:
    LuaException(std::string const&, std::string const& = "");
  };
}

#endif

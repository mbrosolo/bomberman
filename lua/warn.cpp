//
// warn.cpp for bombermn in /home/gressi_b/Epitech/B4/bomberman/lua
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Sun Jun  3 18:03:46 2012 gressi_b
// Last update Sun Jun  3 18:03:46 2012 gressi_b
//

#include <iostream>

#include "Lua.hh"

void
lua::warn(LuaException const& e)
{
  std::cerr << "LuaExceptio in " << e.where() << ": " << e.what() << std::endl;
}

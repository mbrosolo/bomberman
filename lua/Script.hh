//
// lua.hh for bomberman in /home/gressi_b/Epitech/lua
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 25 16:31:40 2012 gressi_b
// Last update Wed Apr 25 16:31:40 2012 gressi_b
//

#ifndef	__LUA_SCRIPT_HPP__
#define	__LUA_SCRIPT_HPP__

#include <string>
#include <vector>

#include "luabase.hh"
#include "LuaException.hh"
#include "Table.hh"
#include "Luna.hpp"

namespace lua
{
  /**
   * @author gressi_b
   * @class Script
   * @brief Class which represents a script file so
   * it can be loaded, launched or closed for instance
   */

  class Script
  {
  private:
    prim_t*		_script;

  private:
    std::string	_getError(int eno) const;

  public:
    Script(prim_t* = 0);
    Script(Script const&);
    Script&	operator=(Script const&);
    ~Script();

  public:
    /**
     * @brief up a certain key
     */
    void	up(std::string const&) const;

    /**
     * @brief error handling
     * generate a lua error
     */
    void	error(std::string const&) const;

    /**
     * @brief script closure
     * Close file and free memory
     */
    void	close() const;

    /**
     * @brief created a new Table
     */
    Table*	newTable() const;

    /**
     * @brief get a new Table
     */
    Table*	getTable(std::string const&) const;

    /**
     * @brief script loading
     * Must be called before "call" method
     */
    void	load(std::string const&);

    /**
     * @brief script calling
     * Execute loaded lua script
     */
    void	call() const;

    /**
     * @brief set a return value (int)
     */
    void	ret(int) const;

    /**
     * @brief set a return value (double)
     */
    void	ret(double) const;

    /**
     * @brief set a return value (string)
     */
    void	ret(std::string const&) const;

    /**
     * @brief set a return value (bool)
     */
    void	ret(bool) const;
    /**
     * @brief get lua_State pointer used by C lua API (low-level)
     * should be only used for debug
     */
    prim_t*	getPrim() const;

    /**
     * @brief get number of argument passed to a function
     */
    int		getArgc() const;

    /**
     * @brief get an argument passed to a function from its indice
     */
    std::string	getArgv(int n) const;

    bool	operator==(prim_t*) const;

    /**
     * @brief export a C++ object to lua
     * Luna<>::record() must have been called first
     */
    template<class T>
    void	recordObject(T* obj, std::string const& name)
    {
      Luna<T>::record(this->_script, obj, name);
    }

    /**
     * @brief record a C/C++ name
     * So it can be used in the lua script
     */
    void	record(function_t, std::string const&);

    /**
     * @brief record an integet value so it can be used in lua script
     */
    void	record(int, std::string const&);

    /**
     * @brief exports a class: kind of converts its methods
     * to a lua metatable
     */
    template<class T>
    void	exportClass() const
    {
      Luna<T>::exports(this->_script);
    }
  };
}

#endif

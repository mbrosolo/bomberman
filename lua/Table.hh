//
// Table.hh for lua in /home/gressi_b/Epitech/B4/bomberman/lua/sample
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon May 28 11:00:17 2012 gressi_b
// Last update Mon May 28 11:00:17 2012 gressi_b
//

#ifndef	__LUA_TABLE_HH__
#define	__LUA_TABLE_HH__

#include <string>
#include <vector>

#include "luabase.hh"

namespace lua
{
  /**
   * @author gressi_b
   * @class Table
   * @brief Used to easily manipulate lua table from C++
   */
  class Table
  {
  private:
    prim_t*		_script;
    size_t		_n;
    size_t		_size;
    int			_deep;
    bool		_created;

  public:
    Table();
    Table(prim_t *p, bool = true);
    Table(prim_t *p, std::string const&);
    Table(Table const&);
    Table&	operator=(Table const&);
    ~Table();

    void	reset();

    /**
     * @brief exports table to script
     */
    void	record(std::string const&);

    /**
     * @brief get Table (create a new instance)
     */
    Table*	getTable(int) const;
    Table*	getTable(std::string const&) const;

    /**
     * @brief create a new table
     */
    Table*	newTable(std::string const&);
    Table*	newTable();
 
    /**
     * @brief push int to table
     */
    void	push(int);
    void	push(std::string const&);

    /**
     * @brief define a new key
     */
    void	define(std::string const&, int);
    void	define(std::string const&, std::string const&);
    size_t	size() const;

    template<typename T>
    void
    push(std::vector<T> const& v)
    {
      typename std::vector<T>::const_iterator	it;
      int					i;

      ++this->_n;
      ++this->_size;
      lua_pushnumber(this->_script, this->_n);
      Table*	t = new Table(this->_script);
      for (i = 1, it = v.begin(); it != v.end(); i++, it++)
	t->push(*it);
      delete t;
      lua_rawset(this->_script, -3);
    }

    template<typename T>
    void
    define(std::string const& key, std::vector<T> const& v)
    {
      typename std::vector<T>::const_iterator	it;
      int					i;
      ++this->_size;

      lua_pushstring(this->_script, key.c_str());
      Table*	t = new Table(this->_script);
      for (i = 1, it = v.begin(); it != v.end(); i++, it++)
	t->push(*it);
      delete t;
      lua_rawset(this->_script, -3);
    }
  };
}

#endif

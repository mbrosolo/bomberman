//
// luabase.hh for lua in /home/gressi_b/Epitech/B4/bomberman/lua/sample
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon May 28 14:22:20 2012 gressi_b
// Last update Mon May 28 14:22:20 2012 gressi_b
//

#ifndef	__LUA_BASE_HH__
#define	__LUA_BASE_HH__

extern "C" {
#include "include/lua.h"
#include "include/lualib.h"
#include "include/lauxlib.h"
}

namespace lua
{
  typedef lua_State	prim_t;
  typedef int	(*function_t)(prim_t*);
}

#endif

//
// Luna.hpp for bomberman in /home/gressi_b/Epitech/B4/bomberman/lua/sample
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed May 30 14:22:03 2012 gressi_b
// Last update Wed May 30 14:22:03 2012 gressi_b
//

#ifndef	__LUA_LUNA_HPP__
#define	__LUA_LUNA_HPP__

#include <string>
#include <sstream>
#include "luabase.hh"
 #include "Script.hh"

#define LUA_METHOD(class, name)	{ #name, &class::name }

namespace lua
{
  /**
   * @author gressi_b
   * @class Luna
   * @brief Used to share C++ object with lua.
   * Mainly used by Script class
   */
  template <typename T>
  class Luna
  {
  public:
    typedef int (T::*mfp_t)(prim_t *L);

    /**
     * @struct RegType
     * @brief Used to record each method and its name in to lua API
     */
    struct RegType
    {
      const char*	name;
      mfp_t		mfunc;
    };

    /**
     * @brief Exports a C++ object to lua
     */
    static void	record(prim_t* L, T* obj, std::string const& name)
    {
      lua_pushlightuserdata(L, static_cast<void*>(obj));
      T**	ud = static_cast<T**>(lua_newuserdata(L, sizeof(T*)));
      *ud = obj;
      luaL_getmetatable(L, T::className);
      lua_setmetatable(L, -2);
      lua_setglobal(L, name.c_str());
    }

    /**
     * @brief Record a class, its name and its methodes
     */
    static void	exports(prim_t *L)
    {
      lua_newtable(L);
      int methods = lua_gettop(L);
      luaL_newmetatable(L, T::className);
      int metatable = lua_gettop(L);
      lua_pushstring(L, T::className);
      lua_pushvalue(L, methods);
      lua_settable(L, LUA_RIDX_GLOBALS);
      lua_pushliteral(L, "__metatable");
      lua_pushvalue(L, methods);
      lua_settable(L, metatable);
      lua_pushliteral(L, "__index");
      lua_pushvalue(L, methods);
      lua_settable(L, metatable);
      lua_pushliteral(L, "__tostring");
      lua_pushcfunction(L, _tostring_T);
      lua_settable(L, metatable);
      // This shard of code allow user to instantiate class from lua script
      // and lua garbage collection: I forbid this here
      //
      // lua_pushliteral(L, "__gc");
      // lua_pushcfunction(L, _gc_T);
      // lua_settable(L, metatable);
      // lua_newtable(L);
      // int mt = lua_gettop(L);
      // lua_pushliteral(L, "__call");
      // lua_pushcfunction(L, _new_T);
      // lua_pushliteral(L, "new");
      // lua_pushvalue(L, -2);
      // lua_settable(L, methods);
      // lua_settable(L, mt);
      // lua_setmetatable(L, methods);
      for (RegType *l = T::methods; l->name; l++)
	{
	  lua_pushstring(L, l->name);
	  lua_pushlightuserdata(L, static_cast<void*>(l));
	  lua_pushcclosure(L, _thunk, 1);
	  lua_settable(L, methods);
	}
      lua_pop(L, 2);
    }

  private:
    Luna();

    static T*	check(prim_t *L, int narg)
    {
      T**	ud = static_cast<T**>(luaL_checkudata(L, narg, T::className));

      if (!*ud)
	{
	  luaL_where(L, 1);
	  luaL_error(L, "%s: bad argument %d to 'func' (%s expected, %s)",
		     lua_tostring(L, 1), narg, T::className,
		     luaL_typename(L, narg));
	}
      return *ud;
    }

    /**
     * @brief function called before each method,
     * allow us to call member function
     */
    static int	_thunk(prim_t *L)
    {
      T*	obj = check(L, 1);
      lua_remove(L, 1);
      RegType*	l = static_cast<RegType*>(lua_touserdata(L, lua_upvalueindex(1)));
      return (obj->*(l->mfunc))(L);
    }

    /**
     * @brief called when class is instantiated in lua script
     */
    static int	_new_T(prim_t *L)
    {
      lua_remove(L, 1);
      T*	obj = new T(L);
      T**	ud = static_cast<T**>(lua_newuserdata(L, sizeof(T*)));

      *ud = obj;
      luaL_getmetatable(L, T::className);
      lua_setmetatable(L, -2);
      return 1;
    }

    /**
     * @brief allow garbage collection to handle object instantiated
     * in lua script
     */
    static int	_gc_T(prim_t *L)
    {
      T**		ud = static_cast<T**>(lua_touserdata(L, 1));

      delete *ud;
      return 0;
    }

    /**
     * @brief funtion called when printing object
     */
    static int	_tostring_T(prim_t *L)
    {
      std::stringstream	ss;
      T**		ud = static_cast<T**>(lua_touserdata(L, 1));

      ss << '<' << T::className << " at " << *ud << '>';
      lua_pushstring(L, ss.str().c_str());
      return 1;
    }
  };
}

#endif

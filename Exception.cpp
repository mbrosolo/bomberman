//
// Exception.cpp for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon Mar 26 11:15:04 2012 gressi_b
// Last update Thu May 31 15:37:05 2012 Sarglen
//

#include "Exception.hh"

// Exception -------------------------------------------------------------------

Exception::Exception(std::string const& what, std::string const& where)
  : _what(what), _where(where)
{
}

// Member functions ------------------------------------------------------------

char const*	Exception::what() const throw()
{
  return (this->_what.c_str());
}

std::string const&	Exception::where() const
{
  return (this->_where);
}

//
// signal.hh for bm in /home/gressi_b/bm
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 11 13:29:32 2012 gressi_b
// Last update Sat May 19 20:24:03 2012 benjamin bourdin
//

#ifndef	__SIGNAL_HH__
#define	__SIGNAL_HH__

#include <signal.h>

#ifdef SIG_ERR
# undef SIG_ERR
# define SIG_ERR	(reinterpret_cast<sighandler_t>(-1))
#endif
#ifdef SIG_DFL
# undef SIG_DFL
# define SIG_DFL	(reinterpret_cast<sighandler_t>(0))
#endif
#ifdef SIG_IGN
# undef SIG_IGN
# define SIG_IGN	(reinterpret_cast<sighandler_t>(1))
#endif

namespace bm
{
  /**
   * @author gressi_b
   * @namespace signal
   * @brief contains several functions to encapsulate C syscall
   * to hanble signals
   */
  namespace signal
  {
    typedef sighandler_t	handler_t;

    handler_t	signal(int, handler_t);
    handler_t	ignore(int);
    handler_t	setDefault(int);
  }
}

#endif

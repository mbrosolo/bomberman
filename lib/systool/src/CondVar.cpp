//
// CondVar.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:29:51 2012 gaetan senn
// Last update Sat May  5 14:16:56 2012 gaetan senn
//

#include "CondVar.hh"
#include "CondVarException.hh"
#include "Mutex.hh"

#include	<iostream>

// CondVar -----------------------------------------------------

systool::CondVar::CondVar(IMutex & m)
  : _cond(), _mutex(m)
{
  pthread_cond_init(&(this->_cond), 0);
}

systool::CondVar::~CondVar()
{
  pthread_cond_destroy(&(this->_cond));
}

// Member functions --------------------------------------------

void	systool::CondVar::signal()
{
  if (pthread_cond_signal(&(this->_cond)))
    throw CondVarException("pthread_cond_signal()", "CondVar::signal()");
}

void	systool::CondVar::broadcast()
{
  if (pthread_cond_broadcast(&(this->_cond)))
    throw CondVarException("pthread_cond_broadcast()", "CondVar::broadcast()");
}

void	systool::CondVar::wait()
{
  ScopedLock	scope(this->_mutex);

  if (pthread_cond_wait(&(this->_cond),
			static_cast<Mutex::mutex_t*>(this->_mutex.getMutex())))
    throw CondVarException("pthread_cond_wait()", "CondVar::wait()");
}

int	systool::CondVar::timedwait(int sec, long int nsec)
{
  int		r;
  timespec	t;
  ScopedLock	scope(this->_mutex);

  clock_gettime(CLOCK_REALTIME, &t);
  t.tv_sec += sec;
  t.tv_nsec += nsec;
  r = pthread_cond_timedwait(&(this->_cond),
			     static_cast<Mutex::mutex_t*>
			     (this->_mutex.getMutex()), &t);
  if (r && r != ETIMEDOUT)
    throw CondVarException("pthread_cond_timedwait()", "CondVar::timedwait()");
  return r;
}

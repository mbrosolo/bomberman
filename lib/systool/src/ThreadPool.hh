//
// ThreadPool.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:44:31 2012 gaetan senn
// Last update Tue May 22 11:06:30 2012 gaetan senn
//

#ifndef __THREADPOOL__
#define __THREADPOOL__

#include <iostream>
#include <list>
#include <vector>
#include <map>
#include <signal.h>
#include "ITask.hh"
#include "SafeQueue.hpp"
#include "CondVar.hh"
#include "Thread.hpp"
#include "Mutex.hh"
#include "ScopedLock.hh"
#include "IThreadPool.hh"

namespace systool
{
  enum					eState
    {
      WORK,
      WAIT,
      STOPPED,
      TOTO
    };
  class ThreadPool;

  typedef struct		s_threadinfo
  {
    systool::IThread<ThreadPool>	*thread;
    eState		state;
  }			t_threadinfo;

  class					ThreadPool
  {
  private:
    systool::Mutex					mapsecur;
    systool::Mutex					mprocesswait;
    systool::Mutex					mcreation;
    systool::Mutex					mthreadwait;
    systool::CondVar				processwait;
    systool::CondVar				waiter;
    systool::SafeQueue<systool::ITask *>			queue;
    size_t				nbthread;
    std::vector<t_threadinfo *>		threads;
    bool						_threadPerTask;

  public:
    ThreadPool(const size_t nbthread, const bool threadPerTask = false);
    ~ThreadPool();
    void		start();
    void		stop();
    void		pause();
    void		resume();
    void		cancel();
    void		addThread();
    bool		inactivePool();
    void		addTask(systool::ITask *);
    void		*threadWaitTask(void *);
  };
}

#endif

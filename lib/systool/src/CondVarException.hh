//
// CondVarException.hh for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr  4 16:29:00 2012 gressi_b
// Last update Mon May 14 13:26:18 2012 gaetan senn
//

#ifndef	__COND_VAR_EXCEPTION_HH__
#define	__COND_VAR_EXCEPTION_HH__

#include <string>

#include "SystoolException.hh"

class CondVarException : public systool::Exception
{
public:
  CondVarException(std::string const&, std::string const& = "");
};

#endif

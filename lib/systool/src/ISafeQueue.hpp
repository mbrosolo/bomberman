//
// ISafeQueue.hpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:39:17 2012 gaetan senn
// Last update Sat May  5 14:25:57 2012 gaetan senn
//

#ifndef __ISAFEQUEUE_H_
#define __ISAFEQUEUE_H_

namespace systool
{
  template <typename T>
  class		ISafeQueue
  {
  public:
    virtual ~ISafeQueue() {}

    virtual void		push(T) = 0;
    virtual bool		tryPop(T *) = 0;
    virtual bool		isFinished() = 0;
    virtual void		setFinished() = 0;
  };
}

#endif

//
// Semaphore.cpp for systool in /home/ga/Documents/Projet/bomberman/bomberman/lib/threadpool/src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sun May  6 18:37:18 2012 gaetan senn
// Last update Mon May 21 18:09:13 2012 gaetan senn
//

#include "Semaphore.hh"
#include "SemaphoreException.hh"

Semaphore::Semaphore(std::string const &repo, int id_proj)
  : sem_id(0), key(), nbsem(), repository(repo)
{
  if ((key = ftok(repo.c_str(), id_proj)) == -1)
    throw SemaphoreException("ftok()", "Semaphore::Semaphore");
  nbsem = 0;
}

Semaphore::~Semaphore()
{
  unload();
}

void		Semaphore::unload()
{
  if (unlink(repository.c_str()) == -1)
    throw SemaphoreException("unlink()", "Semaphore::unload");
}

void		Semaphore::load(int _nbsem)
{
  if (nbsem != 0 || _nbsem == 0)
    throw SemaphoreException("nbsem fail", "Semaphore::load");
  nbsem = _nbsem;
  if ((sem_id =  semget(key, _nbsem, IPC_EXCL)) == -1)
    {
      if ((sem_id = semget(key, nbsem, 0666 | IPC_CREAT)) == -1)
	throw SemaphoreException("semget", "Semaphore::semget");
    }
}

void		Semaphore::setval(unsigned short sem_pos, short value)
{
  if (sem_pos >= nbsem)
    throw SemaphoreException("nbsem fail", "Semaphore::setval");
  if (semctl(sem_id, sem_pos, SETVAL, value) == -1)
    throw SemaphoreException("setmctl", "Semaphore::semctl");
}

void		Semaphore::setop(unsigned short sem_pos, short action)
{
  struct	sembuf	sbf;

  if (sem_pos >= nbsem)
    throw SemaphoreException("sempos", "Semaphore::setop");
  sbf.sem_num = sem_pos;
  sbf.sem_op = action;
  sbf.sem_flg = 0;
  if (semop(sem_id, &sbf, 1) == -1)
    throw SemaphoreException("semop", "Semaphore::setop");
}

//
// Math.hh for systool in /home/ga/Documents/Projet/bomberman/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Mon May  7 16:00:25 2012 gaetan senn
// Last update Mon May 21 15:26:30 2012 anna texier
//

#ifndef __MATH__
#define __MATH__

#include <stdio.h>

namespace systool
{
  template <typename T>
  class		Math
  {
  public:
    Math();
    ~Math();
    static T	getX(T pos, T width, T height)
    {
      return (pos - (pos / width) * height);
    }
    static T	getY(T pos, T width, T height)
    {
      static_cast<void>(height);
      return (pos / width);
    }
    static T	getLeft(T pos, T width, T height)
    {
      static_cast<void>(height);
      if ((pos % width) == 0)
	return (-1);
      return (pos - 1);
    }
    static T	getTop(T pos, T width, T height)
    {
      static_cast<void>(height);
      if (pos < width)
	return (-1);
      return (pos - width);
    }
    static T	getBottom(T pos, T width, T height)
    {
      static_cast<void>(height);
      if (pos >= (width * (height -1)))
	return (-1);
      return (pos + width);
    }
    static T	getRight(T pos, T width, T height)
    {
      static_cast<void>(height);
      if ((pos + 1) % width == 0)
	return (-1);
      return (pos + 1);
    }
    static bool	c1dto2d(T pos, T width, T height, T *res)
    {
      static_cast<void>(height);
      res[0] = (pos % width);
      res[1] = (pos / width);
      return (true);
    }
    static T	c2dto1d(T x, T y, T width)
    {
        return ((y * width) + x);
    }
  };
}

#endif

//
// Thread.hpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:43:02 2012 gaetan senn
// Last update Tue May 22 17:56:04 2012 gaetan senn
//

#ifndef	__THREAD_H_
#define	__THREAD_H_

#include	<pthread.h>
#include	<iostream>
#include	<signal.h>

#include	"IThread.hh"
#include	"ThreadException.hh"

static void sig_pause(int signo)
{
  static_cast<void>(signo);
  pause();
  return ;
}

static void	sig_resume(int signo)
{
  static_cast<void>(signo);
  return ;
}

namespace systool
{
  template <typename T>
  class		Thread : public IThread<T>
  {
  public:
    typedef void	*(T::*plink)(void *);
  private:
    typedef struct	t_encap
    {
      T			*_classlink;
      plink		_pt;
      void		*data;
    }			s_encap;

    enum IThread<T>::eState	st;
    pthread_t			thread;
    T				*_classlink;
    plink				_pt;
    s_encap			_encap;

  public:
    Thread(T *classlink, plink pt)
      : st(), thread(), _classlink(classlink) , _pt(pt), _encap()
    {
      signal(SIGUSR1, sig_pause);
      signal(SIGUSR2, sig_resume);
      _encap._classlink = _classlink;
      _encap._pt = _pt;
    }

    Thread(const Thread<T> &o) : _classlink(o._classlink) , _pt(o._pt) {
      signal(SIGUSR1, sig_pause);
      signal(SIGUSR2, sig_resume);
      _encap._classlink = _classlink;
      _encap._pt = _pt;
    }

    pthread_t		*getPthread() { return &thread;};

    Thread<T> const & operator=(Thread<T> const &);

    enum IThread<T>::eState	stat() const {
      return (st);
    }

    void		start(void *data) {
      _encap.data = data;
      if (pthread_create(&thread, NULL,
			 &go, static_cast<void *>(&_encap)) != 0)
	throw ThreadException("pthread_create() failure",
			      "systool::Thread::start()");
      st = IThread<T>::INITIALIZED;
    }

    void		cancel()
    {
      if (pthread_cancel(thread) != 0)
	throw ThreadException("pthread_cancel() : No Thread Founded",
			      "systool::Thread::cancel()");
      st = IThread<T>::WAITTHREAD;
    }

    void		wait() {
      if (pthread_join(thread, NULL) == -1)
	throw ThreadException("pthread_cancel() : Failure",
			      "systool::Thread::wait()");
    }

    static void *go(void *data) {
      s_encap		*_getdata;

      _getdata = static_cast<s_encap *>(data);
      ((_getdata->_classlink)->*(_getdata->_pt))(_getdata->data);
      return (NULL);
    }
  };
}

#endif

//
// IThreadPool.hh for systool in /home/ga/Documents/Projet/bomberman/bomberman/lib/threadpool/src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat May  5 15:10:48 2012 gaetan senn
// Last update Sat May  5 15:27:52 2012 gaetan senn
//

#ifndef __ITHREADPOOL__
#define __ITHREADPOOL__

#include "ITask.hh"

namespace systool
{
  class			IThreadPool
  {
  public:
    virtual ~IThreadPool(){};

    virtual void	start() = 0;
    virtual void	stop() = 0;
    virtual void	cancel() = 0;
    virtual void	addThread() = 0;
    virtual void	addTask(ITask *) = 0;
    virtual void	*threadWaitTask(void *) = 0;
  };
}

#endif

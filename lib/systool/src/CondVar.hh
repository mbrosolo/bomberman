//
// CondVar.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:32:39 2012 gaetan senn
// Last update Mon May  7 13:55:06 2012 gaetan senn
//

#ifndef	__COND_VAR_HH__
#define	__COND_VAR_HH__

#include	<cerrno>
#include	<pthread.h>

#include "ICondVar.hh"
#include "IMutex.hh"
#include "ScopedLock.hh"

namespace systool
{
  class CondVar : public ICondVar
  {
  private:
    pthread_cond_t	_cond;
    IMutex &		_mutex;
  private:
    CondVar(CondVar const&);
    CondVar&	operator=(CondVar const&);
  public:
    explicit CondVar(IMutex & m);
    ~CondVar();
    void	signal();
    void	broadcast();
    void	wait();
    int		timedwait(int, long int);
  };
}
#endif

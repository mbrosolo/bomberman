//
// ThreadPool.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:44:12 2012 gaetan senn
// Last update Tue May 22 14:55:20 2012 gaetan senn
//

#include "ThreadPool.hh"
#include "Cout.hh"

systool::ThreadPool::ThreadPool(const size_t  _nbthread, const bool threadPerTask)
  : mapsecur(), mprocesswait(), mcreation(), mthreadwait(),
    processwait(mprocesswait), waiter(mthreadwait), queue(),
    nbthread(_nbthread), threads(), _threadPerTask(threadPerTask)
{
  size_t		i;
  t_threadinfo		*t;

  for (i = 0; i < nbthread; i++)
    {
      t = new t_threadinfo;
      t->thread = new Thread<ThreadPool>(this, &ThreadPool::threadWaitTask);
      t->state = WAIT;
      threads.push_back(t);
    }
}

systool::ThreadPool::~ThreadPool()
{
  cancel();
  std::vector<t_threadinfo *>::iterator		it;

  for (it = threads.begin(); it != threads.end(); ++it)
    {
      delete((*it)->thread);
      delete(*it);
    }
}

void		systool::ThreadPool::start()
{
  size_t	pos;

  for (pos = 0; pos < nbthread; ++pos)
    threads[pos]->thread->start(threads[pos]);
}

bool		systool::ThreadPool::inactivePool()
{
  std::vector<t_threadinfo *>::iterator	it;
  ScopedLock lock(mapsecur);

  if (threads.empty())
    return (false);
  for (it = threads.begin(); it != threads.end() ; ++it)
    {
      if ((*it)->state != STOPPED)
	return (false);
    }
  return (true);
}

#include <sys/types.h>
#include <signal.h>

void		systool::ThreadPool::pause()
{
  std::vector<t_threadinfo *>::iterator		it;
  ScopedLock		lock(mapsecur);

  for (it = threads.begin(); it != threads.end(); ++it)
    {
      pthread_kill(*(*it)->thread->getPthread(), SIGUSR1);
    }
}

void		systool::ThreadPool::resume()
{
  std::vector<t_threadinfo *>::iterator		it;
  ScopedLock		lock(mapsecur);

  sleep(1);
  for (it = threads.begin(); it != threads.end(); ++it)
    pthread_kill(*(*it)->thread->getPthread(), SIGUSR2);
}

void		systool::ThreadPool::stop()
{
  std::vector<t_threadinfo *>::iterator		it;

  sleep(1);
  queue.setFinished();
  waiter.broadcast();
  for (it = threads.begin(); it != threads.end(); ++it)
    (*it)->thread->wait();
}

void		systool::ThreadPool::cancel()
{
  std::vector<t_threadinfo *>::iterator	it;

  queue.setFinished();
  waiter.broadcast();
  mapsecur.lock();
  for (it = threads.begin(); it != threads.end(); ++it)
    {
      if ((*it)->state != STOPPED)
	((*it)->thread)->cancel();
    }
  mapsecur.unlock();
}

void		systool::ThreadPool::addThread()
{
  t_threadinfo		*t;
  ScopedLock		lock(mapsecur);

  nbthread++;
  t = new t_threadinfo;
  t->thread = new Thread<ThreadPool>(this, &ThreadPool::threadWaitTask);
  t->state = WAIT;
  threads.push_back(t);
  t->thread->start(t);
}

void		systool::ThreadPool::addTask(ITask *_task)
{
  queue.push(_task);
  waiter.broadcast();
  if (!queue.isEmpty())
    {
      this->addThread();
      waiter.broadcast();
    }
}

void		*systool::ThreadPool::threadWaitTask(void *_threadinfo)
{
  ITask		*task;
  t_threadinfo	*threadinfo;

  threadinfo = static_cast<t_threadinfo *>(_threadinfo);
  while (!queue.isFinished())
    {
      mapsecur.lock();
      threadinfo->state = WAIT;
      mapsecur.unlock();
      if (!queue.tryPop(&task))
	{
	  mapsecur.lock();
	  threadinfo->state = WORK;
	  mapsecur.unlock();
	  task->runTaskThread(NULL);
	}
      else
	waiter.wait();
    }
  mapsecur.lock();
  threadinfo->state = STOPPED;
  //  processwait.signal();
  mapsecur.unlock();
  return (NULL);
}

//
// ScopedLock.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:42:32 2012 gaetan senn
// Last update Sat May  5 14:23:04 2012 gaetan senn
//

#ifndef	__SCOPEDLOCK_H_
#define	__SCOPEDLOCK_H_

#include	<exception>
#include	"IMutex.hh"
#include	"Mutex.hh"

namespace systool
{
  class		ScopedLock
  {
    IMutex	&_mutex;

  public:
    ScopedLock(IMutex &);
    ~ScopedLock();
  };
}

#endif

//
// ScopedLock.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:42:26 2012 gaetan senn
// Last update Sat May  5 14:23:32 2012 gaetan senn
//

#include	"ScopedLock.hh"
#include	"MutexException.hh"

#include <iostream>

systool::ScopedLock::ScopedLock(IMutex &mutex) : _mutex(mutex)
{
  try {
    _mutex.lock();
  }
  catch (MutexException const &e) {
    std::cout << "Fatal Error of type " << e.what() << "from : "
	      << e.where() << std::endl;
  }
}

systool::ScopedLock::~ScopedLock()
{
  try {
    _mutex.unlock();
  }
  catch (MutexException const &e) {
    std::cout << "Fatal Error of type " << e.what() << "from : "
	      << e.where() << std::endl;
  }
}

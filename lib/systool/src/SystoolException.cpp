//
// Exception.cpp for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon Mar 26 11:15:04 2012 gressi_b
// Last update Mon May 14 13:26:02 2012 gaetan senn
//

#include "SystoolException.hh"

// ThreadPool::Exception -----------------------------------------------------------

systool::Exception::Exception(std::string const& what, std::string const& where)
  : _what(what), _where(where)
{
}

// Member functions ------------------------------------------------------------

char const*	systool::Exception::what() const throw()
{
  return (this->_what.c_str());
}

std::string const&	systool::Exception::where() const
{
  return (this->_where);
}

//
// ProcessException.cpp for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr  4 16:30:19 2012 gressi_b
// Last update Sat May  5 14:13:30 2012 gaetan senn
//

#include <cstring>
#include <cerrno>

#include "ProcessException.hh"

// bomberman::layer::ProcessException ---------------------------------------------

systool::ProcessException::ProcessException(std::string const& what,
						 std::string const& where)
  : Exception(what + ": " + strerror(errno), where)
{
}

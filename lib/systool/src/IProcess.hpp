//
// IProcess.hh for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Tue Apr 10 15:08:49 2012 gressi_b
// Last update Sat May  5 14:24:45 2012 gaetan senn
//

#ifndef	__IPROCESS_HPP__
#define	__IPROCESS_HPP__

namespace systool
{
  template<typename T>
  class IProcess
  {
  public:
    virtual ~IProcess() {}

    typedef void	(*handler_t)(T);

    virtual void	spawn(T args) = 0;
    virtual void	wait() = 0;
    virtual void	kill(int sig) const = 0;
    virtual void	quit(int status) const = 0;

    virtual int		getStatus() const = 0;
  };
}

#endif

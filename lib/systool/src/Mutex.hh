//
// Mutex.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:41:57 2012 gaetan senn
// Last update Sat May  5 16:55:46 2012 benjamin bourdin
//

#ifndef	__MUTEX_HH__
#define	__MUTEX_HH__

#include <pthread.h>

#include "IMutex.hh"

namespace systool
{
  class Mutex : public systool::IMutex
  {
  public:
    typedef pthread_mutex_t	mutex_t;

  private:
    mutex_t		_mutex;

  private:
    Mutex(Mutex const&);
    Mutex&	operator=(Mutex const&);

  public:
    explicit Mutex();
    ~Mutex();

    void	lock();
    bool	tryLock();
    void	unlock();

    void*		getMutex();
  };
}

#endif

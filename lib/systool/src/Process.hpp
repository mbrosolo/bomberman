//
// Process.hh for plazza in /home/gressi_b/plazza/threads
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon Apr  2 11:26:20 2012 gressi_b
// Last update Sat May  5 15:06:00 2012 gaetan senn
//

#ifndef	__PROCESS_HPP__
#define	__PROCESS_HPP__

#include <string>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <csignal>
#include <sys/types.h>
#include <sys/wait.h>

#include "IProcess.hpp"
#include "ProcessException.hh"

namespace systool
{
  template<typename T>
  class Process : public IProcess<T>
  {

  private:
    pid_t	_pid;
    typename IProcess<T>::handler_t	_func;
    int		_status;

  private:
    Process();
    Process(Process const&);
    Process&	operator=(Process const&);

  public:
    Process(typename IProcess<T>::handler_t f)
      : _pid(-1), _func(f), _status(0)
    {}

    ~Process() {}

    // Calling spawn call fork and make child process execute
    // handler_t _func function.
    //
    void	spawn(T args)
    {
      this->_pid = fork();
      if (this->_pid == -1)
	throw ProcessException("fork(2) failure", "Process<>::spawn()");
      if (this->_pid == 0)
	{
	  this->_func(args);
	  exit(0);
	}
    }

    void	wait()
    {
      if (this->_pid > 0)
	{
	  if (::wait(&(this->_status)) == -1)
	    throw ProcessException("wait(2) failure", "Process<>::wait()");
	}
    }

    void	kill(int sig) const
    {
      if (this->_pid > 0)
	{
	  if (::kill(this->_pid, sig) == -1)
	    throw ProcessException("kill(2) failure", "Process<>::kill()");
	}
    }

    void	quit(int status) const
    {
      if (this->_pid == 0)
	exit(status);
    }

    // Getters --------------------------------------

    int		getStatus() const
    {
      return (this->_status);
    }
  };
}

#endif

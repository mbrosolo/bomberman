//
// SafeQueue.hpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:39:38 2012 gaetan senn
// Last update Mon May 21 18:13:47 2012 gaetan senn
//

#ifndef	__SAFEQUEUE_H_
#define	__SAFEQUEUE_H_

#include	<iostream>
#include	<queue>
#include	"IMutex.hh"
#include	"ISafeQueue.hpp"
#include	"ScopedLock.hh"

namespace systool
{
  template <typename T>
  class		SafeQueue : public systool::ISafeQueue<T>
  {
    std::queue<T>	DataPaquet;
    IMutex		*LockData;
    bool		finished;

  public:
    SafeQueue() : DataPaquet(), LockData(new Mutex()), finished(){
      finished = false;
    }

    SafeQueue(const SafeQueue<T> &o)
      : DataPaquet(o.DataPaquet), LockData(o.LockData), finished(o.finished)
    {
    }

    const SafeQueue<T> &operator=(const SafeQueue<T> &o)
    {
      static_cast<void>(o);
      return (*this);
    }

    ~SafeQueue() {
      delete LockData;
    }

    void		push(T type) {
      ScopedLock	lock(*LockData);

      if (!finished)
	DataPaquet.push(type);
      else
	std::cout << "Queue Locked" << std::endl;
    }

    bool		tryPop(T * value) {
      ScopedLock	lock(*LockData);

      if (!DataPaquet.empty())
	{
	  *value = DataPaquet.front();
	  DataPaquet.pop();
	  return (false);
	}
      return (true);
    }

    bool		isEmpty()
    {
      ScopedLock lock(*LockData);
      return (DataPaquet.empty());
    }

    bool		isFinished() {
      ScopedLock	lock(*LockData);

      if (DataPaquet.empty() && finished)
	return (true);
      return (false);
    }

    void		setFinished() {
      ScopedLock	lock(*LockData);
      finished = true;
    }
  };
}

#endif

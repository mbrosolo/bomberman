//
// ISemaphore.hh for systool in /home/ga/Documents/Projet/bomberman/bomberman/lib/threadpool/src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sun May  6 18:28:03 2012 gaetan senn
// Last update Sun May  6 23:37:15 2012 gaetan senn
//

#ifndef __ISEMAPHORE__
#define __ISEMAPHORE__

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#include <iostream>

class		ISemaphore
{
public:
  virtual ~ISemaphore(){}

  virtual void		unload() = 0;
  virtual void		load(int _nbsem) = 0;
  virtual void		setop(unsigned short sem_pos, short action) = 0;
  virtual void		setval(unsigned short sem_pos, short value) = 0;
};

#endif

//
// MutexException.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:41:34 2012 gaetan senn
// Last update Mon May 14 15:24:59 2012 gaetan senn
//

#include <cstring>
#include <cerrno>

#include "MutexException.hh"

// MutexException ---------------------------------------------

systool::MutexException::MutexException(std::string const& what,
					       std::string const& where)
  : Exception(what + ": " + strerror(errno), where)
{
}

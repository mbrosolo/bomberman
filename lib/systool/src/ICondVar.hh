//
// ICondVar.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:38:26 2012 gaetan senn
// Last update Mon May  7 13:55:10 2012 gaetan senn
//

#ifndef	_ICONDVAR_H_
#define	_ICONDVAR_H_

namespace systool
{
  class		ICondVar
  {
  public:
    virtual ~ICondVar(void) {}

    virtual void	wait(void) = 0;
    virtual void	signal(void) = 0;
    virtual void	broadcast(void) = 0;
    virtual int		timedwait(int, long int) = 0;
  };
}

#endif

//
// CondVarException.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:33:10 2012 gaetan senn
// Last update Fri May  4 11:33:12 2012 gaetan senn
//

#include <cstring>
#include <cerrno>

#include "CondVarException.hh"

// plazza::layer::CondVarException ---------------------------------------------

CondVarException::CondVarException(std::string const& what,
						   std::string const& where)
  : Exception(what + ": " + strerror(errno), where)
{
}

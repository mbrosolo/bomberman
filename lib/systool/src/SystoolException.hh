//
// Exception.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:14:45 2012 gaetan senn
// Last update Sat May  5 14:13:24 2012 gaetan senn
//

#ifndef	__SYSTOOL_EXCEPTION_HH__
#define	__SYSTOOL_EXCEPTION_HH__

#include <exception>
#include <string>

namespace systool
{
  class Exception : public std::exception
  {
  protected:
    std::string const	_what;
    std::string const	_where;

  public:
    Exception(std::string const&, std::string const& = "");
    virtual ~Exception() throw() {}

    virtual char const*	what() const throw();
    std::string const&	where() const;
  };
}

#endif

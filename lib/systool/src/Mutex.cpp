//
// Mutex.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:41:03 2012 gaetan senn
// Last update Sat May  5 15:04:17 2012 gaetan senn
//

#include <errno.h>

#include "Mutex.hh"
#include "MutexException.hh"

// Mutex -------------------------------------------------------

systool::Mutex::Mutex()
  : _mutex()
{
  pthread_mutex_init(&(this->_mutex), 0);
}

systool::Mutex::~Mutex()
{
  pthread_mutex_destroy(&(this->_mutex));
}

// Member functions --------------------------------------

void	systool::Mutex::lock()
{
  if (pthread_mutex_lock(&(this->_mutex)))
    throw MutexException("pthread_mutex_lock() failure",
			 "systool::Mutex::lock()");
}

bool	systool::Mutex::tryLock()
{
  int	err = pthread_mutex_trylock(&(this->_mutex));

  if (err && err != EBUSY)
    throw MutexException("pthread_mutex_trylock() failure",
			 "systool::Mutex::trylock()");
  return (err != EBUSY);
}

void	systool::Mutex::unlock()
{
  if (pthread_mutex_unlock(&(this->_mutex)))
    throw MutexException("pthread_mutex_unlock() failure",
			 "systool::Mutex::unlock()");
}

// Getters ------------------------------------------------

void*	systool::Mutex::getMutex()
{
  return (static_cast<void*>(&this->_mutex));
}

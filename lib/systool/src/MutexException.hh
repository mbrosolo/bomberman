//
// MutexException.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:41:45 2012 gaetan senn
// Last update Mon May 14 13:30:54 2012 gaetan senn
//

#ifndef	__MUTEX_EXCEPTION_HH__
#define	__MUTEX_EXCEPTION_HH__

#include <string>

#include "SystoolException.hh"

namespace systool
{
  class MutexException : public Exception
  {
  public:
    MutexException(std::string const&, std::string const& = "");
  };
}

#endif

//
// factory.hh for a in /home/bourdi_b/plazza
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Sun Apr 15 16:20:35 2012 benjamin bourdin
// Last update Mon May 14 13:17:32 2012 gaetan senn
//


#ifndef __FACTORY_HPP__
#define __FACTORY_HPP__

#include "IThread.hh"
#include "ICondVar.hh"
#include "IProcess.hpp"
#include "IMutex.hh"

#include "Thread.hpp"
#include "CondVar.hh"
#include "Process.hpp"
#include "Mutex.hh"

namespace systool
{
  class Factory {
  public:
    static IMutex	*getMutex(void) {
      return new Mutex();
    }

    static ICondVar	*getCondVar(IMutex &m) {
      return new CondVar(m);
    }

    template<class T>
    static IThread<T>	*getThread(T *classLink, typename Thread<T>::plink pt) {
      return new Thread<T>(classLink, pt);
    }

    template <class T>
    static IProcess<T>	*getProcess(typename IProcess<T>::handler_t f) {
      return new Process<T>(f);
    }

  private:
    Factory();
    ~Factory();
  };
}

#endif

//
// IMutex.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:38:44 2012 gaetan senn
// Last update Sat May  5 14:20:29 2012 gaetan senn
//

#ifndef	__IMUTEX_HH__
#define	__IMUTEX_HH__

namespace systool
{
  class IMutex
  {
  public:
    virtual ~IMutex() {}
    virtual void	lock() = 0;
    virtual bool	tryLock() = 0;
    virtual void	unlock() = 0;
    virtual void*	getMutex() = 0;
  };
}

#endif

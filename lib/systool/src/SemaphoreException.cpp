//
// CondVarException.cpp for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:33:10 2012 gaetan senn
// Last update Sun May  6 23:26:41 2012 gaetan senn
//

#include <cstring>
#include <cerrno>

#include "SemaphoreException.hh"

// systool::SemaphoreException ---------------------------------------------

SemaphoreException::SemaphoreException(std::string const& what,
						   std::string const& where)
  : Exception(what + ": " + strerror(errno), where)
{
}

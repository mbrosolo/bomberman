//
// ITask.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:40:02 2012 gaetan senn
// Last update Sat May  5 14:26:55 2012 gaetan senn
//

#ifndef __ITASK__
#define __ITASK__

/**
 * @file   ITask.hh
 * @author senn gaetan
 * @date   Fri May  4 00:04:50 2012
 *
 * @brief  Class for Task Thread
 * @version 0.1
 *
 */

namespace systool
{
  class			ITask
  {
  public:
    virtual		~ITask(){};
    virtual void		runTaskThread(void *) = 0;
    virtual void		stopTaskThread(void *) = 0;
  };
}

#endif

//
// Semaphore.hh for systool in /home/ga/Documents/Projet/bomberman/bomberman/lib/threadpool/src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sun May  6 18:35:17 2012 gaetan senn
// Last update Sun May  6 23:34:13 2012 gaetan senn
//

#ifndef __SEMAPHORE__
#define __SEMAPHORE__

#include "ISemaphore.hh"

class			Semaphore : public ISemaphore
{
  int			sem_id;
  key_t			key;
  int			nbsem;
  std::string const &	repository;
public:
  Semaphore(std::string const &repo, int value = 007);
  ~Semaphore();
  void			unload();
  void			load(int _nbsem);
  void			setop(unsigned short sem_pos, short action);
  void			setval(unsigned short sem_pos, short value);
};

#endif

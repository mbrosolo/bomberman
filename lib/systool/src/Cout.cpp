//
// Cout.cpp for systool in /home/ga/Documents/Projet/bomberman/bomberman/lib/threadpool/src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat May  5 16:06:28 2012 gaetan senn
// Last update Mon May 21 18:05:19 2012 gaetan senn
//

#include "factory.hpp"
#include "Cout.hh"

systool::Cout *systool::Cout::_instance = NULL;
systool::IMutex *systool::Cout::_minstance = systool::Factory::getMutex();

systool::Cout::Cout()
  : _mprint(NULL)
{
  _mprint = new Semaphore(".");
  _mprint->load(1);
  _mprint->setval(0, 1);
}

systool::Cout::~Cout()
{
  delete _mprint;
}

systool::Cout	*systool::Cout::getInstance()
{
  _minstance->lock();
  if (!systool::Cout::_instance)
    _instance = new systool::Cout();
  _minstance->unlock();
  return _instance;
}

bool	systool::Cout::deleteInstance()
{
  if (systool::Cout::_instance)
    {
      delete _instance;
      _instance = NULL;
      return (true);
    }
  return (false);
}

void		systool::Cout::lock()
{
  _mprint->setop(0, -1);
}

void		systool::Cout::unlock()
{
  std::cout.flush();
  _mprint->setop(0, 1);
}

//
// IThread.hh for ThreadPool in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 11:40:46 2012 gaetan senn
// Last update Tue May 22 14:42:41 2012 gaetan senn
//

#ifndef __ITHREAD_H_
#define __ITHREAD_H_
#include <pthread.h>

namespace systool
{
  template <typename T>
  class		IThread
  {
  public:
    enum eState
      {
	WAIT_MUTEX,
	INITIALIZED,
	LOCKED,
	UNLOCKED,
	TRYLOCK,
	WAITTHREAD
      };

    virtual ~IThread() {}

    virtual enum eState	stat() const = 0;
    virtual void		start(void *data) = 0;
    virtual void		cancel() = 0;
    virtual void		wait() = 0;
    virtual pthread_t		*getPthread() = 0;
  };
}

#endif

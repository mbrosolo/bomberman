//
// Cout.hh for systool in /home/ga/Documents/Projet/bomberman/bomberman/lib/threadpool/src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat May  5 16:03:59 2012 gaetan senn
// Last update Tue May 15 11:25:52 2012 gaetan senn
//

#ifndef __COUT__
#define __COUT__

#include "ISemaphore.hh"
#include "Semaphore.hh"
#include "IMutex.hh"

namespace systool
{
  class			Cout
  {
  private:
    static IMutex *_minstance;
    ISemaphore	*_mprint;
    static Cout	*_instance;

  public:
    Cout();
    Cout(Cout &);
    ~Cout();

    static Cout	*getInstance();
    static bool	deleteInstance();
    Cout const &	operator=(const Cout &);
    void	lock();
    void	unlock();
  };
}

#endif

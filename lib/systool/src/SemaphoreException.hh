//
// CondVarException.hh for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr  4 16:29:00 2012 gressi_b
// Last update Mon May 14 13:27:52 2012 gaetan senn
//

#ifndef	__SEMAPHORE_EXCEPTION_HH__
#define	__SEMAPHORE_EXCEPTION_HH__

#include <string>

#include "SystoolException.hh"

class SemaphoreException : public systool::Exception
{
public:
  SemaphoreException(std::string const&, std::string const& = "");
};

#endif

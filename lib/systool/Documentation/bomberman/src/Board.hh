//
// Board.hh for Bomberman in /home/texier_a//projets/bomberman
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Sat May  5 18:51:08 2012 anna texier
// Last update Wed May  9 13:10:00 2012 gaetan senn
//

#ifndef	__BOARD_H_
#define	__BOARD_H_

#include	<vector>
#include	"ITask.hh"
#include	"Square.hh"

class Board : public systool::ITask
{
private:
  size_t		_width;
  size_t		_height;
  std::vector<Square *>	_map;

public:
  Board(size_t, size_t);
  ~Board();
  Board(const Board &);

  // load
  void		generateMap(size_t, size_t);

  Square	*getCell(size_t, size_t);
  Square	*getCell(size_t);

  size_t	getWidth() const;
  size_t	getHeight() const;

  void		runTaskThread(void *);
  void		stopTaskThread(void *);

  void		printBombMap();
};

#endif

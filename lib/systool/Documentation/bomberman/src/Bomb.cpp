//
// Bombs.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu May  3 16:54:26 2012 anna texier
// Last update Wed May  9 14:03:31 2012 gaetan senn
//

#include	<unistd.h>

#include	"Bomb.hh"
#include	"CondVar.hh"

Bomb::Bomb(eBombType type, size_t range,
	   size_t x, size_t y, Player &player, Board &map)
  : _type(type), _range(range), _x(x), _y(y), _player(player), _map(map),
    _time(timeref[static_cast<int>(type)].second,
	  timeref[static_cast<int>(type)].micro)
{
  //INIT LIST OF POS TO KILL WITH KILL Bomb
}

void		Bomb::setImpact(t_time	&ref, int s, long int m)
{
  ref.second = s;
  ref.micro = m;
}

void		Bomb::StandardBomb()
{
  size_t		pos;

  pos = systool::Math<size_t>::c2dto1d(_x, _y, _map.getWidth());
  //ADD LEFT IMPACT
  if (_x > 0)
    setImpact(impact[systool::Math<size_t>::getLeft(pos, _map.getWidth(),
					    _map.getHeight())], 0, 0);
  //ADD TOP IMPACT
  if (_y > 0)
    setImpact(impact[systool::Math<size_t>::getTop(pos, _map.getWidth(),
					   _map.getHeight())], 0, 0);
  //ADD RIGHT IMPACT
  if (_x < (_map.getWidth() - 1))
    setImpact(impact[systool::Math<size_t>::getRight(pos, _map.getWidth(),
					     _map.getHeight())], 0, 0);
  //ADD BOTTOM IMPACT
  if (_y < (_map.getHeight() - 1))
    setImpact(impact[systool::Math<size_t>::getBottom(pos, _map.getWidth(),
					      _map.getHeight())], 0, 0);
}

Bomb::Bomb(const Bomb &o)
  : AObject(), _type(o._type), _range(o._range), _x(o._x),
    _y(o._y), _player(o._player), _map(o._map) {}

Bomb::~Bomb()
{
}

void		Bomb::start()
{
  std::map<size_t, t_time>::iterator	it;
  struct s_time			time;

  _time.timeout();
  //NORMALY CHANGE SHAPE OF BOMB TO FIRE :)
  // NOW SEND SIGNAL TO NEIGHT -> :)
  for (it = impact.begin(); it != impact.end(); it++)
    {
      _map.getCell((*it).first);
      time = (*it).second;
      if (time.second != 0)
	sleep(time.second);
      if (time.micro != 0)
	usleep(static_cast<useconds_t>(time.micro));
    }
}

void		Bomb::runTaskThread(void *)
{
}

void		Bomb::stopTaskThread(void *)
{
}

AObject::eType		Bomb::getType() const
{
  return (AObject::BOMB);
}

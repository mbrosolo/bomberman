
#include "ThreadPool.hh"
#include "Test.hpp"
#include "Board.hh"

int		main()
{
  systool::ITask		*map = new Board(10, 10);
  systool::ThreadPool	pool(10);

  pool.start();
  pool.addTask(map);
  pool.stop();
}

//
// Test.hpp for Thread in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 01:42:32 2012 gaetan senn
// Last update Mon May  7 11:39:01 2012 gaetan senn
//

#include "ITask.hh"
#include "Cout.hh"

class		Test : public systool::ITask
{
public:
  Test()
  {
  }
  ~Test(){}
  virtual void		runTaskThread(void *)
  {
    (systool::Cout::getInstance())->lock();
    std::cout << "THIS THIS NEW TASK :)" << std::endl;
    (systool::Cout::getInstance())->unlock();
    sleep(2);
    (systool::Cout::getInstance())->lock();
    std::cout << "DONE." << std::endl;
    (systool::Cout::getInstance())->unlock();
  }
  virtual void		stopTaskThread(void *)
  {
  }
};

//
// Board.cpp for Bomberman in /home/texier_a//projets/bomberman
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Sat May  5 18:51:15 2012 anna texier
// Last update Wed May  9 13:38:47 2012 gaetan senn
//

#include	"Board.hh"
#include	<iostream>

Board::Board(size_t width, size_t height)
  : _width(width), _height(height)
{
  generateMap(_width, _height);
}

Board::~Board()
{
}

void	Board::generateMap(size_t x, size_t y)
{
  (void)x;
  (void)y;

  size_t	i = 0;
  size_t	j = 0;

   if (x < 3 || y < 3) // test maxi a ajouter
    throw; // throw une exception pour la taille de la map
  while (j < y)
    _map.push_back(new Square(0, j++, Square::WALL));
  while (++i < (x - 1))
    {
      j = 0;
      _map.push_back(new Square(i, 0, Square::WALL));
      while (++j < (y - 1))
	{ // pour moins de densite, augmenter le 2,
	  // ajouter en parametre ? avec valeur par defaut 2 ou 3
	  if (!(j % (2 + !(y % 2))) &&
	      !(i % (2 + !(x % 2))) //&&)
	      //(random() % 3) != 2)
	      )
	    _map.push_back(new Square(i, j, Square::WALL));
	  else
	    _map.push_back(new Square(i, j, Square::FLOOR));
	}
      _map.push_back(new Square(i, x, Square::WALL));
    }
  j = 0;
  while (j < y)
    _map.push_back(new Square(x, j++, Square::WALL));
}

Square	*Board::getCell(size_t x, size_t y)
{
  return _map[x + y * _width];
}

Square	*Board::getCell(size_t pos)
{
  return _map[pos];
}

size_t	Board::getWidth() const
{
  return _width;
}

size_t	Board::getHeight() const
{
  return _height;
}

void	Board::runTaskThread(void *data)
{
  (void)data;
  this->printBombMap();
}

void	Board::stopTaskThread(void *data)
{
  (void) data;
}

void		Board::printBombMap()
{
  std::vector<Square *>::iterator	it;
  size_t					line = 0;

  for (it = _map.begin(); it != _map.end() ;it++)
    {
      if (line == _width)
	{
	  line = 0;
	  std::cout << std::endl;
	}
      if ((*it)->getType() == Square::WALL)
	std::cout << "H";
      if ((*it)->getType() == Square::FLOOR)
	{
	  //(*it)->
	  std::cout << "|";
	}
      line++;
    }
  std::cout << std::endl;
}

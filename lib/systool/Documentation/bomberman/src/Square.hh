//
// Square.hh for Bomberman in /home/texier_a//projets/bomberman
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu May  3 12:02:26 2012 anna texier
// Last update Wed May  9 13:01:07 2012 gaetan senn
//

#ifndef	__SQUARE_H_
#define	__SQUARE_H_

#include	<list>

class Square
{
public:
  enum	eSquareType
    {
      FLOOR,
      WALL
      // autres
    };

private:
  size_t		_x;
  size_t		_y;
  eSquareType		_type;
  //std::list<AObject *>	_objects;

public:
  Square(size_t x, size_t y, eSquareType type = FLOOR);
  ~Square();
  Square(const Square &);
  Square const &	operator=(const Square &);

  eSquareType	getType() const;
  size_t	getListSize() const;
  void		applyDestruction(){}
  //  AObject	*operator[](const size_t) const;
};

#endif

//
// Time.hh for plazza in /home/texier_a//projets/plazza-2015-texier_a
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Apr 12 19:25:37 2012 anna texier
// Last update Mon May  7 15:31:23 2012 gaetan senn
//

#ifndef __TIME_HH_
#define __TIME_HH_

#include "CondVar.hh"
#include "Mutex.hh"

#include <sys/time.h>

class Time
{
private:
  systool::IMutex		*_mutex;
  systool::ICondVar		*_timeout;
  int second;
  long int micro;

public:
  Time(int second = 1, long int micro = 0);
  ~Time();

  void	timeout();
  void	settime(int const _second, long int const _micro);
  systool::ICondVar	*getCondVar() const;
};

#endif

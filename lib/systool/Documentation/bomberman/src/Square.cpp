//
// Square.cpp for Bomberman in /home/texier_a//projets/bomberman
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu May  3 12:16:18 2012 anna texier
// Last update Wed May  9 13:00:57 2012 gaetan senn
//

#include	"Square.hh"

Square::Square(size_t x, size_t y,
	       eSquareType type)
  : _x(x), _y(y), _type(type)/* _objects()*/ {}

Square::Square(const Square &other) : _type(other._type) /*_objects(other._objects)*/ {}

Square::~Square() {}

Square const &	Square::operator=(const Square &other)
{
  _type = other._type;
  //_objects = other._objects;
  return *this;
}

Square::eSquareType	Square::getType() const
{
  return _type;
}

size_t	Square::getListSize() const
{
  return (0);
  //return _objects.size();
}

/*AObject	*Square::operator[](const size_t n) const
{
  return _objects[n];
  }*/

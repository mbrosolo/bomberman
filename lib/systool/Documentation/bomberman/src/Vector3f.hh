//
// Vector3f.hh for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May  7 13:34:22 2012 benjamin bourdin
// Last update Wed May  9 11:47:05 2012 benjamin bourdin
//

#ifndef __VECTOR_3F_HH__
#define __VECTOR_3F_HH__

struct Vector3f
{

  float x;
  float y;
  float z;

  Vector3f(void);
  Vector3f(Vector3f const &);
  Vector3f(float x, float y, float z);

};

#endif

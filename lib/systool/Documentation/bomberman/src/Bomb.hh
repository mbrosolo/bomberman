//
// Bombs.hh for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu May  3 16:53:25 2012 anna texier
// Last update Wed May  9 14:02:00 2012 gaetan senn
//

#ifndef	__BOMB_H_
#define	__BOMB_H_

#include <stdio.h>
#include <map>

#include "ITask.hh"
#include "Math.hpp"
#include "Player.hh"
#include "Board.hh"
#include "Time.hh"
#include "AObject.hh"

class Bomb : public systool::ITask, public AObject
{
public:
  typedef void (Bomb::*fimpact)();
  typedef struct	s_time
  {
    int			second;
    long int		micro;
  }			t_time;
  enum eBombType
    {
      STANDARD
    };
private:
  eBombType	_type;
  size_t	_range;
  size_t	_x;
  size_t	_y;
  Player &	_player;
  Board &		_map;
  Time		_time;
  std::map<size_t, struct s_time >	impact;
  // CondVar
  // Timer

public:
  Bomb(eBombType, size_t, size_t, size_t, Player &, Board &);
  Bomb(const Bomb &);
  ~Bomb();

  //TASKS
  void		start();
  //THREAD
  void		runTaskThread(void *);
  void		stopTaskThread(void *);
  void		StandardBomb();
  void		setImpact(t_time &ref, int s, long int m);
  AObject::eType		getType() const;
};

static const Bomb::t_time		timeref[1] =
  {
    { 3, 0}
  };

static const Bomb::fimpact	impactref[1] =
  {
    &Bomb::StandardBomb
  };

#endif

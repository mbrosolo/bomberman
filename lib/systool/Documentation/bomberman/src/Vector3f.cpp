//
// Vector3f.cpp for  in /home/bourdi_b/bomberman/graphic
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Mon May  7 13:34:10 2012 benjamin bourdin
// Last update Wed May  9 12:48:46 2012 benjamin bourdin
//

#include	"Vector3f.hh"

Vector3f::Vector3f(void)
  : x(0.0f), y(0.0f), z(0.0f)
{
}

Vector3f::Vector3f(Vector3f const &v)
  : x(v.x), y(v.y), z(v.z)
{
}

Vector3f::Vector3f(float x, float y, float z)
  : x(x), y(y), z(z)
{
}

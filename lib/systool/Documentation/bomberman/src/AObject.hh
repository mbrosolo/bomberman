//
// AObject.hh for bomberman in /home/ga/Documents/Projet/bomberman/bomberman/lib/systool/Documentation/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed May  9 13:21:55 2012 gaetan senn
// Last update Wed May  9 15:40:44 2012 gaetan senn
//

#ifndef __AOBJECT__
#define __AOBJECT__
#include "Vector3f.hh"

class		AObject
{
public:
  enum		eType
    {
      BOMB,
      FIRE
    };
protected:
  Vector3f _position;
  Vector3f _rotation;
public:
  AObject(Vector3f pos = Vector3f(0.0f, 0.0f, 0.0f), Vector3f rot = Vector3f(0.0f, 0.0f, 0.0f))
    : _position(pos), _rotation(rot)
  {
  }
  //virtual void initialize(void) = 0;
  virtual eType		getType() const = 0;
  virtual ~AObject(){}
}

#endif

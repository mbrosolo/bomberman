//
// Time.cpp for plazza in /home/texier_a//projets/plazza-2015-texier_a
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Apr 12 19:25:16 2012 anna texier
// Last update Mon May  7 17:01:33 2012 gaetan senn
//

#include	<iostream>
#include	"Time.hh"

Time::Time(const int _second, const long int _micro)
  : _mutex(new systool::Mutex()), _timeout(new systool::CondVar(*_mutex)),
    second(_second), micro(_micro)
{
}

Time::~Time() {}

void	Time::settime(int const _second, long int const _micro)
{
  second = _second;
  micro = _micro;
}
void	Time::timeout()
{
  _timeout->timedwait(second, micro);
}

systool::ICondVar		*Time::getCondVar() const
{
  return (_timeout);
}

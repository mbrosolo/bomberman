//
// Test.hpp for Thread in /home/ga/Documents/Projet/Library/c++/Thread
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May  4 01:42:32 2012 gaetan senn
// Last update Tue May 22 13:51:14 2012 gaetan senn
//

#include "ITask.hh"
#include "Cout.hh"
#include "Mutex.hh"
#include <cstdio>
#include <list>

class		Test : public systool::ITask
{
  std::list<int>	&list;
  systool::Mutex			_protection;
public:
  Test(std::list<int> &ref)
    : list(ref)
  {
  }
  ~Test(){}
  virtual void		runTaskThread(void *)
  {
    int			test;

    test = 0;
    while (42)
      {
	std::cout << "test = " << test << std::endl;
	test++;
	sleep(1);
      }
    std::cout << "LOL" << std::endl;
  }
  virtual void		stopTaskThread(void *)
  {
  }
};

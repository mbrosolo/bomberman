
#include "ThreadPool.hh"
#include "Test.hpp"
#include	<list>
#include	<pthread.h>
#include	"Cout.hh"

int		main()
{
  systool::ThreadPool	pool(2);
  systool::ITask		*a;
  systool::ITask		*b;
  std::list<int>		* list = new std::list<int>(10, 100);


  //  signal(SIGUSR1, sighand);
  //signal(SIGUSR2, test);

  a = new Test(*list);
  b = new Test(*list);
  pool.start();
  pool.addTask(a);
  pool.addTask(b);
  sleep(2);
  pool.pause();
  sleep(5);
  std::cout << "DONC vOILA XD" << std::endl;
  pool.resume();
  sleep(2);
  pool.stop();
  delete a;
  delete b;
}

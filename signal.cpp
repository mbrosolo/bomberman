//
// signal.cpp for bm in /home/gressi_b/bm
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 11 13:30:35 2012 gressi_b
// Last update Wed Apr 11 13:30:35 2012 gressi_b
//

#include "signal.hh"
#include "SyscallException.hh"

bm::signal::handler_t
bm::signal::signal(int signum, handler_t h)
{
  handler_t	old;

  old = ::signal(signum, h);
  if (old == SIG_ERR)
    throw SyscallException("signal(2) failure", "signal::signal()");
  return (old);
}

bm::signal::handler_t
bm::signal::ignore(int signum)
{
  handler_t	old;

  old = ::signal(signum, SIG_IGN);
  if (old == SIG_ERR)
    throw SyscallException("signal(2) failure", "signal::ignore()");
  return (old);
}

bm::signal::handler_t
bm::signal::setDefault(int signum)
{
  handler_t	old;

  old = ::signal(signum, SIG_DFL);
  if (old == SIG_ERR)
    throw SyscallException("signal(2) failure", "signal::setDefault()");
  return (old);
}

//
// Solid.hh for bomberman in /home/ga/Documents/Projet/bomberman/bomberman/core
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Tue May 15 11:02:30 2012 gaetan senn
// Last update Sun Jun  3 17:57:46 2012 gaetan senn
//

#ifndef __EFFECT__HH
#define __EFFECT__HH

#include	<iostream>
#include	"AObject.hh"
#include	"Primitive.hh"
#include	"ComplexObj.hh"
#include	"MyGame.hh"

class		Effect : public AObject
{
protected:
  size_t	_x;
  size_t	_y;
  AObject::eType	_type;
public:
  Effect(size_t x, size_t y, AObject::eType type = AObject::FIRE);
  ~Effect();
  virtual eType		getObjectType() const;
  virtual void		erase();
};

#endif

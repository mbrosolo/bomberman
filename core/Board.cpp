//
// Board.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Sat May  5 18:51:15 2012 anna texier
// Last update Sun Jun  3 17:27:17 2012 gaetan senn
//

#include	"Board.hh"
#include	"LoadMap.hh"
#include	"Solid.hh"
#include	"Cout.hh"
#include	"APlayer.hh"
#include	"Core.hh"
#include	"MyGame.hh"
#include	"Item.hh"
#include	"Effect.hh"
#include	"Factory.hh"
#include	"ExceptionLoadMap.hh"
#include	"ExceptionCore.hh"

Board::Board()
  : _width(0), _height(0), _scale(0.0f), _addBonus(), _board(),
    Event()
{
}

Board::Board(size_t width, size_t height)
  : _width(width), _height(height), _scale(0.0f), _addBonus(), _board(),
    Event()
{
  if (_width < 3 || _height < 3) // test maxi a ajouter
    throw ExceptionCore("Load Map Fail", "Board(size_t, size_t)");

  for (size_t y = 0 ; y < _height ; ++y)
    for (size_t x = 0 ; x < _width ; ++x)
      {
	_board.push_back(new Square(x, y));
#if defined DEBUG_BOARD && DEBUG_BOARD < 1 || not defined DEBUG_BOARD
	if (x == 0 && y == 0)
#endif
	  pushAObjectUpdate(x, y, new Solid(x, y, AObject::FLOOR));
      }
  _addBonus[0] = _refAObject[AObject::SPEED_BONUS];
  _addBonus[1] = _refAObject[AObject::EXPANDBOMB_BONUS];
  _addBonus[2] = _refAObject[AObject::LINEBOMB_BONUS];
  _addBonus[3] = _refAObject[AObject::ITEM];
  _addBonus[4] = _refAObject[AObject::SPEED_MALLUS];
  _addBonus[5] = _refAObject[AObject::NBBOMB_BONUS];
  generateMap();
}

Board::Board(const char *file)
  : _width(0), _height(0), _scale(0.0f), _addBonus(), _board(),
    Event()
{
  try
    {
      LoadMap load(file, *this);
    }
  catch (ExceptionLoadMap &e)
    {
      std::cout << e.what() << " on " << e.where() << std::endl;
      throw ExceptionCore("Load Map from file", "Board(const char *file)");
    }
}


Board::~Board()
{
  std::vector<Square *>::iterator	it;

  for (it = _board.begin();it != _board.end();it++)
    delete (*it);
}

bool		Board::pushAObjectUpdate(size_t x, size_t y, AObject *o)
{
  this->getSquare(x, y)->addObject(o, true);
  Core::getInstance()->addObjectUpdate(o);
  return (true);

}

bool	Board::isFree() const
{
  std::vector<Square *>::const_iterator	it;

  for (it = _board.begin() ;it != _board.end(); ++it)
    {
      if ((*it)->isImpactable())
	return (true);
    }
  return (false);
}

void	Board::addSquare(Square *s)
{
  _board.push_back(s);
}

void	Board::setWidth(size_t w)
{
  _width = w;
}

void	Board::setHeight(size_t h)
{
  _height = h;
}

void	Board::generateMap()
{
  size_t	x;
  size_t	y;
  int		rand;

  for (x = 0 ; x < _width ; x++)
    _board[x]->addObject(new Solid(x, 0), true);
  for (y = 1 ; y < (_height - 1) ; ++y)
    {
      _board[y * _width]->addObject(new Solid(0, y), true);
      for (x = 1 ; x < (_width - 1) ; ++x)
      	if (!(x % (2 + !(_width % 2))) &&
      	    !(y % (2 + !(_height % 2))) &&
      	    (random() % 2) != 2)
      	  _board[y * _width + x]->addObject(new Solid(x, y), true);
      else if (random() % 4 == 1)
	{
	  rand = random() % _addBonus.size();
	  if (_addBonus.find(rand) != _addBonus.end())
	    {
	      this->getSquare(x, y)->
		addObject((Factory::getInstance()->*
			   _addBonus[random() % _addBonus.size()])(x, y, this), true);
	    }
	  else
	    throw ExceptionCore("Can't find AObject Constructor", "generateMap()");
	}
      _board[y * _width + (_width - 1)]->addObject(new Solid(_width - 1, y), true);
    }
  for (x = 0 ; x < _width ; x++)
    _board[(_height - 1) * _width + x]->addObject(new Solid(x, _height - 1), true);
}

Square	*Board::getSquare(size_t x, size_t y) const
{
  return _board[x + y * _width];
}

Square	*Board::getSquare(size_t pos) const
{
  return _board[pos];
}

size_t	Board::getWidth() const
{
  return _width;
}

void	Board::moveObject(AObject *pt, size_t dx, size_t dy)
{
  dynamic_cast<APlayer *>(pt)->move(dx, dy);
}

size_t	Board::getHeight() const
{
  return _height;
}


void		Board::printBoard() const
{
  std::vector<Square *>::const_iterator	it;

  for (it = _board.begin(); it != _board.end() ;it++)
    (*it)->printObjects();
}

void		Board::updateBoard(const MyClock&gc, gdl::Input&in) const
{
  std::vector<Square *>::const_iterator	it;

  for (it = _board.begin(); it != _board.end() ;it++)
    (*it)->updateObjects(gc, in);
}


bool		Board::pushAObject(size_t x, size_t y, AObject *obj)
{
  _board[systool::Math<size_t>::c2dto1d(x, y, _width)]->addObject(obj, true);
  return (true);
}

bool		Board::pushAObject(size_t pos, AObject *obj)
{
  _board[pos]->addObject(obj, true);
  return (true);
}

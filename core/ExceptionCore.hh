//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Sun Jun  3 16:45:23 2012 gaetan senn
//

#ifndef __EXCEPTION_CORE_HH__
#define __EXCEPTION_CORE_HH__

#include	<string>
#include	"../ExceptionRuntime.hh"

class ExceptionCore : public ExceptionRuntime
{
public:
  ExceptionCore(std::string const &what = "", std::string const &where = "") throw();
  virtual ~ExceptionCore() throw();

};

#endif

//
// APlayer.hh for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri May  4 18:36:43 2012 anna texier
// Last update Sun Jun  3 22:08:22 2012 gaetan senn
//

#ifndef	__APLAYER_H_
#define	__APLAYER_H_

#include	<string>
#include	<list>
#include	<map>
#include	"ITask.hh"
#include	"Board.hh"
#include	"AObject.hh"
#include	"GameEventManager.hh"

/* DEFINE BONUS GET VALUE */
#define		NBNUMBERBOMBBONUS 1

class Bomb;

#define CST_MOVE	100.0f

class APlayer : public AObject
{
  /** 
   * 
   * 
   * @param bonuspt 
   */

  typedef void	*(APlayer::*bonuspt)();
protected:
  Board &		_map; /*!< map of player */
  std::string		_name; /*!< name of player */
  std::list<AObject *>	_items; /*!< item of player */
  std::list<Bomb *>	_bombs; /*!< bomb of player */
  size_t		_lives; /*!< life of player */
  size_t		_score; /*!< score of player */
  float			_timestart; /*!< time start of player */
  std::map<AObject::eType, bonuspt>	_tabbonus; /*!< bonus pt */
  GameEventManager	*_playerKeys; /*!< keyevent of key */

public:
  /** 
   * 
   * 
   * 
   * @return 
   */
  APlayer(Board &, std::string &, size_t x, size_t y,
	  GameEventManager *playerKeys = NULL);
  /** 
   * @brief Aplayer construct copy
   * 
   * 
   * @return 
   */
  APlayer(const APlayer &);
  /** 
   * @brief construct destructor
   * 
   * 
   * @return 
   */
  virtual ~APlayer();
  /** 
   * @brief equals
   * 
   * 
   * @return 
   */
  const APlayer	&operator=(const APlayer &);
  /** 
   * @brief update Player method
   * 
   * @param g 
   */
  virtual void		update(const MyClock&g, gdl::Input &);
  /* SETTER PLAYER */

  /** 
   * @brief run player
   * 
   * @param g 
   */
  virtual void		run(MyClock const& g);

  /** 
   * @brief setlives of player
   * 
   * @param lives 
   */
  void			setLives(size_t lives);
  /** 
   * @brief setscore of player
   * 
   * @param score 
   */
  void			setScore(size_t score);
  /** 
   * @brief set item of player
   * 
   * 
   * @return 
   */
  std::list<AObject *>	const &getItems() const;
  /** 
   * @brief get Bombs of player
   * 
   * 
   * @return 
   */
  std::list<Bomb *>	const &getBombs() const;
  /** 
   * @brief move player
   * 
   * @param size_t 
   * @param size_t 
   */
  void			move(const size_t, const size_t);
  /** 
   * @brief checkSquare of AObject
   * 
   * 
   * @return 
   */
  bool			checkSquare(const Square *);
  /** 
   * @brief getName of PLayer
   * 
   * 
   * @return 
   */
  const std::string	&getName() const;
  /** 
   * @brief drop bomb
   * 
   */
  virtual void		dropBomb();
    /** 
   * @brief getItem of player
   * 
   */
  void			getItem(AObject *);
  /** 
   * @brief getBomb of AObject
   * 
   */
  void			getBomb(AObject *);
  /** 
   * @brief die function
   * 
   */
  void			die();
  /** 
   * @brief setkey of player
   * 
   * @param p 
   */
  void			setKeys(GameEventManager *p);
  /* SCORE METHOD */
  /** 
   * @brief winPoint for player
   * 
   * @param nb 
   */
  void			winPoint(unsigned int nb);
  /* METHOD OF AOBJECT */
  /* BONUS PT */
  /** 
   * @brief getObject Type
   * 
   * 
   * @return 
   */
  virtual eType		getObjectType() const;
  /** 
   * @brief MoveUpdate of player
   * 
   */
  void			moveUpdate();
  /** 
   * @brief get Object
   * 
   */
  void			getHandled();
  /** 
   * @brief getnb bomb
   * 
   * 
   * @return 
   */
  void			*numberBombBonus();
  /** 
   * @brief get speed bonus
   * 
   * 
   * @return 
   */
  void			*speedBonus();
  /** 
   * @brief get expandBombBonus
   * 
   * 
   * @return 
   */
  void			*expandBombBonus();
  /** 
   * @brief getline bomb bonus
   * 
   * 
   * @return 
   */
  void			*lineBombBonus();
  /** 
   * @brief get speed Malus
   * 
   * 
   * @return 
   */
  void			*speedMalus();
  /** 
   * @brief apply bonus
   * 
   * @param type 
   * 
   * @return 
   */
  void			*applyBonus(AObject::eType type);
  /** 
   * @brief erase function
   * 
   */
  void			erase();
  /** 
   * @brief clean function bomb
   * 
   */
  void			cleanBomb();
  /** 
   * @brief move Left object
   * 
   */
  void			moveLeft();
  /** 
   * @brief move right object
   * 
   */
  void			moveRight();
  /** 
   * @brief move up object
   * 
   */
  void			moveUp();
  /** 
   * @brief move down object
   * 
   */
  void			moveDown();
  /** 
   * @brief take object
   * 
   */
  void			take() const;
  /** 
   * @brief leave object
   * 
   */
  void			leave();
  /** 
   * @brief like of object
   * 
   * 
   * @return 
   */
  size_t		life() const;
  /** 
   * @brief getScore of object
   * 
   * 
   * @return 
   */
  size_t		getScore() const;
  /** 
   * @brief getNbBonus
   * 
   * @param type 
   * 
   * @return 
   */
  size_t		getNbBonus(AObject::eType type) const;
};

#endif

//
// AObject.cpp for Bomberman in /home/texier_a//projets/bomberman/core
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Mon May 21 18:12:13 2012 anna texier
// Last update Sun Jun  3 22:00:40 2012 tosoni_t
//

#include	"AObject.hh"
#include	"Color.hh"
#include	"MyGame.hh"

AObject::AObject(size_t x, size_t y, bool destructable, bool dam, bool handled,
		 eType type, unsigned int value,
		 float timeremaining, float timeremaininggetted)
  : _x(x), _y(y), _destructable(destructable), _dam(dam), _handled(handled),
    _type(type), _form(NULL),
    _beRemovable(false), _toMove(false), _value(value),
    _timeremaining(timeremaining), _timeremaininggetted(timeremaininggetted)
{
}

AObject::~AObject() {}

AObject::AObject(AObject const &o)
  : _x(o._x), _y(o._y), _destructable(o._destructable),
    _dam(o._dam), _handled(o._handled),
    _type(o._type), _form(o._form),
    _beRemovable(o._beRemovable), _toMove(o._toMove), _value(o._value),
    _timeremaining(o._timeremaining),
    _timeremaininggetted(o._timeremaininggetted)
{
}

void					AObject::setRemaining(float value)
{
  _timeremaining += value;
}

void					AObject::setRemainingGetted(float value)
{
  _timeremaininggetted += value;
}

void					AObject::setBeRemovable(bool beRemovable)
{
  _beRemovable = beRemovable;
}

void					AObject::setValue(size_t value)
{
  _value = value;
}

void					AObject::setTimeRemaining(float time)
{
  _timeremaining = time;
}

void					AObject::setTimeRemainingGetted(float timeGetted)
{
  _timeremaininggetted = timeGetted;
}

void					AObject::setDestructable(bool d)
{
  _destructable = d;
}

void					AObject::setDam(bool d)
{
  _dam = d;
}

void					AObject::setHandled(bool d)
{
  _handled = d;
}

std::string const &		AObject::getKey() const
{
  return graphic::MyGame::getInstance()->getTextureManager()->getKey(_form->getTexture());
}

bool				AObject::isDestructable() const
{
  return (_destructable);
}

bool				AObject::isDam() const
{
  return (_dam);
}

bool				AObject::isHandled() const
{
  return (_handled);
}

AObject&			AObject::operator=(AObject const &o)
{
  *this = o;
  return *this;
}

void				AObject::draw()
{
  if (_form)
    _form->draw();
}

void				AObject::update(const MyClock& gc,
						gdl::Input& in)
{
  if (_form)
    _form->update(gc, in);
}

graphic::AForm			*AObject::getForm(void) const
{
  return _form;
}

void				AObject::moveLeft()
{
  std::cout << ENDCOLOR << std::endl;
}

void				AObject::moveRight()
{
  std::cout << ENDCOLOR << std::endl;
}

void				AObject::moveUp()
{
  std::cout << ENDCOLOR << std::endl;
}

void				AObject::moveDown()
{
  std::cout << ENDCOLOR << std::endl;
}

void				AObject::take()
{
  std::cout << ENDCOLOR << std::endl;
}

void				AObject::leave()
{
  std::cout << ENDCOLOR << std::endl;
}

void				AObject::setToMove(bool value)
{
  _toMove = value;
}

bool				AObject::beRemoved() const
{
  return _beRemovable;
}

unsigned int			AObject::getValue() const
{
  return (_value);
}

size_t				AObject::getX() const
{
  return _x;
}

size_t				AObject::getY() const
{
  return _y;
}

bool				AObject::toMove() const
{
  return _toMove;
}

graphic::Vector const		&AObject::getPosition() const
{
  return (_form->getPosition());
}

void				AObject::setPosition(graphic::Vector const &p)
{
  _form->setPosition(p);
}

void				AObject::setScale(graphic::Scale const &scale)
{
  _form->setScale(scale);
}

size_t				AObject::getRangeX() const
{
  return (0);
}

size_t				AObject::getRangeY() const
{
  return (0);
}

void				AObject::setRangeX(size_t)
{
}

void				AObject::setRangeY(size_t)
{
}

float					AObject::getRemainingGetted() const
{
  return _timeremaininggetted;
}

float					AObject::getRemaining() const
{
  return _timeremaining;
}

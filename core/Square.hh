//
// Square.hh for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu May  3 12:02:26 2012 anna texier
// Last update Sun Jun  3 23:16:12 2012 gaetan senn
//

#ifndef	__SQUARE_H_
#define	__SQUARE_H_

#include	<cstdio>
#include	<list>
#include	<iostream>
#include	<algorithm>
#include	"AObject.hh"
#include	"factory.hpp"
#include	<sys/types.h>
#include	"AForm.hh"

class Square
{
private:
  size_t			_x; /*!< position x */
  size_t			_y; /*!< position y */
  graphic::Scale		_scale; /*!< scale */
  graphic::Vector		_position; /*!< position */
  std::list<AObject   *>	_toBeSaved; /*!< besaved */
  std::list<AObject *>		_objects; /*!< objects */

public:
  /** 
   * @brief Square construct x, y
   * 
   * @param size_t 
   * @param size_t 
   */
  Square(const size_t, const size_t);
  /** 
   * @brief destructor
   * 
   */
  ~Square();
  /** 
   * @brief coplien
   * 
   */
  Square(const Square &);
  /** 
   * @brief equals
   * 
   * 
   * @return 
   */
  Square const &	operator=(const Square &);
  /** 
   * @brief getObject 
   * 
   * 
   * @return 
   */
  std::list<AObject *>&		getObjects();
  /** 
   * 
   * 
   * @param s 
   */
  /* GRAPHIC INFORMATION */
  /** 
   * @brief setScale 
   * 
   * @param s 
   */
  void				setScale(graphic::Scale const &s);
  /** 
   * @brief getScale 
   * 
   * 
   * @return 
   */
  graphic::Scale	const	&getScale() const;
  /** 
   * @brief getPosition 
   * 
   * 
   * @return 
   */
  graphic::Vector	const &	getPosition() const;
  /** 
   * @brief setPosition
   * 
   * @param p 
   */
  void				setPosition(graphic::Vector const &p);
  /* END OF GRAPHIC INFORMATION */

  /** 
   * @brief getObjects
   * 
   * 
   * @return 
   */
  std::list<AObject *>		 const &getObjects() const;
  /** 
   * @brief getNbSaved
   * 
   * 
   * @return 
   */
  std::list<AObject *> const		&getNbSaved();
  /** 
   * @brief printObjects
   * 
   */
  void		printObjects();
  /** 
   * @brief update Object
   * 
   */
  void		updateObjects(const MyClock&, gdl::Input&);
  /** 
   * @brief getListsize 
   * 
   * 
   * @return 
   */
  size_t	getListSize() const;
  /* CURRENTLY FOR DEBUG */
  /** 
   * @brief add Obejct
   * 
   */
  void		addObject(AObject *, bool);
  /** 
   * @brief getHandled
   * 
   * 
   * @return 
   */
  std::list<AObject *>		*getHandled();
  /** 
   * @brief struct for check
   * 
   * 
   * @return 
   */
  struct Check
  {
    bool		operator()(AObject *obj)
    {
      bool		ret;

      if (obj->getObjectType() != AObject::BOMB && obj->isDestructable())
	ret = true;
      else
	ret = false;
      obj->erase();
      return (ret);
    }
  };
  /** 
   * @brief withWall
   * 
   * 
   * @return 
   */
  bool			withWall() const;
  /** 
   * @brief getX
   * 
   * 
   * @return 
   */
  size_t		getX();
  /** 
   * @brief getY
   * 
   * 
   * @return 
   */
  size_t		getY();
  /** 
   * @brief applyDestruction
   * 
   */
  void			applyDestruction();
  /** 
   * @brief clear Object
   * 
   */
  void			clearAObjects();
  /** 
   * @brief move Aobject
   * 
   */
  void			moveAObjecs();
  /** 
   * @brief remoeve element
   * 
   * 
   * @return 
   */
  bool			removeElement(AObject *);
  /** 
   * @brief is Impactable
   * 
   * 
   * @return 
   */
  bool			isImpactable() const;
  /** 
   * @brief get Square Value
   * 
   * 
   * @return 
   */
  unsigned int		getSquareValue() const;
  /** 
   * @brief pop objects
   * 
   * @param obj 
   * 
   * @return 
   */
  AObject		*popObject(AObject *obj);
};

#endif

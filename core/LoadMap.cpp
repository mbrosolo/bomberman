//
// LoadMap.cpp for Bomberman in /home/texier_a//projets/bomberman/core
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Tue May 22 15:47:57 2012 anna texier
// Last update Sun Jun  3 22:10:47 2012 tosoni_t
//

#include	"LoadMap.hh"
#include	"Solid.hh"
#include	"Square.hh"
#include	"Core.hh"
#include	<sstream>
#include	"AObject.hh"
#include	"ExceptionLoadMap.hh"
#include	"Item.hh"
#include	<unistd.h>
#include	"Factory.hh"


LoadMap::LoadMap(const char *file, Board &board)
  : _board(board), _file(), _height(0), _width(0), _ptallocation()
{
  _ptallocation[AObject::WALL] = _refAObject[AObject::WALL];
  _ptallocation[AObject::ITEM] = _refAObject[AObject::ITEM];
  _ptallocation[AObject::SPEED_BONUS] = _refAObject[AObject::SPEED_BONUS];
  _ptallocation[AObject::EXPANDBOMB_BONUS] = _refAObject[AObject::EXPANDBOMB_BONUS];
  _ptallocation[AObject::SPEED_MALLUS] = _refAObject[AObject::SPEED_MALLUS];
  _ptallocation[AObject::NBBOMB_BONUS] = _refAObject[AObject::NBBOMB_BONUS];
  _ptallocation[AObject::LINEBOMB_BONUS] = _refAObject[AObject::LINEBOMB_BONUS];
  _file.open(file);
  if (!_file.is_open())
    throw ExceptionLoadMap("File exist", "LoadMap(const char *file, Board &board)");
  loadFile();
  _board.setHeight(_height);
  _board.setWidth(_width);
  addSafeWallAround();
  Core::getInstance()->setHeight(_height);
  Core::getInstance()->setWidth(_width);
}

LoadMap::~LoadMap()
{
  _file.close();
}

void	LoadMap::addSafeWallAround()
{
  size_t		x = 0;
  size_t		y = 0;

  for (x = 0 ; x < _width ; x++)
    {
      if (!_board.getSquare(x, 0)->withWall())
	_board.getSquare(x, 0)->addObject(new Solid(x, 0), true);
    }
  for (y = 1; y < _height - 1;y++)
    {
      if (!_board.getSquare(0, y)->withWall())
	_board.getSquare(0, y)->
	  addObject(new Solid(0, y), true);
      if (!_board.getSquare(_width - 1, y)->withWall())
	_board.getSquare(_width - 1, y)->
	  addObject(new Solid(_width - 1, y), true);
    }
  for (x = 0 ; x < _width ; x++)
    {
      if (!_board.getSquare(x, _height - 1)->withWall())
	_board.getSquare(x, _height - 1)->
	  addObject(new Solid(x, _height - 1), true);
    }
}

void	LoadMap::loadFile()
{
  size_t	x = 0;
  size_t	y = 0;
  size_t        tmpwidth = 0;
  size_t	       enumcheck;
  std::string		getted;
  std::string::iterator	it;
  AObject		*objectcreate;

  for (std::string buffer ; std::getline(_file, buffer) ; _height++, y++)
    {
      x = 0;
      it = buffer.begin();
      tmpwidth = 0;
      while (it < buffer.end())
	{
	  Square	*square = new Square(x, y);
	  _board.addSquare(square);
	  if (x == 0 && y == 0)
	    _board.getSquare(x, 0)->
	      addObject(new Solid(x, 0, AObject::FLOOR), true);
	  getted.clear();
	  while (it < buffer.end() && (*it) != '!')
	    {
	      getted += (*it);
	      ++it;
	    }
	  if ((*it) == SPLIT)
	    it++;
	  std::istringstream ss(getted);
	  ss >> enumcheck;
	  if (enumcheck != AObject::EMPTY && _ptallocation.
	      find(static_cast<AObject::eType>(enumcheck)) == _ptallocation.end())
	    throw ExceptionLoadMap("On Aobject reference of File",
				   "LoadMap::loadFile()");
	  if (enumcheck != AObject::EMPTY)
	    {
	      if (_ptallocation.find(static_cast<AObject::eType>(enumcheck))
		  != _ptallocation.end())
		{
		  objectcreate = ((Factory::getInstance())->*
				  _ptallocation[static_cast<AObject::eType>(enumcheck)])(x, y, &_board);
		  square->addObject(objectcreate, true);
		}
	      else
		throw ExceptionLoadMap("Can't find object Type", "LoadMap::loadFile()");
	    }
	  x++;
	  tmpwidth++;
	}
      if (tmpwidth < 3 || (_width != 0 && _width != tmpwidth))
	throw ExceptionLoadMap("Wrong size map", "LoadMap::loadFile()");
      else
	_width = tmpwidth;
    }
  if (_height < 3)
    throw ExceptionLoadMap("Wrong height map", "LoadMap::loadFile()");
}

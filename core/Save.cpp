//
// Save.cpp for Bomberman in /home/texier_a//projets/bomberman/core
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri May 25 10:55:44 2012 anna texier
// Last update Sun Jun  3 22:04:44 2012 tosoni_t
//

#include	"Save.hh"
#include	"Core.hh"
#include	"Board.hh"
#include	"Square.hh"
#include	"AObject.hh"
#include	"Solid.hh"
#include	"Item.hh"
#include	"APlayer.hh"
#include	"Player.hh"
#include	"Factory.hh"
#include	"ExceptionCore.hh"

Save::Save()
  : _create(), _board()
{
  _create[AObject::SPEED_BONUS] = _refAObject[AObject::SPEED_BONUS];
  _create[AObject::EXPANDBOMB_BONUS] = _refAObject[AObject::EXPANDBOMB_BONUS];
  _create[AObject::LINEBOMB_BONUS] = _refAObject[AObject::LINEBOMB_BONUS];
  _create[AObject::BOMBFIRE] = _refAObject[AObject::BOMBFIRE];
  _create[AObject::BOMB] = _refAObject[AObject::BOMB];
  _create[AObject::FIRE] = _refAObject[AObject::FIRE];
  _create[AObject::ITEM] = _refAObject[AObject::ITEM];
  _create[AObject::WALL] = _refAObject[AObject::WALL];
  _create[AObject::FLOOR] = _refAObject[AObject::FLOOR];
  _create[AObject::PLAYER] = _refAObject[AObject::PLAYER];
  _create[AObject::NBBOMB_BONUS] = _refAObject[AObject::NBBOMB_BONUS];
}

Save::~Save()
{

}

Save::s_core::s_core()
  : _width(0), _height(0), _totalTime(0.f)
{
  magic[0] = 'B';
  magic[1] = 'O';
  magic[2] = 'M';
  magic[3] = 'B';
  magic[4] = 'E';
  magic[5] = 'R';
  magic[6] = '\0';
}

Save::s_object::s_object()
  : type(AObject::FLOOR), _destructable(false), _dam(false), _handled(false),
    x(0), y(0), beRemovable(false), toMove(false), value(0),
    pos(), rot(), echX(false), echY(false), echZ(false), rangex(0), rangey(0),
    scale(), color(),
    displayType(NONE), key(), time(0.f), timeGetted(0.f), end(CONTINUE)
{
  magic[0] = 'B';
  magic[1] = 'O';
  magic[2] = 'M';
  magic[3] = 'B';
  magic[4] = 'E';
  magic[5] = 'R';
  magic[6] = '\0';
}

Save::s_player::s_player()
  : player(), nbLives(0), scoring(0), ia(false)
{
}

void	Save::setValues(s_object *s, AObject *obj)
{
  strcpy(s->magic, "BOMBER");
  s->rangex = obj->getRangeX();
  s->rangey = obj->getRangeY();
  s->type = obj->getObjectType();
  s->x = obj->getX();
  s->y = obj->getY();
  s->beRemovable = obj->beRemoved();
  s->toMove = obj->toMove();
  s->_destructable = obj->isDestructable();
  s->_dam = obj->isDam();
  s->_handled = obj->isHandled();
  s->value = obj->getValue();
  s->pos = obj->getForm()->getPosition();
  s->rot = obj->getForm()->getRotation();
  s->echX = obj->getForm()->isRotXEnable();
  s->echY = obj->getForm()->isRotYEnable();
  s->echZ = obj->getForm()->isRotZEnable();
  s->rot = obj->getForm()->getScale();
  s->color = obj->getForm()->getColor();
  strcpy(s->key, obj->getKey().c_str());
  s->time = obj->getRemaining();
  s->timeGetted = obj->getRemainingGetted();
}

void	Save::LoadAObject(s_object *s, AObject *o)
{
  o->setDestructable(s->_destructable);
  o->setDam(s->_dam);
  o->setHandled(s->_handled);
  o->setBeRemovable(s->beRemovable);
  o->setToMove(s->toMove);
  o->setValue(s->value);
  o->setTimeRemaining(s->time);
  o->setTimeRemainingGetted(s->timeGetted);
  o->setPosition(s->pos);
  o->setScale(s->scale);
  o->setRangeX(s->rangex);
  o->setRangeY(s->rangey);
  o->getForm()->setTexture(graphic::MyGame::getInstance()->getTextureManager()->
			   getTexture(s->key));
}

void	Save::setValues(s_object *s, Bomb *obj)
{
  strcat(s->magic, "BOMBER");
  s->type = obj->getObjectType();
  s->rangex = obj->getRangeX();
  s->rangey = obj->getRangeY();
  s->x = obj->getX();
  s->y = obj->getY();
  s->_destructable = obj->isDestructable();
  s->_dam = obj->isDam();
  s->_handled = obj->isHandled();
  s->beRemovable = obj->beRemoved();
  s->toMove = obj->toMove();
  s->value = obj->getValue();
  s->pos = obj->getForm()->getPosition();
  s->rot = obj->getForm()->getRotation();
  s->echX = obj->getForm()->isRotXEnable();
  s->echY = obj->getForm()->isRotYEnable();
  s->echZ = obj->getForm()->isRotZEnable();
  s->rot = obj->getForm()->getScale();
  s->color = obj->getForm()->getColor();
  /*  s->displayType; // Ca se trouve ou ? */
  s->time = obj->getRemaining();
  s->timeGetted = obj->getRemainingGetted();
}

void	Save::writeSquare(Square *square, std::ofstream &file,
			  s_object *s, const std::list<AObject *> &toBeSaved)
{
  memset(s, '\0', sizeof(*s));
  strcat(s->magic, "BOMBER");
  s->pos = square->getPosition();
  s->scale = square->getScale();
  s->end = (!toBeSaved.empty()) ? DATA : EMPTY;
  s->x = square->getX();
  s->y = square->getY();
  file.write(reinterpret_cast<const char*>(s), sizeof(*s));
}

void	Save::writeAObject(std::ofstream &file,
			   s_object *s, const std::list<AObject *> &toBeSaved, bool end)
{
  std::list<AObject *>::const_iterator	test;
  for (std::list<AObject *>::const_iterator it = toBeSaved.begin() ;
       it != toBeSaved.end() ;++it)
    {
      memset(s, '\0', sizeof(*s));
      setValues(s, *it);
      test = it;
      test++;
      s->end = (test != toBeSaved.end() || !end) ? CONTINUE : ENDDATA;
      file.write(reinterpret_cast<const char*>(s), sizeof(*s));
    }
}

void	Save::writeAObject(std::ofstream &file,
			   s_object *s, const std::list<Bomb *> &toBeSaved, bool end)
{
  std::list<Bomb *>::const_iterator	test;
  for (std::list<Bomb *>::const_iterator it = toBeSaved.begin() ;
       it != toBeSaved.end() ;++it)
    {
      memset(s, '\0', sizeof(*s));
      setValues(s, *it);
      test = it;
      test++;
      s->end = (test != toBeSaved.end() || !end) ? CONTINUE : ENDDATA;
      file.write(reinterpret_cast<const char*>(s), sizeof(*s));
    }
}

void	Save::writeCore(std::ofstream &file, Core *b)
{
  s_core		*core = new s_core;

  memset(core, '\0', sizeof(*core));
  strcat(core->magic, "BOMBER");
  core->_width = b->getWidth();
  core->_height = b->getHeight();
  core->_totalTime = b->getTotalTime();
  file.write(reinterpret_cast<const char*>(core), sizeof(*core));
}

void	Save::loadCoreData(std::ifstream &file, Core *c)
{
  _board = new Board();
  s_core		*core = new s_core;

  memset(core, '\0', sizeof(*core));
  file.read(reinterpret_cast<char*>(core), sizeof(*core));
  if (strcmp(core->magic, "BOMBER") != 0)
    {
      std::cerr << "ERROR" << std::endl;
      throw ExceptionCore("wrong map format", "loadCoreData");
    }
  _board->setHeight(core->_height);
  _board->setWidth(core->_width);
  c->setTotalTime(core->_totalTime);
  c->setBoard(_board);
  c->setHeight(core->_height);
  c->setWidth(core->_width);
}

void	Save::writePlayers(s_player *p, s_object *s, std::ofstream &file,
			   std::map<APlayer *, bool> const &players, bool end)
{
  std::map<APlayer *, bool>::const_iterator it2;

  for (std::map<APlayer *, bool>::const_iterator it = players.begin() ;
	 it != players.end() ; ++it)
      {
	memset(p, '\0', sizeof(*p));
	setValues(&(p->player), it->first);
	p->player.end = (!it->first->getItems().empty() || !it->first->getBombs().empty()) ? DATA : EMPTY;
	p->nbLives = it->first->life();
	p->scoring = it->first->getScore();
	p->ia = (end) ? true : false;
	file.write(reinterpret_cast<const char*>(p), sizeof(*p));
	/* write object of player */
	writeAObject(file, s, it->first->getItems(),
		     (it->first->getBombs().empty()) ? true : false);
	writeAObject(file, s, it->first->getBombs(), true);
	/* WRITE END PLAYER IF DONE :) */
	it2 = it;
	++it2;
    }
  if (end)
    writeEnd(file, p);
}

void	Save::writeEnd(std::ofstream &file, s_object *s)
{
  memset(s, '\0', sizeof(*s));
  strcat(s->magic, "BOMBER");
  s->end = END;
  file.write(reinterpret_cast<const char*>(s), sizeof(*s));
}

void	Save::writeEnd(std::ofstream &file, s_player *p)
{
  memset(p, '\0', sizeof(*p));
  p->player.end = END;
  file.write(reinterpret_cast<const char*>(p), sizeof(*p));
}

void	Save::saveGame(const std::string & filename)
{
  std::ofstream	file;
  _board = Core::getInstance()->getBoard();
  std::map<APlayer *, bool>	players = Core::getInstance()->getPlayers();
  std::map<APlayer *, bool>	cpu = Core::getInstance()->getCpu();
  size_t	maxPos = _board->getWidth() * _board->getHeight();
  Square	*square = NULL;
  s_object	*s = new s_object;
  s_player	*p = new s_player;
  std::list<AObject *>		toBeSaved;

  file.open(filename.c_str(), std::ios::trunc | std::ios::out | std::ios::binary);
  if (!file.is_open())
    throw ExceptionCore("open file error", "saveGame");;

  /* WRITE CORE DATA */
  writeCore(file, Core::getInstance());

  /* WRITE MAP DATA */
  for (size_t pos = 0 ; pos < maxPos ; pos++)
    {
      square = _board->getSquare(pos);
      toBeSaved.clear();
      toBeSaved = square->getNbSaved();
      writeSquare(square, file, s, toBeSaved);
      writeAObject(file, s, toBeSaved, true);
    }
  /* WRITE END OF COPY MAP */
  writeEnd(file, s);
  /* WRITE PLAYER DATA */
  writePlayers(p, s, file, players, false);
  writePlayers(p, s, file, cpu, true);
  file.close();
}

void	Save::addSafeWallAround()
{
  size_t		x = 0;
  size_t		y = 0;

  for (x = 0 ; x < _board->getWidth() ; x++)
    {
      if (!_board->getSquare(x, 0)->withWall())
	_board->getSquare(x, 0)->addObject(new Solid(x, 0), true);
    }
  for (y = 1; y < _board->getHeight() - 1;y++)
    {
      if (!_board->getSquare(0, y)->withWall())
	_board->getSquare(0, y)->
	  addObject(new Solid(0, y), true);
      if (!_board->getSquare(_board->getWidth() - 1, y)->withWall())
	_board->getSquare(_board->getWidth() - 1, y)->
	  addObject(new Solid(_board->getWidth() - 1, y), true);
    }
  for (x = 0 ; x < _board->getWidth() ; x++)
    {
      if (!_board->getSquare(x, _board->getHeight() - 1)->withWall())
	_board->getSquare(x, _board->getHeight() - 1)->
	  addObject(new Solid(x, _board->getHeight() - 1), true);
    }
}

void	Save::loadGame(const std::string & filename)
{
  std::ifstream	file;
  std::string	str;
  AObject		*objcreate;
  s_object	*s = new s_object;

  file.open(filename.c_str());
  if (!file.is_open())
    throw ExceptionCore("open error", "loadGame");;
  /* LOAD CORE DATA */
  loadCoreData(file, Core::getInstance());
  do
    {
      /* GET SQUARE DATA */
      memset(s, '\0', sizeof(*s));
      file.read(reinterpret_cast<char*>(s), sizeof(*s));
      if (strcmp(s->magic, "BOMBER") != 0)
	{
	  file.close();
	  throw ExceptionCore("wrong map format", "loadCoreData");
	}
      if (s->end == END)
	break;
      /* ADD NEW SQUARE */
      Square	*square = new Square(s->x, s->y);
      _board->addSquare(square);
      square->setPosition(s->pos);
      square->setScale(s->scale);

      /* CHECK FOR THE DEFAULT FLOOR */
      if (s->x == 0 && s->y == 0)
	square->addObject(new Solid(s->x, s->y, AObject::FLOOR), true);

      /* ADD ELEMENT ON SQUARE */
      if (s->end == DATA)
	{
	  while (s->end != ENDDATA)
	    {
	      memset(s, '\0', sizeof(*s));
	      file.read(reinterpret_cast<char*>(s), sizeof(*s));
	      if (_create.find(s->type) != _create.end())
		{
		  objcreate = (Factory::getInstance()->*_create[s->type])(s->x, s->y, _board);
		  square->addObject(objcreate, true);
		  LoadAObject(s, objcreate);
		}
	    }
	}
    } while (s->end != END);


  s_player	*p = new s_player;
  readPlayers(p, file, s);
  file.close();
  addSafeWallAround();
}


void			Save::readPlayers(s_player *p, std::ifstream &file,
					  s_object *s)
{
  AObject		*objcreate;
  AObject		*player;
  do
    {
      memset(p, '\0', sizeof(*p));
      file.read(reinterpret_cast<char*>(p), sizeof(*p));
      if (p->player.end == END)
	break;
      if (!p->ia)
	player = (Factory::getInstance()->*_create[p->player.type])
	  (p->player.x, p->player.y, _board);
      else
	{
	  player = new PlayerIA(*_board, "", p->player.x, p->player.y, PlayerIA::EASY);
	  try
	    {
	      dynamic_cast<PlayerIA *>(player)->setIA();
	    }
	  catch (...)
	    {
	    }
	}

      _board->getSquare(p->player.x, p->player.y)->addObject(player, true);
      if (!p->ia)
	Core::getInstance()->addPlayer(dynamic_cast<APlayer *>(player));
      else
	Core::getInstance()->addCpu(dynamic_cast<APlayer *>(player));
      LoadAObject(&p->player, player);
      (dynamic_cast<APlayer *>(player))->setScore(p->scoring);
      (dynamic_cast<APlayer *>(player))->setLives(p->nbLives);
      /* read data */
      if (p->player.end == DATA)
	{
	  while (s->end != ENDDATA)
	    {
	      memset(s, '\0', sizeof(*s));
	      file.read(reinterpret_cast<char*>(s), sizeof(*s));
	      if (s->end != VOID && _create.find(s->type) != _create.end())
		{
		  if (s->type == AObject::BOMB)
		    {
		      objcreate = new Bomb(Bomb::STANDARD, s->rangex, s->rangey, s->x, s->y, *dynamic_cast<APlayer *>(player), *_board);
		      dynamic_cast<APlayer *>(player)->getBomb(objcreate);
		    }
		  else
		    {
		      objcreate = (Factory::getInstance()->*_create[s->type])(s->x, s->y, _board);
		      Core::getInstance()->removeElementUpdate(objcreate);
		      dynamic_cast<APlayer *>(player)->getItem(objcreate);
		    }
		  LoadAObject(s, objcreate);
		}
	    }
	}
    } while (p->player.end != END);
}

//
// Core.hh for Bomberman in /home/ga/Documents/Projet/bomberman/bomberman/core
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May 18 12:48:21 2012 gaetan senn
// Last update Sun Jun  3 23:20:02 2012 benjamin bourdin
//

#ifndef __CORE__
#define __CORE__

#include	<cstdio>
#include	<map>
#include	"ITask.hh"
#include	"Board.hh"
#include	"factory.hpp"
#include	"GameEventManager.hh"
#include	"APlayer.hh"
#include	"PlayerIA.hh"

#define DEBUG_BOARD	0

#define NBBOMB 5

class		Core
{
private:
  size_t			_width; /*!<  size of core map*/
  size_t			_height; /*!<  height of core map*/
  Board				*_board; /*!<  board pt */
  std::map<APlayer *, bool>	_players; /*!<  player list */
  std::map<APlayer *, bool>	_cpu; /*!<  cpu list */
  static Core			*_instance; /*!<  singelton */
  std::list<AObject *>		_updateList; /*!<  update list object optimization */
  float				_playTime; /*!<  play time of game */
  float				_referenceTime; /*!<  refence for calcul time */
  float				_totalTime; /*!<  total time */
  PlayerIA::eDifficulty		_difficulty; /*!< difficulty of core */
public:
  /** 
   * @brief get about singelton
   * 
   * 
   * @return 
   */
  static Core			*getInstance();
  /** 
   * @brief kill about singelton
   * 
   */
  static void			killInstance();

  /** 
   * @brief constructor of core
   * 
   */
  Core();
  /** 
   * @brief coplien
   * 
   */
  Core(Core const &);
  /** 
   * @brief coplien
   * 
   * 
   * @return 
   */
  Core const &	operator=(Core const &);
  /** 
   * @brief destructor
   * 
   */
  ~Core();

  /** 
   * @brief total time getter
   * 
   * 
   * @return 
   */
  float				getTotalTime(void) const;
  /** 
   * 
   * 
   */
  void				setReferenceTime(const MyClock &);
  /** 
   * @brief set total time
   * 
   * @param float 
   */
  void				setTotalTime(const float);

  /* GRAPHIC CALL */
  /** 
   * @brief draw core
   * 
   */
  void				draw();
  /** 
   * @brief update core
   * 
   */
  void				update(const MyClock&, gdl::Input&);
  /** 
   * @brief run core
   * 
   * @param nblua 
   */
  void				run(const int nblua = 1);
  /** 
   * @brief cancel core
   * 
   */
  void				cancel();
  /** 
   * @brief add player 
   * 
   * @param _playerKeys 
   * 
   * @return 
   */
  bool				addPlayer(GameEventManager *_playerKeys);
  /** 
   * @brief add lua
   * 
   * @param level 
   * 
   * @return 
   */
  bool				addLua(PlayerIA::eDifficulty level);
  /** 
   * @brief get players
   * 
   * 
   * @return 
   */
  std::map<APlayer *, bool> const	&getPlayers() const;
  /** 
   * @brief get cpu
   * 
   * 
   * @return 
   */
  std::map<APlayer *, bool> const	&getCpu() const;
  /** 
   * @brief initialize player
   * 
   * @param size_t 
   * @param size_t 
   */
  void				initialize(const size_t, const size_t);
  /** 
   * @brief initializeMap
   * 
   */
  void				initializeMap(std::string const &);
  /* GETTER */
  /** 
   * @brief get Board
   * 
   * 
   * @return 
   */
  Board				*getBoard(void) const;
  /** 
   * @brief is finished
   * 
   * 
   * @return 
   */
  bool				isFinished() const;
  /** 
   * @brief get height of core
   * 
   * 
   * @return 
   */
  size_t			getHeight() const;
  /** 
   * @brief get width of core
   * 
   * 
   * @return 
   */
  size_t			getWidth() const;
  /*SETTER */
  /** 
   * @brief set height
   * 
   * @param size_t 
   */
  void				setHeight(const size_t);
  /** 
   * @brief set width core
   * 
   * @param size_t 
   */
  void				setWidth(const size_t);
  /** 
   * @brief set board
   * 
   */
  void				setBoard(Board *);
  /** 
   * @brief add player
   * 
   * @param p 
   */
  void				addPlayer(APlayer * p);
  /** 
   * @brief add cpu 
   * 
   * @param p 
   */
  void				addCpu(APlayer * p);
  /* CORE METHOD */
  /** 
   * @brief clean update list
   * 
   */
  void				cleanUpdateList();
  /** 
   * @brief removeElementUpdate 
   * 
   * @param obj 
   * 
   * @return 
   */
  bool				removeElementUpdate(AObject *obj);
  /** 
   * @brief get update lsit
   * 
   * 
   * @return 
   */
  std::list<AObject *>		&getUpdateList();
  /** 
   * @brief addobject update
   * 
   * @param o 
   */
  void				addObjectUpdate(AObject *o);
  /** 
   * @brief set event manager
   * 
   */
  void				setEventManager(std::list<GameEventManager *>);
  /** 
   * @brief init lua
   * 
   * @param int 
   */
  void				initLua(const int);
  /** 
   * @brief not player
   * 
   * @param size_t 
   * 
   * @return 
   */
bool				notplayer(const size_t);
  /** 
   * @brief run save
   * 
   */
  void				runSave();
  /**
   * @brief init object info graph
   * 
   */
  void		initCoordSpace();
  /** 
   * @brief set difficulty of cpu
   * 
   * @param diff 
   */
  void			setDifficulty(const PlayerIA::eDifficulty diff);
};

#endif

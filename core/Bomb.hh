//
// Bombs.hh for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu May  3 16:53:25 2012 anna texier
// Last update Sun Jun  3 23:17:36 2012 benjamin bourdin
//

#ifndef	__BOMB_H_
#define	__BOMB_H_

#include	<stdio.h>
#include	<map>
#include	<list>
#include	"Board.hh"
#include	"Math.hpp"
#include	"Time.hh"
#include	"APlayer.hh"
#include	"AObject.hh"
#include	"Item.hh"
#include	<iostream>

#define	TIME	0.040
#define TIMEBOMB	4.000f
class APlayer;

/*!
 * @class Bomb
 * @brief Define a Bomb
 */
class Bomb : public AObject
{
public:
  /*!
   * @brief Typedef for impact ptr on fct
   */
  typedef void (Bomb::*fimpact)();

  /*!
   * @struct s_time
   * @brief Timer bomb
   */
  typedef struct	s_time
  {
    int			second; /*!< Seconds */
    long int		micro; /*!< Micro second */
  }			t_time;

  /*!
   * @enum eBombType
   * @brief Type of bomb
   */
  enum eBombType
    {
      STANDARD, /*!< type Standard */
      LINE, /*!< explode on a line */
      COLUMN /*!< explode on a column */
    };

private:
  eBombType		_type; /*!< type of the bomb */
  size_t		_rangex; /*!< x-range of the explosion */
  size_t		_rangey; /*!< y-range of the explosion */
  APlayer &		_player; /*!< player who has the bomb */
  Board &		_map; /*!< The board */
  Time			_time; /*!< The timer */
  std::list<std::list<size_t> *> _listimpact; /*!< List of all impacts */
  std::list<size_t>	_impactLeft; /*!< left impacts */
  std::list<size_t>	_impactRight; /*!< right impacts */
  std::list<size_t>	_impactTop; /*!< top impacts */
  std::list<size_t>	_impactBottom; /*!< down impacts */

  AObject::eType	_objectType; /*!< object type */
  bool			_first; /*!< first explosion */
  bool			_inittime; /*!< init the time */
  bool			_explode; /*!< is in explosion */
  float			_timestart; /*!< beginning of the timer */

  int			_scaleModif; /*!< Modif of the scale */

public:
  /*!
   * @brief Constructor bomb
   * @param type Type of the bomb
   * @param rangex The x-range
   * @param rangey The y-range
   * @param x X position
   * @param y Y position
   * @param player The player who owns
   * @param map The map
   */
  Bomb(eBombType, size_t, size_t, size_t, size_t, APlayer &, Board &);
  /*!
   * @brief Contructor by copy
   * @param copy The copy
   */
  Bomb(const Bomb &);
  /*!
   * @brief Operator = override
   * @param copy The copy
   * @return The class
   */
  Bomb const &	operator=(const Bomb &);
  /*!
   * @brief Destructor
   */
  ~Bomb();

  /* BOMB TOOLS */
  /*!
   * @brief update bombs
   * @param clock The clock
   * @param input The input
   */
  void		update(const MyClock&, gdl::Input&);
  /*!
   * @brief init a line
   */
  void		initLine();
  /*!
   * @brief init a column
   */
  void		initColumn();
  /*!
   * @brief apply a destruction
   */
  void		applyDestruction();
  /*
   * @brief Cause an impact
   * @param clock The clock
   */
  void		impact(const MyClock &);

  /*!
   * @brief Check if an object is impactable
   * @param x The x position
   * @param y The y position
   * @return true if it's impactable, otherwise false
   */
  bool		impactable(size_t, size_t) const;
  /*!
   * @brief Send an impact line
   */
  void		sendImpactLine();
  /*!
   * @brief Send an impact column
   */
  void		sendImpactColumn();
  /*!
   * @brief Send impact with standard bomb
   */
  void		sendImpactStandard();

  /*!
   * @brief Set an impact
   * @param ref The timer
   * @param s The second
   * @param m The microsecond
   */
  void		setImpact(t_time &ref, const int s, const long int m) const;

  /*!
   * @brief Init an impact
   */
  void		initImpact();
  /*!
   * @brief get a range X
   * @return The range X
   */
  size_t	getRangeX() const;
  /*!
   * @brief get a range Y
   * @return the range Y
   */
  size_t	getRangeY() const;
  /*!
   * @brief set range X
   * @param x The x to set
   */
  void				setRangeX(size_t);
  /*!
   * @brief set range Y
   * @param y The y to set
   */
  void				setRangeY(size_t);

  //AOBJECT METHOD VIRTUAL
  /*!
   * @brief Erase a bomb
   */
  void		erase(void);
  /*!
   * @brief Get an object Type
   * @return The bomb type
   */
  AObject::eType		getObjectType() const;
};

static const Bomb::t_time		timeref[3] =
  {
    { 3, 0},
    { 3, 0},
    { 3, 0}
  };

#endif

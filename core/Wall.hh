//
// Wall.hh for bomberman in /home/ga/Documents/Projet/bomberman/bomberman/core
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Tue May 15 11:01:39 2012 gaetan senn
// Last update Tue May 15 15:19:50 2012 gaetan senn
//

#ifndef __WALL_HH__
#define __WALL_HH__

#include "Solid.hh"

class Wall : public Solid
{
public:
  Wall(size_t, size_t);
  ~Wall() {}
};

#endif

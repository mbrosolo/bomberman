//
// Player.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri May  4 10:44:35 2012 anna texier
// Last update Sun Jun  3 18:02:32 2012 gaetan senn
//

#include	"Core.hh"
#include	"Player.hh"
#include	"Bomb.hh"

Player::Player(Board &map, GameEventManager *key, std::string name, size_t x, size_t y)
  : APlayer(map, name, x, y, key),
    _left(0), _right(0), _up(0), _down(0), _dropBomb(0)
{
  {
    std::map<APlayer *, bool> listPlayer = Core::getInstance()->getPlayers();
    std::map<APlayer *, bool>::iterator it = listPlayer.begin();
    if (it == listPlayer.end())
      {
	_form->setModel(graphic::MyGame::getInstance()->getModelManager()->getModel("PLAYER1"));
	_form->getModel()->set_default_model_color(gdl::Color(255, 0, 0));
      }
    else if (it == listPlayer.begin())
      {
	_form->setModel(graphic::MyGame::getInstance()->getModelManager()->getModel("PLAYER2"));
	_form->getModel()->set_default_model_color(gdl::Color(0, 255, 0));
      }
    else
      {
	graphic::MyGame::getInstance()->getModelManager()->loadModelDynamic(name, "medias/assets/marvin.fbx");
	_form->setModel(graphic::MyGame::getInstance()->getModelManager()->getModel(name));
	_form->getModel()->set_default_model_color(gdl::Color(0, 0, 255));
      }
    _form->initialize();
  }
}

Player::~Player(){
  _map.getSquare(_x, _y)->popObject(this);
}

void	Player::setMapping(int left, int right, int up, int down, int dropBomb)
{
  _left = left;
  _right = right;
  _up = up;
  _down = down;
  _dropBomb = dropBomb;
}

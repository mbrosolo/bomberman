//
// ExceptionLoading.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 07:02:02 2012 benjamin bourdin
// Last update Sun Jun  3 16:54:00 2012 gaetan senn
//

#include	"ExceptionCore.hh"

ExceptionCore::ExceptionCore(std::string const &what, std::string const &where) throw()
: ExceptionRuntime(what, where)
{
}

ExceptionCore::~ExceptionCore() throw()
{
}

//
// APlayer.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri May  4 18:42:09 2012 anna texier
// Last update Sun Jun  3 16:51:04 2012 gaetan senn
//

#include	<sstream>
#include	"APlayer.hh"
#include	"Math.hpp"
#include	"Bomb.hh"
#include	"Core.hh"
#include	"ComplexObj.hh"
#include	"MyGame.hh"
#include	"Model.hh"

#define		LIMITWIDTH 4.0
#define		LIMITHEIGHT 4.0
#define		WIDTHPLAYERIMPACT 0.40
#define		SPEEDTURN 10
#define		LIMITBOMB 1
#define		RANGEBOMBLINE 2
#define		RANGEBOMBCOLUMN 2
#define		DEFAULTBOMB 1
#define		STEP 1

APlayer::APlayer(Board &map, std::string &name, size_t x, size_t y,
		 GameEventManager *playerKeys)
  : AObject(x, y, false, false, false, AObject::PLAYER), _map(map),
    _name(name), _items(), _bombs(), _lives(2), _score(0),
    _timestart(0),
    _tabbonus(), _playerKeys(playerKeys)
{
  static int	id = 1;

  _tabbonus[AObject::SPEED_BONUS] = &APlayer::speedBonus;
  _tabbonus[AObject::EXPANDBOMB_BONUS] = &APlayer::expandBombBonus;
  _tabbonus[AObject::LINEBOMB_BONUS] = &APlayer::lineBombBonus;
  _tabbonus[AObject::SPEED_MALLUS] = &APlayer::speedMalus;
  _tabbonus[AObject::NBBOMB_BONUS] = &APlayer::numberBombBonus;
  _form = new graphic::Model();
  _form->incRotX(90);
  _form->rotEnableX();
  _form->rotDisableY();
  _form->rotDisableZ();
  if (name == "")
    {
      std::stringstream	ss;
      std::string	tmp;
      ss << id;
      ss >> tmp;
      _name = "Player_";
      _name.append(tmp);
    }
  id++;
}

APlayer::~APlayer()
{
  std::list<AObject *>::iterator	it;
  std::list<Bomb *>::iterator	it2;

  for (it = _items.begin(); it != _items.end();it++)
    delete (*it);
  for (it2 = _bombs.begin(); it2 != _bombs.end() ;it2++)
    delete (*it2);
}

void	APlayer::erase()
{
  this->die();
}

void	APlayer::die()
{
  if (this->_lives > 0)
    this->_lives--;
}

void			APlayer::setLives(size_t lives)
{
  _lives = lives;
}

void			APlayer::setScore(size_t score)
{
  _score = score;
}

void		APlayer::winPoint(unsigned int nb)
{
  _score += nb;
}

std::list<AObject *>	const &APlayer::getItems() const
{
  return _items;
}

std::list<Bomb *>	const &APlayer::getBombs() const
{
  return _bombs;
}

void			*APlayer::numberBombBonus()
{
  size_t			*a = new size_t;

  *a = NBNUMBERBOMBBONUS;
  return ((void *)a);
}

void			*APlayer::speedBonus()
{
  int			*a = new int;
  *a = 2;
  return ((void *)a);
}

void			*APlayer::speedMalus()
{
  int			*a = new int;

  *a = -2;
  return ((void *)a);
}

void			*APlayer::expandBombBonus()
{
  int			*a = new int[2];
  a[0] = 1;
  a[1] = 1;
  return ((void *)a);
}

void			*APlayer::lineBombBonus()
{
  int			*a = new int[2];

  a[0] = _map.getWidth();
  a[1] = 0;
  return ((void *)a);
}

size_t		APlayer::life() const
{
  return (_lives);
}

size_t		APlayer::getScore() const
{
  return (_score);
}

AObject::eType		APlayer::getObjectType() const
{
  return (AObject::PLAYER);
}

void	APlayer::move(const size_t x, const size_t y)
{
  if (_map.getSquare(static_cast<size_t>(x),
		     static_cast<size_t>(y))->isImpactable())
    {
      if (_map.getSquare(static_cast<size_t>(_x),
			 static_cast<size_t>(_y))->popObject(this))
	{
	  _x = x;
	  _y = y;
	  _map.getSquare(static_cast<size_t>(_x),
			 static_cast<size_t>(_y))->addObject(this, true);
	}
    }
}

bool	APlayer::checkSquare(const Square *square)
{
  if (square->getListSize() == 0)
    return true;
  return false;
}

void		APlayer::cleanBomb()
{
  std::list<Bomb *>::iterator	       it;

  for (it = _bombs.begin(); it != _bombs.end(); ++it)
    {
      if ((*it)->beRemoved())
	{
	  _map.getSquare((*it)->getX(), (*it)->getY())->popObject((*it));
	  delete (*it);
	  it = _bombs.erase(it);
	}
    }
}

const std::string	&APlayer::getName() const
{
  return _name;
}

void	APlayer::dropBomb()
{
  int		*res;
  size_t		*incstock;
  size_t		_limitbomb = DEFAULTBOMB;
  size_t		linebomb = RANGEBOMBLINE;
  size_t		columnbomb = RANGEBOMBCOLUMN;

  incstock = (size_t *)applyBonus(AObject::NBBOMB_BONUS);
  if (incstock)
    {
      _limitbomb = _limitbomb + (*incstock *
				 (getNbBonus(AObject::NBBOMB_BONUS)));
      delete incstock;
    }
  /* CHECK BOMB TO BE REMOVED :) */
  if (_bombs.size() < _limitbomb)
    {
      /* APPLY EXPAND BOMB BONUS */
      res = (int *)applyBonus(AObject::EXPANDBOMB_BONUS);
      if (res)
	{
	  linebomb += res[0];
	  columnbomb += res[1];
	  delete res;
	}
      /* APPLY BOMB LINE BONUS */
      res = (int *)applyBonus(AObject::LINEBOMB_BONUS);
      if (res)
	{
	  linebomb += res[0];
	  columnbomb += res[1];
	  delete res;
	}
      _bombs.push_back(new Bomb(Bomb::STANDARD, linebomb, columnbomb, _x,
				_y, *this, _map));
    }
}

void			APlayer::setKeys(GameEventManager *p)
{
  _playerKeys = p;
}

size_t			APlayer::getNbBonus(AObject::eType type) const
{
  size_t			nb = 0;
  std::list<AObject *>::const_iterator	it;

  for (it = _items.begin() ;it != _items.end(); ++it)
    if ((*it)->getObjectType() == type)
      nb++;
  return (nb);
}

void			*APlayer::applyBonus(AObject::eType type)
{
  std::list<AObject *>::iterator	it;
  for (it = _items.begin() ;it != _items.end(); ++it)
    if ((*it)->getObjectType() == type)
      return ((this->*_tabbonus[type])());
  return (NULL);
}


void	APlayer::getItem(AObject *item)
{
  _items.push_back(item);
}

void	APlayer::getBomb(AObject *item)
{
  _bombs.push_back(dynamic_cast<Bomb *>(item));
}

void	APlayer::moveLeft()
{
  graphic::Vector	cur = _form->getPosition();
  graphic::Vector	square = _map.getSquare(_x - 1, _y)->getPosition();
  graphic::Scale	scale = _form->getScale();
  int			inc = STEP;
  int			*res;
  int			*res2;
  float			check = LIMITWIDTH;

  res = static_cast<int *>(applyBonus(AObject::SPEED_BONUS));
  res2 = static_cast<int *>(applyBonus(AObject::SPEED_MALLUS));
  if (res)
    {
      inc += *res;
      delete res;
    }
  if (res2)
    {
      inc += *res2;
      delete res2;
    }
  if (!_map.getSquare(_x, _y - 1)->isImpactable()
      || !_map.getSquare(_x, _y + 1)->isImpactable())
    check -= WIDTHPLAYERIMPACT;
  if (cur.first * scale.first <= (square.first * scale.first) + check)
    {
      if (_map.getSquare(_x - 1, _y)->isImpactable())
	{
	  _x--;
	  _toMove = true;
	  /* CHECK IF THE SQUARE HAVE BONUS TO TOOK :) */
	  _items.splice(_items.end(),
			*_map.getSquare(_x - 1, _y)->getHandled());
	  if (inc > 0)
	    _form->incPosX(-inc * graphic::MyGame::getInstance()->getMyClock().
			   getElapsedTime() * CST_MOVE);
	  _form->turnLeft(SPEEDTURN);
	}
    }
  else
    {
      if (inc > 0)
	_form->incPosX(-inc * graphic::MyGame::getInstance()->getMyClock().
		       getElapsedTime() * CST_MOVE);
      _form->turnLeft(SPEEDTURN);
    }
  _form->getModel()->play("RUN");
}

void		APlayer::run(MyClock const& g)
{
  static_cast<void>(g);
}

void		APlayer::moveUpdate()
{
  std::list<Bomb *>::iterator		it;

  for (it = _bombs.begin(); it != _bombs.end() ;++it)
    Core::getInstance()->addObjectUpdate((*it));
}

void		APlayer::update(const MyClock&g, gdl::Input &in)
{
  std::list<AObject *>::iterator	it;
  std::list<Bomb *>::iterator		it2;

  this->run(g);
  for (it2 = _bombs.begin();it2 != _bombs.end(); ++it2)
    (*it2)->update(g, in);
  for (it = _items.begin();it != _items.end(); ++it)
    {
      if ((*it)->getRemainingGetted() <= 0 &&
	  (*it)->getRemainingGetted() > NODELETE)
	{
	  delete (*it);
	  it = _items.erase(it);
	}
      else
	{
	  (*it)->setRemainingGetted(-(g.getTotalGameTime() - _timestart));
	  (*it)->update(g, in);
	}
    }
  _form->update(g, in);
  _timestart = g.getTotalGameTime();
  if (_playerKeys)
    _playerKeys->checkEvents(this);
  cleanBomb();
}



void	APlayer::getHandled()
{
  _items.splice(_items.end(), *_map.getSquare(_x, _y)->getHandled());
}

void	APlayer::moveRight()
{
  graphic::Vector	cur = _form->getPosition();
  graphic::Vector	square = _map.getSquare(_x + 1, _y)->getPosition();
  graphic::Scale	scale = _form->getScale();
  int			inc = STEP;
  int			*res;
  float			check = LIMITWIDTH;

  res = static_cast<int *>(applyBonus(AObject::SPEED_BONUS));
  if (res)
    {
      inc += *res;
      delete res;
    }
  if (!_map.getSquare(_x, _y - 1)->isImpactable() ||
      !_map.getSquare(_x, _y + 1)->isImpactable())
    check -= WIDTHPLAYERIMPACT;
  if (cur.first * scale.first >= (square.first * scale.first) - check)
    {
      if (_map.getSquare(_x + 1, _y)->isImpactable())
	{
	  _x++;
	  _toMove = true;
	  _items.splice(_items.end(), *_map.getSquare(_x - 1, _y)->
			getHandled());
	  _form->incPosX(+(inc) * graphic::MyGame::getInstance()->getMyClock().
			 getElapsedTime() * CST_MOVE);
	  _form->turnRight(SPEEDTURN);
	}
    }
  else
    {
      _form->incPosX(+(inc) * graphic::MyGame::getInstance()->getMyClock().
		     getElapsedTime() * CST_MOVE);
      _form->turnRight(SPEEDTURN);
    }

  _form->getModel()->play("RUN");
}

void	APlayer::moveUp()
{
  graphic::Vector	cur = _form->getPosition();
  graphic::Vector	square = _map.getSquare(_x, _y - 1)->getPosition();
  graphic::Scale	scale = _form->getScale();
  float			check = LIMITHEIGHT;
  int			inc = STEP;
  int			*res;

  res = static_cast<int *>(applyBonus(AObject::SPEED_BONUS));
  if (res)
    {
      inc += *res;
      delete res;
    }
  if (!_map.getSquare(_x - 1, _y)->isImpactable() || !_map.
      getSquare(_x + 1, _y)->isImpactable())
    check -= WIDTHPLAYERIMPACT;
  if (cur.second * scale.second >= (square.second * scale.second) - check)
    {
      if (_map.getSquare(_x, _y - 1)->isImpactable())
	{
	  _y--;
	  _toMove = true;
	  _items.splice(_items.end(), *_map.
			getSquare(_x - 1, _y)->getHandled());
	  _form->incPosY(inc * graphic::MyGame::getInstance()->getMyClock().
			 getElapsedTime() * CST_MOVE);
	  _form->turnUp(SPEEDTURN);
	}
    }
  else
    {
      _form->incPosY(inc * graphic::MyGame::getInstance()->getMyClock().
		     getElapsedTime() * CST_MOVE);
      _form->turnUp(SPEEDTURN);
    }

  _form->getModel()->play("RUN");
}

void	APlayer::moveDown()
{
  graphic::Vector	cur = _form->getPosition();
  graphic::Vector	square = _map.getSquare(_x, _y + 1)->getPosition();
  graphic::Scale	scale = _form->getScale();
  float			check = LIMITHEIGHT;
  int			*res;
  int			inc = STEP;

  res = static_cast<int *>(applyBonus(AObject::SPEED_BONUS));
  if (res)
    {
      inc += *res;
      delete res;
    }
  if (!_map.getSquare(_x - 1, _y)->isImpactable() || !_map.
      getSquare(_x + 1, _y)->isImpactable())
    check -= WIDTHPLAYERIMPACT;
  if (cur.second * scale.second <= (square.second * scale.second) + check)
    {
      if (_map.getSquare(_x, _y + 1)->isImpactable())
	{
	  _y++;
	  _toMove = true;
	  _items.splice(_items.end(), *_map.
			getSquare(_x - 1, _y)->getHandled());
	  _form->incPosY(-inc * graphic::MyGame::getInstance()->getMyClock().
			 getElapsedTime() * CST_MOVE);
	  _form->turnDown(SPEEDTURN);
	}
    }
  else
    {
      _form->incPosY(-inc * graphic::MyGame::getInstance()->getMyClock().
		     getElapsedTime() * CST_MOVE);
      _form->turnDown(SPEEDTURN);
    }
  _form->getModel()->play("RUN");
}

void	APlayer::take() const
{
}

void	APlayer::leave()
{
  dropBomb();
}

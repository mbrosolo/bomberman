//
// Player.hh for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri May  4 10:41:46 2012 anna texier
// Last update Sun Jun  3 18:02:42 2012 gaetan senn
//

#ifndef	__PLAYER_H_
#define	__PLAYER_H_

#include	"APlayer.hh"
#include	<cstdio>
#include	"Color.hh"

class Player : public APlayer
{
private:
  // type des touches, a voir avec partie graphique
  int		_left;
  int		_right;
  int		_up;
  int		_down;
  int		_dropBomb;

public:
  Player(Board &, GameEventManager *, std::string name, size_t x, size_t y);
  Player(const Player &);
  ~Player();
  Player&		operator=(Player const &);
  void			setMapping(int, int, int, int, int);
};

#endif

//
// Save.hh for Bomberman in /home/texier_a//projets/bomberman/core
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri May 25 10:54:22 2012 anna texier
// Last update Sun Jun  3 19:58:41 2012 gaetan senn
//

#ifndef	__SAVE_H_
#define	__SAVE_H_

#include	<fstream>
#include	"AObject.hh"
#include	"Bomb.hh"
#include	"TrioDatas.hh"
#include	"Core.hh"
#include	"Factory.hh"

#define MAX_SIZE_KEY	20

class Save
{
private:
  enum	endType
    {
      DATA,
      END,
      ENDDATA,
      EMPTY,
      CONTINUE,
      NEXT,
      VOID
    };

  enum	displayType
    {
      NONE,
      TEXTURE,
      VIDEO,
      MODEL
    };

  struct		s_core
  {
    s_core();
    char		magic[7];
    size_t		_width;
    size_t		_height;
    float		_totalTime;
  };

  struct		s_object
  {
    s_object();
    char		magic[7];
    AObject::eType	type;
    bool		_destructable;
    bool		_dam;
    bool		_handled;
    size_t		x;
    size_t		y;
    bool		beRemovable;
    bool		toMove;
    size_t		value;
    graphic::Vector	pos;
    graphic::Vector	rot;
    bool		echX;
    bool		echY;
    bool		echZ;
    size_t		rangex;
    size_t		rangey;
    graphic::Scale	scale;
    graphic::Color	color;
    enum displayType	displayType;
    char		key[MAX_SIZE_KEY];
    //    std::string		key;
    float		time;
    float		timeGetted;
    endType		end;
  };

  struct		s_player
  {
    s_player();
    s_object		player;
    size_t		nbLives;
    int			scoring;
    bool		ia;
  };

  typedef	AObject *(Factory::*ptr_t)(size_t x, size_t y, Board *);

  std::map<AObject::eType, ptr_t>	_create;
  Board					*_board;

  void			writeSquare(Square *, std::ofstream &, s_object *,
				    const std::list<AObject *> &);
  void			writeCore(std::ofstream &file, Core *b);
  void			writeAObject(std::ofstream &file,
				     s_object *s, const std::list<AObject *> &toBeSaved
				     , bool);
  void			writeAObject(std::ofstream &file,
				     s_object *s, const std::list<Bomb *> &toBeSaved,
				     bool end);
  void			writeEnd(std::ofstream &file, s_object *s);
  void			LoadAObject(s_object *s, AObject *o);
  void			loadCoreData(std::ifstream &file, Core *c);
  void			setValues(s_object *, AObject *);
  void			setValues(s_object *, Bomb *);
  void			addSafeWallAround();
  void			writeEnd(std::ofstream &file, s_player *p);
  void			writePlayers(s_player *p, s_object *s, std::ofstream &file,
				     std::map<APlayer *, bool> const &players, bool end);
  void			readPlayers(s_player *p, std::ifstream &file, s_object *s);
public:
  Save();
  ~Save();
  Save(const Save &);
  Save &operator=(const Save &);

  void			saveGame(const std::string &);
  void			loadGame(const std::string &);
};

#endif

//
// PlayerIA.hh for Bomberman in /home/texier_a//projets/bomberman
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Sat May  5 17:34:39 2012 anna texier
// Last update Thu May 24 17:50:05 2012 gressi_b
//

#ifndef	__PLAYER_IA_H__
#define	__PLAYER_IA_H__

#include <map>
#include <cstdio>
#include "APlayer.hh"
#include "../lua/Lua.hh"

class PlayerIA : public APlayer
{
private:
  static float const	PERIOD;

public:
  enum eDifficulty
    {
      EASY,
      MEDIUM,
      HARD,
      NB_DIFFICULTY
    };

  enum eDirection
    {
      UP = 1,
      RIGHT,
      DOWN,
      LEFT
    };

  static char const*			className;
  static lua::Luna<PlayerIA>::RegType	methods[];

public:
  typedef std::map<std::string, int>	map_t;

private:
  lua::Script		_script;
  eDifficulty		_difficulty;
  static std::string	_IAfile[NB_DIFFICULTY];
  float			_referenceTime;
  float			_currentTime;
  bool			_setted;
  lua::Table*		_Tmap;
  int			_counter;
  map_t			_count;
  eDirection		_lastDir;

private:
  void		_exportDatas();

public:
  PlayerIA(Board&, std::string name, size_t x, size_t y, eDifficulty d);
  PlayerIA(const PlayerIA &);
  ~PlayerIA();
  const PlayerIA	&operator=(const PlayerIA &);

  void		setIA();
  void		unsetIA();
  void		run(MyClock const&);

  int		test(lua::prim_t*);
  int		moveLeft(lua::prim_t*);
  int		moveRight(lua::prim_t*);
  int		moveUp(lua::prim_t*);
  int		moveDown(lua::prim_t*);
  int		getLastDir(lua::prim_t*);

  int		life(lua::prim_t*);
  int		getCounter(lua::prim_t*);
  int		incCounter(lua::prim_t*);
  int		resetCounter(lua::prim_t*);
  int		dropBomb(lua::prim_t*);

  static int	bitAnd(lua::prim_t*);
};

#endif

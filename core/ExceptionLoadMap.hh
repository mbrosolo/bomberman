//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Thu May 31 16:06:54 2012 gaetan senn
//

#ifndef __EXCEPTION_LOADMAP_HH__
#define __EXCEPTION_LOADMAP_HH__

#include	<string>
#include	"../ExceptionRuntime.hh"

class ExceptionLoadMap : public ExceptionRuntime
{
public:
  ExceptionLoadMap(std::string const &what = "", std::string const &where = "") throw();
  virtual ~ExceptionLoadMap() throw();

};

#endif

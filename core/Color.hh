//
// Color.hh for Color in /home/ga/Documents/Projet/bomberman/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat May 26 18:55:17 2012 gaetan senn
// Last update Sat May 26 19:02:45 2012 gaetan senn
//

#ifndef __COLOR__
#define __COLOR__

#define STARTRED "\033[31m"
#define STARTBLUE "\033[34m"
#define STARTCYAN "\033[36m"
#define ENDCOLOR "\033[0m"
#endif

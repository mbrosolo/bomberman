//
// Factory.hh for bomberman in /home/ga/Documents/Projet/bomberman/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun  2 22:30:22 2012 gaetan senn
// Last update Sun Jun  3 15:20:35 2012 gaetan senn
//

#ifndef __FACTORY__
#define __FACTORY__

#include	"AObject.hh"

class		Board;

class		Factory
{
  typedef	AObject		*(Factory::*_factory)(size_t x,
						      size_t y, Board *);
private:
  static	Factory		*_instance;
  std::map<AObject::eType, _factory>	_creatorpt;
public:
  static	Factory *getInstance();
  static	void	killInstance();

  Factory();
  Factory(Factory const &);
  Factory	const & operator=(Factory const &);
  ~Factory();

  /* METHOD FACTORY */
  AObject		*createAObject(AObject::eType);

  /* GETTER FACTORY */
  AObject		*createSpeedBonus(size_t x, size_t y, Board *);
  AObject		*createExpandBombBonus(size_t x, size_t y, Board *);
  AObject		*createLineBombBonus(size_t x, size_t y, Board *);
  AObject		*createPlayer(size_t x, size_t y, Board *);
  AObject		*createBombFire(size_t x, size_t y, Board *);
  AObject		*createBomb(size_t x, size_t y, Board *);
  AObject		*createFire(size_t x, size_t y, Board *);
  AObject		*createItem(size_t x, size_t y, Board *);
  AObject		*createWall(size_t x, size_t y, Board *);
  AObject		*createFloor(size_t x, size_t y, Board *);
  AObject		*createSpeedMallus(size_t x, size_t y, Board *);
  AObject		*createNbBombBonus(size_t x, size_t y, Board *);
  AObject		*notDefined(size_t x, size_t y , Board *);
};

typedef	AObject		*(Factory::*_factory)(size_t x, size_t y, Board *);

static const	_factory    _refAObject [] =
  {
    &Factory::createSpeedBonus,
    &Factory::createExpandBombBonus,
    &Factory::createLineBombBonus,
    &Factory::createPlayer,
    &Factory::createBombFire,
    &Factory::createBomb,
    &Factory::createFire,
    &Factory::createItem,
    &Factory::createWall,
    &Factory::notDefined,
    &Factory::createFloor,
    &Factory::createSpeedMallus,
    &Factory::createNbBombBonus
  };

#endif

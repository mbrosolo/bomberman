//
// Square.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu May  3 12:16:18 2012 anna texier
// Last update Sun Jun  3 23:06:51 2012 gaetan senn
//

#include	"Square.hh"
#include	"Core.hh"

struct Check2
{
  bool		operator()(AObject *obj)
  {
    return (obj->beRemoved());
  }
};

struct Check3
{
  bool		operator()(AObject *obj)
  {
    if (obj->toMove())
      {
	Core::getInstance()->getBoard()->getSquare(obj->getX(), obj->getY())->addObject(obj, false);
	obj->setToMove(false);
	return (true);
      }
    return (false);
  }
};

struct	SaveCheck
{
  bool		operator()(AObject *obj)
  {
    if (obj->getObjectType() != AObject::PLAYER &&
	obj->getObjectType() != AObject::BOMBFIRE &&
	obj->getObjectType() != AObject::FIRE &&
	obj->getObjectType() != AObject::BOMB &&
	obj->getObjectType() != AObject::FLOOR)
      return (true);
    return (false);
  }
};

Square::Square(const size_t x, const size_t y)
  : _x(x), _y(y), _scale(), _position(), _toBeSaved(), _objects()
{
}

std::list<AObject *>		 const &Square::getObjects() const
{
  return (_objects);
}

Square::~Square()
{
  std::list<AObject *>::iterator		it;

  for (it = _objects.begin(); it != _objects.begin();it++)
    {
      delete (*it);
    }
}

std::list<AObject*>&	Square::getObjects()
{
  return this->_objects;
}

size_t		Square::getX()
{
  return _x;
}

size_t		Square::getY()
{
  return _y;
}

graphic::Scale	const	&Square::getScale() const
{
  return (_scale);
}

std::list<AObject *> const &		Square::getNbSaved()
{
  std::list<AObject *>::iterator	it;

  _toBeSaved.clear();
  for (it = _objects.begin();it != _objects.end();++it)
    {
      if (SaveCheck()(*it))
	_toBeSaved.push_back(*it);
    }
  return (_toBeSaved);
}

void				Square::setScale(graphic::Scale const &s)
{
_scale = s;
}

void				Square::setPosition(graphic::Vector const &p)
{
  _position = p;
}

void		Square::applyDestruction()
{
  _objects.remove_if(Check());
}

void	       Square::clearAObjects()
{
  _objects.remove_if(Check2());
}

bool			Square::isImpactable() const
{
  std::list<AObject *>::const_iterator	it;

  for (it = _objects.begin();it != _objects.end();it++)
    {
      if ((*it)->isDam())
	return (false);
    }
  return (true);
}

unsigned int		Square::getSquareValue() const
{
  unsigned int	point = 0;

    std::list<AObject *>::const_iterator	it;
    for (it = _objects.begin();it != _objects.end();it++)
      {
	point+= (*it)->getValue();
      }
    return (point);
}

std::list<AObject *>		*Square::getHandled()
{
  std::list<AObject *>	*_bonus = new std::list<AObject *>();
  std::list<AObject *>::iterator		it;

  for (it = _objects.begin();it != _objects.end();++it)
    {
      if ((*it)->isHandled())
	{
	  _bonus->push_back((*it));
	  Core::getInstance()->removeElementUpdate((*it));
	  it = _objects.erase(it);
	}
    }
  return (_bonus);
}

void		Square::moveAObjecs()
{
  _objects.remove_if(Check3());
}

bool			Square::removeElement(AObject *addr)
{
  std::list<AObject *>::iterator	it;

  it = find(_objects.begin(), _objects.end(), addr);
  if (it != _objects.end())
    {
      _objects.erase(it);
      return (true);
    }
  return (false);
}

graphic::Vector		const&		Square::getPosition() const
{
  return (_position);
}

size_t	Square::getListSize() const
{
  return _objects.size();
}

void	Square::printObjects()
{
  std::list<AObject *>::const_iterator	it;
  std::list<AObject *>::const_iterator	it2;

  clearAObjects();
  moveAObjecs();
  for (it = _objects.begin();it != _objects.end();++it)
    (*it)->draw();
}


void	Square::updateObjects(const MyClock&gc, gdl::Input&in)
{
  std::list<AObject *>::const_iterator	it;

  for (it = _objects.begin();it != _objects.end();++it)
    (*it)->update(gc, in);
}

void		Square::addObject(AObject *obj, bool setPosition)
{
  obj->setScale(_scale);
  if (setPosition)
    obj->setPosition(_position);
  _objects.push_back(obj);
}

AObject		*Square::popObject(AObject *obj)
{
  if (removeElement(obj))
    return (obj);
  else
    return (NULL);
}

bool		Square::withWall() const
{
  std::list<AObject *>::const_iterator		it;

  for (it = _objects.begin(); it != _objects.end() ;++it)
    {
      if ((*it)->getObjectType() == AObject::WALL)
	return (true);
    }
  return (false);
}

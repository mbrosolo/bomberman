//
// Bombs.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu May  3 16:54:26 2012 anna texier
// Last update Sun Jun  3 23:14:31 2012 benjamin bourdin
//

#include	<unistd.h>
#include	"Bomb.hh"
#include	"CondVar.hh"
#include	"Cout.hh"
#include	"Effect.hh"
#include	"Math.hpp"
#include	"Primitive.hh"
#include	"ComplexObj.hh"
#include	<pthread.h>
#include	"MyGame.hh"
#include	"Model.hh"

Bomb::Bomb(eBombType type, size_t rangex, size_t rangey,
	   size_t x, size_t y, APlayer &player, Board &map)
  : AObject(x, y, false, true, false, AObject::BOMB, 10, TIMEBOMB),
    _type(type), _rangex(rangex), _rangey(rangey),
     _player(player), _map(map),
    _time(timeref[static_cast<int>(type)].second,
	  timeref[static_cast<int>(type)].micro),
    _listimpact(), _impactLeft(), _impactRight(),
    _impactTop(), _impactBottom(), _objectType(AObject::BOMB), _first(true),
    _inittime(true), _explode(false), _timestart(0),
    _scaleModif(0)
{
  _form = new graphic::Model();
  _form->initialize();
  _form->setModel(graphic::MyGame::getInstance()->
		  getModelManager()->getModel("BOMB"));

  _form->incRotX(90);
  _form->rotEnableX();
  _form->rotDisableY();
  _form->rotDisableZ();

  _map.getSquare(_x, _y)->addObject(this, true);
}

void		Bomb::setImpact(t_time &ref, const int s, const long int m) const
{
  ref.second = s;
  ref.micro = m;
}

bool		Bomb::impactable(size_t x, size_t y) const
{
  if (_map.getSquare(x, y)->isImpactable())
    return (true);
  return (false);
}

void		Bomb::initImpact()
{
  int		width = _map.getWidth();
  int		height = _map.getHeight();
  int		i;

  if (_type == LINE || _type == STANDARD)
    {
      i = _x - 1;
      while (i >= 0 && i >= static_cast<int>((_x) - _rangex))
	{
	  _impactLeft.push_back(systool::Math<size_t>::
				c2dto1d(i,_y, width));
	  if (!impactable(i, _y))
	    break;
	  i--;
	}
      i = _x + 1;
      while (i < width && i <= static_cast<int>((_x) + _rangex))
	{
	  _impactRight.push_back(systool::Math<size_t>::
				 c2dto1d(i, _y, width));
	  if (!impactable(i, _y))
	    break;
	  i++;
	}
      }
  if (_type == COLUMN || _type == STANDARD)
    {
      i = _y - 1;
      while (i >= 0 && i >= static_cast<int>((_y) - (_rangey)))
	{
	  _impactTop.push_back(systool::Math<size_t>::
			       c2dto1d(_x, i, width));
	  if (!impactable(_x, i))
	    break;
	  i--;
	}
      i = _y + 1;
      while (i < height && i <= static_cast<int>((_y) + (_rangey)))
	{
	  _impactBottom.push_back(systool::Math<size_t>::
				  c2dto1d(_x, i, width));
	  if (!impactable(_x, i))
	    break;
	  i++;
	}
    }
}

Bomb::Bomb(const Bomb &o)
  : AObject(o._x, o._y, o._destructable, o._dam, o._handled, AObject::BOMB),
    _type(o._type), _rangex(o._rangex), _rangey(o._rangey),
    _player(o._player), _map(o._map),
    _time(o._time), _listimpact(o._listimpact),
    _impactLeft(o._impactLeft), _impactRight(o._impactRight),
    _impactTop(o._impactTop), _impactBottom(o._impactBottom),
    _objectType(o._objectType), _first(o._first), _inittime(true),
    _explode(false),
    _timestart(o._timestart),
    _scaleModif(o._scaleModif)
{
}

 Bomb::~Bomb()
 {
 }

void		Bomb::initLine()
{
  if (_type == LINE || _type == STANDARD)
    {
      if (!_impactLeft.empty())
	_listimpact.push_back(&_impactLeft);
      if (!_impactRight.empty())
	_listimpact.push_back(&_impactRight);
    }
}

void		Bomb::initColumn()
{
  if (_type == COLUMN || _type == STANDARD)
    {
      if (!_impactTop.empty())
	  _listimpact.push_back(&_impactTop);
      if (!_impactBottom.empty())
	_listimpact.push_back(&_impactBottom);
    }
}

void		Bomb::applyDestruction()
{
  std::list<std::list<size_t> * >::iterator  it;
  size_t				   pos;

  for (it = _listimpact.begin() ;it != _listimpact.end();++it)
    {
      pos = (*it)->front();
      (*it)->pop_front();
      /* GET VALUE FROM SQUARE TO WIN :) */
      _player.winPoint(_map.getSquare(pos)->getSquareValue());
      _map.getSquare(pos)->applyDestruction();
    }
}

void		Bomb::impact(const MyClock&g)
{
  std::list<std::list<size_t> *>::iterator  it;
  std::list<std::list<size_t> *>::iterator  next;
  size_t				   pos;
  static bool				del = false;
  static  float				time = 0.0f;

  /* INIT LIST OF IMPACT */
  /*THE DO FUNCTION IS NEED TO BE REPLACE bY IF FOR BE CALL WITH GRAPHICS :) */
  if (!_listimpact.empty())
    {
      if (del == true)
	{
	  if (g.getTotalGameTime() - time > TIME)
	    {
	      applyDestruction();
	      del = false;
	    }
	}
      if (!del && !_listimpact.empty())
	{
	  for (it = _listimpact.begin();it != _listimpact.end();++it)
	    {
	      if (!(*it)->empty())
		{
		  pos = (*it)->front();
		  if (_map.getSquare(pos)->isImpactable())
		    {
		      AObject * tmp = new Effect(_x, _y);
		      tmp->setPosition(_map.getSquare(pos)->getPosition());
		      _map.getSquare(pos)->
			addObject(tmp, true);
		    }
		  next = it;
		  next++;
		}
	      if (next == _listimpact.end())
		{
		  del = true;
		  time = g.getTotalGameTime();
		}
	    }
	}
    }
}

void		Bomb::update(const MyClock&g, gdl::Input&)
{
  if (_inittime)
    {
      _timestart = g.getTotalGameTime();
      _inittime = false;
    }
  _timeremaining -= (g.getTotalGameTime() - _timestart);
  _timestart = g.getTotalGameTime();
  graphic::Scale sc = getForm()->getScale();
  if (_scaleModif == 0)
    {
      sc.first += 0.02f;
      sc.third += 0.02f;
    }
  else if (_scaleModif == 50)
    {
      sc.first -= 0.02f;
      if (sc.first < 0.0f)
      	sc.first = 0.0f;
      sc.third -= 0.02f;
      if (sc.third < 0.0f)
      	sc.third = 0.0f;
    }
  ++_scaleModif;
  if (_scaleModif == 100)
    _scaleModif = 0;
  this->getForm()->setScale(sc);
  if (_explode || _timeremaining <= 0)
    {
      if (_first)
	{
	  graphic::MyGame::getInstance()->getSoundManager()->play("EXPLOSION");
	  /* THERE CHANGE TEXTURE TO FIRE */
	  _objectType = AObject::BOMBFIRE;
	  _map.getSquare(_x, _y)->applyDestruction();
	  _form->setModel(NULL);
	  initImpact();
	  _first = false;
	}
      _listimpact.clear();
      initLine();
      initColumn();
      if (!_listimpact.empty())
	impact(g);
      else
	_beRemovable = true;
    }
}


AObject::eType		Bomb::getObjectType() const
{
  AObject::eType	d;

  d = _objectType;
  return (d);
}

void		Bomb::erase()
{
  _explode = true;
}

size_t		Bomb::getRangeX() const
{
  return (_rangex);
}

size_t		Bomb::getRangeY() const
{
  return (_rangey);
}

void				Bomb::setRangeX(size_t x)
{
  _rangex = x;
}

void				Bomb::setRangeY(size_t y)
{
  _rangey = y;
}

//
// Item.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Mon May  7 15:16:51 2012 anna texier
// Last update Sun Jun  3 18:01:34 2012 gaetan senn
//

#include	"Item.hh"
#include	"Primitive.hh"
#include	"ComplexObj.hh"
#include	"Core.hh"

Item::Item(AObject::eType type, Board &map, size_t x, size_t y,
	   bool destruct, unsigned int point, float timegetted)
  : AObject(x, y, destruct, true, false, type, point, TIMEACCESSIBLE, timegetted), _map(map), _x(x), _y(y),
    _timestart(0), _pt()
{
  _pt[AObject::ITEM] = "WOOD";
  _pt[AObject::SPEED_BONUS] = "BONUSSPEED";
  _pt[AObject::SPEED_MALLUS] = "MALUSLEFT";
  _pt[AObject::LINEBOMB_BONUS] = "ULTIMATEBOMB";
  if (type != ITEM)
    _destructable = false;
  _form = new graphic::Complex::Cube(graphic::Vector(300.0f * static_cast<float>(x), 0.0f, -300.0f * static_cast<float>(y)),
				     graphic::Vector(0.0f, 0.0f, 0.0f),
				     graphic::Color(1,0.f, 0.f));
  _form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->
		    getTexture(_pt[AObject::ITEM]));
  _form->initialize();
  _form->incRotX(90);
  _form->rotEnableX();
  _form->rotDisableY();
  _form->rotDisableZ();
  Core::getInstance()->addObjectUpdate(this);
}

Item::~Item()
{
}

void			Item::update(const MyClock&g, gdl::Input&)
{
  if (_type != ITEM && _handled)
    {

      _form->incRotY(+1);
      _form->incRotX(+1);
      _form->rotEnableY();

      if (_timestart == 0)
	_timestart = g.getTotalGameTime();
      _timeremaining -= (g.getTotalGameTime() - _timestart);
      _timestart = g.getTotalGameTime();
      if (_timeremaining <= 0)
	_beRemovable = true;
    }
}

void		Item::erase()
{
  int		rand;
  if (_type != ITEM)
    {
      rand = random() % 2;
      if (rand)
	{
	  if (_pt.find(_type) != _pt.end())
	    _form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->
			      getTexture(_pt[_type]));
	  else
	    _form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->
			      getTexture("?"));
	}
      else
	_form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->
			  getTexture("?"));
      _dam = false;
      _destructable = false;
      _handled = true;
    }
  else
      _beRemovable = true;
}

AObject::eType	Item::getObjectType() const
{
  return _type;
}


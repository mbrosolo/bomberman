//
// PlayerIA.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Sat May  5 17:34:43 2012 anna texier
// Last update Sun Jun  3 17:32:12 2012 benjamin bourdin
//

#include <cstdlib>

#include "MyGame.hh"
#include "PlayerIA.hh"

//float	PlayerIA::PERIOD = 0.02f;
float const	PlayerIA::PERIOD = 0.02f;

char const*			PlayerIA::className = "PlayerIA";
lua::Luna<PlayerIA>::RegType	PlayerIA::methods[] = {
  LUA_METHOD(PlayerIA, test),
  LUA_METHOD(PlayerIA, moveLeft),
  LUA_METHOD(PlayerIA, moveRight),
  LUA_METHOD(PlayerIA, moveUp),
  LUA_METHOD(PlayerIA, moveDown),
  LUA_METHOD(PlayerIA, getLastDir),
  LUA_METHOD(PlayerIA, life),
  LUA_METHOD(PlayerIA, getCounter),
  LUA_METHOD(PlayerIA, incCounter),
  LUA_METHOD(PlayerIA, resetCounter),
  LUA_METHOD(PlayerIA, dropBomb),
  { 0, 0 }
};

std::string	PlayerIA::_IAfile[] = {
  "script/bomberman_easy.lua",
  "script/bomberman_medium.lua",
  "script/bomberman_hard.lua"
};

// PlayerIA --------------------------------------------------------------------

PlayerIA::PlayerIA(Board &map, std::string name,
		   size_t x, size_t y, eDifficulty d)
  : APlayer(map, name, x, y), _script(), _difficulty(d),
    _referenceTime(graphic::MyGame::getInstance()->getMyClock().getTotalGameTime()),
    _currentTime(_referenceTime), _setted(false), _Tmap(0), _counter(0),
    _count(),  _lastDir(LEFT)
{
  graphic::MyGame::getInstance()->getModelManager()->loadModelDynamic(name, "medias/assets/marvin.fbx");
  _form->setModel(graphic::MyGame::getInstance()->getModelManager()->getModel(name));
  _form->getModel()->set_default_model_color(gdl::Color(0, 0, 255));
  _form->initialize();

}

PlayerIA::~PlayerIA()
{
  if (this->_setted == true)
    this->unsetIA();
}

// Member functions ------------------------------------------------------------

void
PlayerIA::setIA()
{
  size_t	x;
  size_t	y;
  size_t	w;
  size_t	h;

  this->_script.load(this->_IAfile[this->_difficulty].c_str());
  this->_script.exportClass<PlayerIA>();
  this->_script.record(PlayerIA::bitAnd, "bitAnd");
  this->_script.recordObject(this, "cpu");
  this->_Tmap = this->_script.newTable();
  w = this->_map.getWidth();
  h = this->_map.getHeight();
  for (y = 0; y < h; ++y)
    {
      for (x = 0; x < w; ++x)
	this->_Tmap->push(70);
    }
  lua::Table*	size = this->_Tmap->newTable("size");
  size->define("x", this->_map.getWidth());
  size->define("y", this->_map.getHeight());
  delete size;
  this->_Tmap->record("map");
  this->_setted = true;
}

void
PlayerIA::unsetIA()
{
  this->_setted = false;
  this->_script.close();
  delete this->_Tmap;
}

void
PlayerIA::run(MyClock const& g)
{
  if (this->_setted == false)
    return;
  float	newReferenceTime = g.getTotalGameTime();
  float	delta = newReferenceTime - this->_referenceTime;

  if (delta >= PERIOD)
    {
      this->_exportDatas();
      this->_referenceTime = newReferenceTime;
      try {
	this->_script.call();
      }
      catch (lua::LuaException const& e) {
	lua::warn(e);
      }
    }
}

void
PlayerIA::_exportDatas()
{
  size_t	x;
  size_t	y;
  size_t	w;
  size_t	h;
  lua::Table*	map = this->_Tmap;

  w = this->_map.getWidth();
  h = this->_map.getHeight();
  map->reset();
  this->_script.up("map");
  this->_script.record(this->_x, "x");
  this->_script.record(this->_y, "y");
  for (y = 0; y < h; ++y)
    {
      for (x = 0; x < w; ++x)
	{
	  std::list<AObject*> const&	objs = this->_map.getSquare(x, y)->getObjects();
	  std::list<AObject*>::const_iterator	it;
	  int					squr = 0;

	  for (it = objs.begin(); it != objs.end(); it++)
	    squr |= (1 << static_cast<int>((*it)->getObjectType()));
	  map->push(squr);
	}
    }
}

int
PlayerIA::test(lua::prim_t* L)
{
  static_cast<void>(L);
  std::cout << "__CALL__: Player::test()" << std::endl;
  this->_script.ret(0);
  return 1;
}

int
PlayerIA::moveLeft(lua::prim_t* L)
{
  static_cast<void>(L);
  this->_lastDir = LEFT;
  this->APlayer::moveLeft();
  return 0;
}

int
PlayerIA::moveRight(lua::prim_t* L)
{
  static_cast<void>(L);
  this->_lastDir = RIGHT;
  this->APlayer::moveRight();
  return 0;
}

int
PlayerIA::moveUp(lua::prim_t* L)
{
  static_cast<void>(L);
  this->_lastDir = UP;
  this->APlayer::moveUp();
  return 0;
}

int
PlayerIA::moveDown(lua::prim_t* L)
{
  static_cast<void>(L);
  this->_lastDir = DOWN;
  this->APlayer::moveDown();
  return 0;
}

int
PlayerIA::getLastDir(lua::prim_t* L)
{
  static_cast<void>(L);
  this->_script.ret(static_cast<int>(this->_lastDir));
  return 1;
}

int
PlayerIA::life(lua::prim_t* L)
{
  static_cast<void>(L);
  size_t hp = this->APlayer::life();
  this->_script.ret(static_cast<int>(hp));
  return 1;
}

int
PlayerIA::getCounter(lua::prim_t* L)
{
  static_cast<void>(L);
  if (this->_script.getArgc() != 1)
    throw lua::LuaException("Expected 1 argument", "PlayerIA::getCounter()");
  std::string		key = this->_script.getArgv(0);
  map_t::iterator	it = this->_count.find(key);

  if (it == this->_count.end())
    {
      this->_count[key] = 0;
      this->_script.ret(0);
    }
  else
    this->_script.ret(it->second);
  return 1;
}

int
PlayerIA::incCounter(lua::prim_t* L)
{
  static_cast<void>(L);
  if (this->_script.getArgc() != 1)
    throw lua::LuaException("Expected 1 argument", "PlayerIA::getCounter()");
  std::string		key = this->_script.getArgv(0);
  map_t::iterator	it = this->_count.find(key);

  if (it == this->_count.end())
    this->_count[key] = 1;
  else
    this->_count[key]++;
  return 0;
}

int
PlayerIA::resetCounter(lua::prim_t* L)
{
  static_cast<void>(L);
  if (this->_script.getArgc() != 1)
    throw lua::LuaException("Expected 1 argument", "PlayerIA::getCounter()");
  std::string		key = this->_script.getArgv(0);

  this->_count[key] = 0;
  return 0;
}

int
PlayerIA::dropBomb(lua::prim_t*L)
{
  static_cast<void>(L);
  this->APlayer::dropBomb();
  return 0;
}

// static ----------------------------------------------------------------------

int
PlayerIA::bitAnd(lua::prim_t* L)
{
  lua::Script	script(L);

  if (script.getArgc() != 2)
    throw lua::LuaException("Expected 2 arguments", "PlayerIA::bitAnd()");

  std::string	a = script.getArgv(0);
  std::string	b = script.getArgv(1);

  int	res = atoi(a.c_str()) & atoi(b.c_str());
  script.ret(res);
  return 1;
}

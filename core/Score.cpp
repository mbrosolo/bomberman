//
// HighScore.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Fri May 25 07:22:36 2012 benjamin bourdin
// Last update Sun Jun  3 14:08:29 2012 benjamin bourdin
//

#include	<sstream>
#include	<fstream>
#include	<algorithm>
#include	<iostream>
#include	"Score.hh"
#include	"MyGame.hh"

const std::string	score::HighScore::_storage = ".highscore_bomberman";
//const std::string	score::HighScore::_antiCheatFile = "/tmp/.highscore_bomberman_anticheatsystem";

namespace score_utils
{
  void	printScore(score::Score *sc)
  {
    std::cout << "pts=" << sc->getPts() << " date=" << sc->getDate()
	      << std::endl;
  }

  void	delScore(score::Score *sc)
  {
    delete sc;
  }

}

score::Score::Score(const int pts)
  : _date(), _pts(pts), _text()
{
  setCurDate();
  _text.initialize();
  _text.setScale(graphic::Scale(0.4f, 0.4f, 0.4f));
}

score::Score::~Score()
{
}

int	score::Score::getPts(void) const
{
  return _pts;
}


std::string const &	score::Score::getDate(void) const
{
  return _date;
}

void	score::Score::setDate(const std::string &date)
{
  _date = date;
}

graphic::Text &	score::Score::getText(void)
{
  std::stringstream ss;
  std::string	str;
  std::string	strFull("");

  ss << getPts();
  ss >> str;
  strFull = strFull + str;
  strFull = strFull + std::string(" - ");
  ss.clear(); str.clear();

  strFull = strFull + getDate();

  _text.setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("TEXT_TABLE"));
  _text.updateText(strFull);
  return _text;
}

void	score::Score::setCurDate(void)
{
  _date = graphic::MyGame::getInstance()->getMyClock().getCurDate();
}

void	score::Score::setPts(const int pts)
{
  _pts = pts;
}

score::HighScore	*score::HighScore::_instance = NULL;

score::HighScore::HighScore()
  : _listScore()
{
  recoverHighScoreFromFile();
}

score::HighScore::~HighScore()
{
  storeHighScoreFile();
  std::for_each(_listScore.begin(), _listScore.end(), &score_utils::delScore);
}

score::HighScore*	score::HighScore::getHighScoreInstance(void)
{
  if (_instance == NULL)
    _instance = new HighScore();
  return _instance;
}

void	score::HighScore::killHighScoreInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}

void	score::HighScore::setScore(const int pts)
{
  for (listScore_t::iterator it = _listScore.begin(); it != _listScore.end(); ++it)
    {
      if (pts >= (*it)->getPts())
	{
	  _listScore.insert(it, new Score(pts));
	  // we remove the worst score in the top10
	  if (_listScore.size() > 10)
	    {
	      it = _listScore.end();
	      --it;
	      _listScore.erase(it);
	    }
	  return;
	}
    }
  _listScore.push_back(new Score(pts));
}

void	score::HighScore::printHighScore(void) const
{
  std::for_each(_listScore.begin(), _listScore.end(), &score_utils::printScore);
}

void	score::HighScore::storeHighScoreFile(void) const
{
  std::ofstream fs;

  fs.open(_storage.c_str(), std::ios::trunc | std::ios::out);
  if (fs.is_open())
    {
      for (listScore_t::const_iterator it = _listScore.begin(); it != _listScore.end(); ++it)
	{
	  fs << (*it)->getPts() << "|" << (*it)->getDate() << std::endl;
	}
      fs.close();
    }
}

score::HighScore::listScore_t &		score::HighScore::getListScore(void)
{
  return _listScore;
}

void	score::HighScore::recoverHighScoreFromFile(void)
{
  std::ifstream fs;

  fs.open(_storage.c_str());
  if (fs.is_open())
    {
      unsigned int nbLine = 0;
      for (std::string line; std::getline(fs, line); ++nbLine)
	{
	  std::size_t len = 0;
	  score::Score *sc = new score::Score;

	  for (unsigned int n = 0; n < 2; ++n)
	    {
	      std::string part;
	      for (; len < line.size() && line[len] != '|'; ++len)
		part = part + line[len];
	      if (line[len] == '|')
		++len;

	      int	res;
	      if (n == 0)
		{
		  std::stringstream ss(part);
		  ss >> res;
		}

	      switch (n) {
	      case 0:
		sc->setPts(res); break;
	      case 1:
		sc->setDate(part); break;
	      }
	    }

	  if (nbLine < 10)
	    _listScore.push_back(sc);
	}
      fs.close();
    }
}

//
// Solid.hh for bomberman in /home/ga/Documents/Projet/bomberman/bomberman/core
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Tue May 15 11:02:30 2012 gaetan senn
// Last update Sat Jun  2 16:28:45 2012 gaetan senn
//

#ifndef __SOLID__HH
#define __SOLID__HH

#include	"AObject.hh"

class		Solid : public AObject
{
protected:
  size_t	_pos;
  AObject::eType	_type;

public:
  Solid(size_t, size_t, AObject::eType type = AObject::WALL);
  ~Solid();

  virtual AObject::eType	getObjectType() const;
  virtual void	erase();
};

#endif

//
// Solid.cpp for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Mon May 21 17:21:24 2012 anna texier
// Last update Sun Jun  3 22:08:45 2012 tosoni_t
//

#include	<iostream>
#include	"Solid.hh"
#include	"ComplexObj.hh"
#include	"MyGame.hh"

Solid::Solid(size_t x, size_t y, AObject::eType type)
  : AObject(x, y, false, false, false, type), _pos(0), _type(type)
{
  _dam = (_type == FLOOR) ? false : true;
  if (_type == AObject::WALL)
    {
      _form = new graphic::Complex::Cube(graphic::Vector(300.0f * static_cast<float>(x), 0.0f,
							 -300.0f * static_cast<float>(y)),
					 graphic::Vector(0.0f, 0.0f, 0.0f),
					 graphic::Color(0.5f, 0.5f, 0.5f));
      _form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("STEEL"));
    }
  else
    {
      _form = new graphic::Complex::Cube(graphic::Vector(300.0f * static_cast<float>(x), -300.0f,
							 -300.0f * static_cast<float>(y)),
					 graphic::Vector(0.0f, 0.0f, 0.0f),
					 graphic::Color(0.6f, 0.4f, 0.2f));
    }
  _form->initialize();
}

Solid::~Solid() {}

AObject::eType	Solid::getObjectType() const
{
  return (_type);
}

void	Solid::erase()
{
}

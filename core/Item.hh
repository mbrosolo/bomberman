//
// Item.hh for Bomberman in /home/texier_a//projets/bomberman
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Mon May  7 15:03:24 2012 anna texier
// Last update Sat Jun  2 23:09:29 2012 gaetan senn
//

#ifndef	__ITEM_H_
#define	__ITEM_H_

#include	"AObject.hh"
#include	"Board.hh"
#include	"Square.hh"
#include	"MyGame.hh"
#include	<map>
#define		TIMEACCESSIBLE 8.0f

class Item : public AObject
{
private:
  Board		&_map;
  size_t	_x;
  size_t	_y;
  float		_timestart;
  std::map<AObject::eType, std::string>	_pt;
public:
  Item(AObject::eType, Board &, size_t, size_t,
       bool destruct = true, unsigned int point = 0,
       float timegetted = DEFAULTTIME);
  ~Item();
  Item(const Item &);
  const Item &operator=(const Item &);

  /* AOBJECT METHOD */
  virtual void		erase();
  AObject::eType	getObjectType() const;
  void			update(const MyClock&g, gdl::Input&);
};

#endif

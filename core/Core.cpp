//
// Core.cpp for Bomberman in /home/ga/Documents/Projet/bomberman/bomberman/core
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri May 18 12:48:13 2012 gaetan senn
//

#include	"Core.hh"
#include	"Player.hh"
#include	"MyGame.hh"
#include	"Math.hpp"
#include	"Solid.hh"
#include	"Item.hh"
#include	"ExceptionLoadMap.hh"
#include	"ExceptionCore.hh"
#include	"GameEventManager.hh"

Core	*Core::_instance = NULL;

Core::Core()
  : _width(0), _height(0),
    _board(), _players(), _cpu(), _updateList(),
    _playTime(0.0f),
    _referenceTime(0.0f),
    _totalTime(0.0f), _difficulty(PlayerIA::EASY)
{
  /* NORMAY CHECK THE SIZE DONATE */
}

Core::~Core()
{
  delete _board;
}

void				Core::addObjectUpdate(AObject *o)
{
  _updateList.push_back(o);
}

size_t			Core::getHeight() const
{
  return _height;
}

size_t			Core::getWidth() const
{
  return _width;
}

void			Core::addCpu(APlayer *p)
{
  _cpu[p] = true;
}

void			Core::setDifficulty(const PlayerIA::eDifficulty diff)
{
  _difficulty = diff;
}

void		Core::setEventManager(std::list<GameEventManager *> list)
{
  std::list<GameEventManager *>::iterator it;
  std::map<APlayer *, bool>::iterator	it2;

  it = list.begin();
  for (it2 = _players.begin();it2 != _players.end();++it2)
    {
      if (it != list.end())
	{
	  (it2->first)->setKeys((*it));
	  it++;
	}
      else
	throw ExceptionCore("On set Event Manger",
			    "Core::setEventManager(std::list<GameEventManager *>)");
    }
}

std::map<APlayer *, bool> const	&Core::getPlayers() const
{
  return (_players);
}

void				Core::addPlayer(APlayer * p)
{
  _players[p] = true;
}

std::map<APlayer *, bool> const	&Core::getCpu() const
{
  return (_cpu);
}

std::list<AObject *>		&Core::getUpdateList()
{
  return (_updateList);
}

bool	Core::notplayer(const size_t pos)
{
  std::map<APlayer *, bool>::iterator	it;

  /* AT THIS POINT WE NEED TO CHECK AROUND THE POSITION IF THERE IS PLAYERS */

  size_t res[2];
  systool::Math<size_t>::c1dto2d(pos, _board->getWidth(), _board->getHeight(), res);

  const size_t x = res[0];
  const size_t y = res[1];
  const size_t w = _board->getWidth();

  for (size_t tmpy = ((((int)y - 1) < 0) ? 0 : ((int)y - 1)); tmpy < ((int)y + 1); ++tmpy)
    {
      for (size_t tmpx = ((((int)x - 1) < 0) ? 0 : ((int)x - 1)); tmpx < ((int)x + 1); ++tmpx)
	{
	  const size_t p = systool::Math<size_t>::c2dto1d(tmpx, tmpy, w);

	  if (_board->getSquare(p)->isImpactable() == false)
	    return false;

	  for (it = _players.begin() ;it != _players.end() ;++it)
	    {
	      if (systool::Math<size_t>::c2dto1d((*it).first->getX(),
						 (*it).first->getY(),
						 _board->getWidth()) == p)
		return false;
	    }

	  for (it = _cpu.begin() ;it != _cpu.end() ;++it)
	    {
	      if (systool::Math<size_t>::c2dto1d((*it).first->getX(),
						 (*it).first->getY(),
						 _board->getWidth()) == p)
		return false;
	    }
	}
    }
  return true;
}

bool	Core::addPlayer(GameEventManager *_playerKeys)
{
  size_t		pos;
  size_t		res[2];
  bool			continu = true;

  /* CURRENTLY SET RANDOMLY THE PLAYER ON THE BOARD */
  if (_board->isFree() == false)
    return (false);
  while (continu)
    {
      pos = rand() % ((_width * _height) - 1);
      if (_board->getSquare(pos)->isImpactable() && notplayer(pos))
	{
	  systool::Math<size_t>::c1dto2d(pos, _width, _height, res);
	  APlayer *	p = new
	    Player(*_board, _playerKeys, "", res[0], res[1]);
	  _players[p] = true;
	  _board->getSquare(p->getX(), p->getY())->addObject(p, true);
	  continu = false;
	}
    }
  return (true);
}

void		Core::initLua(const int nblua)
{
  for (int i = 0;i < nblua;i++)
    {
      addLua(_difficulty);
    }
}

bool	Core::addLua(PlayerIA::eDifficulty level)
{
  size_t		pos;
  size_t		res[2];
  size_t		maxPos = 0;
  bool			continu = true;

  /* CURRENTLY SET RANDOMLY THE PLAYER ON THE BOARD */
  if (_board->isFree() == false)
    return (false);
  while (continu && maxPos < _board->getWidth() * _board->getHeight())
    {
      pos = rand() % ((_width * _height) - 1);
      if (_board->getSquare(pos)->isImpactable() && notplayer(pos))
	{
	  maxPos = 0;
	  systool::Math<size_t>::c1dto2d(pos, _width, _height, res);

	  APlayer *	p = new
	    PlayerIA(*_board, "", res[0], res[1], level);

	  try {
	    dynamic_cast<PlayerIA*>(p)->setIA();
	  }
	  catch (lua::LuaException const& e) {
	    lua::warn(e);
	  }
	  _cpu[p] = true;
	  _board->getSquare(p->getX(), p->getY())->addObject(p, true);
	  continu = false;
	}
      ++maxPos;
    }
  if (continu)
    std::cerr << "Not enough place to set AI" << std::endl;
  return (true);
}

void	Core::initializeMap(std::string const &file)
{
  try
    {
      _board = new Board(file.c_str());
    }
  catch (ExceptionLoadMap &e)
    {
      std::cerr << "Can't init map load default" << std::endl;
      initialize(20, 20);
    }
}

void	Core::initialize(const size_t w, const size_t h)
{
  _width = w;
  _height = h;
  _board = new Board(w, h);
}

Core	*Core::getInstance(void)
{
  if (_instance == NULL)
    _instance = new Core();
  return _instance;
}

void	Core::killInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}

void	Core::setHeight(const size_t h)
{
  _height = h;
}

void	Core::setWidth(const size_t w)
{
  _width = w;
}

void	Core::setBoard(Board *board)
{
  _board = board;
}

Board	*Core::getBoard(void) const
{
  return _board;
}

void		Core::runSave()
{
  initCoordSpace();
  setTotalTime(0.0f);
}

void		Core::run(const int nblua)
{
  initLua(nblua);
  initCoordSpace();
  setTotalTime(0.0f);
}

void	Core::setReferenceTime(const MyClock &cl)
{
  _totalTime += _playTime;
  _referenceTime = cl.getTotalGameTime();
  _playTime = 0.0f;
}

void	Core::setTotalTime(const float time)
{
  _totalTime = time;
}

float	Core::getTotalTime(void) const
{
  return _totalTime + _playTime;
}

void		Core::draw()
{
  /* FOR EACH PLAYERS JUST DRAW ROUNDED 20 PX */
  std::map<APlayer *, bool>::iterator	it;
  int				x;
  int				y;
  int				rangex;
  int				rangey;

  //  usleep(1000);
  _board->getSquare(0, 0)->printObjects();

  const int moreX =
    ((graphic::MyGame::getInstance()->getCameraManagement()->isMulti() == true) ?
     0 : 10);

  for (it = _players.begin() ;it != _players.end() ;++it)
    {
      x = (*it).first->getX() - 20 - moreX;
      if (x < 0)
	x = 0;
      y = (*it).first->getY() - 20;
      if (y < 0)
	y = 0;
      rangex = x + 40 + 2*moreX;
      if (rangex >= (_board->getWidth()))
	rangex = _board->getWidth() - 1;
      rangey = y + 40;
      if (rangey >= static_cast<int>(_board->getHeight()))
	rangey = _board->getHeight() - 1;
      for (int ty = y; ty <= rangey;ty++)
	{
	  for (int tx = x; tx <= rangex;tx++)
	    {
	      _board->getSquare(tx, ty)->printObjects();
	    }
	}
    }
}

bool		Core::removeElementUpdate(AObject *obj)
{
  std::list<AObject *>::iterator	it;

  it = find(_updateList.begin(), _updateList.end(), obj);
  if (it != _updateList.end())
    {
      _updateList.erase(it);
      return (true);
    }
  return (false);
}

void		Core::cleanUpdateList()
{
  std::list<AObject *>::iterator		it;

  for (it = _updateList.begin(); it != _updateList.end(); ++it)
    {
      if ((*it)->beRemoved())
	{
	  _board->getSquare((*it)->getX(), (*it)->getY())->popObject((*it));
	  delete (*it);
	  it = _updateList.erase(it);
	  }
    }
}

void		Core::update(const MyClock&gc, gdl::Input&in)
{
  std::map<APlayer *, bool>::iterator		it;
  std::map<APlayer *, bool>::iterator		cpu;
  std::list<AObject *>::iterator		it2;

  _playTime = (gc.getTotalGameTime() - _referenceTime);
  cleanUpdateList();
  for (it2 = _updateList.begin(); it2 != _updateList.end(); ++it2)
    {
      (*it2)->update(gc, in);
    }
  for (cpu = _cpu.begin(); cpu != _cpu.end(); ++cpu)
    {
      if ((*cpu).second == true)
	{
	  (*cpu).first->update(gc, in);
	  if (((*cpu).first)->life() == 0)
	    {
	      (*cpu).first->moveUpdate();
	      (*cpu).first->setBeRemovable(true);
	      cpu->second = false;
	    }
	}
    }
  for (it = _players.begin(); it != _players.end();++it)
    {
      if ((*it).second == true)
	{
	  (*it).first->update(gc, in);
	  (*it).first->getHandled();
	  if (((*it).first)->life() == 0)
	    {
	      (*it).first->moveUpdate();
	      (*it).first->setBeRemovable(true);
	      //FOR SAVE THE PARTY AND SAVE THE SCORE OF PLAYER DON'T DELETE PARTY :)
	      it->second = false;
	    }
	}
    }
}

bool		Core::isFinished() const
{
  std::map<APlayer *, bool>::const_iterator		it;

  size_t nHumanAlive = 0;
  for (it = _players.begin(); it != _players.end();++it)
    if ((*it).second)
      ++nHumanAlive;

  if (nHumanAlive >= 2)
    return false;

  // if one player
  if (nHumanAlive == 1)
    {
      // no cpu
      for (it = _cpu.begin(); it != _cpu.end(); ++it)
	if ((*it).second)
	  return false;
    }

  // here the game is finished
  for (it = _players.begin(); it != _players.end();++it)
    score::HighScore::getHighScoreInstance()->setScore((*it).first->getScore());
  return (true);
}

void	Core::initCoordSpace(void)
{
  const size_t	widthObject = static_cast<size_t>(graphic::MyGame::WINDOW_WIDTH) / (_board->getWidth()); // +1 = marge pour faire joli
  const size_t	heightObject = static_cast<size_t>(graphic::MyGame::WINDOW_HEIGHT) / (_board->getHeight() + 2); // +1 = marge pour faire joli
  size_t	sizeObject = (widthObject > heightObject) ? widthObject : heightObject;
  size_t	line = 0;

  graphic::Vector	pos;

  if (sizeObject * _board->getWidth() > static_cast<size_t>(graphic::MyGame::WINDOW_WIDTH))
    sizeObject = widthObject;
  else if (sizeObject * (_board->getHeight() + 2) > static_cast<size_t>(graphic::MyGame::WINDOW_HEIGHT))
    sizeObject = heightObject;

  if (sizeObject < 10 || sizeObject > 25)
    sizeObject = 20;

  float	coeffScale = (static_cast<float>(sizeObject) / 150.0f);


  float	posX = -graphic::MyGame::WINDOW_WIDTH + static_cast<float>(sizeObject), posY = graphic::MyGame::WINDOW_HEIGHT - static_cast<float>(sizeObject) * 2.0f, posZ = 0.0f;

  size_t n = 0;
  for (Board::map_t::iterator it = _board->_board.begin(); it != _board->_board.end(); ++it, ++line)
    {
      if (line == _board->getWidth())
	{
	  line = 0;
	  posX = -graphic::MyGame::WINDOW_WIDTH + static_cast<float>(sizeObject);
	  posY -= static_cast<float>(sizeObject) * 2.0f;
	  n++;
	}

      if (n == _board->getHeight() / 2 && line == _board->getWidth() / 2)
	{
	  pos.first = posX; pos.second = posY; pos.third = posZ - sizeObject;
	}

      (*it)->setPosition(graphic::Vector(posX, posY, posZ));
      (*it)->setScale(graphic::Scale(coeffScale, coeffScale, coeffScale));

      for (std::list<AObject *>::iterator it_obj = (*it)->getObjects().begin(); it_obj != (*it)->getObjects().end(); ++it_obj)
	{
	  // positionne correctement l'obj

#if defined DEBUG_BOARD && DEBUG_BOARD > 0
	  if ((*it_obj)->getObjectType() == AObject::PLAYER || it_obj == (*it)->_objects.begin())
	    {
	      if (it_obj == (*it)->_objects.begin())
		(*it_obj)->getForm()->setTexture(graphic::MyGame::getLoadTexture()->getTexture("FER"));
#else
	      if ((*it_obj)->getObjectType() == AObject::PLAYER)
		{
#endif
		  (*it_obj)->getForm()->setPosition(graphic::Vector(posX, posY, posZ + 10 - static_cast<float>(sizeObject) * 2.0f));
		  // (*it_obj)->getForm()->setScale(graphic::Scale(coeffScale, coeffScale, ));
		}
	      else
		(*it_obj)->getForm()->setPosition(graphic::Vector(posX, posY, posZ));
	      (*it_obj)->getForm()->setScale(graphic::Scale(coeffScale, coeffScale, coeffScale));
	    }

	  posX += static_cast<float>(sizeObject) * 2.0f;
	}

#if defined DEBUG_BOARD && DEBUG_BOARD < 1 || not defined (DEBUG_BOARD)
      {
	Board::map_t::iterator it = _board->_board.begin();
	std::list<AObject *>::iterator it_obj = (*it)->getObjects().begin();


	graphic::Video *video = new graphic::Video("medias/grass.avi");
	(*it_obj)->getForm()->setVideo(video);

	if ((_board->getHeight() % 2) == 0)
	  pos.second += sizeObject;
	if ((_board->getWidth() % 2) == 0)
	  pos.first -= sizeObject;
	(*it_obj)->getForm()->setPosition(pos);

	(*it_obj)->getForm()->setScale(graphic::Scale(static_cast<float>(_board->getWidth()) * coeffScale, coeffScale * static_cast<float>(_board->getHeight()), 0.0f));
      }
#endif
    }


//
// AObject.hh for bomberman in /home/ga/Documents/Projet/bomberman/bomberman/lib/systool/Documentation/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed May  9 13:21:55 2012 gaetan senn
// Last update Sun Jun  3 21:56:46 2012 gaetan senn
//

#ifndef		__AOBJECT_H_
#define		__AOBJECT_H_

#include	"AForm.hh"
#include	<iostream>
#include	"MyClock.hh"

/* DEFAULT OPTION OF AOBJECT */

#define		DEFAULTSCORE 200
#define		DEFAULTTIME 5
#define		TIMEBOMBEXPAND	20
#define		NODELETE -100

class AObject
{
public:
  /* BE CAREFULL DON'T FORGET TO IMPLEMENT THE POINTER
     FUNCTION INTO THE FACTORY WHEN YOU ADD NEW AOBJECT TYPE*/

  /*!
   * @enum eType
   * @brief Define the type of Object
   */
  enum		eType
    {
      SPEED_BONUS = 0,
      EXPANDBOMB_BONUS = 1,
      LINEBOMB_BONUS = 2,
      PLAYER = 3,
      BOMBFIRE = 4,
      BOMB = 5,
      FIRE = 6,
      ITEM = 7,
      WALL = 8,
      EMPTY = 9,
      FLOOR = 10,
      SPEED_MALLUS = 11,
      NBBOMB_BONUS = 12
    };

protected:
  size_t		_x;   /*!< position x */
  size_t		_y; /*!< position y */
  bool			_destructable; /*!< destructable */
  bool			_dam; /*!< dam */
  bool			_handled; /*!< handled x */
  eType			_type; /*!< type Object */
  graphic::AForm	*_form;   /*!< form data */
  bool			_beRemovable; /*!< beRemovable */
  bool			_toMove; /*!< to move */
  unsigned int		_value; /*!< value score */
  float			_timeremaining;/*!< time Remaining*/
  float			_timeremaininggetted;/*!< timeremaininggetted */

public:
  /**
   *
   *
   * @param _x 
   * @param _y 
   * @param destructable 
   * @param dam 
   * @param handled 
   * @param type 
   * @param value 
   * @param timeremaining 
   * @param timeremaininggetted 
   * 
   * @return 
   */
  explicit AObject(size_t _x, size_t _y, bool destructable,
		   bool dam, bool handled, eType type = EMPTY,
		   unsigned int value = 0, float timeremaining = 0.0f,
		   float timeremaininggetted = 0.0f);
  virtual ~AObject();
  /** 
   * 
   * 
   * @param copy 
   */
  AObject(AObject const &copy);
  /** 
   * 
   * 
   * @param copy 
   * 
   * @return 
   */
  AObject & operator=(AObject const &copy);

  /* CURRENTLY FOR COUT TEST */
  /**
   * @brief get Object Type of AObject
   * 
   * 
   * @return 
   */
  virtual eType				getObjectType() const = 0;

  /*GRAPHIC METHOD*/
  /** 
   * @brief getKey of Texture Aobject
   * 
   * 
   * @return 
   */
  std::string const &			getKey() const;
  /*GETTER METHOD */
  /** 
   * @brief getPosition of Aobject on Graphic
   * 
   * 
   * @return 
   */
  graphic::Vector const			&getPosition() const;
  /** 
   * @brief getForm of AObject Graphic
   * 
   * 
   * @return 
   */
  graphic::AForm			*getForm(void) const;
  /*SETTER METHOD */
  /** 
   * @brief setPosition of AObject graphic
   * 
   * @param p 
   */
  void					setPosition(graphic::Vector const &p);
  /** 
   * @brief setScale of AObject
   * 
   * @param scale 
   */
  void					setScale(graphic::Scale const &scale);
  /** 
   * @brief setRemaining time
   * 
   * @param value 
   */
  void					setRemaining(float value);
  /** 
   * @brief setRemainingGetted time
   * 
   * @param value 
   */
  void					setRemainingGetted(float value);
  /** 
   * @brief set beRemovable Aobject for Core check
   * 
   * @param beRemovable 
   */
  void					setBeRemovable(bool beRemovable);
  /** 
   * @brief set Value of win point about game 
   * 
   * @param value 
   */
  void					setValue(size_t value);
  /** 
   * @brief setTimeRemaining
   * 
   * @param time 
   */
  void					setTimeRemaining(float time);
  /** 
   * @brief setTimeRemainingGetted
   * 
   * @param timeGetted 
   */
  void					setTimeRemainingGetted(float timeGetted);
	/* ACTION GRAPH */
  /** 
   * @brief setDestructable
   * 
   * @param d 
   */
  void					setDestructable(bool d);
  /** 
   * @brief setDam
   * 
   * @param d 
   */
  void					setDam(bool d);
  /** 
   * @brief setHandled
   * 
   * @param d 
   */
  void					setHandled(bool d);
  /** 
   * @brief draw AObject for graphic
   * 
   */
  virtual void				draw();
  /** 
   * @brief moveLeft for graphic
   * 
   */
  virtual void				moveLeft();
  /** 
   * @brief moveRight for graphic
   * 
   */
  virtual void				moveRight();
  /** 
   * @brief moveUP for graphic
   * 
   */
  /** 
   * @brief move up AObject
   * 
   */
  virtual void				moveUp();
  /** 
   * @brief move down AObject
   * 
   */
  virtual void				moveDown();
  /** 
   * @brief call update of AObject
   * 
   */
  virtual void				update(const MyClock&, gdl::Input&);
  /** 
   * @brief take AObject
   * 
   */
  virtual void				take();
  /** 
   * @brief leave AObject
   * 
   */
  virtual void				leave();
  /* BOARD METHOD UTILS */
	/*SETTER */
  /** 
   * 
   * 
   * @param value 
   */
  virtual void				setToMove(bool value);
	/*GETTER */
  /** 
   * @brief get X position
   * 
   * 
   * @return 
   */
  size_t				getX() const;
  /** 
   * @brief get Y position
   * 
   * 
   * @return 
   */
  size_t				getY() const;
  /** 
   * @brief check if AObject is destructable
   * 
   * 
   * @return 
   */
  bool					isDestructable() const;
  /** 
   * @brief check if AObject can be removed
   * 
   * 
   * @return 
   */
  bool					beRemoved() const;
  /** 
   * @brief check is the AObject is Dam about the other AObject
   * 
   * 
   * @return 
   */
  bool					isDam() const;
  /** 
   * @brief check if the AObject is type handled
   * 
   * 
   * @return 
   */
  bool					isHandled() const;
  /** 
   * @brief get value score of Object
   * 
   * 
   * @return 
   */
  virtual unsigned int			getValue() const;
  /** 
   * @brief check if AObject be to moved
   * 
   * 
   * @return 
   */
  bool					toMove() const;
  /** 
   * @brief get RemainingGetted time
   * 
   * 
   * @return 
   */
  float					getRemainingGetted() const;
  /** 
   * @brief get Remaining time
   * 
   * 
   * @return 
   */
  float					getRemaining() const;
	/* ACTION METHOD */
  /* ERASE METHOD IF AOBJECT TYPE IMPACT IS ON SQUARE */
  /** 
   * @brief erase AOBJECT
   * 
   */
  virtual void				erase() = 0;
  /** 
   * @brief get Range x of AOBJECT
   * 
   * 
   * @return
   */
  virtual size_t			getRangeX() const;
  /** 
   * @brief get Range Y of AOBJECT
   * 
   * 
   * @return
   */
  virtual size_t			getRangeY() const;
  /** 
   * @brief set Range x of AOBJECT
   * 
   * @param x the x
   */
  virtual void				setRangeX(size_t x);
  /** 
   * @brief set Range y of AOBJECT
   * 
   * @param y the y
   */
  virtual void				setRangeY(size_t y);
};

#endif

//
// Board.hh for Bomberman in /home/texier_a//projets/bomberman
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Sat May  5 18:51:08 2012 anna texier
// Last update Sun Jun  3 22:07:29 2012 gaetan senn
//

#ifndef	__BOARD_H_
#define	__BOARD_H_

#include	<vector>
#include	<iostream>
#include	<cstdio>
#include	<map>
#include	"Square.hh"
#include	"Math.hpp"
#include	"Factory.hh"

class		APlayer;

  /*!
   * @enum eEvent
   * @brief Define the key of player
   */
enum eEvent
  {
    UP,
    RIGHT,
    LEFT,
    TOP,
    QUIT,
    DOWN,
    PAUSE,
    DROP
  };

class Board
{
public:
  typedef	AObject		*(Factory::*ptfactory)
    (size_t, size_t, Board *);
  typedef	std::vector<Square *>	map_t;
private:
  size_t				_width; /*!<  width of map */
  size_t				_height; /*!<  height of map */
  float					_scale; /*!<  scale of map */
  std::map<size_t, ptfactory>		_addBonus; /*!<  bonnus pt */

  // tmp
public:
  map_t					_board; /*!<  local map*/

private:
  std::map<eEvent, bool>		Event; /*!<  event map */

public:
  /** 
   * @brief constructor
   * 
   */
  Board();
  /** 
   * @brief constructor with size map
   * 
   * @param size_t 
   * @param size_t 
   */
  Board(size_t, size_t);
  /** 
   * @brief constructor with load from file map
   * 
   */
  Board(const char *);
  /** 
   * @brief destructor
   * 
   */
  ~Board();
  /** 
   * @brief coplien
   * 
   */
  Board(const Board &);
  /** 
   * @brief coplie too
   * 
   * 
   * @return 
   */
  Board const &operator=(const Board &);

  /* BETA COUT PRINT */

  /** 
   * @brief addSquare to map
   * 
   */
  void		addSquare(Square *);
  /** 
   * @brief setWidth from map
   * 
   * @param size_t 
   */
  void		setWidth(size_t);
  /** 
   * @brief setHeight from map
   * 
   * @param size_t 
   */
  void		setHeight(size_t);

  /** 
   * @brief generateMap from file
   * 
   */
  void		generateMap();
  /** 
   * @brief getSquare at pos x, y
   * 
   * @param size_t 
   * @param size_t 
   * 
   * @return 
   */
  Square	*getSquare(size_t, size_t) const;
  /** 
   * @brief getSquare of pos x
   * 
   * @param size_t 
   * 
   * @return 
   */
  Square	*getSquare(size_t) const;
  /** 
   * @brief add update into item
   * 
   * @param size_t 
   * @param size_t 
   * 
   * @return 
   */
  bool		pushAObjectUpdate(size_t, size_t, AObject *);
  /** 
   * @brief add object
   * 
   * @param size_t 
   * @param size_t 
   * 
   * @return 
   */
  bool		pushAObject(size_t, size_t, AObject *);
  /** 
   * @brief push with pos x
   * 
   * @param size_t 
   * 
   * @return 
   */
  bool		pushAObject(size_t, AObject *);
  /** 
   * @brief move Object
   * 
   */
  void		moveObject(AObject *, size_t, size_t);
  /** 
   * @brief get size of map
   * 
   * 
   * @return 
   */
  size_t	getWidth() const;
  /** 
   * @brief get height of map
   * 
   * 
   * @return 
   */
  size_t	getHeight() const;
  /** 
   * @brief isFree check board Aobject be placed into map
   * 
   * 
   * @return 
   */
  bool		isFree() const;
  /** 
   * @brief run about Thread pool
   * 
   */
  void		runTaskThread(void *);
  /** 
   * @brief stop thread task
   * 
   */
  void		stopTaskThread(void *);
  /** 
   * @brief print board
   * 
   */
  void		printBoard() const;
  /** 
   * @update board data
   * 
   */
  void		updateBoard(const MyClock&, gdl::Input&) const;
};

#endif

//
// Wall.cpp for bomberman in /home/gressi_b/Epitech/B4/bomberman/core
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Fri May 25 14:49:12 2012 gressi_b
// Last update Fri May 25 14:49:12 2012 gressi_b
//

#include "Wall.hh"

// Wall ------------------------------------------------------------------------

Wall::Wall(size_t x, size_t y)
  : Solid(x, y, AObject::WALL)
{
}

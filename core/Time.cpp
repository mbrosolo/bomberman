//
// Time.cpp for plazza in /home/texier_a//projets/plazza-2015-texier_a
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Apr 12 19:25:16 2012 anna texier
// Last update Mon May 14 14:33:19 2012 gaetan senn
//

#include	<iostream>
#include	"Time.hh"

Time::Time(const int _second, const long int _micro)
  : _mutex(new systool::Mutex()), _timeout(new systool::CondVar(*_mutex)),
    second(_second), micro(_micro)
{
}

Time::~Time() {}

Time::Time(const Time &t)
  : _mutex(t._mutex), _timeout(t._timeout), second(t.second)
  , micro(t.micro)
{
}

void	Time::settime(int const _second, long int const _micro)
{
  second = _second;
  micro = _micro;
}
void	Time::timeout()
{
  _timeout->timedwait(second, micro);
}

void	Time::stop()
{
  _timeout->signal();
}

systool::ICondVar		*Time::getCondVar() const
{
  return (_timeout);
}

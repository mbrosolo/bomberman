//
// ExceptionLoading.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 07:02:02 2012 benjamin bourdin
// Last update Thu May 31 16:01:03 2012 gaetan senn
//

#include	"ExceptionLoadMap.hh"

ExceptionLoadMap::ExceptionLoadMap(std::string const &what, std::string const &where) throw()
: ExceptionRuntime(what, where)
{
}

ExceptionLoadMap::~ExceptionLoadMap() throw()
{
}

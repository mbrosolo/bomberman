//
// Factory.cpp for bomberman in /home/ga/Documents/Projet/bomberman/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun  2 22:31:51 2012 gaetan senn
// Last update Sun Jun  3 22:03:19 2012 tosoni_t
//

#include	"Factory.hh"
#include	"Item.hh"
#include	"Solid.hh"
#include	"Effect.hh"
#include	"Core.hh"
#include	"Player.hh"

Factory		*Factory::_instance = NULL;

Factory::Factory()
  : _creatorpt()
{
  _creatorpt[AObject::SPEED_BONUS] = &Factory::createSpeedBonus;
  _creatorpt[AObject::EXPANDBOMB_BONUS] = &Factory::createExpandBombBonus;
  _creatorpt[AObject::LINEBOMB_BONUS] = &Factory::createLineBombBonus;
  _creatorpt[AObject::PLAYER] = &Factory::createPlayer;
  _creatorpt[AObject::BOMBFIRE] = &Factory::createBombFire;
  _creatorpt[AObject::BOMB] = &Factory::createBomb;
  _creatorpt[AObject::FIRE] = &Factory::createFire;
  _creatorpt[AObject::ITEM] = &Factory::createItem;
  _creatorpt[AObject::WALL] = &Factory::createWall;
  _creatorpt[AObject::FLOOR] = &Factory::createFloor;
  _creatorpt[AObject::SPEED_MALLUS] = &Factory::createSpeedMallus;
  _creatorpt[AObject::NBBOMB_BONUS] = &Factory::createNbBombBonus;
}

Factory::~Factory()
{
}

AObject		*createAObject(AObject::eType)
{
  return (NULL);
}

Factory		*Factory::getInstance(void)
{
  if (!_instance)
    _instance = new Factory();
  return (_instance);
}

void		Factory::killInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}

/* GETTER FACTORY */

AObject		*Factory::createSpeedBonus(size_t x, size_t y, Board * _board)
{
  return (new Item(AObject::SPEED_BONUS, *_board,
		   x, y, true, DEFAULTSCORE, DEFAULTTIME));
}

AObject		*Factory::createExpandBombBonus(size_t x, size_t y, Board * _board)
{
  return (new Item(AObject::EXPANDBOMB_BONUS, *_board,
		   x, y, true, DEFAULTSCORE, DEFAULTTIME));
}

AObject		*Factory::createLineBombBonus(size_t x, size_t y, Board * _board)
{
  return (new Item(AObject::LINEBOMB_BONUS,
		   *_board, x, y, true, DEFAULTSCORE, DEFAULTTIME));
}

AObject		*Factory::createPlayer(size_t x, size_t y, Board * _board)
{
  return (new Player(*_board, NULL , "", x, y));
}

AObject		*Factory::createBombFire(size_t x, size_t y, Board *)
{
  return (new Effect(x, y));
}

AObject		*Factory::createBomb(size_t, size_t, Board *)
{
  return (NULL);
}

AObject		*Factory::createFire(size_t x, size_t y, Board *)
{
  return (new Effect(x, y));
}

AObject		*Factory::createItem(size_t x, size_t y, Board * _board)
{
  return (new Item(AObject::ITEM, *_board, x, y, true, DEFAULTSCORE));
}

AObject		*Factory::createWall(size_t x, size_t y, Board *)
{
  return (new  Solid(x, y));
}

AObject		*Factory::createFloor(size_t x, size_t y, Board *)
{
  return (new Solid(x, y, AObject::FLOOR));
}

AObject		*Factory::createSpeedMallus(size_t x, size_t y, Board * _board)
{
  return (new Item(AObject::SPEED_MALLUS, *_board, x, y, true, DEFAULTSCORE));
}

AObject		*Factory::createNbBombBonus(size_t x, size_t y, Board * _board)
{
  return (new Item(AObject::NBBOMB_BONUS,
		   *_board, x, y, true, DEFAULTSCORE, NODELETE));
}

AObject		*Factory::notDefined(size_t, size_t, Board *)
{
  std::cerr << "CALL UNKNOW FACTORY" << std::endl;
  return (NULL);
}

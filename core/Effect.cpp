//
// Effect.cpp for bomberman in /home/ga/Documents/Projet/bomberman/bomberman
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sun Jun  3 17:52:38 2012 gaetan senn
// Last update Sun Jun  3 17:57:31 2012 gaetan senn
//

#include "Effect.hh"

Effect::Effect(size_t x, size_t y, AObject::eType type )
  : AObject(x, y, true, false, false, type), _x(x), _y(y), _type(type)
{
  _form = new graphic::Complex::Cube(graphic::Vector(0.0f, 0.0f, 0.0f),
				     graphic::Vector(0.0f, 0.0f, 0.0f),
				     graphic::Color(0.50f, 0.80f, 0.90f));
  _form->setTexture(graphic::MyGame::getInstance()->getTextureManager()->getTexture("FLAME"));
  _form->initialize();
}

Effect::~Effect()
{
}

AObject::eType		Effect::getObjectType() const
{
  return (_type);
}

void		Effect::erase()
{
  delete this;
}

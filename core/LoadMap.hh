//
// LoadMap.hh for Bomberman in /home/texier_a//projets/bomberman/core
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Tue May 22 15:47:25 2012 anna texier
// Last update Sun Jun  3 17:15:40 2012 gaetan senn
//

#ifndef	__LOAD_MAP_H_
#define	__LOAD_MAP_H_

#include	<fstream>
#include	<string>
#include	<map>
#include	"Board.hh"
#define		SPLIT '!'
#include	"Factory.hh"

class LoadMap
{
  typedef AObject	*(Factory::*factory)(size_t, size_t, Board *);
private:
  enum	eContent
    {
      FLOOR = 0,
      WALL,
      STDITEM
    };

  Board &	_board;
  std::ifstream	_file;
  size_t	_height;
  size_t	_width;
  std::map<AObject::eType, factory>	_ptallocation;
public:
  void	addSafeWallAround();
  LoadMap(const char *, Board &);

  /* METHOD CREATION AOBJECT */
  ~LoadMap();

  void	loadFile();
};

#endif

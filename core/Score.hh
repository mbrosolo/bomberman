//
// HighScore.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Fri May 25 07:20:43 2012 benjamin bourdin
// Last update Sun Jun  3 12:33:09 2012 benjamin bourdin
//

#ifndef __HIGHSCORE_HH__
#define __HIGHSCORE_HH__

#include	<string>
#include	<list>
#include	"Text.hh"

namespace score
{
  class Score
  {

  public:
    Score(const int pts = 0);
    ~Score();

    void	setPts(const int pts);
    void	setDate(const std::string &date);

    int			getPts(void) const;
    std::string const &	getDate(void) const;

    graphic::Text		&getText(void);

  private:
    std::string	_date;
    int		_pts;

    graphic::Text	_text;

    void	setCurDate(void);

  };

  class HighScore
  {
  public:
    static HighScore	*getHighScoreInstance(void);
    static void		killHighScoreInstance(void);
    void		setScore(const int pts);
    void		printHighScore(void) const;

    typedef std::list<Score *>	listScore_t;

    listScore_t &	getListScore(void);

  private:
    HighScore();
    ~HighScore();
    HighScore(HighScore const &);
    HighScore& operator=(HighScore const &);

  private:
    static HighScore	*_instance;

    static const std::string	_storage;
    //    static const std::string	_antiCheatFile;

    listScore_t		_listScore;

    void	storeHighScoreFile(void) const;
    void	recoverHighScoreFromFile();
    //    void	checkAntiCheatSystem(void);

  };
}

#endif

//
// SyscallException.hh for bm in /home/gressi_b/bm
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 11 14:24:43 2012 gressi_b
// Last update Thu May 24 16:47:37 2012 anna texier
//

#ifndef	__SYSCALL_EXCEPTION_HH__
#define	__SYSCALL_EXCEPTION_HH__

#include "Exception.hh"

namespace bm
{
  /**
   * @author gressi_b
   * @class SyscallException
   * @brief class used to handle system call exceptions
   */
  class SyscallException : public Exception
  {
  public:
    SyscallException(std::string const&, std::string const& = "");
  };
}

#endif

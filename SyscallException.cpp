//
// SyscallException.cpp for bm in /home/gressi_b/bm
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Apr 11 14:25:52 2012 gressi_b
// Last update Wed Apr 11 14:25:52 2012 gressi_b
//

#include <string.h>
#include <errno.h>

#include "SyscallException.hh"

// bm::SyscallException --------------------------------------------------------

bm::SyscallException::SyscallException(std::string const& what, std::string const& where)
  : Exception(what + ": " + strerror(errno), where)
{
}

##
## Makefile for bm in /home/gressi_b/bm
##
## Made by gressi_b
## Login   <gressi_b@epitech.net>
##
## Started on  Mon Mar 26 09:47:14 2012 gressi_b
## Last update Sun Jun  3 23:29:34 2012 benjamin bourdin
##

GRAPHDIR	=	graphic
COREDIR		=	core
LUADIR		=	lua

include		$(GRAPHDIR)/Makefile
include		$(COREDIR)/Makefile
include		$(LUADIR)/Makefile

DEBUG		=	no
ARCH		=	$(shell uname -m)
TYPE		=	$(shell uname -s)

NAME		=	bomberman

CXX		=	g++

LIBDIR		=	./lib/
OBJDIR		=	build_$(TYPE)-$(ARCH)

CXXFLAGS	=	-W -Wall -Wextra -ansi -pedantic #-Wold-style-cast
CXXFLAGS	+=	-Weffc++ #-Wconversion
CXXFLAGS	+=	-I $(LIBDIR)/systool/src/ -Wl,-rpath -Wl,`pwd`/$(LIBDIR)
CXXFLAGS	+=	-I ./ -I ./$(GRAPHDIR)/includes/ -I ./$(COREDIR)/ -I ./$(GRAPHDIR)/opencv -I ./$(GRAPHDIR)/

LDFLAGS		=	-lpthread -lrt
LDFLAGS		+=	-L./$(LIBDIR) -llua -ldl -lsystool -lpthread -lrt
LDFLAGS		+=	-L./$(GRAPHDIR)/libs/ -Wl,--rpath=./$(GRAPHDIR)/libs/
LDFLAGS		+=	-lsfml-graphics -lsfml-window -lsfml-system -lgdl_gl -lGL -lGLU
LDFLAGS		+=	-lml -lcvaux -lhighgui -lcv -lcxcore -ltiff -llapack -lblas -lgfortran -lquadmath
LDFLAGS		+=	-lfmodexL64 -lfmodex64


SRC		=	ExceptionRuntime.cpp \
			Exception.cpp \
			SyscallException.cpp \
			signal.cpp \
			$(addprefix $(COREDIR)/, $(SRC_CORE)) \
			$(addprefix $(GRAPHDIR)/, $(SRC_GRAPH)) \
			$(addprefix $(LUADIR)/, $(SRC_LUA))

OBJ		=	$(addprefix $(OBJDIR)/, $(SRC:.cpp=.o))

ifeq ($(DEBUG),yes)
    CXXFLAGS	+=	-g3
    # NAME	:=	$(NAME)-debug
    OBJDIR	:=	$(OBJDIR)-debug
else
    CXXFLAGS	+=	-O2
endif

all:			lib $(NAME)

$(NAME):		$(OBJ)
			$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

$(OBJDIR)/%.o:		%.cpp
			@mkdir -p $(@D)
			$(CXX) $(CXXFLAGS) -o $@ -c $^

lib:
			$(MAKE) re -C $(LIBDIR)

clean:
			rm -rf $(OBJDIR)
			@find . -name "*~" -delete
			@find . -name "*\#" -delete

fclean:			clean
			rm -f $(NAME)

mrproper:		$(NAME) clean

re:			fclean all

link:
			rm -rf $(NAME)
			@make

doc:
			@doxygen doxygen.conf

.PHONY:			all clean fclean mrproper re doc lib link

--[[
   Object to us:
   * x
   * y
   * cpu: C++ PlayerIA object
   - moveLeft, moveRight, moveUp, moveDown
   - getCounter, incCounter, resetCounter
   * map = {
     size = { x, y }
   }
--]]

function printf(s, ...)
   return io.write(s:format(...))
end

function get2dArray()
   local arr = {}
   for y = 1, map.size.y do
      arr[y] = {}
      for x = 1, map.size.x do
	 arr[y][x] = map[(y - 1) * map.size.x + x]
      end
   end
   return arr
end

local board = get2dArray()

function inspect()
   for y = 1, map.size.y do
      for x = 1, map.size.x do
	 printf('%4d ', board[y][x])
      end
      io.write('\n')
   end
end

function validCoor(x, y)
   return not (x < 1 or x > map.size.x or y < 1 or y > map.size.y)
end

function checker(i)
   return function(x, y)
      local b = 2 ^ i
      return (bitAnd(board[x][y], b) == b)
   end
end

local caseHas = {
   speedBonus = checker(0),
   expandbombBonus = checker(1),
   linebombBonus = checker(2),
   player = checker(3),
   bombfire = checker(4),
   bomb = checker(5),
   fire = checker(6),
   item = checker(7),
   wall = checker(8),
   empty = checker(9),
   floor = checker(10),
   speedMallus = checker(11),
   nbbombBonus = checker(12)
}

function canMove(x, y)
   return (validCoor(x, y)
	   and not caseHas.wall(x, y)
	   and not caseHas.bomb(x, y)
	   and not caseHas.item(x, y))
	   and not caseHas.expandbombBonus(x, y)
	   and not caseHas.linebombBonus(x, y)
	   and not caseHas.nbbombBonus(x, y))
end

local m = {
   up = {
      x = 0, y = -1,
      move = function()
	 cpu:moveUp()
      end
   },
   right = {
      x = 1, y = 0,
      move = function()
	 cpu:moveRight()
      end      
   },
   down = {
      x = 0, y = 1,
      move = function()
	 cpu:moveDown()
      end      
   },
   left = {
      x = -1, y = 0,
      move = function()
	 cpu:moveLeft()
      end      
   }
}

local rand = {}
do
   rand[1] = m.up
   rand[2] = m.right
   rand[3] = m.down
   rand[4] = m.left
end

function moveRandom()
   if (cpu:getCounter("bomb") > 100) then
      cpu:dropBomb()
      cpu:resetCounter("bomb")
      return
   end
   cpu:incCounter("bomb")

   local r = cpu:getLastDir()
   if (canMove(x + rand[r].x, y + rand[r].y)) then
      rand[r].move()
   else
      r = r + 1
      if (r > 4) then r = 1 end
      rand[r].move()
   end
end

-------------------------------------------------------------------------------|
-------------------------------------------------------------------------------|

moveRandom()
